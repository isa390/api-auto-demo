# 220304_函数
# def fun1():  #定义一个函数
#     print('Hello')

# fun1()  #调用函数
# print(fun1())

# def fun2():
#     return 'Hello'
# # fun2()
# print(fun2())


# sin(30°) = 0.5
# sin(45°) = 2**0.5/2
# sin(90°) = 1

# def sumdata(a,b):  #a,b形式参数,简称形参
#     return a+b
# print(sumdata(3,6))  #3,6实际参数，简称实参
# print(sumdata(3))  #报错，实参数量小于形参数量
# print(sumdata(3,6,9))  #报错，实参数量大于形参数量

# def sumdata(a=100,b=98):  #对形参的赋值相当于缺省值
#     return a+b
# print(sumdata(3,6))  #3,6实际参数，简称实参
# print(sumdata(3))  #当用户未输入对应的参数时，使用缺省值
# print(sumdata(b=3))  #完整写法，指定赋值给某个参数
# print(sumdata(99,b=6))  #前面用简略写法,后面用完整写法
# print(sumdata(a=99,6))  #前面用完整写法,后面用简略写法,这种写法会报错
# print(sumdata(3,6,9))  #报错，实参数量大于形参数量

#函数中可以有多个return
# def absolute_value(n):
#     if n>=0:
#         return n
#         return '这是一个不可达语句'  #第一个return之后的语句,都是不可达语句
#     else:
#         return -n
# print(absolute_value(-6))

#函数可以return多个值,有多个值时,以元组形式返回
# def sumdata2(a,b):
#     return a+b,a-b,a*b,a**b
# print(sumdata2(2,25))

# def sumdata2(a,b):
#     return [a+b,a-b,a*b,a**b]
# print(sumdata2(2,25))

#可变长度参数*args  允许用户输入任意个参数
# def fun3(*args):
#     return args
# print(fun3(1,2,3,4,5,6))

#如果有*args的参数,*args放在其他参数的后面
# def fun3(a,*args):
#     return a,args
# print(fun3(1,2,3,4,5,6))

#解包,*args的值少一层元组
# def fun3(a,*args):
#     return a,*args
# print(fun3(1,2,3,4,5,6))

#旧版本python写法
# def fun3(a,*args):
#     return (a,*args)
# print(fun3(1,2,3,4,5,6))

#关键字参数  **kwargs,允许用户输入任意个参数,以键=值的格式输入,返回时以字典形式返回
# def fun6(**kwargs):
#     return kwargs
# print(fun6(a=98,b='book',c='cake'))

#内置函数
#int(),str(),type(),len(),print()

#课堂小结
#函数的特性,函数的返回值
#形参与实参,简略写法与完整写法
#*args,**kwargs