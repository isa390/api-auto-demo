# 220302_条件判断语句
# python对于缩进有严格的要求,不需要缩进的地方,不能缩进,应该缩进的地方必须缩进
# python对于缩进几个空格不作要求,可以1个,也可以多个,一般默认为4个.pycharm中按tab键可以快速缩进
# if 10>90:
#     print('Hello')
# else:  #所有if条件都不满足的情况下执行
#     print('qqq')

# 用户输入一个分数,如果分数大于等于90分,则打印优秀,否则如果大于等于80分,则打印不错,
# 否则如果大于等于60分,则打印及格,否则打印不及格
# score = input('请输入一个数:  ')  #input()返回值为str型
# if score.isdigit():  #判断对象是不是纯数字
#     score = int(score)
#     if score >= 90:
#         print('优秀')
#     elif score >= 80:
#         print('不错')
#     elif score >=60:
#         print('及格')
#     else:
#         print('不及格')
# else:
#     print('您输入的不是数字')

#这种写法,每个if之间没有互斥性,错误的写法
# score = int(input('请输入一个数:  '))  #input()返回值为str型
# if score >= 90:
#     print('优秀')
# if score >= 80:
#     print('不错')
# if score >=60:
#     print('及格')
# else:
#     print('不及格')


#先判断是否不是数字,再往后执行,这种写法也可以
# score = input('请输入一个数:  ')  #input()返回值为str型
# if not score.isdigit():  #判断对象是不是纯数字
#     print('您输入的不是数字')
# else:
#     score = int(score)
#     if score >= 90:
#         print('优秀')
#     elif score >= 80:
#         print('不错')
#     elif score >=60:
#         print('及格')
#     else:
#         print('不及格')

#一个人的年龄大于等于60岁，并且为男性，我们称之为老先生
# age = 60
# gender = 'male'
# if age >= 60 and gender == 'male':
#     print('old gentleman')

# if age >= 60:
#     if gender == 'male':
#         print('old gentleman')

#课堂小结
#python的缩进
#input()的用法
#isdigit()的用法
#if else,if elif else
#复合条件判断