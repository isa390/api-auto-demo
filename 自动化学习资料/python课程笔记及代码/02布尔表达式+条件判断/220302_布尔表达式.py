# 220302_布尔表达式
# True,False
# python中,=表示赋值,==表示判断恒等,!=表示判断是否不相等
# print(1==2)

# print('a'>'A')  #字符串之间的比较,比较的是ASCII码的大小,a=97,A=65
# print('aA' > 'Aa')  # 字符串只比较第一位,第一位相同时向后比较第二位,以此类推

# in,not in
# list1 = [100,200,[300,400,500]]
# print(100 in list1)
# print(300 in list1)  #300在子列表里,不在外层的列表里

# not 非,非真即假,非假即真

# and,or,not
# and  一假为假,全真为真
# or  一真为真,全假为假
# print(1>2 and 2>1)
# print(1>2 or 2>1)

# 优先级  not > and > or,括号可以改变优先级
# print(2>1 and 1>2 and not True or 3>2)
# print(2>1 and 1>2 and (not True or 3>2))

def fun1():
    print('Hello')
    return True


# 程序执行时,只要它能判断布尔表达式结果为假,那么后面的式子就不再执行,直接返回False
# print(1>2 and 5>4 and 6>5 and 10>9 and 100>98 and 99>96 and fun1())

# 程序执行时,只要它能判断布尔表达式结果为真,那么后面的式子就不再执行,直接返回True
# print(1>2 or 1>5 or 3>9 or 10>100 or fun1())

# 浅拷贝与深拷贝
# 有两个接口,一个接口A,里面的值是[100,200,300,[400,500]],一个接口B,要求接口B的值与接口A一致.
# 领导要求修改接口A的第0位改为980,但是不可以修改接口B的值
# 领导要求修改接口A的子列表的第0位为890,不可以修改接口B的值

listA = [100, 200, 300, [400, 500]]
# listB = listA  #赋值,相当于起了一个别名,没有生成新的对象
# listA[0] = 980
# print(listA)
# print(listB)

# 浅拷贝
import copy

# listB = copy.copy(listA)  # 浅拷贝,生成了新的对象,子列表仍然是同一个对象
# listA[0] = 980
# listA[-1][0] = 890
# print(listA)
# print(listB)

#深拷贝
# listB = copy.deepcopy(listA)  #深拷贝,列表与子列表都是新的对象
# listA[0] = 980
# listA[-1][0] = 890
# print(listA)
# print(listB)

#切片,等价于浅拷贝
listB = listA[:]
listA[0] = 980
listA[-1][0] = 890
print(listA)
print(listB)

#课堂小结
#True,False
#in,not in
#and,or,not组合条件查询
#优先级not>and>or
#浅拷贝,深拷贝