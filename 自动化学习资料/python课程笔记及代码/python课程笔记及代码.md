### 220228_字符串

```python
# 220228_字符串
# url='https://www.baidu.com'  #单引号的字符串
# url2="https://www.baidu.com"  #双引号的字符串
# print(url,url2)

# 如果字符串中有单引号,外面用单引号报错
# invalid syntax  语法错误
# str1='It's ok'
# str1 = "It's ok"
# print(str1)

# 如果字符串中有双引号,外面用双引号报错
# str2 = "He said:"Are you ok?""
# str2 ='He said:"Are you ok?"'
# print(str2)

# 三引号可以用三个单引号表示,也可以用三个双引号表示
# str6='''天门中断楚江开
# 碧水东流至此回'''
# print(str6)

# 单引号或双引号也可以使用\n进行换行
# str6_2='两岸青山相对出\n孤帆一片日边来'
# print(str6_2)

# str9='abcde\b'  #backspace
# print(str9)

# 转义符的处理
# filepath = 'D:\note1.txt'
# print(filepath)

# 方案一,\前面再加一个\
# filepath = 'D:\\note1.txt'
# print(filepath)

# 方案二,字符串的外面加一个r,表示字符串中的所有转义符均不生效
# filepath = r'D:\note1.txt'
# print(filepath)

# 方案三,表示路径时,可以用/代替\
# filepath = 'D:/note1.txt'
# print(filepath)

# 字符串的拼接
# 字符串为str型,整数为int型,小数为float型
# print('1'+'2')  #12
# print('1'+2)  #str与int不能相加或拼接
# print('1'*2)  #将字符串打印两次

# a = '9'
# b = 6
# print(int(a)+b)  #相加
# print(a+str(b))  #拼接
# print(type(a))  #type(),返回对象的类型

# 字符串的下标
# 下标从0开始,长度为n的字符串,最大下标是n-1
# 也可以使用负下标,最后一位的下标可以表示为-1
# str10 = 'abcdefg'
# print(str10[0])
# print(str10[-7])

#字符串是不可变对象,不可以修改某一位的值
# str10[1]='q'

#字符串的切片
#[起始值:终止值]  包含起始值,不包含终止值
# print(str10[0:2])  #切片生成是新的对象,不影响原对象
# print(str10[3:6])  #def
# print(str10[-4:-1])  #def

#不写,则表示全取
# print(str10[5:])  #fg
# print(str10[-2:])  #fg
#
# print(str10[:3])  #abc
# print(str10[:-4])  #abc

#字符串的步长
str10 = 'abcdefg'
# print(str10[::2])  #aceg
print(str10[2::-1])  #起始值与终止值的正负不影响方向,步长的正负影响方向,步长为负数时,从右向左取值

# 字符串的概念
# 单引号,双引号,三引号表示字符串
# 字符串的拼接
# 转义符的作用,取消转义的三种方案
# 字符串的下标,正下标,负下标
# 字符串的切片,切片的起始值,终止值,步长

查阅资料,预习什么是浅拷贝和深拷贝,两者有何区别?


import copy
list1=[100,200,300,[700,800,900]]
list1_new=list1  #赋值,没有生成新的对象
list1_new=copy.copy(list1)  #浅拷贝,生成了新的对象,如果列表中有子列表,子列表仍然是同一个对象
list1_new=copy.deepcopy(list1)  #深拷贝,列表和子列表都是新对象

```



### 220228_列表与元组

```python
# 220228_列表与元组
# 列表是一种容器,由一系列元素组成,元素之间有先后顺序关系
# 列表可以存储任何类型的数据，每个元素是任意类型
# list1 = [1,'abcdefg',[10,20],(10,20),{'A':'apple'}]
# list1[0]=1998  #列表是可变对象,可以修改某一位的值
# print(list1)

# 列表中的子列表
# list2=[100,200,[300,400,500]]
# list2[2][0]=600
# print(list2)


# 列表的新增
# append,insert,extend
# append(),添加到列表的末尾
# list3=[100,200,300]
# list3.append(900)
# print(list3)

# list3.insert(1,680)  #添加到指定位置,参数1,下标  参数2,值
# print(list3)

# 列表的拼接
# list3_2=[900,1000]
# list3.extend(list3_2)
# print(list3)

# 列表的删除
# pop,remove,del
# list6=[11,22,33,44,55]
# list6.pop()  #不指定参数,则删除最后一位,也可以指定参数,删除指定位置的值
# print(list6)

# list6.remove(55)  #根据值进行删除,有多个相同的值,删除最左边的
# print(list6)

# del list6[-1]
# print(list6)

# 列表的切片,用法与字符串的切片一致
# 切片生成的是新对象,不影响原对象
# list9 = [100, 200, 300, 400, 500, 600]
# print(list9[0:3])
# print(list9[-6:-3])

# 元组
# 元组与列表类似,也可以使用下标与切片,不过元组是不可变对象,不可以修改其中的元素的值
# tuple1 = (100, 200, 300, 400, 500)
# tuple1[0]=90  #报错,元组是不可变对象

#元组中只有一个值时,加一个逗号
# tuple2 = (10,)
# print(type(tuple2))

#如果元组中有子列表,子列表中的值可以修改
tuple3 = (100, 200, [300, 400, 500])
tuple3[2][0]=600
print(tuple3)

#课堂小结
#列表的概念,列表可以存放任意对象,列表是可变对象
#列表的新增  append,insert,extend
#列表的删除  pop,remove,del
#列表的下标与切片,用法与字符串一致
#元组的概念,元组可以存放任意对象,元组是不可变对象
#元组也可以使用下标与切片,用法与列表一致

写一个号段筛选程序,需求如下:
用户从控制台输入一个手机号，判断出运营商(移动（假设号段是130-150）、联通（假设是151-170）、电信（假设是171-199）),如果用户输入的位数不对，提示用户位数有误;如果用户输入非数字，提示有非法字符




input1=input('请输入一个手机号:  ')
if not input1.isdigit():  #判断用户输入的是否不是纯数字,如果不是,则提示用户输入的值不正确
    print('您输入的不是数字')
else:
    if len(input1)!=11:  #如果用户输入的位数不是11位,提示位数不正确
        print('位数不正确')
    else:
        num1=int(input1[0:3])  #取得手机号的前三位,并转为int型
        if 130<=num1<=150:
            print('移动')
        elif 151<=num1<=170:
            print('联通')
        elif 171<=num1<=190:
            print('电信')
        else:
            print('您输入的手机号不属于任何运营商')
```

### 220302_布尔表达式

```python
# 220302_布尔表达式
# True,False
# python中,=表示赋值,==表示判断恒等,!=表示判断是否不相等
# print(1==2)

# print('a'>'A')  #字符串之间的比较,比较的是ASCII码的大小,a=97,A=65
# print('aA' > 'Aa')  # 字符串只比较第一位,第一位相同时向后比较第二位,以此类推

# in,not in
# list1 = [100,200,[300,400,500]]
# print(100 in list1)
# print(300 in list1)  #300在子列表里,不在外层的列表里

# not 非,非真即假,非假即真

# and,or,not
# and  一假为假,全真为真
# or  一真为真,全假为假
# print(1>2 and 2>1)
# print(1>2 or 2>1)

# 优先级  not > and > or,括号可以改变优先级
# print(2>1 and 1>2 and not True or 3>2)
# print(2>1 and 1>2 and (not True or 3>2))

def fun1():
    print('Hello')
    return True


# 程序执行时,只要它能判断布尔表达式结果为假,那么后面的式子就不再执行,直接返回False
# print(1>2 and 5>4 and 6>5 and 10>9 and 100>98 and 99>96 and fun1())

# 程序执行时,只要它能判断布尔表达式结果为真,那么后面的式子就不再执行,直接返回True
# print(1>2 or 1>5 or 3>9 or 10>100 or fun1())

# 浅拷贝与深拷贝
# 有两个接口,一个接口A,里面的值是[100,200,300,[400,500]],一个接口B,要求接口B的值与接口A一致.
# 领导要求修改接口A的第0位改为980,但是不可以修改接口B的值
# 领导要求修改接口A的子列表的第0位为890,不可以修改接口B的值

listA = [100, 200, 300, [400, 500]]
# listB = listA  #赋值,相当于起了一个别名,没有生成新的对象
# listA[0] = 980
# print(listA)
# print(listB)

# 浅拷贝
import copy

# listB = copy.copy(listA)  # 浅拷贝,生成了新的对象,子列表仍然是同一个对象
# listA[0] = 980
# listA[-1][0] = 890
# print(listA)
# print(listB)

#深拷贝
# listB = copy.deepcopy(listA)  #深拷贝,列表与子列表都是新的对象
# listA[0] = 980
# listA[-1][0] = 890
# print(listA)
# print(listB)

#切片,等价于浅拷贝
listB = listA[:]
listA[0] = 980
listA[-1][0] = 890
print(listA)
print(listB)

#课堂小结
#True,False
#in,not in
#and,or,not组合条件查询
#优先级not>and>or
#浅拷贝,深拷贝

根据身份证号判断其拥有者的性别






id_card='320108199809081681'
if int(id_card[-2])%2==1:
    print('男性')
else:
    print('女性')
```

### 220302_条件判断语句

```python
# 220302_条件判断语句
# python对于缩进有严格的要求,不需要缩进的地方,不能缩进,应该缩进的地方必须缩进
# python对于缩进几个空格不作要求,可以1个,也可以多个,一般默认为4个.pycharm中按tab键可以快速缩进
# if 10>90:
#     print('Hello')
# else:  #所有if条件都不满足的情况下执行
#     print('qqq')

# 用户输入一个分数,如果分数大于等于90分,则打印优秀,否则如果大于等于80分,则打印不错,
# 否则如果大于等于60分,则打印及格,否则打印不及格
# score = input('请输入一个数:  ')  #input()返回值为str型
# if score.isdigit():  #判断对象是不是纯数字
#     score = int(score)
#     if score >= 90:
#         print('优秀')
#     elif score >= 80:
#         print('不错')
#     elif score >=60:
#         print('及格')
#     else:
#         print('不及格')
# else:
#     print('您输入的不是数字')

#这种写法,每个if之间没有互斥性,错误的写法
# score = int(input('请输入一个数:  '))  #input()返回值为str型
# if score >= 90:
#     print('优秀')
# if score >= 80:
#     print('不错')
# if score >=60:
#     print('及格')
# else:
#     print('不及格')


#先判断是否不是数字,再往后执行,这种写法也可以
# score = input('请输入一个数:  ')  #input()返回值为str型
# if not score.isdigit():  #判断对象是不是纯数字
#     print('您输入的不是数字')
# else:
#     score = int(score)
#     if score >= 90:
#         print('优秀')
#     elif score >= 80:
#         print('不错')
#     elif score >=60:
#         print('及格')
#     else:
#         print('不及格')

#一个人的年龄大于等于60岁，并且为男性，我们称之为老先生
# age = 60
# gender = 'male'
# if age >= 60 and gender == 'male':
#     print('old gentleman')

# if age >= 60:
#     if gender == 'male':
#         print('old gentleman')

#课堂小结
#python的缩进
#input()的用法
#isdigit()的用法
#if else,if elif else
#复合条件判断

现有一个游戏系统的日志文件，记录内容的字符串 的格式 如下所示

A girl come in, the name is Jack, level 955;

其中包含的 the name is 后面会跟着人名，随后紧跟一个逗号， 这是固定的格式。

其它部分可能都是会变化的，比如，可能是下面这些

A old lady come in, the name is Mary, level 94454

A pretty boy come in, the name is Patrick, level 194

请大家实现一个函数，名为getName，如下所示

def getName(srcStr):
    函数体
该函数的参数srcStr 是上面所描述的格式字符串（只处理一行），该函数需要将其中的人名获取出来，并返回

比如 调用 getName('A old lady come in, the name is Mary, level 94454')

返回结果应该是 'Mary'




def getName(srcStr):
    return srcStr.split('the name is ')[1].split(',')[0]
```



### 220304_函数

```python
# 220304_函数
# def fun1():  #定义一个函数
#     print('Hello')

# fun1()  #调用函数
# print(fun1())

# def fun2():
#     return 'Hello'
# # fun2()
# print(fun2())


# sin(30°) = 0.5
# sin(45°) = 2**0.5/2
# sin(90°) = 1

# def sumdata(a,b):  #a,b形式参数,简称形参
#     return a+b
# print(sumdata(3,6))  #3,6实际参数，简称实参
# print(sumdata(3))  #报错，实参数量小于形参数量
# print(sumdata(3,6,9))  #报错，实参数量大于形参数量

# def sumdata(a=100,b=98):  #对形参的赋值相当于缺省值
#     return a+b
# print(sumdata(3,6))  #3,6实际参数，简称实参
# print(sumdata(3))  #当用户未输入对应的参数时，使用缺省值
# print(sumdata(b=3))  #完整写法，指定赋值给某个参数
# print(sumdata(99,b=6))  #前面用简略写法,后面用完整写法
# print(sumdata(a=99,6))  #前面用完整写法,后面用简略写法,这种写法会报错
# print(sumdata(3,6,9))  #报错，实参数量大于形参数量

#函数中可以有多个return
# def absolute_value(n):
#     if n>=0:
#         return n
#         return '这是一个不可达语句'  #第一个return之后的语句,都是不可达语句
#     else:
#         return -n
# print(absolute_value(-6))

#函数可以return多个值,有多个值时,以元组形式返回
# def sumdata2(a,b):
#     return a+b,a-b,a*b,a**b
# print(sumdata2(2,25))

# def sumdata2(a,b):
#     return [a+b,a-b,a*b,a**b]
# print(sumdata2(2,25))

#可变长度参数*args  允许用户输入任意个参数
# def fun3(*args):
#     return args
# print(fun3(1,2,3,4,5,6))

#如果有*args的参数,*args放在其他参数的后面
# def fun3(a,*args):
#     return a,args
# print(fun3(1,2,3,4,5,6))

#解包,*args的值少一层元组
# def fun3(a,*args):
#     return a,*args
# print(fun3(1,2,3,4,5,6))

#旧版本python写法
# def fun3(a,*args):
#     return (a,*args)
# print(fun3(1,2,3,4,5,6))

#关键字参数  **kwargs,允许用户输入任意个参数,以键=值的格式输入,返回时以字典形式返回
# def fun6(**kwargs):
#     return kwargs
# print(fun6(a=98,b='book',c='cake'))

#内置函数
#int(),str(),type(),len(),print()

#课堂小结
#函数的特性,函数的返回值
#形参与实参,简略写法与完整写法
#*args,**kwargs
```





###  220304_对象的方法

```python
# 220304_对象的方法
# str1='cdefgcdefg'
# print(str1.find('e',5)) #返回某个或某些字符在字符串中的位置,默认从头开始查找,也可以从指定位置开始查找,找不到值时,返回-1
# print(str1.index('e',5))  #用法与find类似,当找不到值时抛异常

# strip  #去掉字符串前后的空格,返回去掉空格之后的字符串
str2 = '     青  山  横  北  郭  ,  白  水  绕  东  城     '
# str2 = str2.strip()
# print(str2)

# 替换字符串中的某个或某些字符,返回替换后的字符串,返回值是str型
# str2 = str2.replace(' ','')
# print(str2)

# str3 = '<p>中国男足勇夺世界杯冠军</p>'
# str3 = str3.replace('<p>','').replace('</p>','')
# print(str3)

# startswith  检查字符串是否以某个或某些字符开头,返回值是布尔型
# 根据身份证号判断是否是南京的身份证
# id_card = '320104199810070863'
# if id_card.startswith('3201'):
#     print('南京的身份证')

# endswith  检查字符串是否以某个或某些字符结尾,返回值是布尔型
# 判断身份证的最后一位是否是X
# if id_card.endswith('X'):
#     print('最后一位是X')

# isalpha,检查字符串中是否是纯字母,isdigit,检查字符串中是否是纯数字

# split  切割字符串,它有个一个参数,以参数作为分隔符,返回值是列表

# str6 ='tnhwrt'
# str6_new=str6.split('t')
# print(str6_new)  #如果切割符位于首位或末尾,会产生空值

# 上节课思考题
# 写一个号段筛选程序,需求如下:
# 用户从控制台输入一个手机号,判断出运营商(移动(130-150),联通(151-170),电信(171-190))
# 如果用户输入的位数不对,提示用户位数有误,如果用户输入非数字,提示有非法字符

mobile = input('请输入一个手机号:  ')
if not mobile.isdigit():  # 判断用户输入的是否不是纯数字
    print('您输入的不是数字')
else:
    if len(mobile) != 11:  #判断用户输入的长度是否不是11位
        print('位数不正确')
    else:
        number = int(mobile[0:3])  #取得手机号的前三位,并转为int型
        if 130 <= number <= 150:
            print('移动')
        elif 151 <= number <= 170:
            print('联通')
        elif 171 <= number <= 190:
            print('电信')
        else:
            print('您输入的手机号不属于任何运营商')
```

### 220306_循环语句

```python
# 220306_循环语句
# i = 1
# while i <= 10:  #while后面的布尔表达式如果为真,则执行循环中的语句
#     print(i)
#     i += 1  #i自增长1

#for循环打印从1到10的数字
# for i in range(1,11):  #range(起始值,终止值,步长)  包含起始值,不包含终止值,步长默认为1
#     print(i)

#如果有明确的循环次数,建议用for循环,如果循环次数步确定,建议用while循环,两者可以互相替换


#打印100以内的奇数
# for i in range(1,101,2):
#     print(i)

# for i in range(1,101):
#     if i%2==1:
#         print(i)

#打印从10到1的值
# for i in range(10,0,-1):  #步长为负数时,起始值要大于终止值
#     print(i)

#for循环也可以省略起始值,默认为0
# for i in range(10):
#     print(i)

#遍历列表
# list1 = ['谢晋','徐渭','杨慎']
#使用下标的方式进行遍历
# for i in range(len(list1)):
#     print(list1[i])

#直接遍历
# for one in list1:
#     print(one)

#break与continue
# for i in range(1,11):
#     if i==5:
#         # break  #终止循环
#         # continue  #跳出当次循环
#         pass  #占位符,防止语法错误
#     print(i)
# else:  #当循环中没有出现break,则循环结束时,运行一次else中的语句
#     print('循环运行完毕')

#写一个倒计时程序
# import time  #加载time模块
# for i in range(10,0,-1):
#     print(f'\r倒计时{i}秒',end='')  #\r光标回到行首
#     time.sleep(1)  #程序等待1秒
# else:
#     print('\r倒计时结束')

#sep=' '  间隔符,print()的多个参数之间,以间隔符隔开
# print(1,2,3,4,5,sep='')

# end='\n'  结束符,print()结束时加的字符
# print(1,end='')
# print(2)

#课堂小结
#循环的概念
#while循环
#for循环
#遍历列表
#break,continue
#循环的else


1.下面的log变量记录了云服务器上 当天上传的文件信息
其中第一列是文件名，第二列是文件大小

请编写一个程序，统计出不同类型的 文件的大小总和
比如：
jpeg  9988999
json   324324
png   2423233
----------------------------------


log = '''
f20180111014341/i_51a7hC3W.jpeg	169472	FrITJxleSP7wUD-MWw-phL_KP6Eu	15156063244230469	image/jpeg	0	
f20180111014341/j_R0Hpl4EG.json	1036	ForGzwzV3e-uR3_UzvppJs1VgfQG	15156064773253144	application/json	0	
f20180111020739/i_0TDKs0rD.jpeg	169472	FrITJxleSP7wUD-MWw-phL_KP6Eu	15156076847077556	image/jpeg	0	
f20180111020739/j_JFO6xiir.json	1040	FmUhTchdLOd7LBoE8OXzPLDKcW60	15156077904192983	application/json	0	
f20180111090619/i_1BwNksbL.jpg	49634	FtXBGmipcDha-67WQgGQR5shEBu2	15156329458714950	image/jpeg	0	
f20180111090619/i_3BKlsRaZ.jpg	30152	FoWfMSuqz4TEQl5FT-FY5wqu5NGf	15156330575626044	image/jpeg	0	
f20180111090619/i_5XboXSKh.jpg	40238	Fl84WaBWThHovIBsQaNFoIaPZcWh	15156329453409855	image/jpeg	0	
f20180111090619/i_6DiYSBKp.jpg	74017	FrYG3icChRmFGnWQK6rYxa88KuQI	15156329461803290	image/jpeg	0	
f20180111090619/i_76zaF2IM.jpg	38437	Fui8g5OrJh0GQqZzT9wtepfq99lJ	15156334738356648	image/jpeg	0	
f20180111090619/i_B6TFYjks.jpg	37953	FleWqlK2W1ZmEgAatAEcm1gpR0kC	15156329464034474	image/jpeg	0	
f20180111090619/i_N9eITqj3.jpg	38437	Fui8g5OrJh0GQqZzT9wtepfq99lJ	15156330419595764	image/jpeg	0	
f20180111090619/i_QTSNWmA6.jpg	37953	FleWqlK2W1ZmEgAatAEcm1gpR0kC	15156333104224056	image/jpeg	0	
f20180111090619/i_XdHcAfh1.jpg	56479	FjLQIQ3GxSEHDfu6tRcMylK1MZ05	15156334227270309	image/jpeg	0	
f20180111090619/i_Xyy723MU.jpg	50076	FsfZpQzqu084RUw5NPYW9-Yfam_R	15156334229987458	image/jpeg	0	
f20180111090619/i_d8Go0EOv.jpg	30152	FoWfMSuqz4TEQl5FT-FY5wqu5NGf	15156334736228515	image/jpeg	0	
f20180111090619/i_diuHmX53.jpg	40591	FuTx1pw4idbKnV5MSvNGxCA5L470	15156333878320713	image/jpeg	0	
f20180111090619/i_qQKzheSH.jpg	55858	Fj0A3i8V7fzzOiPQFL79ao15hkN9	15156329456666591	image/jpeg	0	
f20180111090619/i_rHL5SYk8.jpg	40238	Fl84WaBWThHovIBsQaNFoIaPZcWh	15156336509742181	image/jpeg	0	
f20180111090619/i_xZmQxUbz.jpg	40238	Fl84WaBWThHovIBsQaNFoIaPZcWh	15156333240603466	image/jpeg	0	
f20180111090619/i_zBDNgXDv.jpeg	73616	FlgNwq8lypgsxrWs_ksrS_x47SQV	15156334232887875	image/jpeg	0	
f20180111090619/j_4mxbEiVh.json	2990	Fpq-3yl3Yr1CadNrJVSDnpeRhQtT	15156331445226898	application/json	0	
f20180111090619/j_i1K74768.json	3042	Fl5PpDw1TsZXMuhoq1RUrOeGZ6br	15156335067090003	application/json	0	
f20180111095839/i_Q7KMKeda.png	518522	Fl-yB1_ruL2uxZN9k7DjB62h9dYH	15156359599713253	image/png	0	
f20180111095839/j_5DpqHolV.json	184	FoYvi7cmSrzuVjUgCRzW5kU95SVo	15156359719719064	application/json	0	
f20180111100442/i_No8kToIV.jpg	48975	Fu1cw3f--5Vpz9kLGeJfvljhCtyZ	15156364349642377	image/jpeg	0	
f20180111100442/i_P1bkvSeg.jpg	68200	FvYe8vi46TjUKhEy_UwDqLhO6ZsW	15156363800690634	image/jpeg	0	
f20180111100442/i_T1AulKcD.jpg	52641	Fj2YzvdC1n_1sF93ZZgrhF3OzOeY	15156364021186365	image/jpeg	0	
f20180111100442/i_X8d8BN07.jpg	50770	FivwidMiHbogw77lqgkIKrgmF3eA	15156363969737156	image/jpeg	0	
f20180111100442/i_g0wtOsCX.jpg	76656	Fmtixx0mP9CAUTNosjLuYQHL6k0P	15156363448222155	image/jpeg	0	
f20180111100442/i_h5OT9324.jpg	72672	FvbIqPLTh2cQHTIBv2akUfahZa_Z	15156364401354652	image/jpeg	0	
f20180111100442/i_he8iLYI6.jpg	49399	FjeJvwjwhU-hKZsq66UoBg9_tEJs	15156363907932480	image/jpeg	0	
f20180111100442/i_kg29t7Pp.jpg	76293	FuYj__sSeEN7AsXMbxO24Z8Suh8d	15156364156384686	image/jpeg	0	
f20180111100442/i_oz1YoBI1.jpg	75620	FkY3xsUMwOI01zgoH1iXXgiQeq6I	15156364089112904	image/jpeg	0	
f20180111100442/i_xrOT98on.jpg	50021	Fql7ookM1Rc6V7VairKAfnKe-o9w	15156363856357316	image/jpeg	0	
f20180111135114/i_Zqt8Tmoe.png	161629	FlELw59_mV3VqDBLyu1BKN4fIWnx	15156500155209863	image/png	0	
f20180111135114/j_uhHoMXKq.json	159	FrypljwAr2LgoLAePBNTUYTUAgDt	15156500200488238	application/json	0	
f20180111142119/i_s83iZ2GR.png	92278	Fns8tdh3JCkRmfE_COYEu4o8w03E	15156517082371259	image/png	0	
f20180111142119/j_0g45JRth.json	159	Fq1rFwdRguYRXrp61nGZ5TsUG1V-	15156517143375596	application/json	0	
f20180111144306/i_yE5TC84E.png	139230	Fjf61ymabEnEvnr5ZMHFjXGCrYlP	15156530038824150	image/png	0	
f20180111144306/j_OF4WVtSH.json	159	FqwkKcxfo8jd0jFUyuH4X2CrnE9q	15156530083419530	application/json	0	
f20180111150230/i_KtnER4g3.png	120044	FuwOWdrqzcr2-UScem-LzEMgMezs	15156541734892258	image/png	0	
f20180111150230/j_xMSUEejY.json	158	FjJr_4deMqFphGaptm-2Pa6wwRP2	15156541771989216	application/json	0	
f20180111151741/i_JuSWztB3.jpg	92506	FrIjRevHSi6xv4-NQa2wrHu5a1zQ	15156550875370965	image/jpeg	0	
f20180111153550/i_9wWzVenl.gif	769872	FvslKY9JUaCQm-lu02E34tvAP_oG	15156561674621628	image/gif	0	
'''













【参考答案】在下边















# 记录各种类型的文件的数量统计，存储格式如下
# [['jpg',4566],['json',4566]]
fileLenTable = []

# 根据文件类型找到记录对象,进行累加
def putRecordToTable(fileType,fileLen):
    for one in fileLenTable:
        if one[0] == fileType:
            one[1] += fileLen
            return

    # 没有找到,创建一个记录元素
    fileLenTable.append([fileType,fileLen])
    return


for line in log.split('\n'):
    if line.strip() == '':
        continue

    parts = line.split('\t')
    name,size = parts[:2]

    ext = name.split('.')[-1]

    putRecordToTable(ext,int(size))


print(fileLenTable)
```



### 220306_格式化字符串

```python
 # 220306_格式化字符串
# a=10
# b=8
# print(str(a)+'+'+str(b)+'='+str(a+b))  #10+8=18
# print('%s+%s=%s'%(a,b,a+b))  #格式化字符串

#方案一
#%s字符串,%d整数,%f浮点数
# info1 = '我是%s,你是%s,他是%s,今年是%d年.'%('德华','学友','黎明',2022)
# print(info1)

#前面用%d,后面用字符串,报错
# info1 = '今年是%d年.'%('德华')
# print(info1)

#前面用%s,后面用数字,不报错
# info1 = '他是%s'%(2022)
# print(info1)

#前面的空位比后面的值多,报错
# info1 = '我是%s,你是%s,他是%s,今年是%d年.'%('德华','学友','黎明')
# print(info1)

#前面的空位比后面的值少,报错
# info1 = '我是%s,你是%s,他是%s'%('德华','学友','黎明','富城')
# print(info1)

#补齐到指定位数,默认右对齐
# info1 = '他是%10s,今年是%10d年.'%('黎明',2022)
# print(info1)

#左对齐
# info1 = '他是%-10s,今年是%-10d年.'%('黎明',2022)
# print(info1)

#补0
# info1 = '他是%10s,今年是%010d年.'%('黎明',2022)
# print(info1)

#%f浮点型,默认保留6位小数
# number1 = '您输入的数字是%f'%(3.6)
# print(number1)

#保留两位小数
# number1 = '您输入的数字是%.2f'%(3.6)
# print(number1)

#补齐到10位,保留两位小数
# number1 = '您输入的数字是%10.2f'%(3.6)
# print(number1)

#方案二
# str1 = 'My name is {},your name is {},age is {}.'.format('Clark','Ralf',21)
# print(str1)

#前面的空位比后面多,报错
# str1 = 'My name is {},your name is {},age is {}.'.format('Clark','Ralf')
# print(str1)

#前面的空位比后面少,不报错
# str1 = 'My name is {},your name is {}.'.format('Clark','Ralf',21)
# print(str1)

#大括号里不写下标称之为顺序取值法,写下标称之为下标取值法
# str1 = 'My name is {2},your name is {2},age is {2}.'.format('Clark','Ralf',21)
# print(str1)

#顺序取值法与下标取值法不可以混用
# str1 = 'My name is {},your name is {},age is {2}.'.format('Clark','Ralf',21)
# print(str1)

#补齐,字符串默认左对齐,数字默认右对齐
# str1 = 'My name is {:10},your name is {:10},age is {:10}.'.format('Clark','Ralf',21)
# print(str1)

#对齐方式,<左对齐,>右对齐,^居中对齐
# str1 = 'My name is {:>10},your name is {:^10},age is {:<10}.'.format('Clark','Ralf',21)
# print(str1)

#补0
# str1 = 'My name is {:>010},your name is {:^010},age is {:>010}.'.format('Clark','Ralf',21)
# print(str1)

#python3.6以后的版本中,可以使用更加简便的写法
name1 = 'Clark'
name2 = 'Ralf'
age = 21
print(f'My name is {name1},your name is {name2},age is {age}.')

#课堂小结
#方案一,%s,%d,%f,补齐,对齐方式,前后数量一致性
#方案二,补齐,对齐方式,顺序取值法,下标取值法
#f''的用法


请实现一个程序，实现如下需求点：


1.程序开始的时候提示用户输入学生年龄信息 格式如下：

Jack Green ,   21  ;  Mike Mos, 9;

我们假设 用户输入 上面的信息，必定会遵守下面的规则：
  学生信息之间用分号隔开（分号前后可能有不定数量的空格），
  每个学生信息里的 姓名和 年龄之间用 逗号隔开（逗号前后可能有不定数量的空格） 

2. 程序随后将输入的学生信息分行显示，格式如下
Jack Green          :21;
Mike Mos            :09;
学生的姓名要求左对齐，宽度为20， 年龄信息右对齐，宽度为2位，不足前面补零


【参考答案】在下面














inputStr = input('Please input student age info:')
studentInfo = inputStr.split(';')
for one in studentInfo:
    # check if it is valid input 
    if ',' not in one: 
        continue
        
    name,age = one.split(',')
    name = name.strip()
    age  = age.strip()
    
    #  check is age digit
    if not age.isdigit():
        continue
    
    age = int(age)

    print('%-20s :  %02d' % (name, age))
    # print('{:20} :  {:02}'.format(name, age))
    # print(f'{name:20} :  {age:02}')
```

### 220307_字典与json

```python
# 220307_字典与json
# 字典是以键值对形式出现的存储对象
# 字典是可变对象
# 字典的键可以存放不可变对象
# 字典的值可以存放任意类型的对象
# 字典的键是唯一的
# 字典是无序的

# dict1 = {'A': 'apple'}
# print(dict1['A'])
# print(dict1['B'])  #找不到键，报错
# dict1['A']='ace'  #字典里有对应的键,修改
# dict1['B']='book'  #字典里没有对应的键,新增
# print(dict1)
# print('A' in dict1)  #判断键是否在字典里
# print('apple' in dict1)  #判断某个对象是否在字典里,根据键判断,而不是值
# del dict1['A']  #删除字典的键值对
# print(dict1)

# 清空字典
# dict1.clear()  #删除字典中的键值对,内存中的地址不变
# dict1 = {}  #重新定义一个空字典,内存中的地址有改变

# dict2={'A':'apple','B':'book','C':'cake'}
# 遍历字典中的键
# for key in dict2.keys():
#     print(key)

# 遍历字典中的值
# for value in dict2.values():
#     print(value)

# for key,value in dict2.items():
#     print(key,value)

# 字典是无序的
# dict3 = {'A': 'apple', 'B': 'book'}
# dict6 = {'B': 'book', 'A': 'apple'}
# print(dict3 == dict6)

#集合,相当于只有键的字典
# set1 = {'A','B','C','D','E','A'}
# print(set1)

# json
str1 = '''{
"aac003": "张三",
"aac030": "13574553997",
"crm003": "1",
"crm004": "1"
}'''

#有一个注册页面的接口请求,传了一个json,其中的手机号唯一,传json时,要求修改为随机手机号
import json
from random import randint
dict1 = json.loads(str1)  #将json转为字典
dict1['aac030']=f'138{randint(10000000,99999999)}'
str1 = json.dumps(dict1,ensure_ascii=False)  #将字典转为json,如果有中文乱码,加一个参数ensure_ascii=False
print(type(str1))

#课堂小结
#字典的概念与特性
#字典的键的唯一性
#字典是无序的
#字典的新增,需改,删除
#遍历字典
#json.loads(),json.dumps()

现有文件1（如下，请保存到文件file1.txt中）， 记录了公司员工的薪资，其内容格式如下

name: Jack   ;    salary:  12000
 name :Mike ; salary:  12300
name: Luk ;   salary:  10030
  name :Tim ;  salary:   9000
name: John ;    salary:  12000
name: Lisa ;    salary:   11000

每个员工一行，记录了员工的姓名和薪资，
每行记录 原始文件中并不对齐，中间有或多或少的空格

现要求实现一个python程序，计算出所有员工的税后工资（薪资的90%）和扣税明细，
以如下格式存入新的文件 file2.txt中，如下所示

name: Jack   ;    salary:  12000 ;  tax: 1200 ; income:  10800
name: Mike   ;    salary:  12300 ;  tax: 1230 ; income:  11070
name: Luk    ;    salary:  10030 ;  tax: 1003 ; income:   9027
name: Tim    ;    salary:   9000 ;  tax:  900 ; income:   8100
name: John   ;    salary:  12000 ;  tax: 1200 ; income:  10800
name: Lisa   ;    salary:  11000 ;  tax: 1100 ; income:   9900

要求像上面一样的对齐
tax 表示扣税金额和 income表示实际收入。注意扣税金额和 实际收入要取整数  


【参考答案】 在下面



























inFileName  = 'file1.txt'
outFileName = 'file2.txt'

with open(inFileName) as ifile, open(outFileName,'w') as ofile:
    beforeTax = ifile.read().splitlines()
    # or we could use   beforeTax = ifile.read().split('\n')
    for one in beforeTax:
        if one.count(';') != 1: # ensure valid
            continue
        
        namePart,salaryPart = one.split(';')   
        # name Part like  name: Jack  |  salaryPart like    salary:  12000]
        
        if namePart.count(':') != 1: # ensure valid
            continue
        if salaryPart.count(':') != 1: # ensure valid
            continue
            
        name   = namePart.split(':')[1].strip()
        salary = int(salaryPart.split(':')[1].strip())
        
        income = int(salary*0.9)
        tax    = int(salary*0.1)
        
        outPutStr = 'name: {:10}   ;    salary:  {:6} ;  tax: {:6} ; income:  {:6}'.format(name,salary,tax,income)
        
        print outPutStr
        
        ofile.write(outPutStr + '\n')
```

### 220307_文件的读写

```python
# 220307_文件的读写
# filepath = 'd:/note1.txt'
# file1 = open(filepath,'r',encoding='utf-8')  #打开一个文件,参数1,文件路径  参数2,r/w/a  参数2不写时缺省值为r
# print(file1.read())  #返回文件内容,以str形式返回
# file1.close()  #关闭文件  使用open()时,必须close()关闭文件,否则会一直占用内存

# with open(filepath) as file1:
#     print(file1.read())

# filepath2 = './note2.txt'
# with open(filepath2,encoding='utf-8') as file2:
#     print(file2.read())
    # print(file2.readline())  #读取一行内容,返回值是str型
    # print(file2.readlines())  #读取文件全部内容,返回值是列表,每一行是一个元素
    # print(file2.read().splitlines())  #读取文件全部内容,返回值是列表,每一行是一个元素,没有\n

#文件的读写
#w+  可以同时读写,如果文件不存在,则新建,写入时,清空之前的内容写入
#r+  可以同时读写,如果文件不存在,则报错,写入时,覆盖写入
#a+  可以同时读写,如果文件不存在,则新建文件,写入时是追加写入

#文件一开始为abcde,w+写入qq,文件内容保存的是qq
#文件一开始为abcde,r+写入qq,文件内容保存的是qqcde
#文件一开始为abcde,a+写入qq,文件内容保存的是abcdeqq

# with open('./1.txt','w+',encoding='utf-8') as file1:
#     file1.write('till they tell the true')
#     file1.seek(0)  #光标回到文件首位
#     print(file1.read())

#某项目性能测试，要求快速生成多个账号，格式为sq001~sq999,密码123456，账号和密码之间用逗号隔开
with open('./账号220307.txt','w+') as file1:
    for i in range(1,1000):
        if i <999:
            file1.write(f'sq{i:03},123456\n')
        else:
            file1.write(f'sq{i:03},123456')

#课堂小结
#open(),with open()
#read(),readline(),readlines(),read().splitlines()
#w+,r+,a+
#seek()
#使用seek()时的汉字占用字节数,gbk2字节,utf-8,3字节

现有一个数据库记录文件（见附件0005_1.txt），保存了学生课程签到的数据库记录。 内容格式如下 ，

('2017-03-13 11:50:09', 271, 131),
('2017-03-14 10:52:19', 273, 131),
('2017-03-13 11:50:19', 271, 126),
每一行记录保存了学生的一次签到信息。

每一次签到信息的记录，分为三个部分， 分别是签到时间、签到课程的id号、签到学生的id号

要求大家实现下面的函数。其中参数fileName 为 数据库记录文件路径， 输出结果是将数据库记录文件中的学生签到信息保存在一个字典对象中，并作为返回值返回。

def putInfoToDict(fileName):

要求返回的字典对象的格式是这样的：

key 是各个学生的id号， value是 该学生的签到信息

   其中value，里面保存着该学生所有签到的信息

       其中每个签到的信息是字典对象，有两个元素： key 是lessonid的 记录课程id，key是checkintime的 记录签到时间

比如，对于上面的示例中的3条记录，相应的返回结果如下：

{
    131: [
        {'lessonid': 271,'checkintime':'2017-03-13 11:50:09'},
        {'lessonid': 273,'checkintime':'2017-03-14 10:52:19'},
    ],
    
    
    126: [
        {'lessonid': 271,'checkintime':'2017-03-13 11:50:19'}
    ]
    
}




【附件0005_1.txt内容如下】：
        ('2017-03-13 11:50:09', 271, 131),
	('2017-03-13 11:50:19', 271, 126),
	('2017-03-13 11:50:25', 271, 85),
	('2017-03-13 11:50:31', 271, 118),
	('2017-03-13 11:50:34', 271, 119),
	('2017-03-13 11:50:44', 271, 109),
	('2017-03-13 11:50:55', 271, 79),
	('2017-03-13 11:50:58', 271, 121),
	('2017-03-13 11:51:02', 271, 116),
	('2017-03-13 11:51:05', 271, 125),
	('2017-03-13 11:51:32', 271, 64),
	('2017-03-13 11:51:38', 271, 123),
	('2017-03-13 11:51:39', 271, 122),
	('2017-03-13 11:51:41', 271, 82),
	('2017-03-13 11:51:41', 271, 51),
	('2017-03-13 11:51:41', 271, 53),
	('2017-03-13 11:51:43', 271, 98),
	('2017-03-13 11:51:44', 271, 128),
	('2017-03-13 11:51:48', 271, 38),
	('2017-03-13 11:51:48', 271, 67),
	('2017-03-13 11:51:58', 271, 110),
	('2017-03-13 11:52:01', 271, 40),
	('2017-03-13 11:52:07', 271, 35),
	('2017-03-13 11:52:13', 271, 108),
	('2017-03-13 11:52:14', 271, 77),
	('2017-03-13 11:52:18', 271, 115),
	('2017-03-13 11:52:21', 271, 68),
	('2017-03-13 11:52:22', 271, 129),
	('2017-03-13 11:52:27', 271, 127),
	('2017-03-13 11:52:30', 271, 81),
	('2017-03-13 11:52:44', 271, 37),
	('2017-03-13 11:52:46', 271, 43),
	('2017-03-13 11:53:10', 271, 133),
	('2017-03-13 11:53:39', 271, 56),
	('2017-03-13 11:54:17', 271, 78),
	('2017-03-13 11:54:19', 271, 130),
	('2017-03-13 11:54:24', 271, 99),
	('2017-03-13 11:54:27', 271, 90),
	('2017-03-13 11:54:47', 271, 114),
	('2017-03-13 11:55:22', 271, 103),
	('2017-03-13 11:55:28', 271, 106),
	('2017-03-13 11:55:49', 271, 104),
	('2017-03-13 11:56:04', 271, 96),
	('2017-03-13 11:56:32', 271, 111),
	('2017-03-13 11:56:44', 271, 55),
	('2017-03-13 11:56:58', 271, 88),
	('2017-03-13 11:57:14', 271, 101),
	('2017-03-13 11:57:33', 271, 107),
	('2017-03-13 11:58:01', 271, 69),
	('2017-03-13 11:58:03', 271, 71),
	('2017-03-13 11:58:18', 271, 61),
	('2017-03-13 11:58:29', 271, 74),
	('2017-03-13 11:58:30', 271, 76),
	('2017-03-13 11:58:53', 271, 91),
	('2017-03-13 11:59:01', 271, 66),
	('2017-03-13 11:59:14', 271, 83),
	('2017-03-13 11:59:18', 271, 46),
	('2017-03-13 11:59:40', 271, 135),
	('2017-03-13 12:00:08', 271, 113),
	('2017-03-13 12:00:14', 271, 75),
	('2017-03-13 12:00:18', 271, 34),
	('2017-03-13 12:00:39', 271, 93),
	('2017-03-13 12:02:27', 271, 45),
	('2017-03-13 12:03:30', 271, 94),
	('2017-03-13 12:05:23', 271, 87),
	('2017-03-13 12:05:30', 271, 102),
	('2017-03-13 12:05:52', 271, 60),
	('2017-03-13 12:06:31', 271, 134),
	('2017-03-13 12:06:41', 271, 132),
	('2017-03-13 12:07:28', 271, 95),
	('2017-03-14 00:50:07', 272, 38),
	('2017-03-14 00:50:13', 272, 115),
	('2017-03-14 00:50:34', 272, 85),
	('2017-03-14 00:50:53', 272, 126),
	('2017-03-14 00:51:12', 272, 51),
	('2017-03-14 00:51:25', 272, 87),
	('2017-03-14 00:51:29', 272, 114),
	('2017-03-14 00:51:34', 272, 108),
	('2017-03-14 00:51:38', 272, 131),
	('2017-03-14 00:51:51', 272, 67),
	('2017-03-14 00:52:11', 272, 129),
	('2017-03-14 00:52:11', 272, 116),
	('2017-03-14 00:52:16', 272, 56),
	('2017-03-14 00:53:26', 272, 35),
	('2017-03-14 00:53:31', 272, 133),
	('2017-03-14 00:53:51', 272, 110),
	('2017-03-14 00:54:01', 272, 64),
	('2017-03-14 00:54:22', 272, 104),
	('2017-03-14 00:54:50', 272, 98),
	('2017-03-14 00:54:55', 272, 125),
	('2017-03-14 00:55:18', 272, 121),
	('2017-03-14 00:55:49', 272, 45),
	('2017-03-14 00:55:50', 272, 75),
	('2017-03-14 00:56:02', 272, 96),
	('2017-03-14 00:56:13', 272, 90),
	('2017-03-14 00:56:23', 272, 54),
	('2017-03-14 00:56:35', 272, 102),
	('2017-03-14 00:57:06', 272, 122),
	('2017-03-14 00:57:08', 272, 123),
	('2017-03-14 00:57:44', 272, 74),
	('2017-03-14 00:58:14', 272, 135),
	('2017-03-14 00:58:36', 272, 78),
	('2017-03-14 00:59:01', 272, 113),
	('2017-03-14 01:02:38', 272, 95),
	('2017-03-14 01:04:26', 272, 130),
	('2017-03-14 01:04:52', 272, 83),
	('2017-03-14 01:05:49', 272, 107),
	('2017-03-14 01:07:16', 272, 61),
	('2017-03-14 11:50:13', 273, 67),
	('2017-03-14 11:50:15', 273, 141),
	('2017-03-14 11:50:25', 273, 121),
	('2017-03-14 11:50:31', 273, 120),
	('2017-03-14 11:50:46', 273, 108),
	('2017-03-14 11:50:49', 273, 126),
	('2017-03-14 11:50:59', 273, 35),
	('2017-03-14 11:51:00', 273, 43),
	('2017-03-14 11:51:07', 273, 85),
	('2017-03-14 11:51:10', 273, 103),
	('2017-03-14 11:51:11', 273, 106),
	('2017-03-14 11:51:17', 273, 105),
	('2017-03-14 11:51:18', 273, 81),
	('2017-03-14 11:51:19', 273, 115),
	('2017-03-14 11:51:28', 273, 114),
	('2017-03-14 11:51:32', 273, 59),
	('2017-03-14 11:51:52', 273, 92),
	('2017-03-14 11:51:54', 273, 38),
	('2017-03-14 11:52:02', 273, 131),
	('2017-03-14 11:52:08', 273, 94),
	('2017-03-14 11:52:20', 273, 110),
	('2017-03-14 11:52:24', 273, 56),
	('2017-03-14 11:52:45', 273, 75),
	('2017-03-14 11:52:50', 273, 64),
	('2017-03-14 11:52:52', 273, 133),
	('2017-03-14 11:52:56', 273, 116),
	('2017-03-14 11:53:02', 273, 95),
	('2017-03-14 11:53:04', 273, 125),
	('2017-03-14 11:53:24', 273, 102),
	('2017-03-14 11:53:24', 273, 74),
	('2017-03-14 11:53:31', 273, 129),
	('2017-03-14 11:53:33', 273, 138),
	('2017-03-14 11:53:33', 273, 122),
	('2017-03-14 11:53:37', 273, 91),
	('2017-03-14 11:53:39', 273, 130),
	('2017-03-14 11:53:40', 273, 113),
	('2017-03-14 11:53:44', 273, 87),
	('2017-03-14 11:53:56', 273, 55),
	('2017-03-14 11:54:40', 273, 104),
	('2017-03-14 11:55:04', 273, 51),
	('2017-03-14 11:55:11', 273, 77),
	('2017-03-14 11:55:13', 273, 119),
	('2017-03-14 11:55:46', 273, 142),
	('2017-03-14 11:55:56', 273, 69),
	('2017-03-14 11:56:23', 273, 109),
	('2017-03-14 11:56:43', 273, 37),
	('2017-03-14 11:56:54', 273, 136),
	('2017-03-14 11:56:55', 273, 83),
	('2017-03-14 11:56:55', 273, 98),
	('2017-03-14 11:56:59', 273, 53),
	('2017-03-14 11:57:14', 273, 60),
	('2017-03-14 11:57:24', 273, 111),
	('2017-03-14 11:57:26', 273, 46),
	('2017-03-14 11:57:50', 273, 61),
	('2017-03-14 11:57:51', 273, 76),
	('2017-03-14 11:58:01', 273, 45),
	('2017-03-14 11:58:07', 273, 99),
	('2017-03-14 11:58:11', 273, 82),
	('2017-03-14 11:58:32', 273, 139),
	('2017-03-14 11:58:47', 273, 127),
	('2017-03-14 11:59:06', 273, 68),
	('2017-03-14 12:00:07', 273, 135),
	('2017-03-14 12:01:29', 273, 78),
	('2017-03-14 12:02:18', 273, 107),
	('2017-03-14 12:02:44', 273, 96),
	('2017-03-14 12:03:42', 273, 54),
	('2017-03-14 12:06:04', 273, 40),
	('2017-03-14 12:06:28', 273, 39),
	('2017-03-16 00:50:04', 274, 116),
	('2017-03-16 00:50:06', 274, 38),
	('2017-03-16 00:50:13', 274, 68),
	('2017-03-16 00:50:15', 274, 79),
	('2017-03-16 00:50:17', 274, 115),
	('2017-03-16 00:50:25', 274, 98),
	('2017-03-16 00:50:37', 274, 77),
	('2017-03-16 00:50:40', 274, 57),
	('2017-03-16 00:50:49', 274, 107),
	('2017-03-16 00:51:09', 274, 67),
	('2017-03-16 00:51:11', 274, 104),
	('2017-03-16 00:51:19', 274, 142),
	('2017-03-16 00:51:30', 274, 135),
	('2017-03-16 00:51:31', 274, 114),
	('2017-03-16 00:51:37', 274, 64),
	('2017-03-16 00:51:58', 274, 75),
	('2017-03-16 00:52:15', 274, 111),
	('2017-03-16 00:52:33', 274, 78),
	('2017-03-16 00:52:54', 274, 129),
	('2017-03-16 00:53:03', 274, 110),
	('2017-03-16 00:53:23', 274, 122),
	('2017-03-16 00:53:36', 274, 37),
	('2017-03-16 00:53:45', 274, 85),
	('2017-03-16 00:54:19', 274, 123),
	('2017-03-16 00:54:26', 274, 102),
	('2017-03-16 00:54:48', 274, 103),
	('2017-03-16 00:54:52', 274, 51),
	('2017-03-16 00:55:41', 274, 131),
	('2017-03-16 00:56:13', 274, 121),
	('2017-03-16 00:56:55', 274, 70),
	('2017-03-16 00:56:58', 274, 124),
	('2017-03-16 00:57:22', 274, 126),
	('2017-03-16 00:58:14', 274, 62),
	('2017-03-16 00:58:27', 274, 133),
	('2017-03-16 00:58:28', 274, 120),
	('2017-03-16 00:58:57', 274, 55),
	('2017-03-16 00:59:30', 274, 96),
	('2017-03-16 00:59:30', 274, 113),
	('2017-03-16 01:00:03', 274, 125),
	('2017-03-16 01:02:29', 274, 130),
	('2017-03-16 01:02:47', 274, 95),
	('2017-03-16 01:03:04', 274, 60),
	('2017-03-16 01:03:06', 274, 83),
	('2017-03-16 01:07:59', 274, 56);


【参考答案，往下翻】































def putInfoToDict(fileName):
    retDict = {}
    with open(fileName) as f:
        lines = f.read().splitlines()
        
        for line in lines:
            # remove '(' and ')'
            line = line.replace('(','').replace(')','').replace(';','').strip()
            
            parts = line.split(',')
            ciTime     = parts[0].strip().replace("'",'')
            lessonid   = int(parts[1].strip())
            
            
            userid     = int(parts[2].strip())
            
            toAdd = {'lessonid':lessonid,'checkintime':ciTime}
            # if not in, need to create list first
            if userid not in retDict:
                retDict[userid] = []            
            retDict[userid].append(toAdd)
    
            # or just 
            #retDict.setdefault(userid,[]).append(toAdd)
    
    return retDict
    

    
ret = putInfoToDict('0005_1.txt')

import pprint
pprint.pprint(ret)
```

### 思考题讲解

```python
# 220309_思考题讲解2
# 第五次课思考题1
# ('2017-03-13 11:50:09', 271, 131)
# (①签到时间,②课程id,③学生id)
# {
#     ③: [
#         {'lessonid': ②, 'checkintime': ①},
#         {'lessonid': ②, 'checkintime': ①},
#     ],
#
#     ③: [
#         {'lessonid': ②, 'checkintime': ①}
#     ]
#
# }

# 解题思路
# 1.分析需求,预期实现什么效果
# 2.提取出原始字符串①②③
# 3.按照题目要求,将①②③放入对应的位置


def putInfoToDict(fileName):
    dict1 = {}  # 外层字典
    dict2 = {}  # 内层字典
    with open(fileName) as file1:
        list1 = file1.read().splitlines()  #读取文件内容,返回值是列表,每一行是一个元素
        for one in list1:
            one1 = one.replace('(','').replace(')','').replace("'",'').strip(',').strip('\t')
            checkin_time,lesson_id,student_id = one1.split(',')  #取得签到时间,课程id,学生id
            lesson_id = lesson_id.strip()
            student_id = student_id.strip()
            dict2 = {'lessonid':lesson_id,'checkintime':checkin_time}  #将签到信息放入内层字典
            if student_id not in dict1:
                dict1[student_id] = []  #如果学生id在外层字典中未出现过,则新建一个学生id与空列表的键值对
            dict1[student_id].append(dict2)  #将dict2的签到信息添加到列表中
    return dict1


import pprint
pprint.pprint(putInfoToDict('D:/0005_1.txt'))

# 第五次课思考题2
# 解题思路
# 1.分析需要提取的数据
# 2.按照题目要求的格式写入文件

# name: Jack   ;    salary:  12000
#  name :Mike ; salary:  12300
# name: Luk ;   salary:  10030
#   name :Tim ;  salary:   9000
# name: John ;    salary:  12000
# name: Lisa ;    salary:   11000

with open('D:/第5次课练习题2数据.txt') as file1, open('./file2_220309.txt', 'w+') as file2:
    file_line = file1.read().splitlines()
    for one in file_line:
        one1, one2 = one.split(';')
        name = one1.split(':')[1].strip()  # 取得name的值
        salary = int(one2.split(':')[1].strip())  # 取得salary的值
        file2.write(
            f'name: {name:<7};    salary:  {salary:>5} ;  tax:{int(salary * 0.1):>5} ; income:{int(salary * 0.9):>7}\n')

        
        
        # 220309_思考题讲解1
# 第三次课思考题1
# 根据身份证号判断其拥有者的性别
# id_card = '320104199809080797'
# if int(id_card[-2]) % 2 == 1:
#     print('男性')
# else:
#     print('女性')

# 第三次课思考题2
# str1 = 'A girl come in, the name is Jack, level 955;'
# str2 = 'A old lady come in, the name is Mary, level 94454'
# str3 = 'A pretty boy come in, the name is Patrick, level 194'
#
#
# def getName(srcStr):
#     srcStr = srcStr.split(',')[1]  # the name is Jack
#     srcStr = srcStr.split(' ')[-1]  # Jack
#     return srcStr
#
#
# print(getName(str3))

# 第四次课思考题1
# 解题思路
# 1.以分号作为分隔符拆分字符串
# 2.以逗号作为分隔符拆分姓名与年龄
# 3.按照题目要求的格式进行打印
# str1 = 'Jack Green ,   21  ;  Mike Mos, 9;'
# str1 = input('请输入:  ')
# list1 = str1.split(';')
# for one in list1:
#     if one != '':  #只处理非空的值
#         # one1 = one.split(',')[0]  #姓名
#         # one2 = one.split(',')[1]  #年龄
#         name,age= one.split(',')
#         name = name.strip()
#         age = age.strip()
#         print(f'{name:<20}:{age:>02};')

# 第四次课思考题2
# 解题思路
# 1.提取文件类型
# ①以行为单位,每次取一行  ②以\t作为分隔符,分隔各个段落,取得有文件类型的一段  ③以"."作为分隔符,取得文件类型
# 2.提取文件大小
# 3.对相同类型的文件进行累加

log = '''
f20180111014341/i_51a7hC3W.jpeg	169472	FrITJxleSP7wUD-MWw-phL_KP6Eu	15156063244230469	image/jpeg	0	
f20180111014341/j_R0Hpl4EG.json	1036	ForGzwzV3e-uR3_UzvppJs1VgfQG	15156064773253144	application/json	0	
f20180111020739/i_0TDKs0rD.jpeg	169472	FrITJxleSP7wUD-MWw-phL_KP6Eu	15156076847077556	image/jpeg	0	
f20180111020739/j_JFO6xiir.json	1040	FmUhTchdLOd7LBoE8OXzPLDKcW60	15156077904192983	application/json	0	
f20180111090619/i_1BwNksbL.jpg	49634	FtXBGmipcDha-67WQgGQR5shEBu2	15156329458714950	image/jpeg	0	
f20180111090619/i_3BKlsRaZ.jpg	30152	FoWfMSuqz4TEQl5FT-FY5wqu5NGf	15156330575626044	image/jpeg	0	
f20180111090619/i_5XboXSKh.jpg	40238	Fl84WaBWThHovIBsQaNFoIaPZcWh	15156329453409855	image/jpeg	0	
f20180111090619/i_6DiYSBKp.jpg	74017	FrYG3icChRmFGnWQK6rYxa88KuQI	15156329461803290	image/jpeg	0	
f20180111090619/i_76zaF2IM.jpg	38437	Fui8g5OrJh0GQqZzT9wtepfq99lJ	15156334738356648	image/jpeg	0	
f20180111090619/i_B6TFYjks.jpg	37953	FleWqlK2W1ZmEgAatAEcm1gpR0kC	15156329464034474	image/jpeg	0	
f20180111090619/i_N9eITqj3.jpg	38437	Fui8g5OrJh0GQqZzT9wtepfq99lJ	15156330419595764	image/jpeg	0	
f20180111090619/i_QTSNWmA6.jpg	37953	FleWqlK2W1ZmEgAatAEcm1gpR0kC	15156333104224056	image/jpeg	0	
f20180111090619/i_XdHcAfh1.jpg	56479	FjLQIQ3GxSEHDfu6tRcMylK1MZ05	15156334227270309	image/jpeg	0	
f20180111090619/i_Xyy723MU.jpg	50076	FsfZpQzqu084RUw5NPYW9-Yfam_R	15156334229987458	image/jpeg	0	
f20180111090619/i_d8Go0EOv.jpg	30152	FoWfMSuqz4TEQl5FT-FY5wqu5NGf	15156334736228515	image/jpeg	0	
f20180111090619/i_diuHmX53.jpg	40591	FuTx1pw4idbKnV5MSvNGxCA5L470	15156333878320713	image/jpeg	0	
f20180111090619/i_qQKzheSH.jpg	55858	Fj0A3i8V7fzzOiPQFL79ao15hkN9	15156329456666591	image/jpeg	0	
f20180111090619/i_rHL5SYk8.jpg	40238	Fl84WaBWThHovIBsQaNFoIaPZcWh	15156336509742181	image/jpeg	0	
f20180111090619/i_xZmQxUbz.jpg	40238	Fl84WaBWThHovIBsQaNFoIaPZcWh	15156333240603466	image/jpeg	0	
f20180111090619/i_zBDNgXDv.jpeg	73616	FlgNwq8lypgsxrWs_ksrS_x47SQV	15156334232887875	image/jpeg	0	
f20180111090619/j_4mxbEiVh.json	2990	Fpq-3yl3Yr1CadNrJVSDnpeRhQtT	15156331445226898	application/json	0	
f20180111090619/j_i1K74768.json	3042	Fl5PpDw1TsZXMuhoq1RUrOeGZ6br	15156335067090003	application/json	0	
f20180111095839/i_Q7KMKeda.png	518522	Fl-yB1_ruL2uxZN9k7DjB62h9dYH	15156359599713253	image/png	0	
f20180111095839/j_5DpqHolV.json	184	FoYvi7cmSrzuVjUgCRzW5kU95SVo	15156359719719064	application/json	0	
f20180111100442/i_No8kToIV.jpg	48975	Fu1cw3f--5Vpz9kLGeJfvljhCtyZ	15156364349642377	image/jpeg	0	
f20180111100442/i_P1bkvSeg.jpg	68200	FvYe8vi46TjUKhEy_UwDqLhO6ZsW	15156363800690634	image/jpeg	0	
f20180111100442/i_T1AulKcD.jpg	52641	Fj2YzvdC1n_1sF93ZZgrhF3OzOeY	15156364021186365	image/jpeg	0	
f20180111100442/i_X8d8BN07.jpg	50770	FivwidMiHbogw77lqgkIKrgmF3eA	15156363969737156	image/jpeg	0	
f20180111100442/i_g0wtOsCX.jpg	76656	Fmtixx0mP9CAUTNosjLuYQHL6k0P	15156363448222155	image/jpeg	0	
f20180111100442/i_h5OT9324.jpg	72672	FvbIqPLTh2cQHTIBv2akUfahZa_Z	15156364401354652	image/jpeg	0	
f20180111100442/i_he8iLYI6.jpg	49399	FjeJvwjwhU-hKZsq66UoBg9_tEJs	15156363907932480	image/jpeg	0	
f20180111100442/i_kg29t7Pp.jpg	76293	FuYj__sSeEN7AsXMbxO24Z8Suh8d	15156364156384686	image/jpeg	0	
f20180111100442/i_oz1YoBI1.jpg	75620	FkY3xsUMwOI01zgoH1iXXgiQeq6I	15156364089112904	image/jpeg	0	
f20180111100442/i_xrOT98on.jpg	50021	Fql7ookM1Rc6V7VairKAfnKe-o9w	15156363856357316	image/jpeg	0	
f20180111135114/i_Zqt8Tmoe.png	161629	FlELw59_mV3VqDBLyu1BKN4fIWnx	15156500155209863	image/png	0	
f20180111135114/j_uhHoMXKq.json	159	FrypljwAr2LgoLAePBNTUYTUAgDt	15156500200488238	application/json	0	
f20180111142119/i_s83iZ2GR.png	92278	Fns8tdh3JCkRmfE_COYEu4o8w03E	15156517082371259	image/png	0	
f20180111142119/j_0g45JRth.json	159	Fq1rFwdRguYRXrp61nGZ5TsUG1V-	15156517143375596	application/json	0	
f20180111144306/i_yE5TC84E.png	139230	Fjf61ymabEnEvnr5ZMHFjXGCrYlP	15156530038824150	image/png	0	
f20180111144306/j_OF4WVtSH.json	159	FqwkKcxfo8jd0jFUyuH4X2CrnE9q	15156530083419530	application/json	0	
f20180111150230/i_KtnER4g3.png	120044	FuwOWdrqzcr2-UScem-LzEMgMezs	15156541734892258	image/png	0	
f20180111150230/j_xMSUEejY.json	158	FjJr_4deMqFphGaptm-2Pa6wwRP2	15156541771989216	application/json	0	
f20180111151741/i_JuSWztB3.jpg	92506	FrIjRevHSi6xv4-NQa2wrHu5a1zQ	15156550875370965	image/jpeg	0	
f20180111153550/i_9wWzVenl.gif	769872	FvslKY9JUaCQm-lu02E34tvAP_oG	15156561674621628	image/gif	0	
'''

dict1 = {}
line_list = log.split('\n')  #以行为单位,取得每一行的数据
for one in line_list:
    if one != '':  #只处理非空行
        one1 = one.split('\t')[0]  #取得含有文件类型的那一段
        file_type = one1.split('.')[1]  #取得文件类型
        file_size = int(one.split('\t')[1])  #取得文件大小,转为int型
        if file_type not in dict1:  #如果在dict1中,文件类型没有出现过
            dict1[file_type] = file_size  #新增文件类型与文件大小
        else:
            dict1[file_type] += file_size  #将文件大小进行累加
print(dict1)



统计10000以内有多少个含有9的数.




































count=0
for i in range(1,10001):
    if '9' in str(i):
        count+=1
print(count)
```

### 220311_模块与包

```python
# 220311_模块与包
# import copy  #标准库,导入后使用
# import selenium  #第三方库,安装后,导入使用

#一个.py文件就是一个模块
#包是一种特殊的文件夹,如果目录里有__init__.py,则这个文件夹就是包
#当导入包时,__init__.py文件里的代码会执行一次
#包或文件夹都可以导入
# import AUTO59_1  #导入一个包或文件夹

#导入模块的几种形式
# import qqq  #在当前目录内,可以这样导入,在标准路径里,也可以这样导入
# print(qqq.fun1(6,9))

# import arrow
# now = arrow.now()  #打印当前时间
# print(now.format('YYYY-MM-DD hh:mm:ss'))

# from AUTO59_1 import qqq  #from 包 import 模块
# from pathlib import Path
# print(Path(__file__))  #打印当前文件的目录与文件名
# print(Path(__file__).parent)  #打印当前文件的目录

# from AUTO59_1.qqq import fun1  #导入某个模块里的某个函数
# print(fun1(6,9))

#标准路径
import sys
# for one in sys.path:  #遍历标准路径
#     print(one)
#sys.path的第一个路径是当前目录,第二个路径是工程目录,其他的是标准路径
#python的第三方库放在python所在目录的\lib\site-packages
# sys.path.append('D:/PKG200')  #将目录添加到标准路径
# import QQQ

# sys.path.append('D:/')  #将目录添加到标准路径
# from PKG200 import QQQ  #导入时,要注意目录的层级,如果要按这个模式导入,添加标准路径时,应该添加D:\一层

# if __name__ == '__main__':  #以下代码只在本模块内执行
#     pass

# from AUTO59_1 import qqq

#安装第三方库
#在cmd中执行pip install 第三方库名
#苹果系统在终端执行pip3 install 第三方库名
#如果下载比较慢,可以使用国内的镜像站进行安装
#豆瓣源
#pip install pytest -i http://pypi.douban.com/simple/ --trusted-host pypi.douban.com
#清华源
#pip install selenium -i https://pypi.tuna.tsinghua.edu.cn/simple/  --trusted-host pypi.tuna.tsinghua.edu.cn

#查看已安装的第三方库
#cmd中执行pip list
#卸载第三方库
#cmd中执行pip uninstall 第三方库名

#安装指定的版本
#pip install selenium == 3.141
```

### 220311_读取yaml文件

```python
# 220311_读取yaml文件
#安装yaml模块 pip install PyYaml
import yaml

# with open('./1.yaml') as file1:
#     text = yaml.load(file1,Loader=yaml.FullLoader)
#     print(text)

#读取多种格式的yaml文件
# with open('./1.yaml',encoding='utf-8') as file1:
#     text = yaml.load_all(file1,Loader=yaml.FullLoader)
#     for one in text:
#         print(one)

#统计10000以内有多少个含有9的数
count = 0
for i in range(10001):
    if '9' in str(i):
        count += 1
print(count)


写一个猜数字游戏,需求如下:
随机生成一个0-100之间的数字,让用户猜,如果用户猜对了,提示:回答正确,游戏结束.
如果猜错了给出对应的提示(您输入的值过大,您输入的值过小),最多允许猜7次.


































from random import randint
answer=randint(0,100)
for i in range(7):
    youranswer = int(input('请输入一个数字:  '))
    if youranswer>answer:
        print('输入的值过大')
    elif youranswer<answer:
        print('输入的值过小')
    else:
        print('回答正确')
        break
```

### 220121_pycharm使用技巧

```python
# 220121_pycharm使用技巧
# 新建文件时,自动生成代码
# settings→editor→file and code templates,选择python script
# ${NAME}  文件名
# ${DATE}  日期

# 自动补齐
# if __name__ == '__main__':  #先输入main,然后按tab键
#     pass

# 自动补齐自定义的段落
# settings→editor→live templates,在右侧点击+号,添加自定义的内容
# 完成之后,在下方勾选python

# 修改注释的颜色
# settings→editor→color scheme→python

# 取消语法检查
# settings→editor→inspections,选择python,取消勾选PEP 8的两个选项

# 真实环境与虚拟环境

# 分屏
# settings→keymap,查询split关键字,找到分屏的图标,设置快捷键

# 设置编码
# settings→editor→file encodings
# 也可以在文件的第一行加上#encoding=utf-8

# 列表的排序
# list1=[68,36,72,10,100,-50,109,27]
# list1.sort(reverse=True)  #排序,没有返回值,reverse=True倒序排序
# print(list1)
# list1_new=sorted(list1,reverse=True)  #排序,返回排序后的列表,reverse=True倒序排序
# print(list1_new)
# list1_new = list1[::-1]  #只翻转,不排序
# print(list1_new)

# 冒泡算法
# 列表中的数字,两两比较,如果前面的数比后面的大,则两数互换,每一轮确定一个最大的数,通过若干轮比较,实现所有数的排序

# 原始数据[4,3,2,1]  n个数,第一轮比较n-1次
# 3421  #第一次比较,将4和3的位置互换
# 3241  #第二次比较,将4和2的位置互换
# 3214  #第三次比较,将4和1的位置互换
# 第二轮比较,原始数据[3,2,1,4]  比较n-2次
# 2314  第一次比较,将3和2的位置互换
# 2134  第二次比较,将3和1的位置互换
# 第三轮比较,原始数据[2,1,3,4]  比较n-3次
# 1234 第一次比较,将2和1的位置互换
# n个数,最多比较n-1轮
# list1 = [68, 36, 72, 10, 100, -50, 109, 27]
# for i in range(len(list1) - 1):  # 控制比较多少轮
#     for j in range(len(list1) - 1 - i):  # 控制比较多少次
#         if list1[j] > list1[j + 1]:
#             list1[j], list1[j + 1] = list1[j + 1], list1[j]
# print(list1)

# for i in range(len(list1) - 1):  # 控制比较多少轮
#     for j in range(len(list1) - 1 - i):  # 控制比较多少次
#         if list1[j] > list1[j + 1]:
#             print(f'第{j}位和第{j + 1}位的顺序不对')
#             print(f'变化之前--------------------->{list1}')
#             list1[j], list1[j + 1] = list1[j + 1], list1[j]
#             print(f'变化之后--------------------->{list1}')
#     print(f'第{i + 1}轮比较结束')
# print(list1)

```

### 220121_requests爬虫

```python
# 220121_requests爬虫
import re
import requests
url = 'http://www.quannovel.com/read/620/'  # 需要进行爬虫的网址
req = requests.post(url)  # 访问网页,获取网页内容
title_list = re.findall('class="name ">(.*?)</a>', req.text)  # 获取章节名
# for one in title_list:
#     print(one)

url_list = re.findall('<a href="(.*?).html', req.text)  # 获取正文网址
# for one in url_list:
#     print(f'{url}{one}.html')

dict1 = {}
for i in range(len(title_list)):
    dict1[title_list[i]] = f'{url}{url_list[i]}.html'  #将目录和网址放到字典里
# for k,v in dict1.items():
#     print(k,v)


获取全书网的任意一本书的正文,每个章节为一个txt文件(如果章节太多可以获取前5章),这些文件全部放在以书名命名的文件夹中




































import re
import os
import requests
url='http://www.quannovel.com/read/620/'  #进行爬虫的网址
req=requests.get(url)  #获取网页的内容
name=re.findall('<h2>(.*?)<i class',req.text)[0]  #获取书名
mulu=re.findall('class="name ">(.*?)</a>',req.text)
wangzhi=re.findall('<a href="(.*?).html"',req.text)
dict1={}
for i in range(len(mulu)):
    dict1[mulu[i]]=f'{url}{wangzhi[i]}.html'  #将目录和网址放入字典
count=1

if not os.path.exists(f'D:/{name}'):  #如果没有以书名命名的目录,则新建
    os.mkdir(f'D:/{name}')
for k,v in dict1.items():
    if count>5:
        break
    else:
        req=requests.get(v)  #获取正文网页的内容
        neirong=re.findall('class="page-content ">(.*?)<div class',req.text,re.S)[0]  #获取文章内容
        neirong=neirong.replace("<p>",'').replace('</p>','')
        with open(f'D:/{name}/{k}.txt','w+') as file1:
            file1.write(neirong)
    print(f'第{count}章爬取完毕')
    count+=1
```

### 220314_爬取全书网正文

```python
# 220314_爬取全书网正文
import os
import re  #正则表达式模块
import requests  #爬虫模块
url = 'http://www.quannovel.com/read/620/'  #需要进行爬虫的网址
req = requests.get(url)  #访问网页,获取网页内容,网页内容为req.text

book_name = re.findall('<h2>(.*?)<i class',req.text)[0]

title_list = re.findall('class="name ">(.*?)</a>',req.text)  #获取章节名

url_list = re.findall('href="(.*?).html',req.text)  #获取网页的数字

dict1 = {}
for i in range(len(title_list)):
    dict1[title_list[i]] = f'{url}{url_list[i]}.html'  #将目录和网址放到字典里  f'{url}{正文网页的数字}.html'

if not os.path.exists(f'D:/{book_name}'):  #如果目录不存在,则新建
    os.mkdir(f'D:/{book_name}')

# count = 1
# for k,v in dict1.items():
#     if count > 5:
#         break
#     else:
#         req = requests.get(v)  #访问正文网页
#         text = re.findall('class="page-content ">(.*?)<div class',req.text,re.S)[0]  #获取文章内容
#         text = text.replace('<p>','').replace('</p>','')
#         with open(f'D:/{book_name}/{k}.txt','w+') as file1:
#             file1.write(text)
#     print(f'第{count}章爬取完毕')
#     count += 1

count = 1
with open(f'D:/{book_name}/{book_name}.txt', 'w+') as file1:
    for k,v in dict1.items():
        if count > 5:
            break
        else:
            req = requests.get(v)  #访问正文网页
            text = re.findall('class="page-content ">(.*?)<div class',req.text,re.S)[0]  #获取文章内容
            text = text.replace('<p>','').replace('</p>','')
            file1.write(k)
            file1.write(text)
            file1.write('------------------------------------------------------\n')
        print(f'第{count}章爬取完毕')
        count += 1
```

### 220314_面向对象基础

```python
# 220314_面向对象基础
# 类是抽象的模板,实例是根据模板创建出来的具体的对象,比如人类就是一个类,彭于晏是人类的一个实例
# 新建一个长方形的类

# self表示实例本身,这个参数是默认的,不需要传值.用户在实例化一个长方形时,需要传长和宽两个值,之后初始化方法将其转为长方形实例的属性.
# 初始化方法里的length转换为self.length之后,类当中的所有含有self参数的方法都可以使用这个属性

class Rectangle:
    def __init__(self, length, width):  # 初始化方法
        self.length = length  # 将用户传的length转为实例自身的属性
        self.width = width  # 将用户传的的width转为实例自身的属性

    def perimiter(self):  # 周长的方法
        return (self.length + self.width) * 2

    def area(self):  # 面积的方法
        return self.length * self.width


#
# rec = Rectangle(10,8)  #实例化
# print(rec.__dict__)  #打印实例的属性
# print(rec.perimiter())
# print(rec.area())


# 在做程序开发中，我们常常会遇到这样的需求：需要执行对象里的某个方法，或需要调用对象中的某个变量，但是由于种种原因我们无法确定这个方法或变量是否存在，这时我们需要用一个特殊的方法或机制要访问和操作这个未知的方法或变量，这种机制就称之为反射。

# hasattr
# print(hasattr(str,'replace'))  #在对象里找有没有某个属性或方法,返回值是布尔型
# print(hasattr(list,'append'))

# getattr
# print(getattr(str,'replace'))  #在对象里找有没有某个属性或方法,返回值是属性或方法本身
# print(getattr(str,'replace1',12345))  #也可以写两个参数,找不到属性或方法时,返回第二个参数

# setattr
# class Class1:
#     a=1
#     def __init__(self):
#         self.b = 200
# setattr(Class1,'a',100)  #修改类属性
# print(Class1.a)
# cls1 = Class1()
# setattr(cls1,'b',99)  #修改实例属性
# print(cls1.b)

# 单例模式
# 一般来说,一个类可以生成任意个实例,单例模式只生成一个实例
# class Single:
#     def __init__(self):
#         pass
#     def __new__(cls, *args, **kwargs):  #构造方法
#         if not hasattr(cls,'obj'):  #判断类当中有没有实例,如果没有则新建
#             cls.obj = object.__new__(cls)  #新建一个类的实例
#         return cls.obj
#
# s1 = Single()
# s2 = Single()
# print(s1 == s2)

class Restaurant:
    def yuxiangrousi(self):
        return '鱼香肉丝'

    def gongbaojiding(self):
        return '宫爆鸡丁'

    def qingjiaotudousi(self):
        return '青椒土豆丝'

    def fanqiejidan(self):
        return '番茄鸡蛋'

    def kaishuibaicai(self):
        return '开水白菜'

customer = Restaurant()
while True:
    menu = input('请点菜:  ')
    if hasattr(Restaurant,menu):
        print('好的,请厨师开始做菜')
        break
    else:
        print('没有这道菜')
        
        
        
        
        
        写一个三角形的类,包括初始化方法,计算周长的方法,计算面积的方法(可以用海伦公式)




































class Sanjiaoxing:
    def __init__(self,a,b,c):
        self.a=a
        self.b=b
        self.c=c
    def zhouchang(self):
        if self.a+self.b<=self.c or self.a+self.c<=self.b or self.b+self.c<=self.a:
            return '无法构成三角形,忽略周长'
        else:
            return self.a+self.b+self.c
    def mianji(self):
        if self.a + self.b <= self.c or self.a + self.c <= self.b or self.b + self.c <= self.a:
            return '无法构成三角形,忽略面积'
        else:
            p=(self.a+self.b+self.c)/2
            return (p*(p-self.a)*(p-self.b)*(p-self.c))**0.5
sjx=Sanjiaoxing(3,4,5)
print(sjx.zhouchang())
print(sjx.mianji())
 
```



```python
# real_logger
from loguru import logger
from time import strftime

class MyLog():  #新建一个类
    __call_flag=True  #变量设置为真

    # 单例模式
    def __new__(cls, *args, **kwargs):  # 构造方法
        if not hasattr(cls, 'obj'):  # 判断类当中有没有实例,如果没有则新建
            cls.obj = object.__new__(cls)  # 生成实例对象
        return cls.obj
    def get_log(self):
        if self.__call_flag:  #如果变量为真
            __curdate=strftime('%Y%m%d-%H%M%S')
            logger.remove(handler_id=None)  #不在控制台显示
            logger.add(f'文件名{__curdate}.log',rotation='200KB',compression='zip',encoding='utf-8')  #对logger进行设置
            self.__call_flag=False  #变量设置为假
        return logger



if __name__ == '__main__':
    log100 = MyLog().get_log()  #实例化log100
    log100.error('张三')
    from time import sleep
    sleep(2)
    log101 = MyLog().get_log()  #实例化log101,如果是单例模式,log100和log101的内容会写在一个文件里,如果不是单例模式则写在两个文件里
    log101.error('李四')
```

### 220316_面向对象进阶

```python
# 220316_面向对象进阶

class Rectangle:
    def __init__(self, length, width):  # 初始化方法
        self.length = length  # 将用户传的length转为实例自身的属性
        self.width = width  # 将用户传的的width转为实例自身的属性

    def perimeter(self):  # 周长的方法
        return (self.length + self.width) * 2

    def area(self):  # 面积的方法
        return self.length * self.width

    @classmethod  # 装饰器,声明下面的方法是类方法
    def features(cls):
        print('两边的长相等,两边的宽也相等,长和宽的角度是90°')

    @staticmethod  # 装饰器,声明下面的方法是静态方法
    def sumdata(a, b):
        return a + b


# Rectangle.features()
rec = Rectangle(6, 3)
# print(rec.perimeter())
# print(rec.area())
# Rectangle.perimeter()  #类不能调用实例方法
# rec.features()  #实例可以调用类方法

# print(Rectangle.sumdata(1,2))  #静态方法可以由类调用,也可以由实例调用
# print(rec.sumdata(3,6))

# 使用type()查看对象是方法还是函数
# print(type(rec.perimeter))  #实例方法是method
# print(type(rec.features))  #类方法是method
# print(type(rec.sumdata))  #静态方法

# inpect模块,判断对象是否是某个类型,返回值是布尔型
import inspect


# 类方法与实例方法都是method,静态方法是function
# print(inspect.ismethod(rec.perimeter))
# print(inspect.ismethod(rec.features))
# print(inspect.ismethod(rec.sumdata))

# print(inspect.isfunction(rec.perimeter))
# print(inspect.isfunction(rec.features))
# print(inspect.isfunction(rec.sumdata))

# 写一个正方形的类
# 完全继承
# class Square(Rectangle):
#     pass
#
#
# squ = Square(6, 6)
# print(squ.perimeter())
# print(squ.area())

# 部分继承,改写父类的某些方法
# class Square(Rectangle):
#     def __init__(self,side):
#         self.length = side
#         self.width = side
#
# squ = Square(6)
# print(squ.perimeter())
# print(squ.area())
# squ.features()

# 改写父类的方法,保留父类方法的同时,增加一些代码
# class Square(Rectangle):
#     @classmethod
#     def features(cls):
#         super().features()  # 继承父类features方法的代码
#         print('长和宽也相等')
#
#
# Square.features()


# 所有的类,都是object的子类
class Class1:
    '''
    云想衣裳花想容
    春风拂槛露华浓
    '''


# cls1 = Class1()
# print(cls1.__dict__)  #显示实例的属性
# print(cls1.__doc__)  #显示类的注释
# print(Class1.__name__)  #显示类的名称
# print(Class1.__base__)  #显示父类的名称
# print(Class1.__bases__)  #显示所有父类的名称

#多继承,一个类可以有多个父类
class Money1:
    def money(self):
        print('一个亿')

class Money2:
    def money(self):
        print('两个亿')

class Human(Money1,Money2):  #调用父类中的同名方法时,按继承顺序进行调用
    pass
man= Human()
man.money()
```

### 220316_面向对象高级

```python
# 220316_面向对象高级
# 私有属性与私有方法
# 私有属性与私有方法不能从外部被调用,也不能被子类继承
# 在属性或方法的前面加上__,就是私有属性或私有方法
# 如果前后都有__,不是私有属性或私有方法

# class Class100:
#     __str1 = 'abc'  # 私有属性
#     str2 = 'def'
#
#     def __method1(self):  # 私有方法
#         print('这是私有方法')
#
#     def method2(self):
#         print(self.__str1)
#         self.__method1()
# cls10 = Class100()
# # print(cls10.__str1)
# # cls10.__method1()
# cls10.method2()

# @property
# class Class6:
#     def __init__(self):
#         self.a = 100
#     @property  #装饰器,声明下面的方法是一个属性,而不是方法
#     def b(self):
#         return self.a
# cls6 = Class6()
# print(cls6.b)


#多态
#不同的类中有同名的方法,调用方法时,根据对象的不同,实现的操作也不同,称之为多态
#比如,调用狗的叫的方法为狗叫,调用猫的叫的方法为猫叫
# class Dog:
#     def say(self):
#         print('汪汪汪')
#
# class Cat:
#     def say(self):
#         print('喵喵喵')
#
# dog = Dog()
# cat = Cat()
#
# def animal_say(animal):
#     animal.say()
# animal_say(cat)

# class Restaurant:
#     pass
#
# class Yuxiangrousi(Restaurant):
#     def menu(self):
#         print('鱼香肉丝')
#
# class Gongbaojiding(Restaurant):
#     def menu(self):
#         print('宫爆鸡丁')
#
# class Qingjiaotudousi(Restaurant):
#     def menu(self):
#         print('青椒土豆丝')
#
# customer1 = Yuxiangrousi()
# customer2 = Gongbaojiding()
# customer3 = Qingjiaotudousi()

# def waiter(obj):
#     obj.menu()
# waiter(customer3)

#第7次课思考题
# 写一个猜数字游戏,需求如下:
# 随机生成一个0-100之间的数字,让用户猜,如果用户猜对了,提示:回答正确,游戏结束.
# 如果猜错了给出对应的提示(您输入的值过大,您输入的值过小),最多允许猜7次.

# from random import randint
# answer = randint(0,100)
# for i in range(7):
#     input1 = int(input('请输入一个数字:  '))
#     if input1 == answer:
#         print('回答正确')
#         break
#     elif input1 > answer:
#         print('数字过大')
#     elif input1 < answer:
#         print('数字过小')

# 写一个三角形的类
class Triangle:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def perimeter(self):
        if self.a + self.b <= self.c or self.a + self.c <= self.b or self.b + self.c <= self.a:
            return '无法构成三角形,忽略周长'
        else:
            return self.a + self.b + self.c

    def area(self):
        if self.a + self.b <= self.c or self.a + self.c <= self.b or self.b + self.c <= self.a:
            return '无法构成三角形,忽略周长'
        else:
            p = (self.a + self.b + self.c) / 2
            return (p * (p - self.a) * (p - self.b) * (p - self.c)) ** 0.5

tr = Triangle(3,4,5)
print(tr.perimeter())
print(tr.area())




要求大家用面向对象的设计编写一个python程序，实现一个文字游戏系统。

动物园里面有10个房间，房间号从1 到 10。

每个房间里面可能是体重200斤的老虎或者体重100斤的羊。
游戏开始后，系统随机在10个房间中放入老虎或者羊。

然后随机给出房间号，要求游戏者选择敲门还是喂食。

如果选择喂食：
喂老虎应该输入单词 meat，喂羊应该输入单词 grass
喂对了，体重加10斤。 喂错了，体重减少10斤

如果选择敲门：
敲房间的门，里面的动物会叫，老虎叫会显示 ‘Wow !!’,羊叫会显示 ‘mie~~’。 动物每叫一次体重减5斤。


游戏者强记每个房间的动物是什么，以便不需要敲门就可以喂正确的食物。 
游戏3分钟结束后，显示每个房间的动物和它们的体重。


实现过程中，有什么问题，请通过课堂上讲解的调试方法，尽量自己发现错误原因。

【参考答案，往下翻】
































from random import randint
import time

class Tiger(object):
    classname = 'tiger'

    def __init__(self,weight=200):
        self.weight = weight

    def roar(self):
        print('wow!!!')
        self.weight -= 5


    def feed(self,food):
        if food == 'meat':
            self.weight += 10
            print('正确，体重 + 10')
        else :
            self.weight -= 10
            print('太惨了，体重 - 10')


class Sheep:
    classname = 'sheep'
    def __init__(self,weight=100):
        self.weight = weight

    def roar(self):
        print('mie~~')
        self.weight -= 5

    def feed(self,food):
        if food == 'grass':
            self.weight += 10
            print('正确，体重 + 10')
        else :
            self.weight -= 10
            print('太惨了，体重 - 10')


class Room:
    def __init__(self,num,animal):
        self.num = num
        self.animal = animal


rooms = []
for no in range(10):
    if randint(0,1):
        ani = Tiger(200)
    else:
        ani = Sheep(100)

    room = Room(no,ani)
    rooms.append(room)

startTime = time.time()
while True:
    curTime = time.time()
    if (curTime - startTime) > 120:
        print('\n\n **********  游戏结束 ********** \n\n')
        for idx, room in enumerate(rooms):
            print('房间 :%s' % (idx + 1), room.animal.classname, room.animal.weight)
        break


    roomno = randint(1, 10)
    room = rooms[roomno-1]  # why -1 ?
    ch = input('我们来到了房间# %s, 要敲门吗?[y/n]' % roomno)
    if ch == 'y':
        room.animal.roar()

    food = input('请给房间里面的动物喂食:')
    room.animal.feed(food.strip())







```

### 220318_异常与日志

```python
# 220318_异常与日志
# 异常就是程序运行时出现了错误
# try except可以抓取异常,语句中,至少有一个except,也可以有多个,也可以有一个else语句,一个finally语句
# try:
#     input1 = int(input('请输入一个数字:  '))
#     print(1/input1)
# except ZeroDivisionError:  #0作为分母的异常
#     print('0不能作为分母')
# except ValueError:  #非数字异常
#     print('您输入的不是数字')
# except:  #不指定异常类型,则捕获任何异常
#     print('程序出现异常')
# else:  #程序未出现异常,则执行else中的语句
#     print('程序未出现异常')
# finally:  #无论程序是否出现异常,都会执行
#     print('程序运行完毕')

# 常见的异常
# NameError　　未定义的变量
# print(a)

# IndexError  下标越界
# list1 = [100]
# print(list1[1])

# FileNotFoundError  找不到文件异常
# with open('./cdefg.txt') as file1:
#     print(file1.read())

# 所有的异常,都是Exception的子类,或者子类的子类
# print(NameError.__base__)
# print(IndexError.__base__)
# print(LookupError.__base__)
# print(FileNotFoundError.__base__)
# print(OSError.__base__)
# print(Exception.__base__)
# print(BaseException.__base__)

#raise 手动抛出异常
# try:
#     raise IOError
# except IOError:
#     print('出现了IO异常')

from loguru import logger
# 日志的级别 debug<info<warning<error<critical


logfile = './log1.log'
# logger.remove(handler_id=None)  #不在控制台打印

# rotation='200KB'  文件达到200KB之后生成新文件  compression='zip'  将达到200KB的文件压缩为.zip格式
logger.add(logfile,encoding='utf-8',rotation='200KB',compression='zip')
# logger.debug('松勤')
# logger.info('松勤')
# logger.warning('松勤')
# logger.error('松勤')
# logger.critical('松勤')


# for i in range(10000):
#     logger.warning('test')

def fun1(a,b):
    return a/b
try:
    print(fun1(100,0))
except:
    logger.exception('报错')
```

### 220318_面向对象思考题讲解

```python
# 220318_面向对象思考题讲解
# 解题思路
# 新建一个老虎的类,老虎的体重是200,类里面有吃的方法,有叫的方法
# 新建一个羊的类,羊的体重是100,类里面有吃的方法,有叫的方法
# 新建一个房间的类,房间的实例可以放动物的实例
# 写游戏相关的代码
# ①调用吃的方法
# ②调用叫的方法
# ③游戏时间的控制
# ④游戏结束时显示每个房间的动物的种类与体重

class Tiger:
    def __init__(self):
        self.name = '老虎'
        self.weight = 200

    def eat(self, food):
        if food == 'meat':
            print('喂食正确')
            self.weight += 10
        elif food == 'grass':
            print('喂食错误')
            self.weight -= 10

    def roar(self):
        print('Wow!!')
        self.weight -= 5


class Sheep:
    def __init__(self):
        self.name = '羊'
        self.weight = 100

    def eat(self, food):
        if food == 'grass':
            print('喂食正确')
            self.weight += 10
        elif food == 'meat':
            print('喂食错误')
            self.weight -= 10

    def roar(self):
        print('mie~~')
        self.weight -= 5


class Room:
    def __init__(self, animal):
        self.animal = animal


roomlist = []  # 定义一个列表,可以存放房间的实例
from random import randint

for i in range(10):
    if randint(1, 2) == 1:  # 在1和2之间随机取一个整数
        animal = Tiger()  # 实例化一个老虎
    else:
        animal = Sheep()  # 实例化一个羊
    room = Room(animal)  # 实例化一个房间,把动物的实例放入房间
    roomlist.append(room)  # 把房间的实例放入列表

import time

start_time = time.time()  # 记录游戏开始时间
while time.time() - start_time <= 10:  # 控制游戏时间
    room_number = randint(0, 9)  # 在0-9之间随机选取一个数字
    random_room = roomlist[room_number]  # 选取随机的房间
    load1 = input(f'当前访问的是{room_number + 1}号房间,请问是敲门还是喂食?1敲门,2喂食')
    if load1 == '1':
        random_room.animal.roar()  # 调用房间里的动物的叫的方法
    elif load1 == '2':
        food = input('请选择喂的食物 meat/grass')
        if food in ('meat', 'grass'):
            random_room.animal.eat(food)
        else:
            print('您输入的食物不正确')
    else:
        print('请输入1或2')
else:
    print('游戏结束')
    for i in range(len(roomlist)):
        print(f'{i + 1}号房间的动物是{roomlist[i].animal.name},体重是{roomlist[i].animal.weight}')

```



```python
将九九乘法表写入到文件中.




































with open('D:/99乘法表.txt','w+') as file1:
    for i in range(1,10):
        for j in range(1,i+1):
            file1.write(f'{j}*{i}={i*j}\t')
        file1.write('\n')
```

### pytest框架入门+allure报告

```python
# test_pytest220320
# pytest命名规范
# 测试文件必须以test_开头（或者以_test结尾）,pytest所在的路径不要使用中文或特殊符号
# 测试类必须以Test开头，并且不能有 __init__ 方法
# 测试方法必须以test_开头
# 断言必须使用 assert

# settings→tools→pyton integrated tools,找到default test runner,选unittests
# unittests模式,测试通过为".",测试不通过为F
import pytest
import os
import allure


# class Test1:
#     def test_c01(self):
#         assert 1 == 1
#     def test_c02(self):
#         assert 2 == 3

# list1 = [[1, 1], [2, 3], [3, 6], [7, 8], [9, 10]]


# class Test2:
#     @pytest.mark.parametrize('result,real_result', list1)  # 数据驱动
#     def test_c100(self, result, real_result):
#         assert result == real_result


# 1、下载allure.zip
# 2、解压allure.zip到一个文件目录中,allure需要有jdk环境
# 3、将allure报告安装目录\bin所在的路径添加环境变量path中
# 4、pip install  allure-pytest


@allure.epic('层级1')
@allure.feature('层级2')
@allure.story('层级3')
@allure.title('层级4')
class Test6:
    def test_c06(self):
        assert 1 == 2


path = '../report/report'
if __name__ == '__main__':
    # pytest.main([__file__, '-s', '--alluredir', '../report'])
    # os.system('allure generate ../report -o ../report/report --clean')

    pytest.main([__file__,'-s','--alluredir',f'{path}','--clean-alluredir'])
    os.system(f'allure serve {path}')

```



```python
# test_example1
# import pytest
# @pytest.fixture(scope='function',autouse=True)  #装饰器,声明下面的函数是setup函数  setup在所有方法之前执行,teardown在所有方法之后执行
# # scope缺省值为function,还有class,module,session级别
# # session级别,fixture的内容写到conftest.py中,目录下的所有文件共用这个配置
# # autouse=True  自动使用setup函数
# def fun1():
#     print('测试开始')
#     yield  #声明下面的代码为teardown的用法
#     print('测试结束')
#
# class Test1:
#     def test_some_data(self):
#         assert 1 == 2
#     def test_some_data2(self):
#         assert 2 == 3
# if __name__ == '__main__':
#     pytest.main([__file__,'-sv'])  #-s允许执行print,-v更加详细的报告

with open('./九九乘法表','w+') as file1:
    for i in range(1,10):
        for j in range(1,i+1):
            file1.write(f'{j}*{i}={i*j}\t')
        file1.write('\n')
```

### 220321_python复习1

```python
# 220321_python复习1
str1 = 'myjerbgethyjumg'
# print(str1[-7:-4])  #thy
# print(str1[-5:-8:-1])  #yht
# print(str1[::-1])  #翻转

list1 = [36, 89, 72, 100, 99, 21, 64]
# list1.sort()  #排序,sort()方法没有返回值
# print(list1)

# list1.sort(reverse=True)  #reverse=True翻转列表
# print(list1)

# list1_new = sorted(list1)  #sorted()函数,返回排序后的列表
# print(list1_new)

# list1_new = sorted(list1,reverse=True)  #reverse=True翻转列表
# print(list1_new)

# 写一个函数,判断字符串是否是回文
# str2 = '上海自来水来自海上'
#
#
# def fun1(str):
#     if str == str[::-1]:
#         return True
#     else:
#         return False
#
#
# print(fun1(str2))

# 列表的增删改
# append,insert,extend
list2 = [100, 200, 300]
# list2.insert(-1,600)
# print(list2)

# pop,remove,del
# print(list2.pop())

# 写一个函数,可以打印斐波那契数列的前n位
# 1,1,2,3,5,8,13,21,34,55,89......
# def fun2(n):
#     list1= []
#     for i in range(n):
#         if i<2:
#             list1.append(1)
#         else:
#             list1.append(list1[-2]+list1[-1])
#     return list1
# print(fun2(20))

# 元组,是不可变对象,可以使用下标和切片
# 元组中只有一个元素时,加一个逗号
# 元组中有子列表时,子列表中的值可以修改

# 浅拷贝与深拷贝
# 有两个接口,一个接口A,里面的值是[100,200,300,[400,500]],一个接口B,要求接口B的值与接口A一致
# 领导要求修改接口A的第0位的值,改成900,但是不可以修改接口B的值
# 领导要求修改接口A的子列表的第0位的值,改成750,不可以修改接口B的值

listA = [100, 200, 300, [400, 500]]
# listB = listA  #赋值,相当于起了一个别名,没有生成新的对象
# listA[0] = 900
# print(listA)
# print(listB)

import copy


# listB = copy.copy(listA)  #浅拷贝,生成了新的对象,子列表仍然是同一个对象
# listA[0] = 900
# listA[3][0] = 750
# print(listA)
# print(listB)

# listB = copy.deepcopy(listA)  #深拷贝,列表与子列表都是新的对象
# listA[0] = 900
# listA[3][0] = 750
# print(listA)
# print(listB)

# 函数
# 写一个函数,用户输入一个参数n,返回从1加到n的和
# def sumdata(n):
#     sum = 0
#     for i in range(1,n+1):
#         sum += i
#     return sum
# print(sumdata(100))

# 写一个函数,不使用循环,求某数的阶乘
# 5! = 5*4*3*2*1 = 5 * 4!
# 4! = 4*3*2*1 = 4 * 3!
# 3! = 3*2*1 = 3 * 2!
# 2! = 2*1 = 2 * 1!
# 1! = 1
# n! = n*(n-1)!
def fun9(n):
    if n == 1:
        return 1
    else:
        return n * fun9(n - 1)


print(fun9(6))


def fun1():
    print('Hello')
    return True

# 程序执行时,只要它能判断布尔表达式结果为假,那么后面的式子就不再执行,直接返回False
# print(1>2 and 5>4 and 6>5 and 10>9 and 100>98 and 99>96 and fun1())

# 程序执行时,只要它能判断布尔表达式结果为真,那么后面的式子就不再执行,直接返回True
# print(2>1 or 1>5 or 3>9 or 10>100 or fun1())


# 循环
# for i in range(1,11):
#     if i==5:
#         # break  #终止循环
#         # continue  #跳出当次循环
#         pass  #占位符,防止语法错误
#     print(i)
# else:  #当循环中没有出现break,则循环结束时,运行一次else中的语句
#     print('循环运行完毕')


#文件的读写
#w+  可以同时读写,如果文件不存在,则新建,写入时,清空之前的内容写入
#r+  可以同时读写,如果文件不存在,则报错,写入时,覆盖写入
#a+  可以同时读写,如果文件不存在,则新建文件,写入时是追加写入

#文件一开始为abcde,w+写入qq,文件内容保存的是qq
#文件一开始为abcde,r+写入qq,文件内容保存的是qqcde
#文件一开始为abcde,a+写入qq,文件内容保存的是abcdeqq
```



```python

```

