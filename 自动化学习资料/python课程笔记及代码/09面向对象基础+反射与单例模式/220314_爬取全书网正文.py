# 220314_爬取全书网正文
import os
import re  #正则表达式模块
import requests  #爬虫模块
url = 'http://www.quannovel.com/read/620/'  #需要进行爬虫的网址
req = requests.get(url)  #访问网页,获取网页内容,网页内容为req.text

book_name = re.findall('<h2>(.*?)<i class',req.text)[0]

title_list = re.findall('class="name ">(.*?)</a>',req.text)  #获取章节名

url_list = re.findall('href="(.*?).html',req.text)  #获取网页的数字

dict1 = {}
for i in range(len(title_list)):
    dict1[title_list[i]] = f'{url}{url_list[i]}.html'  #将目录和网址放到字典里  f'{url}{正文网页的数字}.html'

if not os.path.exists(f'D:/{book_name}'):  #如果目录不存在,则新建
    os.mkdir(f'D:/{book_name}')

# count = 1
# for k,v in dict1.items():
#     if count > 5:
#         break
#     else:
#         req = requests.get(v)  #访问正文网页
#         text = re.findall('class="page-content ">(.*?)<div class',req.text,re.S)[0]  #获取文章内容
#         text = text.replace('<p>','').replace('</p>','')
#         with open(f'D:/{book_name}/{k}.txt','w+') as file1:
#             file1.write(text)
#     print(f'第{count}章爬取完毕')
#     count += 1

count = 1
with open(f'D:/{book_name}/{book_name}.txt', 'w+') as file1:
    for k,v in dict1.items():
        if count > 5:
            break
        else:
            req = requests.get(v)  #访问正文网页
            text = re.findall('class="page-content ">(.*?)<div class',req.text,re.S)[0]  #获取文章内容
            text = text.replace('<p>','').replace('</p>','')
            file1.write(k)
            file1.write(text)
            file1.write('------------------------------------------------------\n')
        print(f'第{count}章爬取完毕')
        count += 1