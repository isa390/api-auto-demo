# 220228_字符串
# url='https://www.baidu.com'  #单引号的字符串
# url2="https://www.baidu.com"  #双引号的字符串
# print(url,url2)

# 如果字符串中有单引号,外面用单引号报错
# invalid syntax  语法错误
# str1='It's ok'
# str1 = "It's ok"
# print(str1)

# 如果字符串中有双引号,外面用双引号报错
# str2 = "He said:"Are you ok?""
# str2 ='He said:"Are you ok?"'
# print(str2)

# 三引号可以用三个单引号表示,也可以用三个双引号表示
# str6='''天门中断楚江开
# 碧水东流至此回'''
# print(str6)

# 单引号或双引号也可以使用\n进行换行
# str6_2='两岸青山相对出\n孤帆一片日边来'
# print(str6_2)

# str9='abcde\b'  #backspace
# print(str9)

# 转义符的处理
# filepath = 'D:\note1.txt'
# print(filepath)

# 方案一,\前面再加一个\
# filepath = 'D:\\note1.txt'
# print(filepath)

# 方案二,字符串的外面加一个r,表示字符串中的所有转义符均不生效
# filepath = r'D:\note1.txt'
# print(filepath)

# 方案三,表示路径时,可以用/代替\
# filepath = 'D:/note1.txt'
# print(filepath)

# 字符串的拼接
# 字符串为str型,整数为int型,小数为float型
# print('1'+'2')  #12
# print('1'+2)  #str与int不能相加或拼接
# print('1'*2)  #将字符串打印两次

# a = '9'
# b = 6
# print(int(a)+b)  #相加
# print(a+str(b))  #拼接
# print(type(a))  #type(),返回对象的类型

# 字符串的下标
# 下标从0开始,长度为n的字符串,最大下标是n-1
# 也可以使用负下标,最后一位的下标可以表示为-1
# str10 = 'abcdefg'
# print(str10[0])
# print(str10[-7])

#字符串是不可变对象,不可以修改某一位的值
# str10[1]='q'

#字符串的切片
#[起始值:终止值]  包含起始值,不包含终止值
# print(str10[0:2])  #切片生成是新的对象,不影响原对象
# print(str10[3:6])  #def
# print(str10[-4:-1])  #def

#不写,则表示全取
# print(str10[5:])  #fg
# print(str10[-2:])  #fg
#
# print(str10[:3])  #abc
# print(str10[:-4])  #abc

#字符串的步长
str10 = 'abcdefg'
# print(str10[::2])  #aceg
print(str10[2::-1])  #起始值与终止值的正负不影响方向,步长的正负影响方向,步长为负数时,从右向左取值

# 字符串的概念
# 单引号,双引号,三引号表示字符串
# 字符串的拼接
# 转义符的作用,取消转义的三种方案
# 字符串的下标,正下标,负下标
# 字符串的切片,切片的起始值,终止值,步长