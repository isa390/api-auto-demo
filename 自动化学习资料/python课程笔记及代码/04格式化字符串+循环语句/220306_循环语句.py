# 220306_循环语句
# i = 1
# while i <= 10:  #while后面的布尔表达式如果为真,则执行循环中的语句
#     print(i)
#     i += 1  #i自增长1

#for循环打印从1到10的数字
# for i in range(1,11):  #range(起始值,终止值,步长)  包含起始值,不包含终止值,步长默认为1
#     print(i)

#如果有明确的循环次数,建议用for循环,如果循环次数步确定,建议用while循环,两者可以互相替换


#打印100以内的奇数
# for i in range(1,101,2):
#     print(i)

# for i in range(1,101):
#     if i%2==1:
#         print(i)

#打印从10到1的值
# for i in range(10,0,-1):  #步长为负数时,起始值要大于终止值
#     print(i)

#for循环也可以省略起始值,默认为0
# for i in range(10):
#     print(i)

#遍历列表
# list1 = ['谢晋','徐渭','杨慎']
#使用下标的方式进行遍历
# for i in range(len(list1)):
#     print(list1[i])

#直接遍历
# for one in list1:
#     print(one)

#break与continue
# for i in range(1,11):
#     if i==5:
#         # break  #终止循环
#         # continue  #跳出当次循环
#         pass  #占位符,防止语法错误
#     print(i)
# else:  #当循环中没有出现break,则循环结束时,运行一次else中的语句
#     print('循环运行完毕')

#写一个倒计时程序
# import time  #加载time模块
# for i in range(10,0,-1):
#     print(f'\r倒计时{i}秒',end='')  #\r光标回到行首
#     time.sleep(1)  #程序等待1秒
# else:
#     print('\r倒计时结束')

#sep=' '  间隔符,print()的多个参数之间,以间隔符隔开
# print(1,2,3,4,5,sep='')

# end='\n'  结束符,print()结束时加的字符
# print(1,end='')
# print(2)

#课堂小结
#循环的概念
#while循环
#for循环
#遍历列表
#break,continue
#循环的else