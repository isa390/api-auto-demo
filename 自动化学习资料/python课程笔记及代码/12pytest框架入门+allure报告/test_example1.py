# test_example1
# import pytest
# @pytest.fixture(scope='function',autouse=True)  #装饰器,声明下面的函数是setup函数  setup在所有方法之前执行,teardown在所有方法之后执行
# # scope缺省值为function,还有class,module,session级别
# # session级别,fixture的内容写到conftest.py中,目录下的所有文件共用这个配置
# # autouse=True  自动使用setup函数
# def fun1():
#     print('测试开始')
#     yield  #声明下面的代码为teardown的用法
#     print('测试结束')
#
# class Test1:
#     def test_some_data(self):
#         assert 1 == 2
#     def test_some_data2(self):
#         assert 2 == 3
# if __name__ == '__main__':
#     pytest.main([__file__,'-sv'])  #-s允许执行print,-v更加详细的报告

with open('./九九乘法表','w+') as file1:
    for i in range(1,10):
        for j in range(1,i+1):
            file1.write(f'{j}*{i}={i*j}\t')
        file1.write('\n')