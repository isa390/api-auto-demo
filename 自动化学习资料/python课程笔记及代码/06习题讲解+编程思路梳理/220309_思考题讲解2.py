# 220309_思考题讲解2
# 第五次课思考题1
# ('2017-03-13 11:50:09', 271, 131)
# (①签到时间,②课程id,③学生id)
# {
#     ③: [
#         {'lessonid': ②, 'checkintime': ①},
#         {'lessonid': ②, 'checkintime': ①},
#     ],
#
#     ③: [
#         {'lessonid': ②, 'checkintime': ①}
#     ]
#
# }

# 解题思路
# 1.分析需求,预期实现什么效果
# 2.提取出原始字符串①②③
# 3.按照题目要求,将①②③放入对应的位置


def putInfoToDict(fileName):
    dict1 = {}  # 外层字典
    dict2 = {}  # 内层字典
    with open(fileName) as file1:
        list1 = file1.read().splitlines()  #读取文件内容,返回值是列表,每一行是一个元素
        for one in list1:
            one1 = one.replace('(','').replace(')','').replace("'",'').strip(',').strip('\t')
            checkin_time,lesson_id,student_id = one1.split(',')  #取得签到时间,课程id,学生id
            lesson_id = lesson_id.strip()
            student_id = student_id.strip()
            dict2 = {'lessonid':lesson_id,'checkintime':checkin_time}  #将签到信息放入内层字典
            if student_id not in dict1:
                dict1[student_id] = []  #如果学生id在外层字典中未出现过,则新建一个学生id与空列表的键值对
            dict1[student_id].append(dict2)  #将dict2的签到信息添加到列表中
    return dict1


import pprint
pprint.pprint(putInfoToDict('D:/0005_1.txt'))

# 第五次课思考题2
# 解题思路
# 1.分析需要提取的数据
# 2.按照题目要求的格式写入文件

# name: Jack   ;    salary:  12000
#  name :Mike ; salary:  12300
# name: Luk ;   salary:  10030
#   name :Tim ;  salary:   9000
# name: John ;    salary:  12000
# name: Lisa ;    salary:   11000

with open('D:/第5次课练习题2数据.txt') as file1, open('./file2_220309.txt', 'w+') as file2:
    file_line = file1.read().splitlines()
    for one in file_line:
        one1, one2 = one.split(';')
        name = one1.split(':')[1].strip()  # 取得name的值
        salary = int(one2.split(':')[1].strip())  # 取得salary的值
        file2.write(
            f'name: {name:<7};    salary:  {salary:>5} ;  tax:{int(salary * 0.1):>5} ; income:{int(salary * 0.9):>7}\n')
