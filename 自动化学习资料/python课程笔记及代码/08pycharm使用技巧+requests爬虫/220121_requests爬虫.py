# 220121_requests爬虫
import re
import requests
url = 'http://www.quannovel.com/read/620/'  # 需要进行爬虫的网址
req = requests.post(url)  # 访问网页,获取网页内容
title_list = re.findall('class="name ">(.*?)</a>', req.text)  # 获取章节名
# for one in title_list:
#     print(one)

url_list = re.findall('<a href="(.*?).html', req.text)  # 获取正文网址
# for one in url_list:
#     print(f'{url}{one}.html')

dict1 = {}
for i in range(len(title_list)):
    dict1[title_list[i]] = f'{url}{url_list[i]}.html'  #将目录和网址放到字典里
# for k,v in dict1.items():
#     print(k,v)