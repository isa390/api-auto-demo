# 220311_模块与包
# import copy  #标准库,导入后使用
# import selenium  #第三方库,安装后,导入使用

#一个.py文件就是一个模块
#包是一种特殊的文件夹,如果目录里有__init__.py,则这个文件夹就是包
#当导入包时,__init__.py文件里的代码会执行一次
#包或文件夹都可以导入
# import AUTO59_1  #导入一个包或文件夹

#导入模块的几种形式
# import qqq  #在当前目录内,可以这样导入,在标准路径里,也可以这样导入
# print(qqq.fun1(6,9))

# import arrow
# now = arrow.now()  #打印当前时间
# print(now.format('YYYY-MM-DD hh:mm:ss'))

# from AUTO59_1 import qqq  #from 包 import 模块
# from pathlib import Path
# print(Path(__file__))  #打印当前文件的目录与文件名
# print(Path(__file__).parent)  #打印当前文件的目录

# from AUTO59_1.qqq import fun1  #导入某个模块里的某个函数
# print(fun1(6,9))

#标准路径
import sys
# for one in sys.path:  #遍历标准路径
#     print(one)
#sys.path的第一个路径是当前目录,第二个路径是工程目录,其他的是标准路径
#python的第三方库放在python所在目录的\lib\site-packages
# sys.path.append('D:/PKG200')  #将目录添加到标准路径
# import QQQ

# sys.path.append('D:/')  #将目录添加到标准路径
# from PKG200 import QQQ  #导入时,要注意目录的层级,如果要按这个模式导入,添加标准路径时,应该添加D:\一层

# if __name__ == '__main__':  #以下代码只在本模块内执行
#     pass

# from AUTO59_1 import qqq

#安装第三方库
#在cmd中执行pip install 第三方库名
#苹果系统在终端执行pip3 install 第三方库名
#如果下载比较慢,可以使用国内的镜像站进行安装
#豆瓣源
#pip install pytest -i http://pypi.douban.com/simple/ --trusted-host pypi.douban.com
#清华源
#pip install selenium -i https://pypi.tuna.tsinghua.edu.cn/simple/  --trusted-host pypi.tuna.tsinghua.edu.cn

#查看已安装的第三方库
#cmd中执行pip list
#卸载第三方库
#cmd中执行pip uninstall 第三方库名

#安装指定的版本
#pip install selenium == 3.141