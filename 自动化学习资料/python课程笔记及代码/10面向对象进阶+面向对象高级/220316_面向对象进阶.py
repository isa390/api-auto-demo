# 220316_面向对象进阶

class Rectangle:
    def __init__(self, length, width):  # 初始化方法
        self.length = length  # 将用户传的length转为实例自身的属性
        self.width = width  # 将用户传的的width转为实例自身的属性

    def perimeter(self):  # 周长的方法
        return (self.length + self.width) * 2

    def area(self):  # 面积的方法
        return self.length * self.width

    @classmethod  # 装饰器,声明下面的方法是类方法
    def features(cls):
        print('两边的长相等,两边的宽也相等,长和宽的角度是90°')

    @staticmethod  # 装饰器,声明下面的方法是静态方法
    def sumdata(a, b):
        return a + b


# Rectangle.features()
rec = Rectangle(6, 3)
# print(rec.perimeter())
# print(rec.area())
# Rectangle.perimeter()  #类不能调用实例方法
# rec.features()  #实例可以调用类方法

# print(Rectangle.sumdata(1,2))  #静态方法可以由类调用,也可以由实例调用
# print(rec.sumdata(3,6))

# 使用type()查看对象是方法还是函数
# print(type(rec.perimeter))  #实例方法是method
# print(type(rec.features))  #类方法是method
# print(type(rec.sumdata))  #静态方法

# inpect模块,判断对象是否是某个类型,返回值是布尔型
import inspect


# 类方法与实例方法都是method,静态方法是function
# print(inspect.ismethod(rec.perimeter))
# print(inspect.ismethod(rec.features))
# print(inspect.ismethod(rec.sumdata))

# print(inspect.isfunction(rec.perimeter))
# print(inspect.isfunction(rec.features))
# print(inspect.isfunction(rec.sumdata))

# 写一个正方形的类
# 完全继承
# class Square(Rectangle):
#     pass
#
#
# squ = Square(6, 6)
# print(squ.perimeter())
# print(squ.area())

# 部分继承,改写父类的某些方法
# class Square(Rectangle):
#     def __init__(self,side):
#         self.length = side
#         self.width = side
#
# squ = Square(6)
# print(squ.perimeter())
# print(squ.area())
# squ.features()

# 改写父类的方法,保留父类方法的同时,增加一些代码
# class Square(Rectangle):
#     @classmethod
#     def features(cls):
#         super().features()  # 继承父类features方法的代码
#         print('长和宽也相等')
#
#
# Square.features()


# 所有的类,都是object的子类
class Class1:
    '''
    云想衣裳花想容
    春风拂槛露华浓
    '''


# cls1 = Class1()
# print(cls1.__dict__)  #显示实例的属性
# print(cls1.__doc__)  #显示类的注释
# print(Class1.__name__)  #显示类的名称
# print(Class1.__base__)  #显示父类的名称
# print(Class1.__bases__)  #显示所有父类的名称

#多继承,一个类可以有多个父类
class Money1:
    def money(self):
        print('一个亿')

class Money2:
    def money(self):
        print('两个亿')

class Human(Money1,Money2):  #调用父类中的同名方法时,按继承顺序进行调用
    pass
man= Human()
man.money()