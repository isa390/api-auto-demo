> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a2d6090437c2a)

> 数据环境：

数据环境：

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220113/1642040740536081816.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

数据驱动

contract_data.yaml

```
test_tc003001:
  name: ['收款合同','付款合同','抵押合同']
  amount: [550,1000,3000]

```

获取数据改为数据驱动想要的格式

```
"""
问题：项目自动化测试脚本迭代出现变革技术方案
要求：测试用例从excel---变革为 yaml用例
注意事项：
    1- 尽可能少改代码！--原则
    2- 新技术方案yaml读取，尽可能写成一样的数据返回！
        [(请求体1，响应数据1),(请求体2，响应数据2)]
    3- 给pytest框架使用！
"""

def get_yamlCase_data(fileDir,case_name):
    #数据驱动需要格式 [(x,y),(x1,y1)]
    content = get_yaml_data(fileDir)[case_name]
    test_data = list(content.values())
    #转换数据--二位列表转换1维列表  元素[(x,y),(x1,y1)]格式
    res = list(zip(*test_data))
    return res

```

进行接口自动化的数据驱动

```
@pytest.mark.parametrize('name,amount',get_yamlCase_data(os.path.join(data_path,'contract_data.yaml'),'test_tc003001'))
def test_tc003001(empty_contracts,init_contract_type,init_account,init_org,name,amount):
    with allure.step('1-拿到部门实例'):
        api_contract = empty_contracts
    with allure.step('2-创建合同'):
        contract = api_contract.add(othercompany=init_account[1]['_id'],
                                    contract_type=init_contract_type[1]['_id'],
                                    company_id=init_org[1]['_id'],
                                    name=name,
                                    amount=amount)
    with allure.step('3列出所有的合同'):
        contract_list = api_contract.query()
    with allure.step('4断言'):
        ApiAssert.define_api_assert(contract,'in',contract_list)

```