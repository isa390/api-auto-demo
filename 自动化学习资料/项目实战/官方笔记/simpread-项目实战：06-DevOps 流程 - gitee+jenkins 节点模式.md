> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a3d0b99fc23e0)

> 方法论 devOps 是一种方法论，是一系列可以帮助开发者和运维人员在实现各自目标（Goal）的前提下，向自己的客户或用户交付最大化价值及最高质量成果的基本原则和实践，能让开发 测试 运维效率协同工作的方法。

DevOps 简介
---------

方法论 devOps 是一种方法论，是一系列可以帮助开发者和运维人员在实现各自目标（Goal）的前提下，向自己的客户或用户交付最大化价值及最高质量成果的基本原则和实践，能让开发 测试 运维效率协同工作的方法。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132015203075476.png)

DevOps 流程操作

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132034544037158.png)

GIT 安装（已安装的忽略）
--------------

在 Windows 平台上安装 Git 同样轻松，有个叫做 msysGit 的项目提供了安装包，可以到 GitHub 的页面上下载 exe 安装文件并运行：

安装包下载地址：[https://gitforwindows.org/](https://gitforwindows.org/)

官网慢，可以用国内的镜像：[https://npm.taobao.org/mirrors/git-for-windows/](https://npm.taobao.org/mirrors/git-for-windows/)。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132050188042511.png)  
  

完成安装之后，就可以使用命令行的 git 工具（已经自带了 ssh 客户端）了，另外还有一个图形界面的 Git 项目管理工具。

在开始菜单里找到 "Git"->"Git Bash"，会弹出 Git 命令窗口，你可以在该窗口进行 Git 操作。

Git 配置
------

Git 提供了一个叫做 git config 的工具，专门用来配置或读取相应的工作环境变量。

这些环境变量，决定了 Git 在各个环节的具体工作方式和行为。

配置个人的用户名称和电子邮件地址：

```
$ git config --global user.name "haiwen"
$ git config --global user.email haiwen@test.com

```

### 查看配置信息

要检查已有的配置信息，可以使用 git config --list 命令：

```
$ git config --list

```

### 生成 SSH Key(密钥)

```
$ ssh-keygen -t rsa -C "你的邮箱"

```

此处会提示`Enter file in which to save the key (/Users/shutong/.ssh/id_rsa):`这样一段内容, 让我们输入文件名，这里不用管，直接按`enter`键就好了。

 之后会有提示你是否需要设置密码，如果设置了每次使用 Git 都会用到密码，一般都是直接不写为空，直接`enter`就好了。

上述操作执行完毕后，在`~/.ssh/`目录会生成`id-rsa`(私钥) 和`id-rsa.pub`(公钥)

Git 关联远程仓库
----------

gitee 操作

1. 注册 gitee 账号并登录

[https://gitee.com/](https://gitee.com/)     账号自行注册

2. 创建仓库

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132074864067448.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132088543088245.png)

3. 查看公钥

```
$cat ~/.ssh/id_rsa.pub

```

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132117372016265.png)

4.gitee 上传自己的 SSH 公钥

选择右上角用户头像 -> 设置，然后选择 "SSH 公钥"，填写一个便于识别的标题，然后把用户主目录下的 .ssh/id_rsa.pub 文件的内容粘贴进去：

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132130839031563.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132153968014981.png)

5. 本地 git 关联远程仓库

一. 命令行方式

①在项目代码里, 创建本地仓库

```
$git init

```

②将项目代码, git 添加暂存区以及提交操作

```
git add .
git commit -m 'first commit'

```

③添加远程仓库地址

找到远程仓库地址

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132181495025032.png)

```
git remote add origin git@gitee.com:zegetest/songqin.git#关联远程仓库
git remote -v #远程仓库地址
git push -u origin master #将本地仓库推送到远程仓库

```

二. idea 方式

idea 打开项目文件

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132191635047148.png)

创建一个仓库

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132210878079448.png)

选择项目目录

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642132234533098507.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138459517039210.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138437634041199.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138477230021878.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138493780077222.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138538612032014.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138553578001531.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138568783016661.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138580990060460.png)

远程仓库可以看到上传的项目代码了

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138627309014781.png)

Jenkins 节点配置
------------

**Jenkins**

[http://120.27.146.185:7073/](http://120.27.146.185:7073/)

所有人共用：tester48/devops

1. 配置 jenkins 节点

①进入 jenkins 节点列表，新建节点，选择固定节点填写节点名称

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138728811008324.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138746504075284.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138767318035003.png)

②电脑任意路径新建个文件夹，自己定义远程工作目录

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138828526020548.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

③找到 jenkins 配置的节点，点击进入

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138853869052485.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138875498034240.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642138894239050024.png)

CI/CD jenkins 配置
----------------

1. 新建任务—> 选择 maven 项目

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139066140064434.png)

选择远程的节点构建

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139113343045289.png)

添加 git 仓库信息   选择添加

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139157384052717.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139142216075311.png)

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

构建触发器选择 webhook Trigger

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139187399051122.png)

返回 gitee 仓库取配置 webhook  

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139208414062272.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

填写你 jenkins 地址 [http://120.27.146.185:7073/generic-webhook-trigger/invoke?token=test](http://120.27.146.185:7073/generic-webhook-trigger/invoke?token=test)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139237285054647.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

返回 jenkins，选择 build

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139301990069029.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139320158045083.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

exec command 命令

```
#!/bin/bash
kill -9 `ps -ef |grep 7777 |grep -v 'grep' |awk '{print $2}'`
cd /usr/local
java -jar sq-mall.jar --server.port=7777

```

### 测试用例操作

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139399932006657.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139415601071728.png)

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

本地编写测试代码, test_001.py

```
import pytest

def test_number():
    assert 1==1

if __name__ == '__main__':
    pytest.main(['-s','test_001.py'])

```

```
git init
git add .
git commit -m "first commit"
git remote add origin https://gitee.com/zegetest/apitest.git
git push -u origin master

```

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139440861051674.png)

创建测试 jenkins 工程

新建项目—> 选择自由风格的项目

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139461627094067.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139475869013728.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139495032023173.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139518696009745.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139538937011329.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

添加邮箱

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139560804057778.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

邮箱模板

```
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${ENV, var="JOB_NAME"}-第${BUILD_NUMBER}次自动化构建</title></head>
<body leftmargin="8" marginwidth="0" topmargin="8" marginheight="4" offset="0">
<table width="95%" cellpadding="0" cellspacing="0"
      >
    <tr>
        <td>以下为${PROJECT_NAME }项目构建信息</td>
    </tr>
    <tr>
        <td><br/> <b><font color="#0B610B">构建信息</font></b>
            <hr size="2" width="100%"/>
        </td>
    </tr>
    <tr>
        <td>
            <ul>
                <li>项目名称：${PROJECT_NAME}</li>
                <li>构建编号：第${BUILD_NUMBER}次构建</li>
                <li>触发原因：${CAUSE}</li>
                <li>构建状态：${BUILD_STATUS}</li>
                <li>构建日志：<a
                        href="${BUILD_URL}console">${BUILD_URL}console</a></li>
                <li>构建Url：<a href="${BUILD_URL}">${BUILD_URL}</a></li>
                <li>工作目录：<a href="${PROJECT_URL}ws">${PROJECT_URL}ws</a>
                </li>
                <li>项目Url：<a href="${PROJECT_URL}">${PROJECT_URL}</a></li>
                <li>allure测试报告：<a
                        href="${PROJECT_URL}${BUILD_NUMBER}/allure">${PROJECT_URL}${BUILD_NUMBER}/allure </a></a> </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td><b><font color="#0B610B">历史变更记录:</font></b>
            <hr size="2" width="100%"/>
        </td>
    </tr>
    <tr>
        <td> ${CHANGES_SINCE_LAST_SUCCESS,reverse=true, format="Changes for
            Build #%n:<br/>%c<br/>",showPaths=true,changesFormat="
            <pre>[%a]<br/>%m</pre>
            ",pathFormat="    %p"}
        </td>
    </tr>
</table>
</body>
</html>

```

往下触发选择 always

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139581425064886.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139597071062403.png)

点击保存

当你项目代码 git 提交时自动触发 maven 打包 并传送到远程服务器 运行项目

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139621719079078.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

构建完自动化的工程自动运行

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139640215094677.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220114/1642139649620051346.png)

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)