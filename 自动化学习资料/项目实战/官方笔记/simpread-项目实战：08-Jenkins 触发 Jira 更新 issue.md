> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a3d16774023ec)

> jira

**jira**

[http://120.27.146.185:8070/](http://120.27.146.185:8070/)

联系宁宁注册

新建一个 Jira 测试计划

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625637929262072461.png)

通过 JQL 语句选择该测试计划

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624522562102086963.png)

点击高级

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625637957725031731.png)

搜索结果

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625637987379056074.png)

我们希望 Jenkins 运行该计划对应的测试用例后，该测试计划状态会自动更新

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625638006388025388.png)

对应 Jenkinsjob

新建一个 Jenkins item 或者进入已有的项目点击配置，进入任务配置页面

选择 Jira issue updater

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625638036652005845.png)

配置相关信息

这里我们希望触发任务时，更新 issue 为处理中，所以我们要点击开始任务

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625638061852024817.png)

自动化脚本执行完成之后（构建后），我们希望更新测试计划为完成

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625638092964016288.png)

填写相关信息，因为我们是在开始任务的基础上完成工作流，所以工作流这里写完成任务，其他的不用变

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625638119878089519.png)

保存任务，执行任务，测试一下。

issue 状态自动更新

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625638146824066328.png)

自动添加留言

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625638172589075203.png)