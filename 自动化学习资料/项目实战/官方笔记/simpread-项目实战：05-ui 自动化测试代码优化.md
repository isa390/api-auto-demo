> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a3d02b13d23cb)

> UI 自动化代码设计

**UI** **自动化代码设计**

**代码设计方案**

*   功能函数模式
    
       简单快捷
    
*   页面对象 (PO) 模式
    
*   可维护性高
    

**PO** **设计模式特点**

*   1. 页面即对象
    
    技术方案： 面对对象—继承封装
    
*   2. 元素定位不写死在代码中
    
    技术方案：配置文件存放元素定位方法，读取—比如: 读取 yml
    
*   3. 配置文件保存具体的元素定位，页面类可以自动读取
    
    技术方案： 自动识别当前类名 self.**class**.**name** 动态赋值 setarrt(对象，name,value) 给对象添加 name 属性 value 值
    
*   4.driver 对象需要保证唯一
    
    技术方案：单例模式 new 方法的改写 python 反射 hasattr(对象, name) 判断对象是否具备 name 属性
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220113/1642058322182062959.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

PO 模式 - 技术点
-----------

1.python 反射—设置属性 setarrt(对象，name,value) 给对象添加 name 属性 value 值效果等同于 对象. name=value

2.python 反射—读取属性 hasattr(对象, xxx)   判断对象是否具备 xxx 属性

func=getattr(对象, funcname) –获取对象数据 func()

3. 单例模式  new 方法的改写