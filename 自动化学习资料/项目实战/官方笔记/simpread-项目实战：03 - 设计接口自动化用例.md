> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a3cd7b4552361)

> 面向功能点

面向数据环境
------

面向功能点

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220113/1642040563458057488.png)

面向数据环境

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220113/1642040586623044321.png)

Pytest 相对路径问题解决
---------------

问题现象： 项目根目录下执行 pytest 命令报错，找不到自定义模块

```
D:\Course\course_combat_47期>pytest testcase
========================================== test session starts
===========================================
platform win32 -- Python 3.6.8, pytest-6.2.3, py-1.10.0, pluggy-0.13.0 rootdir: D:\Course\course_combat_47期
plugins: allure-pytest-2.8.6, forked-1.3.0, html-2.0.0, logger-0.5.1, metadata- 1.8.0, xdist-1.34.0
collected 0 items / 1 error



================================================= ERRORS
=================================================
   ERROR collecting test session

c:\users\shone\appdata\local\programs\python\python36\lib\importlib\ init .py: 126: in import_module
return _bootstrap._gcd_import(name[level:], package, level)
<frozen importlib._bootstrap>:994: in _gcd_import
???
<frozen importlib._bootstrap>:971: in _find_and_load
???
<frozen importlib._bootstrap>:955: in _find_and_load_unlocked
???
<frozen importlib._bootstrap>:665: in _load_unlocked
???
c:\users\shone\appdata\local\programs\python\python36\lib\site- packages\_pytest\assertion\rewrite.py:170: i
n exec_module
exec(co, module. dict )
testcase\api\D-管理员登录\conftest.py:9: in <module> from pylib.api.common import login
E ModuleNotFoundError: No module named 'pylib'
======================================== short test summary info
=========================================
ERROR - ModuleNotFoundError: No module named 'pylib'
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Interrupted: 1 error during collection
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
============================================ 1 error in 0.14s
============================================

```

问题原因：python 寻找包和模块首先根据文件自身为起点指定相对路径，其次根据设定的 pythonpath   为起点指定相对路径。

而现在的项目由于层级结构复杂，并不统一，因此根据文件自身为起点指定的相对路径不可用

解决方法：设定 pythonpath, 统一相对路径起点

1. 通过 python –m pytest testcase 运行测试用例, python 会把当前目录添加到 pythonpath

2. 通过在项目根目录创建一个空的 conftest.py 文件, 用 pytest 可以直接指定当前目录到 pythonpath