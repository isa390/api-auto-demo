#-*- coding: utf-8 -*-
#@File    : login.py
#@Time    : 2022/3/25 21:48
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/3/25

import requests
from handle_data import get_md5_data,RSAendecrypt

"""
参数：
    username  账号
    password: RSA加密的结果
        1- xintian通过md5加密成密文--a
        2- 使用RSA的公钥加密(a)
    sign 签名
        md5(username+password密文)
"""

HOST = 'http://121.41.14.39:8082'#环境变迁
def login(data):#函数定义的参数---形参
    #url
    url = f'{HOST}/account/loginRsa'
    #------------------复杂的加密操作---------
    #1-md5加密明文
    pwd_md5_text = get_md5_data(data['password'])
    #2-rsa加密MD5之后的密文
    rsa = RSAendecrypt()#实例化操作
    pwd_rsa_text = rsa.encrypt(pwd_md5_text)
    #3- 更新到data
    data['password'] = pwd_rsa_text
    #4- 增加sign签名
    data.update({'sign':get_md5_data('th0198'+pwd_rsa_text)})
    print('请求数据--->',data)
    #-------------------------------------
    #请求头不一定需要
    #发请求
    resp = requests.post(url,data=data)
    #打印响应体--字符串数据  html xml
    print(resp.text)#





if __name__ == '__main__':#一般适合本模块的调试代码写里面
    test_data = {
        'username':'th0198','password':'xintian'}
    login(test_data)
    #TODO:2022.03.27---md5加密操作---xintian


