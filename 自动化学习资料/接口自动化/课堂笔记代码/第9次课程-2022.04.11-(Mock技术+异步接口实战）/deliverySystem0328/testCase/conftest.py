#-*- coding: utf-8 -*-
#@File    : conftest.py
#@Time    : 2022/4/8 20:25
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/4/8 
#---验证功能---
import pytest
from libs.login import Login
from configs.config import NAME_PWD
from libs.shop import Shop
"""
scope='session'---整个自动化运行只做一次
    1- 环境检查(运行环境，项目环境)
    2- 登录
"""
@pytest.fixture(scope='session',autouse=True)
def start_running():
    print('---开始自动化测试运行---')

#start_running()#不要直接在conftest文件去调试fixture函数！！！

#---------1、登录操作-----
@pytest.fixture(scope='session')
def login_init():
    print('----1.开始执行的登录操作----')
    token = Login().login(NAME_PWD,get_token=True)
    #return token#后续 的操作不能执行
    yield token#后续代码是可以运行
    print('---我登录完成---')


#---------2、店铺初始化操作-----
#有返回值fixture使用：如果一个fixture函数需要使用另一个fixture返回值，直接使用他的函数名
#没有返回值：@pytest.mark.usefixtures('函数名')

@pytest.fixture(scope='session')
def shop_init(login_init):
    print('----2.创建店铺实例操作----')
    shop_object = Shop(login_init)
    yield shop_object#返回出去给---测试方法使用

#-----店铺更新初始化操作
@pytest.fixture(scope='class')
def shop_update_init(shop_init):

    shop_object = shop_init
    shop_id = shop_init.query({'page': 1, 'limit': 20})['data']['records'][0]['id']
    image_info = shop_init.file_upload('../data/456.png')['data']['realFileName']
    shop_update = {'shop_object':shop_object,'shop_id':shop_id,'image_info':image_info}
    #yield shop_object,shop_id,image_info#元组
    yield shop_update#字典

"""
在使用 pytest.mark.parametrize 对用例进行参数化的时候，传入的值包含中文，运行用例，控制台显示编码问题。
解决方法：在用例的根目录下，新建 conftest.py文件，将下面的代码复制进去
"""
def pytest_collection_modifyitems(items):
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode-escape")
        item._nodeid = item._nodeid.encode("utf-8").decode("unicode-escape")













