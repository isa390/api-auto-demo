#-*- coding: utf-8 -*-
#@File    : baseAPI.py
#@Time    : 2022/3/28 21:31
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/3/28
#这个基类后续可能因为业务模块的增加可以维护
print()
"""
封装思路：
    1- 为所有的业务模块提供的基本接口操作：增删改查+发送接口
    2- 日志  截图都可以在基类里封装
    3- 断言方法
@log装饰器
def login():
    try:
        xxxxx
    except：
        log.error()

"""
#---------------封装的思路启发--------------
"""
发送公共请求方法：
    def request_send(self,method,url):
        - 实际调用发送方法必须传递2个参数，
        - 每一个接口的数据还不一样
        - 反馈：很麻烦
    优化：
        思路：代码与配置分离
        实施：method url 可以放到一个配置文件  apiPathConfig.yml
        难点：代码怎么可以识别到对应的模块、对应的接口、对应的参数
        场景分类：
            - 常规风格的接口格式
                - 方法举例： 增加数据接口： post  ；修改也是 post
                - url举例：增加接口 /sq    修改 /sq_xintian/{id}
                
            - restful接口风格
                - 方法规则：get  post  delete  put
                - url规范： 一样的
"""
import requests
from utils.handle_yaml import get_yaml_data
import inspect
from configs.config import HOST
from utils.handle_loguru import log
import traceback
class BaseAPI:
    def __init__(self,token=None):
        if token:#需要token业务
            self.header = {'Authorization': token}
        else:#登录业务
            self.header = None
        #获取对应模块的接口信息
        self.data = get_yaml_data('../configs/apiPathConfig.yml')[self.__class__.__name__]#根据类名去获取
        #print('类名是--->', self.__class__.__name__)
        #print('类接口数据--->', self.data)

    #---------发送的公共方法-每一个接口都会调用他----------
    def request_send(self,data=None,params=None,files=None,id=''):
        try:
            #api_data == {'path': '/account/sLogin', 'method': 'POST'}
            api_data = self.data[inspect.stack()[1][3]]
            resp = requests.request(
                method=api_data['method'],#方法
                url=f'{HOST}{api_data["path"]}{id}',#url
                data=data,
                params=params,
                files=files,
                headers=self.header)
    #---------log里面输出请求信息-info级别-------
            #日志信息：业务模块+具体接口+接口的信息
    #         log_msg = f'''模块名:{self.__class__.__name__},接口名:{inspect.stack()[1][3]},
    # 请求的url:{resp.request.url},
    # 请求方法:{resp.request.method},
    # 请求体:{resp.request.body},
    # 响应体:{resp.json()}'''
    #         log.info(log_msg)
            log.info(f'模块名:{self.__class__.__name__}')
            log.info(f'接口名:{inspect.stack()[1][3]}')
            log.info(f'请求的url:{resp.request.url}')
            log.info(f'请求方法:{resp.request.method}')
            log.info(f'请求体:{resp.request.body}')
            log.info(f'响应体:{resp.json()}')
            return resp.json()
        except Exception as error:#出现异常--打日志
            log.error(traceback.format_exc())
            raise error


    #------增删改查----------------------------
    def query(self,data):
        return self.request_send(params=data)
    #---增加数据接口---
    def add(self,data):
        return self.request_send(data=data)
    #---更新数据接口---
    def update(self,data):
        return self.request_send(data=data)
    #---删除数据接口---通过id
    def delete(self,id):
        return self.request_send(id=id)
    """
    第一种写法  shop/xt?id=100
    常见用法 shop/100
    """
    #------文件上传-----
    """
    Content-Disposition: form-data; name="file"; filename="QQ截图20200724100920.png"
Content-Type: image/png
    
    文件上传格式： 文件路径、文件名、文件类型
    路径: xx/123.png
    {‘file’:(文件名，文件对象本身，文件类型)}---转化
    {‘file’:('123.png'，open('xx/123.png','rb')，'png')}
    """
    def file_upload(self,file_path:str):
        #1-获取文件名
        file_name = file_path.split('/')[-1]
        #2-文件类型
        file_type = file_path.split('.')[-1]
        file = {'file':(file_name,open(file_path,'rb'),file_type)}
        #3-发送请求
        return self.request_send(files=file)







#-------------------演示案例-----------------------
# import inspect
# def send():
#     #inspect.stack()[1][3]  获取调用者的函数名
#     print(f'---{inspect.stack()[1][3]}调用send方法---')
#
# def login():
#     print('---函数login开始执行了-----')
#     send()
#
# login()