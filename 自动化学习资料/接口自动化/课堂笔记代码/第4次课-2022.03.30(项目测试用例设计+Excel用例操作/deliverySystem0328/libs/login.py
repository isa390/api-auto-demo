#-*- coding: utf-8 -*-
#@File    : login.py
#@Time    : 2022/3/28 21:33
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/3/28
from common.baseAPI import BaseAPI
from utils.handle_data import get_md5_data
class Login(BaseAPI):
    def login(self,data):
        data['password'] = get_md5_data(data['password'])
        res = self.request_send(data)#调用发送方法
        return res

"""
验证： resp.request,headers
data: data=请求数据(字典类型-{'a':1,'b'=2})
    1- 请求数据是表单 a=1&b=2
    2- 表单有json  a=1&b={"name":"xintian"}
json  json=请求数据(字典类型-{'a':1,'b'=2})
    - i请求体是json



"""


if __name__ == '__main__':
    test_data = {'username':'th0198','password':'xintian'}
    res = Login().login(test_data)
    print(res)