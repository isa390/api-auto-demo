#-*- coding: utf-8 -*-
#@File    : handle_yaml.py
#@Time    : 2022/3/30 20:09
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/3/30 
import yaml# pip install PyYaml
def get_yaml_data(file_path:str):
    with open(file_path,encoding='utf-8') as fo:#file_object
        return yaml.safe_load(fo.read())

if __name__ == '__main__':
    res = get_yaml_data('../configs/apiPathConfig.yml')
    print(res)