#-*- coding: utf-8 -*-
#@File    : handle_data.py
#@Time    : 2022/3/27 10:53
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/3/27 
#md5加密
import hashlib#不需要安装---内置库
def get_md5_data(pwd:str,salt=''):
    """
    :param pwd: 加密的字符串
    :param salt: 盐值
    :return: 返回加密后的结果
    """
    #1-创建md5实例
    md5 = hashlib.md5()
    #2-调用加密方法
    pwd = pwd+salt#拼接盐值
    md5.update(pwd.encode('utf-8'))
    return md5.hexdigest()#加密后的结果---16进制

if __name__ == '__main__':
    res = get_md5_data('xintian')
    print(res)