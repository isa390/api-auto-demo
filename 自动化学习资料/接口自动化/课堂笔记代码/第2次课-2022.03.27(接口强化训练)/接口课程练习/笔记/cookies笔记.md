cookies：
1、基本概念：
    1- 客户端请求我们服务器
    2- 服务器在响应头通过set-cookies里面有sessionid给客户端
        sessionID--存服务端
    3- 后续客户端使用这个cookies去访问这个项目！

项目场景：
    1- 访问任何页面都返回cookies
        - 这个cookies暂时是临时的，后续需要登录期认证，变成有效！
    2- 只要等登录成功才会有cookies,否则不会返回cookies
    
cookies关联场景：
    1- 如果项目后续接口直接使用这个cookies,不做任何其他操作，--原生态的cookies----不建议取出sessionid
    2- 如果项目后续接口使用这个cookies,但是同时需要第三方认证，那么这个cookies就需要增加属性了