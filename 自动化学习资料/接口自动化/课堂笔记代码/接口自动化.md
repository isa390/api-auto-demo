### 第一大课

#### 1、Pytest框架入门主讲内容：

Pytest框架简介：文件框架搭建，基础代码搭建

```
pytest.main(['test_func01.py','-s'])#pytest.main表示自动执行，表示pytest自动执行test_func01.py自动化项目文件,-s表示可执行funtion里面print语句，不带“-s”不会执行里面的打印语句
```

Pytest执行测试用例

数据驱动：接口、参数化，批量执行数据、用例

```
@pytest.mark.parametrize('a,b',[(1,2),(3,4),(5,6)])#('变量名',[1,2,3])  pytest装饰器定义变量
```

#### 2、Pytest框架结合Allure操作主讲内容：

allure环境搭建

Pytest结合allure生成报告：allure报告生成命令、可视化目录搭建、定制化报告

Allure报告解读

Tips:  "./"表示当前目录；‘../'表示返回上一级目录

```
pytest.main(['test_func01.py','-s','--alluredir','../report/tmp'])  --alluredir 生成临时文件 存放目录
```

```
os.system('/usr/local/bin/allure generate ../report/tmp -o ../report/report --clean')  os.system执行命令（相当于cmd），allure generate生成报告，-o表示输出，output
```

```
@allure.feature('登录模块')  allure报告一级标题
@allure.story('登录login02')  allure报告一级标题
@allure.title('logino2')   allure报告三级标题
```

### 第二大课

#### 3、接口自动化项目实战（接口测试概述）主讲内容：

开发最佳实践和方法论：DevOps、微服务/敏捷、自动化测试

自动化测试种类（分层）、什么叫接口测试

#### 4、接口自动化项目实战（接口测试数据格式）主讲内容：

外卖系统架构部署；怎样看接口协议（F12、抓包）

登录接口方法：Md5版本解密、data数据驱动

### 第三次大课

#### 5、接口自动化项目实战（接口强化训练上）主讲内容：

登录token串联、cookies串联技术实战

resp.request.url 请求的url.     resp.request.headers 请求头信息

resp.request.body 请求体信息    resp.text请求体信息

#### 6、接口自动化项目实战（接口强化训练下）主讲内容：

MD5加密技术、RSA加密（非对称加密）实现逻辑和技术实战

### 第四次大课：

#### 7、接口自动化项目实战（接口测试BaseApi基类封装上）主讲内容：

接上次RSA加密技术、

#### 8、接口自动化项目实战（接口测试BaseApi基类封装上）主讲内容：

需求分析（怎样创建项目工程）、文件分层路径

BaseApi基类封装：request请求接口，yaml接口变量名数据存储（config文件）、数据驱动

### 第五次大课：

#### 9、接口自动化项目实战（项目测试用例设计）主讲内容：

接上次BaseApi基类封装：yaml数据驱动打开、读取、增删改查

登录调用reques公共方法封装

#### 10、接口自动化项目实战（执行Excel测试用例）主讲内容：

Excel自动化执行：处理Excel方法

### 第六次大课：

#### 11、接口自动化项目实战（Pytest自动化框架）主讲内容：

自动化执行excel用例代码优化，人性化选择用例

#### 12、接口自动化项目实战（数据驱动）主讲内容：

test驱动数据、调用login登录接口、发送数据登录接口

数据优化、yaml需要读取的行列、断言是否符合预期

文件路径单独配置

allure报告配置

### 第七次大课：

#### 13、接口自动化项目实战（业务模块编码）主讲内容：

登录接口优化

编辑类增删改查接口代码：数据接口增删改查（逻辑不理解），接口拼接？

店铺代码封装：登录后获取的token联动传给后面的需要调用的店铺代码

#### 14、接口自动化项目实战（fixture结合项目使用）主讲内容：

代码优化、文件上传发送请求、店铺api封装

### 第八次大课

#### 15、接口自动化项目实战（环境初始化与数据清除）主讲内容：

test_shop.py测试用例代码实现

fixture源码：可选运行到类、还是funtion、还是整个文件

封装优化

#### 16、接口自动化项目实战（Allure生成测试报告）主讲内容：

每一个功能代码日志断言、链接Allure生成报告、店铺初始化操作

编码处理、日志优化（日志配置格式样式）

### 第九次大课

#### 17、接口自动化项目实战（用例定制话执行）主讲内容：

断言封装（新增common断言接口调用，断言优化

#### 18、接口自动化项目实战（Pytest结合Yaml使用）主讲内容：

断言和日志优化（日志信息加上接口名、请求体等信息）

### 第十次大课：

#### 19、接口自动化项目实战（mock原理）主讲内容：

mock服务环境搭建、本地配置环境访问、异步接口mock请求

#### 20、接口自动化项目实战（mock技术实战）主讲内容：

接上节课，异步接口mock请求

主线程（自动化测试运行），子线程创建（查询接口）、多线程运行

### 第十一次大课：

#### 21、接口自动化项目实战（编码处理和装饰器）主讲内容：

编码操作转码、解码

装饰器（代码优化，语法糖）、闭包（函数里面定义函数）

#### 21、接口自动化项目实战（项目代码优化）主讲内容：

接上节装饰器（代码优化，语法糖

代码优化，mark标签（指定跑哪个模块、选择条件执行、Pytest定制化执行）

yaml用法





jenkins搭建配置

用容器打开jenkins容器

docker run -id --name xtjenkins -p 6060:8080-v /var/jenkins_node3:/var/jenkins_home --restart=alwaysregistry.cn-hangzhou.aliyuncs.com/sqqdcl/jenkins:v1

云服务要开端口

安装推荐的插件、用松勤打包的V1版本

jenkins
