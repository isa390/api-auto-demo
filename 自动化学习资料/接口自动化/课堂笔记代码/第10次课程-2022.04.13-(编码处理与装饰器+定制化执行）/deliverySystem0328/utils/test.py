#-*- coding: utf-8 -*-
#@File    : test.py
#@Time    : 2022/4/13 20:25
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/4/13 
#---------------------------------自动化场景-----------------
"""
小明：自动化测试代码开发
def test():
    自动化测试
需求：是否可以得到每一个用例执行时间
"""
# def test():
#     print('---自动化开始执行---')
#     time.sleep(1)#模拟用例执行时间
#     print('---自动化结束执行---')

#------------------7000---小a---------
import time
# def test():
#     start_time = time.time()
#     print('---自动化开始执行---')
#     time.sleep(1)#模拟用例执行时间
#     print('---自动化结束执行---')
#     end_time = time.time()
#     print('自动化执行耗时--->',end_time-start_time)

"""
评审：
    1- 每一个模块接口都需要加---100个--100次  麻烦
    2- 修改原代码结构
"""

# #------------------10000---小b---------
# #新的需求分析：不修改原代码
# import time
# def test():
#     print('---自动化开始执行---')
#     time.sleep(1)#模拟用例执行时间
#     print('---自动化结束执行---')
#
# #------增加一个计时函数--里面去执行这个原代码
# def show_time(func):
#     start_time = time.time()
#     func()#函数调用
#     end_time = time.time()
#     print('自动化执行耗时--->',end_time-start_time)
#
#
# if __name__ == '__main__':
#     show_time(test)

#------------------12000---小c---------
#新的需求分析：不修改原代码+也不能修改执行方式
"""
问题：函数增加功能，但是不能改变函数结构
技术方案：python闭包--装饰器
研究阶段：
    1- 装饰器：再函数的功能上新增功能！----也是一个函数---函数高级用法
    2- 闭包：
        函数里定义函数，内部函数使用了外层函数的变量，外层函数返回值是内函数的函数的对象



"""
import time

#------增加一个计时函数--里面去执行这个原代码
def show_time(func):
    def inner(*args,**kwargs):#关键字参数  a=10
        start_time = time.time()
        res = func(*args,**kwargs)#函数调用== res=login(test_data,get_token=True)
        end_time = time.time()
        print('自动化执行耗时--->',end_time-start_time)
        return res
    return inner#返回内函数的对象

@show_time#装饰器的用法  ==     test=show_time(test)
def test():
    print('---自动化开始执行---')
    time.sleep(1)#模拟用例执行时间
    print('---自动化结束执行---')

@show_time
def test2():
    print('---自动化开始执行---')
    time.sleep(1)#模拟用例执行时间
    print('---自动化结束执行---')




if __name__ == '__main__':
    #test = show_time(test)#返回的是inner对象-----test=inner
    test()# == inner()

    #test2 = show_time(test2)#返回的是inner对象-----test=inner
    #test2()# == inner()

"""
评审：
    1- 优化了，没有去修改原代码
    2- 改变了函数执行方式
"""