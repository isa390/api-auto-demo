#-*- coding: utf-8 -*-
#@File    : handle_data.py
#@Time    : 2022/3/27 10:53
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/3/27 
#md5加密
import hashlib#不需要安装---内置库
def get_md5_data(pwd:str,salt=''):
    """
    :param pwd: 加密的字符串
    :param salt: 盐值
    :return: 返回加密后的结果
    """
    #1-创建md5实例
    md5 = hashlib.md5()
    #2-调用加密方法
    pwd = pwd+salt#拼接盐值
    md5.update(pwd.encode('utf-8'))
    return md5.hexdigest()#加密后的结果---16进制

"""
外卖项目--RSA加密接口
url = 'http://121.41.14.39:8082/account/loginRsa'
参数：
    username  账号
    password: RSA加密的结果
        1- xintian通过md5加密成密文--a
        2- 使用RSA的公钥加密(a)
    sign 签名
        md5(username+password密文)
加密库安装：
    # pip install  pycryptodome -i https://douban.com/simplie
加密流程：
    1- 首先需要一个公钥
"""
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as PKCS1_cipher #重命名
import base64
from typing import Dict
class RSAendecrypt:
    def __init__(self,file_path='./'):
        self.file_path = file_path
    #1-创建密钥对：公钥  私钥
    #2-加密操作
    def encrypt(self,crypt_data):
        #1-打开读取这个公钥文件
        with open(self.file_path+'public.pem') as fo:
            key = fo.read()#2-读取内容----字符串类型
            public_key = RSA.importKey(key)#RSA里面importkey---变成公钥
            cipher = PKCS1_cipher.new(public_key)#生成对象
            rsa_text = base64.b64encode(cipher.encrypt(bytes(crypt_data.encode('utf-8'))))#加密操作
            #需要解码
            return rsa_text.decode('utf-8')

    #3-解密操作


def get_encrypt_data(data:Dict) -> Dict:
    rsadate = RSAendecrypt()#创建加密实例
    data['password'] = rsadate.encrypt(get_md5_data(data['password']))#加密
    sign_data = get_md5_data(data['username']+data['password'])#操作sigin
    data.update({'sign':sign_data})#更新登录数据
    return data#返回字典类型



