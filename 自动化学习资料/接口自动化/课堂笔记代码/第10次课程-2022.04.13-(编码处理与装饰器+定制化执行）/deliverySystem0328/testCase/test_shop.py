#-*- coding: utf-8 -*-
#@File    : test_shop.py
#@Time    : 2022/4/8 20:04
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/4/8
import pytest
import allure
import os
from utils.handle_excel import get_excel_data
from common.baseAPI import BaseAPI
from utils.handle_path import report_path
from common.apiAssert import ApiAssert
#创建测试类----业务模块---逻辑处理
"""
店铺的模块的测试：初始化操作（前置条件）
    1- 首先完成有效的登录操作---拿到token
    2- 完成店铺实例的创建
"""
@allure.epic('外卖系统')#工程级别--项目级别
@allure.feature('店铺模块')#业务级别
@pytest.mark.shop#模块标签
class TestShop:
    #创建测试方法---对应模块里具体的接口
    #------无条件跳过------相当于注释---
    #@pytest.mark.skip(reason='当前模块不运行')

    #----有条件跳过-----:if条件为真，就跳过！
    #@pytest.mark.skipif(True,reason='条件满足，不执行下面操作！')
    @pytest.mark.shop_query  # 店铺查询标签
    @pytest.mark.parametrize('title,req_body,exp_resp', get_excel_data('我的商铺', 'listshopping'))
    @allure.story('店铺查询')  # 接口级别
    @allure.title('{title}')  # 用例标题
    def test_shop_query(self,shop_init,title,req_body,exp_resp):
        #shop_object = shop_init#创建店铺实例
        res = shop_init.query(req_body)#调用店铺的查询方法
        assert res['code'] == exp_resp['code']
        #ApiAssert.api_assert(res, '==', exp_resp, assert_info='code', msg='店铺查询接口断言')


    #编辑的接口
    # @pytest.mark.parametrize('title,req_body,exp_resp', get_excel_data('我的商铺', 'updateshopping'))
    # @allure.story('店铺编辑')  # 接口级别
    # @allure.title('{title}')  # 用例标题
    # def test_shop_update(self,shop_init,title,req_body,exp_resp):
    #     with allure.step('1.用户登录'):
    #         shop_object = shop_init
    #     with allure.step('2.选中编辑店铺'):
    #         shop_id = shop_init.query({'page':1,'limit':20})['data']['records'][0]['id']
    #     with allure.step('3.替换店铺图片'):
    #         image_info = shop_init.file_upload('../data/456.png')['data']['realFileName']
    #     with allure.step('4.提交店铺信息'):#测试接口
    #         res = shop_init.update(req_body,shop_id,image_info)
    #     with allure.step('5.判断是否操作成功'):
    #         assert res['code'] == exp_resp['code']
    #
            #---------方案2---------------------------
            # 编辑的接口
    @pytest.mark.shop_update  # 店铺编辑标签
    @pytest.mark.parametrize('title,req_body,exp_resp', get_excel_data('我的商铺', 'updateshopping'))
    @allure.story('店铺编辑')  # 接口级别
    @allure.title('{title}')  # 用例标题
    def test_shop_update(self, shop_update_init, title, req_body, exp_resp):
        with allure.step('1.提交店铺信息'):  # 测试接口
            res = shop_update_init['shop_object'].update(req_body, shop_update_init['shop_id'], shop_update_init['image_info'])
        with allure.step('2.判断是否操作成功'):
            assert res['code'] == exp_resp['code']


if __name__ == '__main__':
    pytest.main([__file__, '-s','--alluredir', report_path, '--clean-alluredir'])
    os.system(f'/usr/local/bin/allure serve {report_path}')
    #TODO: 2022.04.10-编码处理+日志优化（info/error）+断言封装
