> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37b7fcfa1555)

> 课程内容：

1、接口自动化测试概述
-----------

**目标：**了解接口自动化测试
----------------

**课程内容：**

```
 #接口自动化测试什么时间介入
 答：越早越好
  1- 只要api(接口定义好)--就可以写代码
  2- 调试：
   1- 后端某个模块开发好了---可以直接调试接口代码
   2- 后端没有开发完成--使用mock技术--调试！
```

**小结：**

**常见项目软件体系**

*   **单体项目**
    

*   tomcat 里运行 war
    
*   所有的业务都在 war，这个时候考虑高性能
    

*   **springboot 框架**
    

*   java -jar xx.jar
    

*   **springCloud  微服务**
    

**总结：**

```
 局面：如果开发团队实现devOps和微服务架构，会使得版本迭代或者bug修改变得高效，
 对测试人员的影响：来不及测试，测试不充分
```

**项目类型**

*   **1、前后端分离**
    

*   访问后端，会直接返回 json 数据格式 -- 给前端使用
    
*   意味我们做接口需要处理 json 数据
    

*   2、前后端不分离
    

**自动化测试方案**

*   1、现成的开源工具：postman/jmeter
    

*   **关联接口**
    

*   2、使用一些第三方发平台（收费）
    

*   **对 jmeter 二次开发 web 平台**
    

*   3、自己公司开发自动化测试平台
    

*   你得有这个平台，平台质量很难把控
    

*   **4、代码编辑自动化框架** ---pytest
    

*   灵活。方便。自定义
    

**我理解：**

*   1、自己构建自动化测试框架 --pytest
    

*   编码人员投入多
    
*   流程自己实现
    
*   特点：
    

*   2、自己的框架已经成熟 --- 封装层 i 自己的框架 -- 使用配置 excel,yaml
    

*   换一个项目 -- 套模板，再套模板 --- 懂代码的人员
    
*   sqTest -f  xintian.xls
    

*   3、流程化框架 --- 平台
    

*   web 页面 + 后端 + 执行层（httprunner、pytest）+ 第三方关联（jira+jenkins+....）----API（jenkins_api ）
    

**接口文档：**

*   1、开发给
    
*   2、测试人员自己抓包
    
*   3、接口框架生成文档  -swagger  yapi
    

2、接口测试实战准备
----------

**实战要求：**

*   一颗强大的内心 -- 心态
    
*   耐心 + 细心 + 专心
    
*   python 基础 --- 课上我用到 python 直接使用
    
*   多调试！
    
*   沟通技巧：
    

*   描述问题一定清晰
    

### 2.1 实战项目简介 - 外卖系统（卖家端 web）

**项目描述：**本项目基于 spring boot 和 vue 的前后端分离技术架构。功能完善，包含：后端 API、用户 H5 手机端、管理员 WEB 端、商户 WEB 端。主要功能包括：

**我的商铺：**卖家商铺信息管理

**食品管理：**商铺食品的常规操作

**订单管理：**订单信息管理

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210706/1625542798396077962.png)

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)  

地址：[http://121.41.14.39:8082/shop/index.html#/login](http://121.41.14.39:8082/shop/index.html#/login)

账号密码：联系所在班级的班主任申请

### 2.2、实战项目简介 - 外卖系统（卖家端 web）- 架构

**前端框架：**Vue.js **后端框架：**Spring Boot **数据库层：**mysql+mongodb **数据库连接池：**Druid **缓存：**Ehcache

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210706/1625561921450099036.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210706/1625561991975024178.png)

### 2.3、HTTP 协议解读

**HTTP 协议：**

*   post 一般是有 body
    
*   body 可以放到 url
    

*   [http://xxx.xxx.xx.xx](http://xxx.xxx.xx.xx/): 端口号 / 路径? a=1&b=2
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210706/1625562037381000606.png)

**1. HTTP 协议**

请求报文格式：四个部分

第一部分：请求行。独占一行的。由：请求方法、请求路径、请求协议 / 版本组成。

第二部分：请求头。从第二行到第 N 行。由**键值对** 组成。可以有 1 对，也可以有 N 对。key:value

第三部分：空行。独占一行的。由回车换行组成。

第四部分：**请求正文**（请求消息体）。

**常见请求头含义：**

1、User-Agent：告诉服务器客户端的相关信息（如果是浏览器会有版本，内核等信息）

2、referer：告诉服务器，此请求是从哪个页面上发出来 (防盗链)

3、X-Requested-With：告诉服务器这个是一个 ajax 请求

4、Cookie：带给服务器的 cookie 信息

5、content-type：告诉服务器消息体的数据类型

*   application/x-www-form-urlencoded
    
*   multipart/form-data
    
*   application/json
    
*   text/xml
    

**HTTP 响应报文**

响应报文格式：四个部分组成

第一部分：**状态行**，独占一行。由协议 / 协议的版本、状态码、状态描述符组成

第二部分：**响应头**，从第二行到第 N 行。由键值对组成。

第三部分：**空行**。独占一行的。由回车换行组成。

第四部分：**响应的正文**

**常见响应头含义：**

1、location：告诉浏览器跳到哪里

2、content-length：告诉浏览器回送数据的长度

3、content-type：告诉浏览器回送数据的类型

**requests 库帮助文档**

网址：[http://cn.python-requests.org/zh_CN/latest/](http://cn.python-requests.org/zh_CN/latest/)

* * *

**项目涉及的 url**

*   在线 swagger 接口文档：
    

*   [http://121.41.14.39:8082/doc.html#/home](http://121.41.14.39:8082/doc.html#/home)
    

*   买家端 -- 自行注册
    

*   [http://vip.ytesting.com/waimai.html](http://vip.ytesting.com/waimai.html)
    

*   卖家地址
    
    地址：[http://121.41.14.39:8082/shop/index.html#/login](http://121.41.14.39:8082/shop/index.html#/login)    账号、密码：请联系所在松勤自动化学习群里的班主任领取
    

**附件：当天晚上代码**