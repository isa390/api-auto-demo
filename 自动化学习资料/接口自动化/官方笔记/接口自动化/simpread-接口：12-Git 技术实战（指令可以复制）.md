> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37e04b3015da)

> 目标：GitLab 容器化搭建，Pycharm 推送代码到 Gitlab 实现 Jenkins+Pytest+Allure 自动化测试，发送邮件

**目标：GitLab 容器化搭建，Pycharm 推送代码到 Gitlab 实现 Jenkins+Pytest+Allure 自动化测试，发送邮件**

#课程操作要点

    1- 了解什么是 git

    2- 搭建 gitlab 容器

    3- 安装 git 客户端 不同系统自己网上下载

    4- 在本机练习 git 基本操作

    5- 使用 pycharm 关联 gitlab

    6- jenkins 安装 gitlab 插件与 git 全局变量设置

    7- jenkins 在源代码管理设置 gitlab 的认证 （xxxx.git+gitlab 的 pwd）

    8- 设置 gitlab 认证 jenkins 触发权限

    1- 在主菜单里的一个设置里去设置 network - outband request 记得 2 个都打上勾

    2- 在你自己 gitlab 工程里设置 webhook(jenkins 的 url+token)

    9- 使用 pycharm 直接提交脚本 -- 发送 jenkins 邮件！

**内容：**

*   1、Git 技术的概述
    
*   2、Git 与 SVN 对比
    
*   3、Git 工作流程
    

*   docker 技术实现
    

*   4、Git 环境搭建
    

*   GitHab
    
*   GitLab
    
*   码云 Gitee
    
*   **docker 技术搭建**
    
*   常用的 Git 代码托管服务
    
*   本机 Git 操作环境
    

*   5、Git 常用命令
    

*   1、 环境配置
    
*   2、 获取 Git 仓库
    
*   3、 工作目录、暂存区以及版本库概念
    
*   4、 Git 工作目录下文件的两种状态
    
*   5、 本地仓库操作
    
*   6、 远程仓库的使用
    
*   7、 分支
    
*   8、 标签
    

*   6、Pycharm 配置 Git 环境
    
*   7、Jenkins 关联 Git 操作
    
*   8、自动化测试 CI 实战
    

1、Git 技术的概述
-----------

2、Git 与 SVN 对比
--------------

> 一个开源的框架 -- 需要操做什么流程，下载对应的插件

3、Git 工作流程
----------

*   1、克隆远程仓库到本地
    

*   clone
    

*   2、在本地仓库中 checkout 代码 -- 进行编写
    
*   3、在提交前到本地仓库前 -- 需要到暂存区  index stage
    
*   4、提交到本地仓库、
    

*   **commit**
    

*   5、推送到远程仓库里
    

*   **push**
    

```
 为了更好的学习Git，我们需要了解Git相关的一些概念，这些概念在后面的学习中会经常提到
 #版本库：前面看到的.git隐藏文件夹就是版本库，版本库中存储了很多配置信息、日志信息和文件版本信息等
 #工作目录（工作区）：包含.git文件夹的目录就是工作目录，主要用于存放开发的代码
 #暂存区：.git文件夹中有很多文件，其中有一个index文件就是暂存区，也可以叫做stage。暂存区是一个临时保存
 修改文件的地方
```

4、Git 环境搭建
----------

*   **1、常用的 Git 代码托管服务 --- 远程仓库**
    
*   **GitHub**
    

*   它的服务器在国外
    
*   下载特别慢 -- 可能早上早点从 github 下载比晚上下要好
    
*   一般开源的项目多
    

*   **GitLab**
    

*   GitLab 解决了这个问题，你可以在上面创建私人的免费仓库
    
*   如果公司自己的服务器搭建仓库，首选 gitlab
    

*   **码云 Gitee**
    

*   是国内的一个代码托管平台，由于服务器在国内，所以相比于 GitHub，码云速度会更快
    

### 4.1 gitlab 容器搭建

```
docker run -di -p 443:443 -p 9000:80 -p 8022:22 --hostname 虚拟机的Ip --name mygitlab --restart always -v /srv/gitlab/config:/etc/gitlab -v /srv/gitlab/logs:/var/log/gitlab -v /srv/gitlab/data:/var/opt/gitlab -v /etc/localtime:/etc/localtime:ro --privileged=true gitlab/gitlab-ce:13.9.2-ce.0



#1- 克隆远程仓库到本地
git clone http://192.168.32.129/root/waimai.git
#2- 切换到本地仓库里
cd waimai
#3- 新建文件
touch README.md
#4- 增加到暂存区
git add README.md
#5- 提交到本地仓库
git commit -m "add README"
#6- 推送到远程仓库  对应的分支
git push -u origin master
#7- 分支创建
git branch dev
#8- 查看分支
git branch
#9- 切换分支
git checkout dev
#10- 推送到远程仓库分支
git push origin test(分支名)


#jenkins关联gitlab
#操作前提：双方都已经认证过
 - 1、pycharm发生推送或者其他事件给gitlab，会触发jenkins开始构建
 - 2、jenkins执行构建--在构建配置里，选择对应的 代码源下载最新代码
 - 3、jenkins继续做后续的操作--执行自动化测试---邮件-报告
```

**1、进入 gitlab**

输入你电脑 ip:9001 就可进入 gitlab

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647397617066470.png)

默认用户是 root，刚进入时要修改密码不少于八个字符，修改密码为 xintian123456，再次输入 xintian123456。

进入登录界面，输入用户名 root 密码 xintian123456 点击登录即可。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647443933016417.png)

**2、报错信息 502 解决方案**

查看容器 id，输入 docker ps

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647554701079786.png)

```
 docker exec 77(容器id) gitlab-ctl reconfigure#容器里启动服务
 systemctl stop firewalld#关闭防火墙
```

### 4.2 git 本机客户端

1. 官网下载 git 客户端 [https://git-scm.com/downloads](https://git-scm.com/downloads)，点击 download。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647594927036272.png)

2. 安装 git 客户端

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647636112002156.png)

3、选择好安装路径，然后点击 “Next

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647668933086929.png)

4、选择安装组件

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647755782081976.png)

5、菜单文件夹 -- 可以默认！我的选择 “Don't create a Start Menu folder”

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647783442061737.png)

6、选择编辑器, 默认使用 vim 作为编辑器

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647808061093087.png)

7、修改系统的环境变量 --- 建议选择默认、修改系统的环境变量 --- 建议选择默认

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647830426039317.png)

8、配置行尾结束符（个人选择第三个了；可以根据自己情况而定）我选择默认。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647858119086842.png)

9、配置终端仿真

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625647955808043226.png)

10、选择默认的 “git pull” 行为

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648010336075694.png)

11、额外的配置选项

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648053377075456.png)

12、实验配置选项

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648214850029293.png)

13、安装完成。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648237053069492.png)

```
 #1- 新建一个文件夹--作为本地仓库  sqlocal
 #2- 在这个文件夹里--右键--点击  git bash here--git终端
 #3- 查看Git客户端版本
 git --version
 
 #4- 环境配置---选配
 git config --global user.name "xt"
 git config --global user.name.email "xt@qq.com"
 
 #5- 在本地创建一个本地仓库
 #在前面新建的文件夹sqlocal下--在这个文件夹里--右键--点击  git bash here--git终端
 git init
 
 #6- 克隆远程仓库
 git clone  远程仓库的url   默认是80    如果修改过端口，请一定加上自定义的端口
 
 当输入远程仓库的账号与密码时候，如果输入错误，你下次连就连不上
 
 #7- 使用pycharm关联gitlab--推送我们外卖项目代码工程
```

**使用 TortoiseGit 管理文件版本**

TortoiseGit 是一款开源的 Git 图形界面工具，使用 TortoiseGit 可以简化 Git 相关的操作（本质上还是执行的 Git 相关命令）。 TortoiseGit 下载地址： [https://tortoisegit.org/download/](https://tortoisegit.org/download/)

下载完成可以得到如下安装程序

> 通过 Git 命令完成了 Git 的常用操作，通过 TortoiseGit 来完成如下操作：
> 
> ◆ 创建仓库
> 
> ◆ 克隆仓库
> 
> ◆ 将文件添加到暂存区
> 
> ◆ 提交文件
> 
> ◆ 推送本地仓库至远程仓库
> 
> ◆ 拉取代码
> 
> ◆ 创建分支
> 
> ◆ 切换分支
> 
> ◆ 合并分支

直接双击安装即可，安装完成后在桌面（也可以是其他目录）点击右键，看到如下菜单则说明安装成功

### 4.3 gitlab 新建工程

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648261719013463.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648289665023649.png)

5、Git 常用命令
----------

**使用 jenkins 实现自动化测试**

```
 #目前方案没有gitlab，后续采用该方案
 #1- 先把项目的自动化脚本文件夹放到--宿主机与容器挂载的目录下
 #使用xftp工具，放到--宿主机与容器挂载的目录下，容器下查看下
 cd /var/jenkins_nome
 ls
 #2- jenkins创建工程
 #1- 新建任务
 #2- 构建一个自由风格的软件项目
 #3- 进入工程--点击配置
 #4- 选择构建--操作shell
 #5- 增加邮件--构建后操作
 
 
 
```

6、Pycharm 配置 Git 环境
-------------------

**Pycharm 配置 Git 环境，有 2 个方案：**

*   **1、可以关闭现有工程 ---file---close project（文档使用方法）**
    
*   2、菜单栏 --CSV 也可以操作
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648327049052605.png)![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648351518017934.png)

**注意：docker 部署要加上端口如 [http://192.168.67.141:9001/root/songqinadmin.git](http://192.168.67.141:9001/root/songqinadmin.git)**

点击 clone

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648374367016488.png)![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648397907036116.png)

新建需要提交到远程仓库的文件，点击 ok

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648421241070938.png)

工程右键，找到 git 点击提交工程

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648442905015894.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648474730041017.png)

点击 push

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648505135099374.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648532208057079.png)

查看 gitlab 仓库发现提交成功

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648589577044177.png)

7、Jenkins 关联 Git 操作
-------------------

**目标：需要把 gitlab 与 jenkins 关联起来**

*   1、需要保证这 2 个服务都是 ok
    
*   2、jenkins 版本选择 git
    
*   3、gitlab 只要有设置好的事件触发，会 webHook 通过一个 url  post 方法通知 jenkins
    
*   4、jenkins 收到触发信息，他需要执行构建操作 --- 最新的代码会自动从 gitlab 会获取，执行构建
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648635061043607.png)

搜索 gitlab，allure，选择直接安装。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648669484038410.png)![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648698241001333.png)

重启 docker 镜像

```
 docker ps -a
```

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648727628077492.png)

```
 docker restart fef91a3db6d2（容器id）
```

重新进入 jenkins，配置 JDK 丶 git 丶 allure 路径，直接按给的填写即可

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648789755040753.png)

JAVA_HOME 路径 / usr/local/openjdk-8

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648816373054452.png)

git 路径 / usr/bin/git

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625648851485070360.png)

allure 路径 / opt/allure-2.13.5

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624516956589079582.png)

新建工程并配置选项。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624516971339066515.png)![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624516996690035864.png)

选择 git 添加用户信息

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517015678040257.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517030860062775.png)![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517044444053073.png)![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517060306030446.png)

为了安全起见生产 token 值

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517090178031268.png)

打开 gitlab 仓库配置 hooks

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517114503019418.png)

然后测试一下是否调通

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517182600032153.png)![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517192913005325.png)

8、自动化测试 CI 实战
-------------

**10、Jenkins 实现构建**

```
 #!/bin/bash
 rm -rf allure-results
 cd DeliverySystem/test_case
 pytest -sq --alluredir=${WORKSPACE}/allure-results
 cp DeliverySystem/environment.properties ${WORKSPACE}/allure-results/environment.properties
 exit 0
```

9、安装完 jenkins 后发现时区不对
---------------------

解决：打开 jenkins 的【系统管理】---> 【脚本命令行】，在命令框中输入一下命令【时间时区设为 亚洲上海】：

```
 System.setProperty('org.apache.commons.jelly.tags.fmt.timeZone', 'Asia/Shanghai')
```

点击【运行】，可以看到时间已正常，如图。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624515933220065850.png)

后续：有时候打开又发现时间变了，又是相隔 8 个小时的 utc，每次都要在命令行输入也是很麻烦了，打算这次一次性解决。

10、gitlab 的 ip 地址修改
-------------------

**配置 GitLab 主机名**

**1、修改 / srv/gitlab/config/gitlab.rb**

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517227390070925.png)

```
 vi /srv/gitlab/config/gitlab.rb#也可以直接使用ftp的记事本直接修改
```

把 external_url 改成部署机器的域名或者 IP 地址

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517330577008306.png)

**2、修改 /srv/gitlab/data/gitlab-rails/etc/gitlab.yml**

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517345101016211.png)

找到关键字 * ## Web server settings *

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624517356656097123.png)

将 host 的值改成映射的外部主机 ip 地址和端口，这里会显示在 gitlab 克隆地址

**3、重启容器**

```
 docker restart 容器id
```

11、宿主机时间同步问题
------------

宿主机 linux 查看时间

```
 [root@localhost etc]# date
 2020年 12月 06日 星期日 04:38:43 CST
 
```

**时区是对的，只是时间不对**

安装 ntp

```
 yum install  ntp  #安装ntp服务
```

同步阿里时间

```
 ntpdate  ntp1.aliyun.com
```

**注意事项：**

*   可能网络会断开，请重新 reboot 下 linux
    
*   虚拟机的 ip 可能会改变
    

**2、容器内部时间同步**

```
 docker cp /etc/localtime ec327fc07985:/etc/localtime
 
 #抛异常
 Error response from daemon: Error processing tar file(exit status 1): invalid symlink "/usr/share/zoneinfo/UCT" -> "../usr/share/zoneinfo/Asia/Shanghai”
 
 #解决办法
 docker cp /usr/share/zoneinfo/Asia/Shanghai 容器id:/etc/localtime
 
 #重启容器
 docker restart 容器id
```

1、gitlab  push 事件 --- 触发 jenkins 发生构建 --- 第 7 次构建

2、jenkins 会去 gitlab 拉取最新的代码