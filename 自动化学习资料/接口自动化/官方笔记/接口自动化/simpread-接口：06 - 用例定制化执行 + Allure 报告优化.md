> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37c49ac8158c)

> 需要场景：

**需要场景：**

在回归测试里面，开发修改一个业务的 bug，需要对对应的 业务测试。能不能筛选出对应的业务模块的部分接口！

**现象：**

```
pytest.main(['test_shop.py','-s'])
```

所有的测试用例都执行！

**解决方案：**

> 定制化执行测试用例 ---mark

**流程**

mark 标签 --- 组装流程方便些

**-m 选择对应的标签**

```
#外卖系统
 1- 登录
     login
 1- 店铺 shop
     1- 列出店铺 shop_list
        2- 更新店铺 shop_update
        
注意事项：
 1- mark只能筛选测试类或者测试方法，不能筛 选excel某几条用例
    2-筛选excel某几条用例，在数据驱动的时候就 筛选出来
    
    #cmd运行 多个标签
    pytest -s -m "shop_list or login"
```

```
'''
一个：
 '-m','lesson_add'
多个：
 '-m','lesson_add or lesson_list'
排除法：
   '-m','not lesson_add '
排除法 多个：
   '-m','not (lesson_add or lesson_list)'
'''
```

**遇到的问题：**没有被框架 pytest 认证

**解决：**我去认证

```
PytestUnknownMarkWarning: Unknown pytest.mark.shop_update - is this a typo?  You can register custom marks to avoid this warning - for details, see https://docs.pytest.org/en/latest/mark.html
    @pytest.mark.shop_update  # 店铺列出的标签
```

**标签没有注册，pytest 不能识别**

```
PytestUnknownMarkWarning: Unknown pytest.mark.shop - is this a typo?  You can register custom marks to avoid this warning - for details, see https://docs.pytest.org/en/latest/mark.html
    @pytest.mark.shop
```

pytest.ini

```
[pytest]
markers =
    login: Run login case
    myShop: Run myShop case
```

*   **-k 匹配用例名称**
    

*   匹配：可全名，也可以模糊
    
*   lesson_1.py lesson_2.py------ 需要指定运行 2 个
    
*   pytest -k lesson
    

*   **-v 节点** -- 多层化
    

*   示例： test_lesson.py::TesLesson::test_lesson_add # 测试文件:: 测试类:: 测试方法
    
*   pytest -v  test_lesson.py::TesLesson::test_lesson_add
    

*   **-sq: 简化打印信息**
    

*   -s 输出打印
    
*   -q 简化打印信息
    

*   **跳过 / 条件跳过**
    

*   如果您希望有条件地跳过某些内容，则可以使用 skipif 代替。 if 条件为真，跳过
    
*   跳过测试函数的最简单方法是使用跳过装饰器标记它，可以传递一个可选的原因
    
*   pytest.main(['-rs','test01.py']) 用 - rs 执行，跳过原因才会显示 SKIPPED [1] test01.py:415: 跳过 Test 类，会跳过类中所有方法
    
*   在我们自动化测试过程中，经常会遇到功能阻塞、功能未实现、环境等一系列外部因素问题导致的一些用例执行不了，这时我们就可以用到跳过 skip 用例，如果我们注释掉或删除掉，后面还要进行恢复操作
    
*   **跳过 --skip------- 相当于注释的效果**
    
*   **有条件的跳过 --skipif--**- 在执行过程中会对项目的一些前置条件进行判断 **
    
*   如果您希望有条件地跳过某些内容，则可以使用 skipif 代替。 if 条件为真，跳过
    

* * *

如果需要同时执行两个 py 文件，可以在 cmd 中在文件 py 文件所在目录下执行命令：

```
pytest -s test_1.py test_2.py
```

**运行自动化代码：**

*   新建一个 run.py 通过在命令行 运行 python run.py
    
*   在 win 系统下 建一个 run.bat 批处理文件
    
*   在 linux 系统 run.sh
    
*   jenkins 环境下 构建 shell 脚本运行指令！
    

* * *

2、Allure 报告优化
-------------

**1、浏览器打开 allure 报告**

建议使用火狐浏览器 -   谷歌是 loading 和 404     不要用 chrome,ie 浏览器打开

**2、定制化标签**

> @allure.epic("外卖系统") @allure.feature("商铺模块") @allure.tag("核心关注")

**allure 用例描述**

<table><thead><tr cid="n77" mdtype="table_row"><th>使用方法</th><th>参数值</th><th>参数说明</th></tr></thead><tbody><tr cid="n81" mdtype="table_row"><td>@allure.epic()</td><td>epic 描述</td><td>敏捷里面的概念，定义史诗，往下是 feature</td></tr><tr cid="n85" mdtype="table_row"><td>@allure.feature()</td><td>模块名称</td><td>功能点的描述，往下是 story</td></tr><tr cid="n89" mdtype="table_row"><td>@allure.story()</td><td>用户故事</td><td>用户故事，往下是 title</td></tr><tr cid="n93" mdtype="table_row"><td>@allure.title(用例的标题)</td><td>用例的标题</td><td>重命名 html 报告名称</td></tr><tr cid="n97" mdtype="table_row"><td>@allure.testcase()</td><td>测试用例的链接地址</td><td>对应功能测试用例系统里面的 case</td></tr><tr cid="n101" mdtype="table_row"><td>@allure.issue()</td><td>缺陷</td><td>对应缺陷管理系统里面的链接</td></tr><tr cid="n105" mdtype="table_row"><td>@allure.description()</td><td>用例描述</td><td>测试用例的描述</td></tr><tr cid="n109" mdtype="table_row"><td>@allure.step()</td><td>操作步骤</td><td>测试用例的步骤</td></tr><tr cid="n113" mdtype="table_row"><td>@allure.severity()</td><td>用例等级</td><td>blocker，critical，normal，minor，trivial</td></tr><tr cid="n117" mdtype="table_row"><td>@allure.link()</td><td>链接</td><td>定义一个链接，在测试报告展现</td></tr><tr cid="n121" mdtype="table_row"><td>@allure.attachment()</td><td>附件</td><td>报告添加附件</td></tr></tbody></table>

```
import pytest
import allure
@allure.feature('这里是一级标签')
class TestAllure():
    @allure.title("用例标题0")
    @allure.story("这里是第一个二级标签")
    @allure.title("用例标题1")
    @allure.story("这里是第二个二级标签")
    def test_1(self):
        allure.attach.file(r'E:\Myproject\pytest-allure\test\test_1.jpg', '我是附件截图的名字', attachment_type=allure.attachment_type.JPG)

    @allure.title("用例标题2")
    @allure.story("这里是第三个二级标签")
    
@allure.severity("critical")
@allure.description("这里只是做一个web ui自动化的截图效果")
```

**3、设置用例级别**

```
pytest  -sq --alluredir=../report/tmp --allure-severities=normal,critical
```

```
import pytest
import allure

'''
@allure.severity装饰器按严重性级别来标记case　　　
执行指定测试用例 --allure-severities blocker
BLOCKER = 'blocker'　　阻塞缺陷
CRITICAL = 'critical'　严重缺陷
NORMAL = 'normal'　　  一般缺陷
MINOR = 'minor'　　    次要缺陷
TRIVIAL = 'trivial'　　轻微缺陷　
'''


@allure.severity("normal")
def test_case_1():
    '''修改个人信息-sex参数为空'''
    print("test case 11111111")
    

@allure.severity("critical")
def test_case_2():
    '''修改个人信息-sex参数传F和M两种类型，成功(枚举类型)'''
    print("test case 222222222")


@allure.severity("critical")
def test_case_3():
    '''修改个人信息-修改不是本人的用户信息，无权限操作'''
    print("test case 333333333")

@allure.severity("blocker")
def test_case_4():
    '''修改个人信息-修改自己的个人信息，修改成功'''
    print("test case 4444444")


def test_case_5():
    '''没标记severity的用例默认为normal'''
    print("test case 5555555555")
```

**4、设置 allure 显示环境**

在 Allure 报告中添加环境信息，通过创建 environment.properties 或者 environment.xml 文件，并把文件存放到 allure-results(这个目录是生成最后的 html 报告之前，生成依赖文件的目录) 目录下

environment.properties

```
Browser=Firefox
Browser.Version=77
Stand=songqin_teach
ApiUrl=127.0.0.1/login
python.Version=3.6
```

* * *

**目标：**优化 Allure 报告，实现更加可视化与测试结果展示效果

**内容：**

**小结：**

* * *

**--------------------------------- 本次课程任务 -------------------------------------：**

**完成项目定制化执行、Allure 报告优化操作 -- 不需要提交**

* * *

**------------------------------- 下次课程预告 -------------------------------------：**

**1、allure 优化 - 进一步操作**

**2、Yaml 文件基本操作**

**3、Yaml 测试用例设计**