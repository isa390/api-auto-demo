> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37c24dbd156d)

> 内容：

❶ Pytest 测试框架
-------------

**内容：**

### ❶ 前言

　pytest 是 python 的第三方单元测试框架，比自带 unittest 更简洁和高效，支持 315 种以上的插件，同时兼容 unittest 框架。这就使得我们在 unittest 框架迁移到 pytest 框架的时候不需要重写代码。接下来我们在文中来对分析下 pytest 有哪些简洁、高效的用法。

### ❷ 环境搭建

首先使用 pip 安装 pytest

> pip install pytest
> 
> pip install pytest-html  原生态报告模板

查看 pytest 是否安装成功

> pip show pytest

**1. 创建 test_sample.py 文件，代码如下：**

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210623/1624433098522042826.gif)

```
 #!/usr/bin/env python
 
 # coding=utf-8
 
 import pytest
 def inc(x):
     return x + 1
 
 def test_answer():
     assert inc(3) == 5
 
 if __name__ =="__main__":
     pytest.main()
```

执行结果：

```
 test_sample.py F                                                         [100%]
 
 ================================== FAILURES ===================================
 _________________________________ test_answer _________________________________
 
     def test_answer():
 >       assert inc(3) == 5
 E       assert 4 == 5
 E        +  where 4 = inc(3)
 
 test_sample.py:19: AssertionError
 ============================== 1 failed in 0.41s ==============================
```

从上面的例子可以看出，pytest 中断言的用法直接使用 assert ，和 unittest 中断言 self.assert 用法有所区别。

**2. 总结一下：使用 pytest 执行测试需要遵行的规则：**

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210706/1625562498369045301.png)

**运行：pytest 用例路径 --html=./report/result.html      注意：--html= 没有空格。**

**还可以用 main() 方法来运行：****pytest.main(['当前用例路径','--html = 测试报告 / XX.html'])**

**在 pytest 中有四种 setup 和 teardown，**

*   1、setup_module 和 teardown_module 在整个测试用例所在的文件中所有的方法运行前和运行后运行，只会运行一次；
    
*   2、setup_class 和 teardown_class 则在整个文件中的一个 class 中所有用例的前后运行，
    
*   3、setup_method 和 teardown_method 在 class 内的每个方法运行前后运行，
    
*   4、setup_function、teardown_function 则是在非 class 下属的每个测试方法的前后运行；
    
*   执行：pytest test_login.py   -s        -s  输出 print 信息
    

**小结：**

* * *

❷ 数据驱动
------

**目标：**

**内容：**

*   1、Pytest 数据驱动
    
*   2、Allure 测试报告
    

**小结：**

* * *

**--------------------------------- 本次课程任务 -------------------------------------：**

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210706/1625562610169007330.png)

**------------------------------- 下次课程预告 -------------------------------------：**

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210706/1625562661410021078.png)