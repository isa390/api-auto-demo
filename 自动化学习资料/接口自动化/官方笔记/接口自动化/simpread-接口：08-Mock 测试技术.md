> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37cb037015a0)

> 1、Mock 测试技术 1. 什么是 mock?2. moco 介绍 3. 下载 moco4. 配置 json 文件 5. 启动 moco6. 配置不同的请求 Demo1：约定 URIDemo2：约定请求参数 Demo3：约定请......

1、Mock 测试技术1. 什么是 mock?2. moco 介绍3. 下载 moco4. 配置 json 文件5. 启动 moco6. 配置不同的请求Demo1：约定 URIDemo2：约定请求参数Demo3：约定请求方法Demo4：约定请求头Demo5：约定请求体参数 - formDemo6：约定请求体参数 - jsonDemo7：uri-startsWith 匹配Demo8：uri-endWith 匹配Demo9：uri-contain 包含Demo10：返回状态码Demo11：返回响应头Demo12：重定向Demo13：返回 JSON 格式的数据7. moco 总结2、异步接口课堂笔记1、什么是 mock2、mock 技术方案

### 1. 什么是 mock?

在软件测试过程中，对于一些不容易构造、获取的对象，用一个虚拟的对象来替代它，以达到相同的效果，这个虚拟的对象就是 Mock。

在前后端分离项目中，当后端工程师还没有完成接口开发的时候，前端开发工程师利用 Mock 技术，自己用 mock 技术先调用一个虚拟的接口，模拟接口返回的数据，来完成前端页面的开发。

其实，接口测试和前端开发有一个共同点，就是都需要用到后端工程师提供的接口。所以，当我们做接口测试的时候，如果后端某些接口还不成熟、所依赖的接口不稳定或者所依赖的接口为第三方接口、构造依赖的接口数据太复杂等问题时，我们可以用 mock 的方式先虚拟这些接口返回来代替。提高工作效率。

**1、使用场景：**

*   第一个后端没有开发好，自动化测试的脚本需要提前开发与调试可以使用 mock 技术
    
*   **提供执行效率：**调用第 3 方接口 --- 响应特别的长（有不稳定的情况）--10s---- 在自动化脚本调试的前期
    

**2、实现 mock 方案**

*   1、自己开发一个后端 --- 使用 django  flask--- 对于一般的测试人员有很大的挑战！
    
*   2、使用一些框架使用  moco  减轻测试人员的开发的成本，
    

### 2. moco 介绍

实现 mock 的技术很多，这些技术中，可以分为两类，mock 数据和 mock 服务：

*   mock 数据：即 mock 一个对象，写入一些预期的值，通过它进行自己想要的测试。常见的有：EasyMock、Mockito 、WireMock、JMockit。主要适用于单元测试。
    
*   mock 服务：即 mock 一个 sever，构造一个依赖的服务并给予他预期的服务返回值，适用范围广，更加适合集成测试。如 moco 框架。
    

Moco 是类似一个 Mock 的工具框架，一个简单搭建模拟服务器的程序库 / 工具，下载就是一个 JAR 包。有如下特点：

*   只需要简单的配置 request、response 等即可满足要求
    
*   支持 http、https、socket 协议，可以说是非常的灵活性
    
*   支持在 request 中设置 Headers , Cookies , StatusCode 等
    
*   对 GET、POST、PUT、DELETE 等请求方式都支持
    
*   无需环境配置，有 Java 环境即可
    
*   修改配置后，立刻生效。只需要维护接口，也就是契约即可
    
*   支持多种数据格式，如 JSON、Text、XML、File 等
    
*   可与其他工具集成，如 Junit、Maven 等
    

### 3. 下载 moco

我们可以直接去 github 上获取 moco 的 jar 包，当前版本是：V1.1.0。

地址：[https://github.com/dreamhead/moco](https://github.com/dreamhead/moco) 这里展示了关于 moco 的介绍和源码，我们可以点击箭头处直接下载它的 jar 包。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625642821023096000.png)

### 4. 配置 json 文件

我们需要先编辑一个 json 文件，用来模拟不同的请求，返回不同的响应。

新建一个文件, 格式改为 json，然后打开这个文件进行编辑。如图：

```
[{
 "description":"demo",
 "request":{
  "uri":"/demo1"
  },
 "response":{
  "text":"Hello,demo1"
 }
}]
```

其中

*   description 是注释（描述），由于 json 无法写注释，所以提供了用这个 key
    
*   uri 就是我们这个接口的统一资源标识符，可以根据模拟的接口自行定义
    
*   response 里的内容即为返回的值
    
*   这是一个非常简单的 mock
    

### 5. 启动 moco

**moco 项目是采用 java 开发的，所以启动 moco 前，需要安装 jdk。如果以前安装过的，请忽略！！！**

JDK 安装文档：[http://vip.ytesting.com/q.do?a&id=10005](http://vip.ytesting.com/q.do?a&id=10005)

首先：把我们下载下来的 moco 的 jar 包和刚刚编辑好的 json 文件放到同一个文件夹路径下，如图：

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625642857330046810.png)

然后在该路径下打开 cmd 命令行

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625643076998003654.png)

在该路径下，输入 cmd，按回车，弹出命令行.

**输入命令**：java -jar moco-runner-1.1.0-standalone.jar http -p 9090 -c test.json

其中

*   jar 包的名称可根据自己下载的 jar 包版本来写
    
*   http 代表这个模拟的是 http 请求
    
*   -p 9090 定义是端口号
    
*   -c test.json 是我们编辑的那个 json 文件名。
    

执行命名后，如图:

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625643361753067654.png)

此时我们 mock 的服务以及启动成功了, 相当于在我们本地的 9090 端口上启动的，所以我们可以通过浏览器访问一下 localhost:9090/demo 来看看返回的结果, 注意，这里 / demo 就是我们在 json 文件中定义的 uri。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625643469234064542.png)

如图，我们访问后得到了 json 文件中编辑的返回值。以上我们就完成了用 moco 来帮助我们生成 mock。

### 6. 配置不同的请求

#### Demo1：约定 URI

```
[{
 "description":"demo1=约定URI",
 "request":{
  "uri":"/demo1"
  },
 "response":{
  "text":"Hello,demo1"
 }
}]
```

#### Demo2：约定请求参数

```
[{
 "description":"demo2=约定请求参数",
 "request":{
  "queries":{
   "key1":"abc",
   "key2":"123"
    }
  },
 "response":{
  "text":"Hello,demo2"
 }
}]
```

#### Demo3：约定请求方法

```
[{
 "description":"demo3=约定请求方法",
 "request":{
  "method":"DELETE"
  },
 "response":{
  "text":"Hello,demo3"
 }
}]
```

#### Demo4：约定请求头

```
[{
 "description":"demo4=约定请求头",
 "request":{
  "headers":{
   "Content-Type":"application/xml"
   }
  },
 "response":{
  "text":"Hello,demo4"
 }
}]
```

#### Demo5：约定请求体参数 - form

```
[{
 "description":"demo5=约定请求体参数-form",
 "request":{
  "forms":{
   "key1":"abc"
   }
  },
 "response":{
  "text":"Hello,demo5"
 }
}]
```

#### Demo6：约定请求体参数 - json

```
[{
 "description":"demo6=约定请求体参数-json",
 "request":{
  "json":{
    "key1":"value1",
    "key2":"value2"
   }
  },
 "response":{
  "text":"Hello,demo6"
 }
}]
```

### Demo7：uri-startsWith 匹配

```
[{
 "description":"demo7=uri-startsWith匹配",
 "request":{ 
  "uri":{
    "startsWith":"/sq"
   }
  },
 "response":{
  "text":"Hello,demo7"
 }
}]
```

### Demo8：uri-endWith 匹配

```
[{
 "description":"demo8=uri-endsWith匹配",
 "request":{ 
  "uri":{
    "endsWith":"sq"
   }
  },
 "response":{
  "text":"Hello,demo8"
 }
}]
```

### Demo9：uri-contain 包含

```
[{
 "description":"demo9=uri-contain匹配",
 "request":{ 
  "uri":{
    "contain":"fcl"
   }
  },
 "response":{
  "text":"Hello,demo9"
 }
}]
```

### Demo10：返回状态码

```
[{
 "description":"demo10=响应状态码",
 "request":{ 
  "uri":"demo10"
  },
 "response":{
  "status":200
 }
}]
```

### Demo11：返回响应头

```
[{
 "description":"demo11=响应头",
 "request":{ 
  "uri":"demo11"
  },
 "response":{
  "headers":{
   "Content-Type":"application/json"
  }
 }
}]
```

### Demo12：重定向

```
[{
 "description":"demo12=重定向",
 "request":{ 
  "uri":"demo12"
  },
 "redirectTo":"http://www.baidu.com"
}]
```

### Demo13：返回 JSON 格式的数据

```
[{
    "description":"demo13=返回json格式的数据",
 "request":{
  "uri":"/demo13"
  }, 
 "response":{
  "json":{"key1":"value1","key2":"value2"}
    }
}]
```

### 7. moco 总结

Moco 还可以通过在 json 文件中添加 cookies、header、重定向这些参数，来模拟各种情况下的请求和返回值，可以根据自己工作的需求去对这些东西进行了解。

Moco 是热更新的，所以启动了 jar 包的服务之后，即使修改了 json 文件中的内容，也不需要重启服务就生效。

掌握了 mock 测试的技术，可以让我们在开发没有完成对应接口的时候，有接口文档就提前进入到测试状态，是现在敏捷模式下不可或缺的技术，也是持续集成中一个重要的组成部分。

**课堂演示代码**

```
# author：xintian   
# time:2020-06-23
#-*- coding: utf-8 -*-
print('--------------')
'''
场景：helloworld  首页么有开发好
接口测试：需要做？
方案：mock技术  （python  java  flask  django）
    moco--是mock一种实现方案--简单方便
学习目标：
    1- 什么是mock技术?
    2- 测试为什么需要mock?
    3- moco是什么？
    4- 构建请求 
    5- https---key ----- keyTool
'''
# import requests
# api_url = 'http://127.0.0.1:9999/xin'
# reps = requests.get(api_url)
# print(reps.text)
# print(reps.status_code)
#后续逻辑处理


#2-登录接口
# import requests
# import json
# api_url = 'http://127.0.0.1:9999/api/mgr/loginReq'
# payload = {'username':'auto','password':''}
# reps = requests.post(api_url,data=payload)
# reps.encoding = 'unicode_escape'
# print(reps.text)

#3- 支付接口
import requests
import json
api_url = 'http://127.0.0.1:9999/trade/purchase'
payload = {
            'auth_code':'28763443825664394',
            'buyer_id':'2088202954065786',
            'seller_id':'2088102146225135',
            'subject':'Iphone',
            "total_amount":"88.88"
        }
reps = requests.post(api_url,json=payload)
print(reps.text)
```

== **_下面代码只做了解，不需要掌握_** ==

```
# Flask的特点就是，结构简单，容易入门

#1.安装Flask,  pip install flask

#利用flask编写一个最简单的接口
import random
import time

from flask import Flask,request,json
#实例化一个web服务对象
app=Flask(__name__)

#创建一个方法来处理请求
#定义一个路由--访问服务的根目录就可以得到结果
@app.route('/')
def hello():
    return ''

#构造一个接受post请求的响应
@app.route('/post',methods=['POST'])
def test_post():
    #处理接口发送过来的两个参数，将两个参数合并成一个字符串返回
    d1=request.form['d1']
    d2=request.form['d2']
    return d1+d2
#处理极简交易接口
@app.route('/trade/purchase',methods=['POST'])
def purchase():
    #拿到客户端返回的数据
    res=json.loads(request.get_data())
    out_trade_no=res['out_trade_no']
    auth_code=res['auth_code']
    data={
        'code': '40004',
        'msg': 'Business Failed',
        'sub_code': 'ACQ.TRADE_HAS_SUCCESS',
        'sub_msg': '交易已被支付',
        'trade_no': '2013112011001004330000121536',
        'out_trade_no': '6823789339978248'
    }
    #把out_trade_no改成客户端发送过来的数据
    data['out_trade_no']=out_trade_no
    data['trade_no']=time.strftime('%Y%m%d%H%M%S')+str(random.random()).replace('0.','')
    #验证授权码
    if auth_code !='28763443825664394':
        return {'coode':'50000','msg':'请求码验证失败'}

    return data

if __name__ == '__main__':
    #运行服务，并确定服务运行的IP和端口
    app.run('127.0.0.1','9090')
```

**同步：**

同步的思想是：所有的操作都做完，才返回给用户。这样用户在线等待的时间太长，给用户一种卡死了的感觉（就是系统迁移中，点击了迁移，界面就不动了，但是程序还在执行，卡死了的感觉）。这种情况下，用户不能关闭界面，如果关闭了，即迁移程序就中断了。

**异步：**

将用户请求放入消息队列，并反馈给用户，系统迁移程序已经启动，你可以关闭浏览器了。然后程序再慢慢地去写入数据库去。这就是异步。但是用户没有卡死的感觉，会告诉你，你的请求系统已经响应了。你可以关闭界面了。 **同步，是所有的操作都做完，才返回给用户结果。即写完数据库之后，在相应用户，用户体验不好。**

**异步，不用等所有操作等做完，就相应用户请求。即先相应用户请求，然后慢慢去写数据库，用户体验较好**

**异步接口**

*   **业务场景：**
    

*   1- 店铺向平台申请退单请求
    
*   2 - 平台已经接受你的请求，正在核实信息！
    
*   3 - 平台会告诉在 3 个工作日查询结果
    

**具体实现**

*   1、通过提交申请的接口给服务端
    
*   2、服务器立即返回一个这个申请的 **id**
    
*   3、后续我们使用查询的接口，带上这个 **id** 去查询结果
    
*   4、看是否有返回结果
    

**目前版本状态**

使用 mock 技术 + 异步查询的技术

当业务处理比较耗时时, 接口一般会采用异步处理的方式, 这种异步处理的方式又叫 Future 模式.

**一般流程** 当你请求一个异步接口, 接口会立刻返回你一个结果告诉你已经开始处理, 结果中一般会包含一个任务 id 类似的东西用于追踪结果, 另外会提供一个**查询结果**的接口, 当结果未处理完查询接口会返回相应的 "未完成" 状态, 如果已经处理完, 则会返回相应的数据.

**处理方法** 异步接口我们一般采取轮询的方法, 每隔一定时间间隔取请求一下查询结果的接口, 直到接口返回的状态是已完成 / 查询到指定数据或超时

如果异步接口没有提供追踪 id 和查询接口, 我们可以通过同样的方法轮询查取数据库数据或日志数据直到获取到指定结果或超时

**示例接口**

1.  **订单退出申请接口**
    

**请求地址** /api/order/create/

**请求方法** POST

**请求格式** Json

<table><thead><tr cid="n173" mdtype="table_row"><th>参数</th><th>类型</th><th>说明</th></tr></thead><tbody><tr cid="n177" mdtype="table_row"><td>user_id</td><td>String</td><td>用户 id</td></tr><tr cid="n181" mdtype="table_row"><td>goods_id</td><td>String</td><td>商品 id</td></tr><tr cid="n185" mdtype="table_row"><td>num</td><td>int</td><td>数量</td></tr><tr cid="n189" mdtype="table_row"><td>amount</td><td>float</td><td>总价</td></tr></tbody></table>

**响应示例** 缺少参数:

```
{
    "msg": "参数缺失"
}
```

成功:

```
{
    "order_id": "6666"
}
```

**2、获取订单结果接口**

**请求地址** /api/order/get_result/

**请求方法** GET

<table><thead><tr cid="n203" mdtype="table_row"><th>参数</th><th>类型</th><th>说明</th></tr></thead><tbody><tr cid="n207" mdtype="table_row"><td>order_id</td><td>String</td><td>订单 id</td></tr></tbody></table>

**响应示例** 创建中:

```
{}
```

创建成功:

```
{
    "user_id": "sq123456",
    "goods_id": "20200815",
    "num": 1,
    "amount": 200.6
    "msg": "success"
}
```

**4、技术实施**

*   1、搭建一个新增业务的后端服务 -- 根据接口文档来
    

*   1、申请的接口
    
*   2、查询接口
    

*   2、编辑 json 接口配置文件
    
*   3、编写 python 代码
    
*   4、调试脚本
    

**pytest 框架扩展**

```
# 扩展--pytest  有没有用例失败了重跑的机制  失败了可以按照一定频率去跑多次  频率+次数
import pytest

#reruns=2   重跑次数   ,reruns_delay=2  频率
@pytest.mark.flaky(reruns=2,reruns_delay=2)#只要失败的才重跑
@pytest.mark.parametrize('a',[100,200,300])
def test_001(a):
    assert a == 200

if __name__ == '__main__':
    pytest.main(['mockTest.py','-s'])
```

1、什么是 mock
----------

**也称：测试桩，挡板**

**mock 使用场景**

*   1、某一个新需求，开发来不及开发，测试人员采取敏捷方式，使用 mock 技术，把新增的业务使用 mock，模拟出来，脚本可以先写，后面发版后，再实际调试！
    
*   2、在接口自动化脚本调试阶段需要，如果这个项目，在接口方面需要调用很多第三方接口，接口返回的效率低，使用 mock 技术，先吧整体的流程先实现！
    
*   3、其他行业
    

2、mock 技术方案
-----------

*   **1、现成的 xxx.jar 直接运行，使用配置文件去配置服务后端**
    
*   2、如果你的测试团队有会开发后端的人员，可以使用 django/flask 开发一个简易后端服务
    
*   3、一些现成的工具也有对应 mock 技术