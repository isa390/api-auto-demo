> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37bf18571564)

> 1、编程语言的选型

### 1.1、项目测试需求分析

### 1.2、接口自动化测试架构规划

**1、编程语言的选型**

*   python
    
*   java
    

**2、编程工具的选型**

*   pycharm
    
*   vscode
    

**3、自动化测试框架的选型**

概念：一个架子  （数据驱动），有现成的一些代码 --- 提高效率

*   unittest---python 解释器自带
    
*   **unittest 升级版 - pytest--**
    
*   unittest 升级版 - nose
    
*   httprunner 框架
    
*   rf 框架 --- 关键字
    

**4、报告可视化方案的选型**

*   htmltestrunner--- 简单的 html 报告
    
*   beautifulreport
    
*   **allure**
    

**5、持续方案的选型**

*   ci 持续集成 ---jenkins
    

**6、仓库服务器的选型**

*   github
    
*   **gitlab**
    
*   gitee  码云
    

**7、测试管理工具的选型**

*   禅道
    
*   **jira**
    

* * *

### 1.3、项目代码工程创建

> 不要在一个文件夹里放很多项目文件！

**代码的编写方案**

*   根据业务层封装 ---- 简单一容易入门 ---- **接口课程讲解**
    
*   封装基类  APIObject ---- 继承关系 ----**- 实战课程讲解**
    

*   首先封装一个接口的基本类
    
*   登录 店铺去继承这个基本类
    

**包：代码需要 import  导入**

**文件夹：可以使用路径获取 的**

*   **common**：公共的 ---baseApi.py
    
*   **libs：**基本业务层的代码库包 --- 去继承 common-baseApi.py-BaseAPI 类
    
*   **configs：**配置包
    

*   有可能是 config.py
    
*   yaml
    

*   **data：**数据 / 用例 excel 测试用例**文件夹 -**    （yaml 格式文件）
    

*   代码与数据分离
    

*   **logs：**日志文件夹
    
*   **testCase：**测试用例代码包
    
*   **report：**报告文件夹
    
*   **docs：**项目相关文档文件夹
    
*   **utils：**常规方法包
    

2、执行 Excel 测试用例
---------------

**目标：Excel 测试用例实现登录模块自动化测试**

**自动化测试用例要素**

*   **代码与用例分离：增加删除用例，不需要维护代码！**
    
*   **当有一些无效测试用例 -- 我们在代码里做好筛选！**
    
*   **自动化测试用例应该包含手工用例的一些内容**
    

**测试用例常规方式**

*   **思维导图 xmind 手工测试比较多**
    
*   **excel 用例 --- 适合自动化测试**
    
*   **yaml 用例 --- 适合自动化测试**
    
*   **json 用例 --- 适合自动化测试 (比较麻烦)**
    
*   **使用数据库用例**
    
*   **word 用例**
    

**操作 excel 测试用例流程**

*   **1、读取 excel 数据**
    
*   **2、把 excel 读取的数据关联到请求代码里**
    
*   **3、实际与预期相对比，结果写入测试结果到 excel**
    

3、扩展操作
------

### 增加脚本的模板

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210706/1625562324902044236.png)

```
#-*- coding: utf-8 -*-
#@File    : ${NAME}.py
#@Time    : ${DATE} ${TIME}
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: ${PRODUCT_NAME}
```