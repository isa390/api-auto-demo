> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37dc567c15d3)

> 目标：Jenkins 容器化搭建，结合 Pytest 自动化执行生成 Allure 报告，发送邮件

**目标：**Jenkins 容器化搭建，结合 Pytest 自动化执行生成 Allure 报告，发送邮件

**流程**：

*   **1、搭建 jenkins 环境**
    

*   **镜像：**
    
*   **容器操作**
    

*   1、官方镜像  比较干净 docker pull jenkins
    
*   **2、公司自己的镜像仓库：阿里云仓库里面有：**
    

*   python 3.6.8 解释器
    
*   git
    
*   allure+pytest 等常用的第三库
    

*   方案：docker 容器技术
    
    ```
     新建挂载目录
     #在宿主机的一个目录下创建一个目录，用于挂载目录
     mkdir -p /var/jenkins_node
     #给一个权限--chmod 777 (0111)可读可写可执行  wrx
     chmod 777 /var/jenkins_node
     
     #创建与启动守护式容器---注意容器中的端口使用8080
     docker run -id --name=sqjenkins -p 8080:8080 -v /var/jenkins_node:/var/jenkins_home --restart=always  registry.cn-hangzhou.aliyuncs.com/sqqdcl/jenkins:v1
    ```
    

*   **2、运行 jenkins 容器，做相关配置**
    

*   1、下载需要的插件 --- 选择默认安装就行
    
*   2、我们操作需要的一些库需要自行安装
    

*   **allure 插件**   是 jenkins 需要操作 allure 工具需要的插件
    
*   gitlab--- 下次课 gitlab 课程需要
    

*   **3、查看下 jenkins 容器里面有什么？--- **进入容器操作****
    

*   python 解释器 3.6
    
*   allure
    
*   git
    
*   pip3 list  查下 python3 解释器里面有什么库  pip3 install xxxx
    

*   4、jenkins 全局环境变量配置
    

*   JAVA_HOME
    
*   git
    
*   allure
    

*   **3、验证检查 python 运行环境**
    

*   检查：pytest 指令是否可以直接运行
    
    ```
     #1- 确认环境里有没有pytest
     whereis pytest
     #2- 在/opt/python3/bin  是可以运行的
     #3- 希望在任意路径都可以执行pytest
     #只要你的应用在/usr/bin就可以在任意路径运行
     #软连接：你只要运行这个命令，我自己就会到它的安装路径去找！
     以root权限进入容器
     docker exec -uroot  -it c5 /bin/bash
     
     ln -s 源应用安装路径  执行路径
    ```
    

*   **4、新建 jenkins 工程 -- 构建执行自动化脚本**
    

*   1、点击构建 -- 在 jenkins_home/workspace / 有一个工程目录
    
*   2、因为现在没有学到 git 操作 -- 只能半自动化，手动把项目代码直接通过 xftp 直接传递到 / var/jenkins_node/workspace/songqin-21210802
    
*   3、在构建配置里 -- 去写 shell 脚本 执行项目自动化运行！
    
    ```
     #验证是否可以运行！
     cd Delivery_System-0716/test_case
     pytest -s
     #报错了
     PermissionError: [Errno 13] Permission denied: '/var/jenkins_home/workspace/songqin-21210802/Delivery_System-0716/logs/autoProjext-202108021326.log'
     
     #解决
     chmod -R 777 /var/jenkins_home/workspace/songqin-21210802/Delivery_System-0716
    ```
    

*   **5、jenkins 执行完有 allure 报告**
    

*   在构建配置里去设置一个构建后操作
    
*   构建后 allure 指令去操作报告所存放文件的路径生成报告！
    

*   **6、执行完之后有邮件，邮件里有报告！**
    

*   所有的代码或第三方工具操作邮箱发邮件 --- 使用的授权码。不是登录密码
    
    ```
     SMTP Port
     #1-虚拟机的liunx 可以使用 25
     #2- 阿里云的服务器，一定要使用465,而且勾上后面的ssl
     #在阿里云安全策略组开放这个465
    ```
    
*   1、检查有没有邮件的插件
    
*   2、测试下邮件的基本设置是否可以通
    
*   3、构建操作后，直接发送邮件
    

*   **总结**
    

*   1、代码是手动放进去，
    
*   2、jenkins 也是手动构建
    

*   **扩展** --**- 后面的项目实战课程**
    

*   pipeline 脚本语法（推荐使用）
    
*   控制机
    
*   执行机 --node  节点
    
*   **jenkins 分布式**
    
*   **jenkins 流水线**
    

**2、Jenkins 搭建环境教程**

**1、持续集成的概述**

> CI--- 持续集成
> 
> CD--- 持续交付

**2、Jenkins 简介**

> 一个开源的框架 -- 需要操做什么流程，下载对应的插件

**3、Jenkins 环境搭建与启动**

```
#1- 下载镜像： 
#方案一：
1- 搜索  docker search jenkins---最新版本的
2- 在这个镜像的创建的容器里需要搭建python环境（一系列库）
apt指令安装

#方案二：可以从一个指定的下载源的镜像拉取就行--前提是已经打包好这个镜像---（jenkins+python3+pytest）
#阿里云-公共仓库  
docker pull 镜像

docker pull  registry.cn-hangzhou.aliyuncs.com/sqqdcl/jenkins:v1

#2- 查看镜像---静态的模板
#镜像--容器的模板--打包好的---静态的概念--使用的时候请创建容器docker images

#3- 创建容器
#守护式---  退出容器的终端，不会exit  docker run -id xxxx

#做目录挂载
jenkins 做很多的插件安装--以及其他设置

#在宿主机的一个目录下创建一个目录，用于挂载目录
mkdir -p /var/jenkins_node
#给一个权限--chmod 777 (0111)可读可写可执行  wrx
chmod 777 /var/jenkins_node

#4- 创建与启动守护式容器---注意容器中的端口使用8080
docker run -id --name=sqjenkins -p 8080:8080 -v /var/jenkins_node:/var/jenkins_home registry.cn-hangzhou.aliyuncs.com/sqqdcl/jenkins:v1

#提升技巧
#容器的配置文件修改对应的端口号---修改完请重启容器
/var/lib/docker/containers/容器的id/hostconfig.json


#5- 查看容器是否启动
docker ps -a
#6- 查看宿主机ip
ifconfig    

#7- windows机器访问
http://宿主机ip:7070/

#8- 获取管理员密码
docker logs sqjenkins


#Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:

afc945eb00114bc3a7cdaa42149dd66c #管理员密码

#This may also be found at: /var/jenkins_home/secrets/initialAdminPassword

#9-或者宿主机里的/var/jenkins_node/secrets

cat /var/jenkins_node/secrets/initialAdminPassword#该指令也可以获取
```

**4、Jenkins 插件下载**

```
#需要安装2个插件
gitlab  gitlab HOOK
allure
#注意修改下载源  --插件--高级设置
```

访问 ip:8080 进入 jenkins

安装推荐插件，中间可能有的插件安装失败，不用管它。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625644984300047900.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645008251062720.png)

创建用户名和密码，保存并完成，输入 admin，密码 admin ，保存并完成，开始 jenkins。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645031216027430.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645060607057642.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645090050035443.png)

进入插件管理，下载并安装需要的插件，完成后重启 jenkin 服务。

安装插件 gitlab，gitlab hook,allure

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645125519071416.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645152660077207.png)

搜索 gitlab，allure，选择直接安装。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645190019096610.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645215793067760.png)

重启 docker 镜像

```
docker ps -a
```

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645248400036572.png)

```
docker restart fef91a3db6d2（容器id）
```

**5、Jenkins 全局工具设置** - 直接按给的填写即可

```
#需要设置全局
jdk
git
allure
```

重新进入 jenkins，配置 JDK 丶 git 丶 allure 路径，直接按给的填写即可。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645303420064450.png)

JAVA_HOME 路径 / usr/local/openjdk-8

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645333210038017.png)

git 路径 / usr/bin/git

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645363214000782.png)

allure 路径 / opt/allure-2.13.5

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645389101055868.png)

**6、Jenkins 邮件功能测试**

```
#邮件发送邮箱账号    
songqin_xintian@163.com  
#授权码（注意，不是邮箱密码）所有第三方操作邮件都需要这个授权码
WYMBWTZSUEJQTYGZ
```

卸载下载的邮件插件

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220210/1644473002585062172.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220210/1644473158449076047.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220210/1644473197018030105.png)

重启 jenkins

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220210/1644473272431068638.png)

docker restart sqjenkins

安装本地可以使用版本插件

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220210/1644473525196014524.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220210/1644473540600027166.png)

重启 jenkins 进行邮件配置

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645420239093843.png)

配置管理员邮箱地址，发送人邮箱。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645451505053155.png)

发送邮箱配置，邮箱名称 [songqin_xintian@163.com](mailto:songqin_xintian@163.com) 授权码为 WYMBWTZSUEJQTYGZ

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645484464034404.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645512926039610.png)

测试成功, 保存设置。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645548573058705.png)

**7、Python 环境验证**

```
#在容器里输入  python3
#查看python环境的安装库
pip3 list
```

**8、Pytest 框架环境验证**

```
### 进入docker里加pytest软连接
#管理员进入容器
docker exec -uroot -it sqjenkins /bin/bash
#加pytest软连接
ln -s /opt/python3/bin/pytest /usr/bin/pytest

#软连接是linux中一个常用命令，它的功能是为某一个文件在另外一个位置建立一个同不的链接。
#具体用法是：ln -s 源文件 目标文件。 

#当我们需要在不同的目录，用到相同的文件时，我们不需要在每一个需要的目录下都放一个必须相同的文件，我们只要在其它的 目录下用ln命令链接（link）就可以，不必重复的占用磁盘空间。
```

**使用 jenkins 实现自动化测试**

```
#目前方案没有gitlab，后续采用该方案
#1- 先把项目的自动化脚本文件夹放到--宿主机与容器挂载的目录下
#使用xftp工具，放到--宿主机与容器挂载的目录下，容器下查看下
cd /var/jenkins_nome
ls
#2- jenkins创建工程
#1- 新建任务
#2- 构建一个自由风格的软件项目
#3- 进入工程--点击配置
#4- 选择构建--操作shell  执行pytest  运行脚本
#5- 增加邮件--构建后操作
```

新建工程并配置选项。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645571604056655.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645593152044032.png)

**2. 项目配置**

在完成系统设置后，还需要给需要构建的项目进行邮件配置。

选择**配置构建后操作模块**

进入系统配置页面后，点击上方的**构建后操作**选项，配置内容如下：

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645620115023555.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645656300066365.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645681893069818.png)

添加邮件内容模板

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625645943954072836.png)

```
            [object Object]            
        
        
            
            本邮件由系统自动发出，无需回复！
            
            各位同事，大家好，以下为${PROJECT_NAME }项目构建信息
```

先把 Tirrgers 右上角关闭掉，然后新增 always，可以选择 add 增加你要发送的人员列表。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646243054094869.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646271004032011.png)

最后保存，然后构建，构建成功后邮箱收到邮件。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646325645007417.png)

**10、Jenkins 实现构建**

配置 pytest 命令，生成 allure 报告。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646362682050725.png)

```
#!/bin/bash 
rm -rf allure-results
cd Delivery_System/test_case
pytest -sq --alluredir=${WORKSPACE}/allure-results
exit 0
```

选择构建后操作 allure Report

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646400030006667.png)

构建 -- 增加构建步骤 -- 执行 sh 命令（根据需要进行选择）；构建后操作 -- 选择 Allure Report.

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646562674099259.png)

保存，点击构建。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646582693036411.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646598412078450.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646683604049599.png)

1、解决首次进入 Jenkins 页面慢的问题
-----------------------

安装好 Jenkins 之后，在浏览器地址栏里输入 [http://localhost:8080](http://localhost:8080/) 准备进入 Jenkins 的后台管理系统，这时页面一直卡在那里不动了，页面一直显示

Please wait while Jenkins is getting ready to work ... Your browser will reload automatically when Jenkins is ready.

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646723794024992.png)

出现这个情况时，是因为 Jenkins 的默认更新镜像用的国外的镜像，把更新镜像改成国内的即可。 首先打开 Jenkins 的安装目录， 比如我是安装在 D:\Program Files (x86)\Jenkins 这个目录，打开这个目录，然后打开 hudson.model.UpdateCenter.xml 这个配置文件，

```
将
https://updates.jenkins.io/update-center.json
修改成
http://mirror.xmission.com/jenkins/updates/update-center.json，
```

然后打开 windows 服务管理器，重启 Jenkins 服务，再重新刷新 Jenkins 启动页面即可。

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624514878280030188.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210624/1624514878328081504.png)

上面是配置文件修改前与修改后的对比图。

2、本机安装了 Jenkins，但是安装插件时一直失败
---------------------------

```
#本机安装了Jenkins，但是安装插件时一直失败。更改升级站点也不生效，究其原因是因为default.json中插件下载地址还https://updates.jenkins.io，升级站点设置未生效。

#需要操作一个步骤

#1.进入 Manage Jenkins -》 Manage Plugin -> Advanced 最下面有 Update Site 设置为
https://mirrors.tuna.tsinghua.edu.cn/jenkins/updates/update-center.json
```

3、jenkins 实现 allure 报告添加环境配置
----------------------------

**问题现象：**

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646746914039470.png)

**解决方案**

在工程的根目录下面新建文件 environment.properties

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646764166078763.png)

**执行脚本**

```
#!/bin/bash
rm -rf allure-results
cd /var/jenkins_home/DeliverySystem/test_case
pytest -sq --alluredir=${WORKSPACE}/allure-results
cp /var/jenkins_home/DeliverySystem/environment.properties ${WORKSPACE}/allure-results/environment.properties
exit 0
```

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625646782954037702.png)