> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37c5bc071593)

> 大小写敏感

1、Yaml 基本语法
-----------

### 1. 基本规则

> *   大小写敏感
>     
> *   使用缩进表示层级关系
>     
> *   缩进时不允许使用 Tab，只允许使用空格
>     
> *   缩进的空格数目不重要，只要相同层级的元素左对齐即可
>     
> *   `#` 表示注释，从它开始到行尾都被忽略
>     

### 2. yaml 转字典

```
 # 下面格式读到Python里会是个dict
 name: 灰蓝
 age: 0
 job: Tester
```

输出：

```
 {'job': 'Tester', 'age': 0, 'name': u'\u7070\u84dd'}
```

### 3. yaml 转列表

yaml 中支持列表或数组的表示，如下：

```
 # 下面格式读到Python里会是个list
 - 灰蓝
 - 0
 - Tester
```

输出：

```
 [u'\u7070\u84dd', 0, 'Tester']
```

### 4. 复合结构

字典和列表可以复合起来使用，如下：

```
 # 下面格式读到Python里是个list里包含dict
 - name: 灰蓝
   age: 0
   job: Tester
 - name: James
   age: 30
```

输出：

```
 [{'job': 'Tester', 'age': 0, 'name': u'\u7070\u84dd'}, {'age': 30, 'name': 'James'}]
```

### 5. 基本类型

yaml 中有以下基本类型：

> *   字符串
>     
> *   整型
>     
> *   浮点型
>     
> *   布尔型
>     
> *   null
>     
> *   时间
>     
> *   日期
>     

我们写个例子来看下：

```
 # 这个例子输出一个字典，其中value包括所有基本类型
 str: "Hello World!"
 int: 110
 float: 3.141
 boolean: true  # or false
 None: null  # 也可以用 ~ 号来表示 null
 time: 2016-09-22t11:43:30.20+08:00  # ISO8601，写法百度
 date: 2016-09-22  # 同样ISO8601
```

输出：

```
 {'date': datetime.date(2016, 9, 22), 'None': None, 'boolean': True, 'str': 'Hello World!', 'time': datetime.datetime(2016, 9, 22, 3, 43, 30, 200000), 'int': 110, 'float': 3.141}
```

如果字符串没有空格或特殊字符，不需要加引号，但如果其中有空格或特殊字符，则需要加引号了

```
 str: 灰蓝
 str1: "Hello World"
 str2: "Hello\nWorld"
```

输出：

```
 {'str2': 'Hello\nWorld', 'str1': 'Hello World', 'str': u'\u7070\u84dd'}
```

这里要注意单引号和双引号的区别，单引号中的特殊字符转到 Python 会被转义，也就是到最后是原样输出了，双引号不会被 Python 转义，到最后是输出了特殊字符；可能比较拗口，来个例子理解下：

```
 str1: 'Hello\nWorld'
 str2: "Hello\nWorld"
```

```
 # -*- coding: utf-8 -*-
 import yaml
 
 y = yaml.load(file('test.yaml', 'r'))
 print y['str1']
 print y['str2']
```

输出：

```
 Hello\nWorld
 Hello
 World
```

可以看到，单引号中的'\n'最后是输出了，双引号中的'\n'最后是转义成了回车

字符串处理中写成多行、'|'、'>'、'+'、'-'的意义这里就不讲了。

### 6. 引用

`&` 和 `*` 用于引用

```
 name: &name 灰蓝
 tester: *name
```

这个相当于一下脚本：

```
 name: 灰蓝
 tester: 灰蓝
```

输出：

```
 {'name': u'\u7070\u84dd', 'tester': u'\u7070\u84dd'}
```

### 7. 强制转换

yaml 是可以进行强制转换的，用 `!!` 实现，如下：

```
 str: !!str 3.14
 int: !!int "123"
```

输出：

```
 {'int': 123, 'str': '3.14'}
```

明显能够看出 123 被强转成了 int 类型，而 float 型的 3.14 则被强转成了 str 型。另外 PyYaml 还支持转换成 Python/object 类型，这个我们下面再讨论。

### 8. 分段

在同一个 yaml 文件中，可以用 `---` 来分段，这样可以将多个文档写在一个文件中

```
 ---
 name: James
 age: 20
 ---
 name: Lily
 age: 19
```

这时候我们就得用到我们的 `load_all()` 方法出场了，`load_all()` 方法会生成一个迭代器，可以用 for 输出出来：

```
 # -*- coding: utf-8 -*-
 import yaml
 
 ys = yaml.load_all(file('test.yaml', 'r'))
 for y in ys:
     print y
```

输出：

```
 {'age': 20, 'name': 'James'}
 {'age': 19, 'name': 'Lily'}
```

对应的也有 `dump_all()` 方法，一个意思，就是将多个段输出到一个文件中，举个栗子：

```
 # -*- coding: utf-8 -*-
 import yaml
 
 obj1 = {"name": "James", "age": 20}
 obj2 = ["Lily", 19]
 
 with open('test.yaml', 'w') as f:
     yaml.dump_all([obj1, obj2], f)
```

打开 test.yaml 看看：

```
 {age: 20, name: James}
 --- [Lily, 19]
```

`dump()` 和 `dump_all()` 方法可以传入列表，也可以传入一个可序列化生成器，如 `range(10)`， 如下：

```
 # -*- coding: utf-8 -*-
 import yaml
 fo = open('../config/test.yaml','w',encoding='utf-8')
 yaml.dump(range(10),fo)
 
```

输出：

```
 [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

在 `dump` 和 `dump_all()` 的时候还可以配一堆参数

**目标：**了解 Yaml 基本使用语法

**内容：**

**小结：**课后去实现下 yaml 作为配置文件的用法

2、Yaml 操作
---------

**1、yaml 使用场景**

*   配置文件
    
*   测试用例
    

**2、yaml 语法**

*   字典
    
*   列表
    
*   嵌套
    
*   注释
    
*   & * 变量操作
    
*   多用例
    
*   空格与颜色变化
    
*   ```
     - #test4
       url: /api/mgr/loginReq
       method: post
       detail: 不传账号，不传密码
       data:
         username: "" #如果不填  None
         password: ""
       check:
         retcode: 1
         reason: 用户或者密码错误
    ```
    

3、yaml 里引用 yaml 文件
------------------

问题并没有要求 Python 解决方案，但下面是一个使用 [PyYAML](https://github.com/yaml/pyyaml)...

PyYAML 允许您附加自定义构造函数 (如`!include`) 到 YAML 加载程序

#### 基于类的解决方案

这里有一个基于类的解决方案，它避免了我最初响应的全局根变量。

使用元类注册自定义构造函数：

```
 import yaml
 import os.path
 class Loader(yaml.Loader):#继承
     def __init__(self, stream):
         self._root = os.path.split(stream.name)[0]
         super(Loader, self).__init__(stream)
     def include(self, node):
         filename = os.path.join(self._root,    self.construct_scalar(node))
         with open(filename, 'r') as f:
             return yaml.load(f, Loader)
 Loader.add_constructor('!include', Loader.include)
```

例如：

**a.yaml**

```
 a: 1
 b:
     - 2
     - 3
 c: !include b.yaml
```

**`b.yaml`**

```
 - 10
 - [100, 200, 300]
```

现在可以使用以下方法加载文件：

```
 with open('a.yaml', 'r') as f:
     data = yaml.load(f, Loader)
  print(data)
 {'a': 1, 'b': [2, 3], 'c': [10, [100, 200, 300]]}
```

**windows 环境下批处理文件方案**

```
 @echo off
 echo 松勤-教管系统接口自动化运行准备开始......
 @echo on
 
 
 
 del /f /s /q  G:\SongQin\Python\Demo\teach_sq\report\tmp\*.json
 del /f /s /q  G:\SongQin\Python\Demo\teach_sq\report\tmp\*.jpg
 del /f /s /q  G:\SongQin\Python\Demo\teach_sq\report\report
 
 
 
 @echo off
 echo 环境文件删除工作完成，开始运行脚本......
 @echo on
 
 
 cd  G:/SongQin/Python/Demo/teach_sq/test_case
 pytest  -sq --alluredir=../report/tmp
 
 allure serve ../report/tmp
 
 
 @echo off
 echo 接口自动化运行成功
 pause
```

**运行模式**

*   1- 使用 终端 (win/linux) 运行：   python　-ｍ　run.py
    
*   2- 使用执行文件运行：  run.bat(Win)  ; run.sh(Linux)   运行指令  ./run.sh
    
*   3- jenkins 运行  会使用自带的 sh 功能运行
    

**--------------------------------- 本次课程任务 -------------------------------------：**

**完成 Yaml 基本操作、Yaml 用例执行 -- 不需要提交**

4、获取项目工程路径
----------

```
 """
 在一些代码里使用相对路径会报文件找不到！
 ../data/xxxxxx
 解决方案：
     通过代码自动获取当前运行项目的路径： 
 """
 import os  #
 print(__file__)#当前运行文件的路径
 print(os.path.realpath(__file__))#当前运行文件的绝对路径
 project_path =os.path.split(os.path.realpath(__file__))[0].split('configs')[0]
 print(project_path)#项目路径
```

5、Allure 报告优化
-------------

**1、浏览器打开 allure 报告**

建议使用火狐浏览器 -   谷歌是 loading 和 404     不要用 chrome,ie 浏览器打开

**2、定制化标签**

> @allure.epic("外卖系统") @allure.feature("商铺模块") @allure.tag("核心关注")

**allure 用例描述**

<table><thead><tr cid="n169" mdtype="table_row"><th>使用方法</th><th>参数值</th><th>参数说明</th></tr></thead><tbody><tr cid="n173" mdtype="table_row"><td>@allure.epic()</td><td>epic 描述</td><td>敏捷里面的概念，定义史诗，往下是 feature</td></tr><tr cid="n177" mdtype="table_row"><td>@allure.feature()</td><td>模块名称</td><td>功能点的描述，往下是 story</td></tr><tr cid="n181" mdtype="table_row"><td>@allure.story()</td><td>用户故事</td><td>用户故事，往下是 title</td></tr><tr cid="n185" mdtype="table_row"><td>@allure.title(用例的标题)</td><td>用例的标题</td><td>重命名 html 报告名称</td></tr><tr cid="n189" mdtype="table_row"><td>@allure.testcase()</td><td>测试用例的链接地址</td><td>对应功能测试用例系统里面的 case</td></tr><tr cid="n193" mdtype="table_row"><td>@allure.issue()</td><td>缺陷</td><td>对应缺陷管理系统里面的链接</td></tr><tr cid="n197" mdtype="table_row"><td>@allure.description()</td><td>用例描述</td><td>测试用例的描述</td></tr><tr cid="n201" mdtype="table_row"><td>@allure.step()</td><td>操作步骤</td><td>测试用例的步骤</td></tr><tr cid="n205" mdtype="table_row"><td>@allure.severity()</td><td>用例等级</td><td>blocker，critical，normal，minor，trivial</td></tr><tr cid="n209" mdtype="table_row"><td>@allure.link()</td><td>链接</td><td>定义一个链接，在测试报告展现</td></tr><tr cid="n213" mdtype="table_row"><td>@allure.attachment()</td><td>附件</td><td>报告添加附件</td></tr></tbody></table>

```
 import pytest
 import allure
 @allure.feature('这里是一级标签')
 class TestAllure():
     @allure.title("用例标题0")
     @allure.story("这里是第一个二级标签")
     @allure.title("用例标题1")
     @allure.story("这里是第二个二级标签")
     def test_1(self):
         allure.attach.file(r'E:\Myproject\pytest-allure\test\test_1.jpg', '我是附件截图的名字', attachment_type=allure.attachment_type.JPG)
 
     @allure.title("用例标题2")
     @allure.story("这里是第三个二级标签")
     
 @allure.severity("critical")
 @allure.description("这里只是做一个web ui自动化的截图效果")
```

**3、设置用例级别**

```
 pytest  -sq --alluredir=../report/tmp --allure-severities=normal,critical
```

```
 import pytest
 import allure
 
 '''
 @allure.severity装饰器按严重性级别来标记case　　　
 执行指定测试用例 --allure-severities blocker
 BLOCKER = 'blocker'　　阻塞缺陷
 CRITICAL = 'critical'　严重缺陷
 NORMAL = 'normal'　　  一般缺陷
 MINOR = 'minor'　　    次要缺陷
 TRIVIAL = 'trivial'　　轻微缺陷　
 '''
 
 
 @allure.severity("normal")
 def test_case_1():
     '''修改个人信息-sex参数为空'''
     print("test case 11111111")
     
 
 @allure.severity("critical")
 def test_case_2():
     '''修改个人信息-sex参数传F和M两种类型，成功(枚举类型)'''
     print("test case 222222222")
 
 
 @allure.severity("critical")
 def test_case_3():
     '''修改个人信息-修改不是本人的用户信息，无权限操作'''
     print("test case 333333333")
 
 @allure.severity("blocker")
 def test_case_4():
     '''修改个人信息-修改自己的个人信息，修改成功'''
     print("test case 4444444")
 
 
 def test_case_5():
     '''没标记severity的用例默认为normal'''
     print("test case 5555555555")
```

**4、设置 allure 显示环境**

在 Allure 报告中添加环境信息，通过创建 environment.properties 或者 environment.xml 文件，并把文件存放到 allure-results(这个目录是生成最后的 html 报告之前，生成依赖文件的目录) 目录下

environment.properties

```
 Browser=Firefox
 Browser.Version=77
 Stand=songqin_teach
 ApiUrl=127.0.0.1/login
 python.Version=3.6
```

1、安装 Faker
----------

使用 pip 安装

```
 pip install Faker
```

或者去 Faker 的 [PyPI 页面](https://link.zhihu.com/?target=https%3A//pypi.org/project/Faker/)下载 Wheel 或者 Source 文件进行安装：

2、基本用法
------

```
 from faker import Faker   # 1

   fake = Faker()            # 2

   fake.name()               # 3
 # Donna Kelly

   fake.address()            # 4
 # 519 Donna River
 # Port Natalie, SD 87384
```

\1. 从 faker 模块导入 Faker 这个类。

\2. 实例化，保存到变量 fake 中。

\3. 调用 name() 方法随机生成一个名字。

\4. 调用 address() 方法随机生成地址信息。

如果要生成中文的随机数据，我们可以在实例化时给 locale 参数传入‘zh_CN’这个值：

```
 from faker import Faker           

   fake = Faker(locale='zh_CN')       

   fake.name()                        
 # 庞超

   fake.address()                     
 # 河北省辛集县合川张街p座 489476
```

如果要生成中文繁体字，则可以传入中国台湾这个地区的值'zh_TW'(当然地址显示的是台湾的)：

```
 from faker import Faker           

   fake = Faker(locale='zh_TW')       

   fake.name()                        
 # 羅婉婷

   fake.address()                     
 # 16934 大里縣水源巷35號之0
```

如果要生成其他语种或地区的数据，我们可以传入相应的地区值：

```
 ar_EG - Arabic (Egypt)
 ar_PS - Arabic (Palestine)
 ar_SA - Arabic (Saudi Arabia)
 bg_BG - Bulgarian
 bs_BA - Bosnian
 cs_CZ - Czech
 de_DE - German
 dk_DK - Danish
 el_GR - Greek
 en_AU - English (Australia)
 en_CA - English (Canada)
 en_GB - English (Great Britain)
 en_NZ - English (New Zealand)
 en_US - English (United States)
 es_ES - Spanish (Spain)
 es_MX - Spanish (Mexico)
 et_EE - Estonian
 fa_IR - Persian (Iran)
 fi_FI - Finnish
 fr_FR - French
 hi_IN - Hindi
 hr_HR - Croatian
 hu_HU - Hungarian
 hy_AM - Armenian
 it_IT - Italian
 ja_JP - Japanese
 ka_GE - Georgian (Georgia)
 ko_KR - Korean
 lt_LT - Lithuanian
 lv_LV - Latvian
 ne_NP - Nepali
 nl_NL - Dutch (Netherlands)
 no_NO - Norwegian
 pl_PL - Polish
 pt_BR - Portuguese (Brazil)
 pt_PT - Portuguese (Portugal)
 ro_RO - Romanian
 ru_RU - Russian
 sl_SI - Slovene
 sv_SE - Swedish
 tr_TR - Turkish
 uk_UA - Ukrainian
 zh_CN - Chinese (China Mainland)
 zh_TW - Chinese (China Taiwan)
```

3、其他方法
------

> 注：个别方法具有针对性，比如 province() 方法适用中国，但不适用美国及其他一些国家。

地址相关

```
 fake.address()            # 地址
 # '香港特别行政区大冶县上街钟街k座 664713'

   fake.building_number()    # 楼名    
 # 'v座'

   fake.city()               # 完整城市名
 # '长春县'

   fake.city_name()          # 城市名字(不带市县)
 # '梧州'

   fake.city_suffix()        # 城市后缀名
 # '市'

   fake.country()            # 国家名称
 # '厄立特里亚'

   fake.country_code(representation="alpha-2")
 # 'BZ'                    # 国家编号

   fake.district()           # 地区
 # '沙湾'

   fake.postcode()           # 邮编
 # '332991'

   fake.province()           # 省
 # '河北省'

   fake.street_address()     # 街道地址
 # '武汉街D座'

   fake.street_name()        # 街道名称
 # '广州路'

   fake.street_suffix()      # 街道后缀名
 # '路'
```

汽车相关

```
 fake.license_plate()      # 牌照
 # 'ZCO 000'
```

银行相关

```
 fake.bank_country()          # 银行所属国家
 # 'GB'

   fake.bban()                  # 基本银行账号
 # 'TPET9323218579379'          

   fake.iban()                  # 国际银行代码
 # 'GB82IRVM1531009974701'
```

条形码相关

```
 fake.ean(length=13)    # EAN条形码
 # '5456457843465'

   fake.ean13()           # EAN13条形码
 # '2689789887590'

   fake.ean8()            # EAN8条形码
 # '52227936'
```

颜色相关

```
 fake.color_name()        # 颜色名称
 # 'Orange'

   fake.hex_color()         # 颜色十六进制值
 # '#a5cb7c'

   fake.rgb_color()         # 颜色RGB值
 # '15,245,42'

   fake.rgb_css_color()     # CSS颜色值
 # 'rgb(15,70,13)'

   fake.safe_color_name()   # 安全色
 # 'aqua'

   fake.safe_hex_color()    # 安全色十六进制值
 # '#881100'
```

公司相关

```
 fake.bs()                 # 商业用词
 # 'synthesize strategic vortals'

   fake.catch_phrase()       # 妙句(口号)
 # 'Robust even-keeled service-desk'

   fake.company()            # 公司名称
 # '富罳科技有限公司'

   fake.company_prefix()     # 公司名称前缀
 # '商软冠联'

   fake.company_suffix()     # 公司名称后缀
 # '网络有限公司'
```

信用卡相关

```
 fake.credit_card_expire(start="now", end="+10y", date_format="%m/%y")    # 过期年月
 # '11/20'                                                

   fake.credit_card_full(card_type=None)            # 完整信用卡信息
 # 'VISA 16 digit\n秀珍 卢\n4653084445257690 11/19\nCVC: 935\n'

   fake.credit_card_number(card_type=None)          # 信用卡卡号
 # '4339481813664365360'

   fake.credit_card_provider(card_type=None)        # 信用卡提供商
 # 'VISA 19 digit'

   fake.credit_card_security_code(card_type=None)   # 信用卡安全码
 # '597'
```

货币相关

```
 fake.cryptocurrency()           # 加密货币代码+名称
 # ('TRX', 'TRON')

   fake.cryptocurrency_code()      # 加密货币代码
 # 'MZC'

   fake.cryptocurrency_name()      # 加密货币名称
 # 'Ripple'

   fake.currency()                 # 货币代码+名称
 # ('GNF', 'Guinean franc')

   fake.currency_code()            # 货币代码
 # 'SOS'

   fake.currency_name()            # 货币名称
 # 'Lebanese pound'
```

时间相关

```
 fake.am_pm()        # AM或PM
 # 'PM'

   fake.century()      # 世纪
 # 'XII'

   fake.date(pattern="%Y-%m-%d", end_datetime=None)            # 日期字符串(可设置格式和最大日期)
 # '1998-05-13'

   fake.date_between(start_date="-30y", end_date="today")      # 日期(可设置限定范围)
 # datetime.date(2014, 8, 17)

   fake.date_between_dates(date_start=None, date_end=None)     # 同上
 # datetime.date(2019, 10, 14)

   fake.date_object(end_datetime=None)                         # 日期(可设置最大日期)
 # datetime.date(1981, 12, 20)

   fake.date_of_birth(tzinfo=None, minimum_age=0, maximum_age=115)    # 出生日期
 # datetime.date(1931, 12, 8)

   fake.date_this_century(before_today=True, after_today=False)       # 本世纪日期
 # datetime.date(2003, 5, 4)

   fake.date_this_decade(before_today=True, after_today=False)        # 本年代中的日期
 # datetime.date(2014, 1, 29)

   fake.date_this_month(before_today=True, after_today=False)         # 本月中的日期
 # datetime.date(2019, 10, 10)

   fake.date_this_year(before_today=True, after_today=False)          # 本年中的日期
 # datetime.date(2019, 3, 6)

   fake.date_time(tzinfo=None, end_datetime=None)                     # 日期和时间
 # datetime.datetime(1990, 8, 11, 22, 25)

   fake.date_time_ad(tzinfo=None, end_datetime=None, start_datetime=None)    # 日期和时间(从001年1月1日到现在)
 # datetime.datetime(244, 12, 17, 9, 59, 56)

   fake.date_time_between(start_date="-30y", end_date="now", tzinfo=None)    # 日期时间(可设置限定范围)
 # datetime.datetime(1995, 4, 19, 17, 23, 51)

   fake.date_time_between_dates(datetime_start=None, datetime_end=None, tzinfo=None)    # 同上
 # datetime.datetime(2019, 10, 14, 14, 15, 36)                                  

   fake.date_time_this_century(before_now=True, after_now=False, tzinfo=None)     # 本世纪中的日期和时间
 # datetime.datetime(2009, 8, 26, 18, 27, 9)

   fake.date_time_this_decade(before_now=True, after_now=False, tzinfo=None)      # 本年代中的日期和时间
 # datetime.datetime(2019, 2, 24, 22, 18, 44)

   fake.date_time_this_month(before_now=True, after_now=False, tzinfo=None)       # 本月中的日期和时间
 # datetime.datetime(2019, 10, 3, 9, 20, 44)

   fake.date_time_this_year(before_now=True, after_now=False, tzinfo=None)        # 本年中的日期和时间
 # datetime.datetime(2019, 2, 10, 7, 3, 18)

   fake.day_of_month()   # 几号
 # '23'

   fake.day_of_week()    # 星期几
 # 'Tuesday'

   fake.future_date(end_date="+30d", tzinfo=None)        # 未来日期
 # datetime.date(2019, 10, 28)

   fake.future_datetime(end_date="+30d", tzinfo=None)    # 未来日期和时间
 # datetime.datetime(2019, 10, 28, 21, 4, 35)

   fake.iso8601(tzinfo=None, end_datetime=None)          # iso8601格式日期和时间
 # '1995-04-10T00:45:01'

   fake.month()                                          # 第几月
 # '07'

   fake.month_name()                                     # 月份名称
 # 'December'

   fake.past_date(start_date="-30d", tzinfo=None)        # 过去日期
 # datetime.date(2019, 10, 3)

   fake.past_datetime(start_date="-30d", tzinfo=None)    # 过去日期和时间
 # datetime.datetime(2019, 9, 30, 20, 25, 43)

   fake.time(pattern="%H:%M:%S", end_datetime=None)      # 时间(可设置格式和最大日期时间)
 # '14:26:44'

   fake.time_delta(end_datetime=None)                    # 时间间隔
 # datetime.timedelta(0)

   fake.time_object(end_datetime=None)                   # 时间(可设置最大日期时间)
 # datetime.time(4, 41, 39)

   fake.time_series(start_date="-30d", end_date="now", precision=None, distrib=None, tzinfo=None)
 #

   fake.timezone()    # 时区
 # 'Asia/Baku'

   fake.unix_time(end_datetime=None, start_datetime=None)    # UNIX时间戳
 # 393980728

   fake.year()        # 某年
 # '2016'
```

文件相关

```
 fake.file_extension(category=None)                # 文件扩展名
 # 'avi'

   fake.file_name(category=None, extension=None)     # 文件名
 # '专业.pptx'

   fake.file_path(depth=1, category=None, extension=None)    # 文件路径
 # '/的话/以上.ods'

   fake.mime_type(category=None)                     # MIME类型
 # 'application/xop+xml'

   fake.unix_device(prefix=None)                     # UNIX设备
 # '/dev/xvdq'

   fake.unix_partition(prefix=None)                  # UNIX分区
 # '/dev/xvdc6'
```

坐标相关

```
 fake.coordinate(center=None, radius=0.001)        # 坐标
 # Decimal('147.543284')

   fake.latitude()                                   # 纬度
 # Decimal('66.519139')

   fake.latlng()                                     # 经纬度
 # (Decimal('55.3370965'), Decimal('-15.427896'))

   fake.local_latlng(country_code="US", coords_only=False)    # 返回某个国家某地的经纬度
 # ('25.67927', '-80.31727', 'Kendall', 'US', 'America/New_York')

   fake.location_on_land(coords_only=False)                   # 返回地球上某个位置的经纬度
 # ('42.50729', '1.53414', 'les Escaldes', 'AD', 'Europe/Andorra')

   fake.longitude()                                   # 经度
 # Decimal('70.815233')
```

网络相关

```
 fake.ascii_company_email(*args, **kwargs)        # 企业邮箱(ascii编码)
 # 'qiuyan@xiulan.cn'

   fake.ascii_email(*args, **kwargs)                # 企业邮箱+免费邮箱(ascii编码)
 # 'lei59@78.net'

   fake.ascii_free_email(*args, **kwargs)           # 免费邮箱(ascii编码)
 # 'pcheng@gmail.com'

   fake.ascii_safe_email(*args, **kwargs)           # 安全邮箱(ascii编码)
 # 'fangyan@example.org'

   fake.company_email(*args, **kwargs)              # 企业邮箱
 # 'scao@pingjing.net'

   fake.domain_name(levels=1)                       # 域名
 # 'dy.cn'

   fake.domain_word(*args, **kwargs)                # 二级域名
 # 'gangxiuying'

   fake.email(*args, **kwargs)                      # 企业邮箱+免费邮箱
 # 'na13@ding.cn'

   fake.free_email(*args, **kwargs)                 # 免费邮箱
 # 'fang48@hotmail.com'

   fake.free_email_domain(*args, **kwargs)          # 免费邮箱域名
 # 'yahoo.com'

   fake.hostname(*args, **kwargs)                   # 主机名
 # 'lt-70.53.cn'

   fake.image_url(width=None, height=None)          # 图片URL
 # 'https://placekitten.com/752/243'

   fake.ipv4(network=False, address_class=None, private=None)    # ipv4
 # '160.152.149.78'

   fake.ipv4_network_class()                                     # ipv4网络等级
 # 'b'

   fake.ipv4_private(network=False, address_class=None)          # 私有ipv4
 # '10.99.124.57'

   fake.ipv4_public(network=False, address_class=None)           # 公共ipv4
 # '169.120.29.235'

   fake.ipv6(network=False)                                      # ipv6
 # 'f392:573f:d60f:9aed:2a4c:36d7:fe5b:7034'

   fake.mac_address()                            # MAC地址
 # '62:67:79:8c:c2:40'

   fake.safe_email(*args, **kwargs)              # 安全邮箱
 # 'jing58@example.org'

   fake.slug(*args, **kwargs)                    # URL中的slug
 # ''

   fake.tld()                                    # 顶级域名
 # 'cn'

   fake.uri()                                    # URI
 # 'http://yi.com/list/main/explore/register.php'

   fake.uri_extension()                          # URI扩展
 # '.php'

   fake.uri_page()                               # URI页
 # 'terms'

   fake.uri_path(deep=None)                      # URI路径
 # 'blog/tags/blog'

   fake.url(schemes=None)                        # URL
 # 'http://liutao.cn/'

   fake.user_name(*args, **kwargs)               # 用户名
 # 'xiulan80'
```

图书相关

```
 fake.isbn10(separator="-")        # ISBN-10图书编号
 # '0-588-73943-X'

   fake.isbn13(separator="-")        # ISBN-13图书编号
 # '978-1-116-51399-8'
```

职位相关

```
 fake.job()        # 职位
 # '法务助理'
```

文本相关

```
 fake.paragraph(nb_sentences=3, variable_nb_sentences=True, ext_word_list=None)    # 单个段落
 # '最新事情生产.方面解决名称责任而且.类型其实内容发生电脑.音乐具有今年是一.'

   fake.paragraphs(nb=3, ext_word_list=None)                                         # 多个段落                                            
 # ['使用评论管理.没有广告工作评论是否.', '帖子而且专业.这些比较完全发现准备设计工具.', '完成详细发生空间汽车.新闻电影您的游戏这种操作网站知道.']

   fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)    # 单个句子
 # '直接这样点击单位对于时候.'

   fake.sentences(nb=3, ext_word_list=None)                                 # 多个句子
 # ['电话国际项目管理.', '软件之后提高一样次数电影规定.', '东西会员发展什么不断经济.']

   fake.text(max_nb_chars=200, ext_word_list=None)                          # 单个文本
 # ('资源信息得到因此开发资源资料.\n'
 #  '国家这样等级需要用户如此.电话非常一切游戏所以学校类型.不要正在如果来源认为投资在线.\n'
 #  '这些更新密码其中起来实现有些.以上事情重要通过.\n'
 #  '但是就是介绍最大深圳简介设计.历史这种可以出现中心社区.\n'
 #  '政府当然包括简介全国内容生活.有些地址以上.回复这些来自搜索现在不断经营不断.\n'
 #  '操作为什孩子报告东西拥有如此.相关特别业务日本这种.合作问题准备比较谢谢.')

   fake.texts(nb_texts=3, max_nb_chars=200, ext_word_list=None)             # 多个文本
 # [   '地址控制无法正在必须中心积分一些.支持制作安全.\n'
 #     '比较最新最大她的功能能够是一.主题选择当前显示.\n'
 #     '的话社会现在地区阅读继续所有.美国数据正在深圳不能.\n'
 #     '能够查看其中生活商品.谢谢认为之后以及以下之后这里.\n'
 #     '活动支持人民这么今年.要求包括生活运行技术社会.\n'
 #     '当前更多游戏.下载一点开发论坛法律为了美国.\n'
 #     '如何更新个人谢谢作为还有论坛.销售销售法律学生这么责任一些.',
 #     '日本最大方法活动主题到了结果.教育还有孩子觉得简介出现国际.东西国家图片威望品牌.\n'
 #     '那些会员现在准备可能.威望部分文件主题东西业务一切之间.所以必须当前方法.\n'
 #     '等级大小重要可能下载孩子.来源感觉业务文件以后深圳学校.网络什么新闻都是安全.\n'
 #     '资料重要成功谢谢时候音乐安全相关.电脑系列日期.工具使用搜索来源首页.\n'
 #     '直接企业影响大小什么.相关品牌选择她的规定来源推荐.',
 #     '中文文化数据内容系统.他们这些之间深圳.\n'
 #     '联系城市出现部分都是政府生活.社会同时人民市场现在决定需要.其他政府简介深圳教育加入对于.\n'
 #     '运行是一语言安全通过大小学生.商品然后信息由于虽然.\n'
 #     '因为关于选择希望行业具有深圳.出现价格那么下载提高知道人员.设备直接显示事情帖子正在两个关于.\n'
 #     '系列公司大家.论坛所以完全文章标准.活动中国工具电脑.\n'
 #     '主题作者不能.进行国家系统地区增加.经验质量价格我的.']

   fake.word(ext_word_list=None)                                            # 单个词语
 # '新闻'

   fake.words(nb=3, ext_word_list=None, unique=False)                       # 多个词语
 # ['选择', '历史', '规定']
```

编

电话相关

```
fake.msisdn()                # 完整手机号码(加了国家和国内区号) # '9067936325890'   fake.phone_number()          # 手机号 # '18520149907'   fake.phonenumber_prefix()    # 区号 # 145
```

档案相关

```
fake.profile(fields=None, sex=None)        # 档案(完整) # {   'address': '河南省昆明市清河哈尔滨路H座 496152', #     'birthdate': datetime.date(2014, 11, 20), #     'blood_group': 'AB+', #     'company': '易动力信息有限公司', #     'current_location': (Decimal('77.504143'), Decimal('-167.365806')), #     'job': '培训策划', #     'mail': 'liangyang@yahoo.com', #     'name': '杨磊', #     'residence': '澳门特别行政区台北县西夏兴城街L座 803680', #     'sex': 'F', #     'ssn': '140722200004166520', #     'username': 'lei65', #     'website': [   'http://www.29.cn/', #                    'http://www.lei.cn/', #                    'http://lishao.net/', #                    'https://www.feng.net/']}   fake.simple_profile(sex=None)               # 档案(简单) # {   'address': '广西壮族自治区南宁市花溪孙街c座 653694', #     'birthdate': datetime.date(1993, 12, 16), #     'mail': 'haomin@yahoo.com', #     'name': '任秀英', #     'sex': 'F', #     'username': 'iding'}
```