> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37d7e2cb15cb)

> 目标：操作一个 tomcat 镜像，实现容器化，宿主机挂载执行 war 包

**目标：**操作一个 tomcat 镜像，实现容器化，宿主机挂载执行 war 包

**内容：**

*   1、基本 docker 技术简介
    

docker: 容器技术

k8s: 对容器的编排管理

*   2、docker 与传统的 vm 对比
    

*   vm    几个 G   几十 G
    
*   docker   mb  
    
*   vm    一般几十秒 或者几分钟
    
*   docker   几秒 或者几十毫秒
    
*   启动速度
    
*   大小
    

*   **3、docker 组成**
    
    **镜像：**相当于 python 里面类的概念，是静态的，不能直接使用
    
    **容器：**是通过镜像创建的，相当于实例的概念，是动态的，可以访问的
    
    **仓库：**存放镜像的地方
    
    **docker 宿主机：**你的 docker 安装在哪一个机器，那个机器就是宿主机
    

*   **1、镜像仓库**
    
*   **2、镜像**
    
*   **3、容器**
    

*   4、docker 操作
    

1、静态 IP 设置
----------

1、查看虚拟机的 ip

```
 ifconfig
```

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625643580155068776.png)

2、进入 ip 配置文件设置

```
 vi /etc/sysconfig/network-scripts/ifcfg-ens33
```

3、修改静态 ip、NETMASK、GATEWAY

```
 BOOTPROTO=static
 NETMASK=255.255.255.0
 GATEWAY=192.168.32.2
```

NETMASK  GATEWAY 具体设置什么，根据自己 VM 虚拟机的设置来

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625643613975054403.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625643648583023338.png)

4、复制一下到文件最后，保存退出  :wq

```
 DEVICE=ens33
 ONBOOT=yes
 # ZONE=public
 IPADDR=192.168.32.160
 NETMASK=255.255.255.0
 GATEWAY=192.168.32.2
 DNS1=8.8.8.8
 
```

5、最好设置后如下图

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625643692488037543.png)

6、重启网卡服务

```
 systemctl restart network
```

7、使用 xshell 工具连接

2、docker 进程相关操作
---------------

```
 root   sq
 
 #1- 查看docker 版本
 docker -v
 
 #2- 查看docker 命令
 docker --help
 
 #3- 查看docker 服务端与客户端版本详情
 docker version
 
 #4- 启动docker进程
 systemctl start docker
 
 #5- 关闭docker
 systemctl stop docker
 
 #6- 重启docker
 systemctl restart docker
 
 #7- 查看docker运行状态
 systemctl status docker
 
 #8- 关闭防火墙
 systemctl stop firewalld  #disable
```

**2、镜像操作**

```
 #镜像--容器的模板--打包好的---静态的概念--使用的时候请创建容器
 
 #需求：使用docker搭建一个tomcat/mysql/redis
 #操作流程：
  - 获取对应的镜像--docker pull  tomcat
   - 第一个使用官方的仓库地址
   - 自己的镜像仓库
  - 使用镜像创建容器
  - 运行容器
 
 
 #1- 查看镜像---本地的
 docker images
 
 #2- 搜索镜像--首先本地 --默认的地址
 docker search tomcat
 
 #3- 找到所有的镜像--拉取  pull
 docker pull tomcat #最新版本
 docker pull tomcat:7#最新版本
 
 docker images
 
 docker rmi 镜像id#删除镜像
 
 #4- 本地有了镜像--可以创建容器
 #根据tomcat镜像，创建一个myTomcat容器
 docker create --name=myTomcat tomcat#  只是创建，没有运行
 
 #5- 查看容器
 docker ps #查看在运行的容器
 docker ps -a#查看所有的容器
 #容器很多，筛选出你需要看的容器
 docker ps |grep tomcat
 
 #6- 运行容器
 docker start myTomcat #start 后面可以跟 容器的id（前2-3位） 或者 name
 
 #7- 停止容器
 docker stop myTomcat #start 后面可以跟 容器的id（前2-3位） 或者 name
 
 #8- 删除没有运行的容器
 docker rm 容器id或者名字
 
 #9- 删除在运行的容器
 docker rm -f 容器id或者名字
 
 #10- 删除所有的容器
 #先停止所有的容器
 docker stop $(docker ps -a -q)
 #删除
 docker rm $(docker ps -a -q)
 
 #删除所有镜像
 docker rmi $(docker images -q)
 
```

**3、运行容器的一些操作**

```
 #我需要搭建一个web项目－－－tomcat
 #1- 访问端口   你需要访问的是容器内部的端口
 #2- war项目文件这么传递给容器里的tomcat里webapps下
```

```
 #1- 查看docvker run 命令---创建并且启动容器
 docker run --help
 docker run 后面的参数
  -i #运行容器
  -t #容器启动后，进入命令行
  -v #目录映射--挂载
  -d #守护进程--后台运行
  -p #端口映射  -p 宿主机的端口:容器的端口  
      #如果容器里有tomcat  ，你本机的windows想访问：
   #docker tomcat 端口号是8080，需要在虚拟机映射一个端口   9099
   #windows才可以访问  http://虚拟机的ip:9099
 
 #2- 创建容器，并且进入命令行---进入容器
 docker run -di --name=mytomcat2 tomcat
 docker run -it --name=myTomcat tomcat /bin/bash
 
 #3- 退出
 exit #容器已经退出
 
 #4- 有没有一起启动方法，不退出容器
 #创建运行一个守护的容器
 docker run -id --name=myTomcat2 tomcat 
 #进入
 docker exec -it myTomcat2 /bin/bash
```

**4、**** 宿主机与 docker 容器的文件传递 **

```
 #1- 在宿主机创建一个文件
 touch xt.txt
 #2- 把改文件复制到容器里去
 docker cp xt.txt myTomcat2:/
 #3- 进入根目标
 cd /
 #查看
 ls
 
 #1- 怎么从容器中的文件copy  到宿主机里
 touch abc.txt
 #2- 退出容器
 exit
 #3- 
 docker cp myTomcat2:/abc.txt /root
```

**5、访问 tomcat**

```
 #1-启动并且作为守护进程
 #-p 宿主机的端口:容器里应用的端口 8080
 #war挂载宿主机 -v  宿主机的路径:容器路径    路径会新建
 docker run -di --name=myTomcat -p 8080:8080 -v /usr/local/tomcat/webapps:/usr/local/tomcat/webapps tomcat
 
 http://虚拟机ip:9999
 #2- 把war包放到宿主机的挂载目录里 ，直接刷新浏览器就出现项目的页面
 
 #微服务 k8s  容器式的性能测试
 
 
 #启动容器时关联其他容器   mysql独立的容器(给下账号密码。默认是root)
 
 docker run -di --name=tomcat2 -p 8080:8080   --link mysql  --link redis  --link rabbitmq

#tomcat容器里的配置文件在 /usr/local/tomcat/conf/server.xml
docker  cp  a2:/usr/local/tomcat/conf/server.xml /root
```

3、扩展
----

**1、镜像备份**

```
 #把tomcat镜像打包成文件，
 docker save -o tomcat.tar tomcat
 #把tomcat.tar放到其他电脑，其他人变成镜像使用
 docker load -i tomcat.tar
```

**2、****时间同步问题**

宿主机 linux 查看时间

```
 [root@localhost etc]# date
 2020年 12月 06日 星期日 04:38:43 CST
 
```

**时区是对的，只是时间不对**

安装 ntp

```
 yum install  ntp  #安装ntp服务
```

同步阿里时间

```
 docker cp /etc/localtime ec327fc07985:/etc/localtime
 
 #抛异常
 Error response from daemon: Error processing tar file(exit status 1):invalid symlink"/usr/share/zoneinfo/UCT" -> "../usr/share/zoneinfo/Asia/Shanghai”
 
 #解决办法
 docker cp /usr/share/zoneinfo/Asis/shanghai 容器id:/etc/localtime
 
 #重启容器
 docker restart 容器id
 
 ntpdate  ntpl.aliyun.com
```

**注意事项：**

*   可能网络会断开，请重新 reboot 下 linux
    
*   虚拟机的 ip 可能会改变
    

**3、轻量级图形页面管理之 portainer**

**portainer**   docker ui 页面的

dockerUI

1. 查看 portainer 镜像

```
 [root@localhost ~]# docker search portainer
```

2. 选择喜欢的 portainer 风格镜像，下载

```
 [root@localhost ~]# docker pull portainer/portainer
```

3. 启动 dockerui 容器

```
 [root@localhost ~] docker run -d --name portainerUI -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer
```

4、浏览器访问 http:// 你虚拟机的 ip:9000, 设置一个密码即可，点击创建用户  密码要 8 位以上

5、我们搭建的是单机版，进入页面直接选择 Local ，点击连接

**4、docker logs 命令**

`docker logs [options] 容器`获取容器的日志。

<table><thead><tr cid="n160" mdtype="table_row"><th>名字</th><th>默认值</th><th>描述</th></tr></thead><tbody><tr cid="n164" mdtype="table_row"><td>–details</td><td><br></td><td>显示提供给日志的额外细节</td></tr><tr cid="n168" mdtype="table_row"><td>–follow 或 - f</td><td><br></td><td>按日志输出</td></tr><tr cid="n172" mdtype="table_row"><td>–since</td><td><br></td><td>从某个时间开始显示，例如 2013-01-02T13:23:37</td></tr><tr cid="n176" mdtype="table_row"><td>–tail</td><td>all</td><td>从日志末尾多少行开始显示</td></tr><tr cid="n180" mdtype="table_row"><td>–timestamps 或 - t</td><td><br></td><td>显示时间戳</td></tr><tr cid="n184" mdtype="table_row"><td>–until</td><td><br></td><td>打印某个时间以前的日志，例如 2013-01-02T13:23:37</td></tr></tbody></table>

例如打印容器`mytest`应用后 10 行的内容

```
 docker logs -f --tail 10 a4dac74d48f7
 
 通过docker logs命令可以查看容器的日志。
 
 docker logs -f -t --tail 100 datacenter
 
 命令格式：
 
 $ docker logs [OPTIONS] CONTAINER
   Options:
         --details        显示更多的信息
     -f, --follow         跟踪实时日志
         --since string   显示自某个timestamp之后的日志，或相对时间，如42m（即42分钟）
         --tail string    从日志末尾显示多少行日志， 默认是all
     -t, --timestamps     显示时间戳
         --until string   显示自某个timestamp之前的日志，或相对时间，如42m（即42分钟）
 例子：
 
 查看指定时间后的日志，只显示最后100行：
 
 $ docker logs -f -t --since="2018-02-08" --tail=100 CONTAINER_ID
 查看最近30分钟的日志:
 
 $ docker logs --since 30m CONTAINER_ID
 查看某时间之后的日志：
 
 $ docker logs -t --since="2018-02-08T13:23:37" CONTAINER_ID
 查看某时间段日志：
 
 $ docker logs -t --since="2018-02-08T13:23:37" --until "2018-02-09T12:23:37" CONTAINER_ID
 
```

**5、修改已存在 docker 容器的端口映射**

**查看容器的挂载目录**

```
docker inspect 容器id | grep Mounts -A 20
```

修改已存在 docker 容器的端口映射

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210801/1627791223028071941.png)

**6、查看 docker 挂载目录**

```
docker inspect -f"{{.Mounts}}" 容器id
```

**7、设置容器开机启动**

一些需要服务器开机运行 docker 进程的时候，就可以直接启动某一个容器

```
docker run -id --name=xintianTomcat  --restart always  tomcat
```

4、Docker 相关操作
-------------

4.1、Docker 安装
-------------

### 1. 卸载旧版本

较旧的 Docker 版本称为 docker 或 docker-engine 。如果已安装这些程序，请卸载它们以及相关的依赖项。

```
 sudo yum remove  docker \
          docker-client \
          docker-client-latest \
          docker-common \
          docker-latest \
          docker-latest-logrotate \
          docker-logrotate \
          docker-engine
```

### 2. 安装 Docker Engine-Community

### 使用 Docker 仓库进行安装

在新主机上首次安装 Docker Engine-Community 之前，需要设置 Docker 仓库。之后，您可以从仓库安装和更新 Docker。

**设置仓库**

安装所需的软件包。yum-utils 提供了 yum-config-manager ，并且 device mapper 存储驱动程序需要 device-mapper-persistent-data 和 lvm2。

```
 sudo  yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
```

使用以下命令来设置稳定的仓库。

阿里云

```
 sudo  yum-config-manager \
   --add-repo \
   http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

### 3. 安装 Docker Engine-Community

安装最新版本的 Docker Engine-Community 和 containerd，或者转到下一步安装特定版本：

```
 sudo yum install docker-ce docker-ce-cli containerd.io
```

一直输入 y 回车即可。

### 4. 启动 docker

```
systemctl start docker

设置开机启动docker

systemctl enable docker
```

### 4.2、docker 加速

默认情况下，docker 下载镜像是从官网下载，下载速度特别特别的慢。使用阿里云加速器可以提升获取 Docker 官方镜像的速度。

1.  在指定目录创建文件（如已存在请忽略此步）：
    
    ```
     vi /etc/docker/daemon.json
    ```
    
2.  修改文件内容为：
    
    ```
     {
       "registry-mirrors": ["https://v2c6fjn8.mirror.aliyuncs.com"]
     }
    ```
    
    注意：此网址是从阿里云控制台复制过来的，每个登录用户都不一样。当然用上面的这个地址也可以。![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625643787139084498.png)
    
3.  重启 docker 服务
    
    ```
     systemctl restart docker
    ```
    

**面试问题：**你会使用 dockerfile 创建镜像？

**原理：**打包镜像一定要事先确认好你需要什么基础环境

*   **第 1 种：**使用 war 打包镜像
    

**1、当前路径，新建一个文件**

```
 vi dockerFile-tomcat
```

**2、编辑 dockerFile-tomcat**

```
 FROM  tomcat  #拉起基础镜像
 #把项目文件放进去
 COPY Shopping.war /usr/local/tomcat/webapps
```

**3、使用 ftp 把 Shopping.war 放到当前目录下**

**4- 打包镜像**

```
 docker build -f dockerFile-tomcat -t sqTomcat:v1 .
```

*   **第 2 种：**使用 jar 打包镜像
    
    **1、新建一个文件**
    
    ```
     vi dockerFile-sq
    ```
    

```
 FROM java:8#项目里最好指定版本
 WORKDIR  /opt/docker/images/metabase/
 ADD sq.jar  sq.jar
 EXPOSE  9999 #开放一个访问的端口
 CMD java -jar  sq.jar#运行java -jar xx.jar
```

**2- 打包镜像**

```
 docker build -f dockerFile-sq -t sq:v1 .
```