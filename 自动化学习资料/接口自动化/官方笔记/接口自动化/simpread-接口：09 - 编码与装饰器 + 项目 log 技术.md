> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37d2ec9b15b6)

> 编码处理的场景：

1、编码与装饰器
--------

**编码处理的场景：**

*   1、做爬虫、数据挖掘  响应数据有时候编码注意
    
*   2、一些通讯协议
    
*   3、做接口测试的需要！：代码级、jmeter
    

**encoding  是一个参数或者一个属性，直接使用的，可以 encoding=‘utf-8’**

### 1、编码

> *   位：计算机的最小单位 二进制中的一位 用二进制的 0/1 表示
>     
> *   字节：八位组成一个字节。
>     
> *   字符：我们肉眼可见的文字与符号。
>     
> *   字符集：字符的集合。
>     
> *   编码：将字符转换成计算机可识别的 0/1 代码。
>     
> *   解码：将计算机表示的 0/1 编码转换成肉眼可见的字符
>     

> GBK、GB2312 等与 UTF8 之间都必须通过 Unicode 编码才能相互转换：
> 
> GBK、GB2312－－Unicode－－UTF8
> 
> UTF8－－Unicode－－GBK、GB2312
> 
> GBK,GB2312 以及 Unicode 都既是字符集，也是编码方式，而 UTF-8 只是编码方式，并不是字符集
> 
> GBK 编码中英文字符只占一个字节

```
 字符串通过编码转换为字节码 str--->(encode)--->bytes
 字节码通过解码转换为字符串 bytes--->(decode)--->str
```

### 2、 装饰器

**装饰器概述**

> *   在装饰器本质：一个函数，该函数用来处理其他函数，它可以让其他函数在不修改代码前提下增加额外的功能，装饰器的返回值可以是一个函数对象。
>     
> *   使用场景：插入日志，事务处理，缓存，权限校验等
>     
> *   有了装饰器：可以抽离出大量与函数功能本身无关的雷同代码，并且可以继续复用
>     
> *   总结：为已存在的对象，增加额外的概念
>     

> 需求：领导想知道，该测试用例执行，服务器的响应时间是多少，我们怎么实现该代码？

```
 def foo():
     print('执行测试用例')
     time.sleep(1)
```

**1、实现**

```
 # 自动化测试场景
 import time
 def foo():
     print('执行测试用例')
     time.sleep(1)
 
 def show_time(func): 
     start_time = time.time()#开始时间
     func()#函数调用
     end_time = time.time()#结束时间
     print('服务器响应时间: ',end_time-start_time)
 
 show_time(foo)
```

> *   方逻辑上不难理解，功能也可以实现。
>     
> *   函数修改了名字，容易被其他同事投诉，因为我们每次都需要将一个函数作为一个参数传递给 show_time 函数。
>     
> *   这个方法已经破坏了原有代码的逻辑，之前的运行逻辑直接运行 foo 函数，但是现在不得不运行 show_time(foo)。
>     
> *   那该怎么更好的设计呢？
>     

**2、使用 - 装饰器方案改造**

```
 import time
 def show_time(func):
     def inner():
         start_time = time.time()#开始时间
         func()#函数调用
         end_time = time.time()#结束时间
         print('服务器响应时间: ',end_time-start_time)
     return inner#返回函数对象
 
 def foo():
     print('执行测试用例')
     time.sleep(1)
 
 foo=show_time(foo)
 foo()
```

> *   foo=show_time(foo)  foo()
>     
> *   但是每次执行一次都得重新赋值一次，麻烦，不够优雅
>     
> *   python 针对这个情况，提供一个完美的解决方案：语法糖 @
>     

```
 # 自动化测试场景
 import time
 def show_time(func):
     def inner():
         start_time = time.time()#开始时间
         func()#函数调用
         end_time = time.time()#结束时间
         print('服务器响应时间: ',end_time-start_time)
     return inner#返回函数对象
 
 @show_time #语法糖 等价 foo=show_time(foo)
 def foo():
     print('执行测试用例')
     time.sleep(1)
 
 foo()
 
 #输出结果
 执行测试用例
 服务器响应时间:  1.0
```

**3、带参数的函数装饰器**

```
 # 自动化测试场景
 import time
 def some_body_run(name):
     def show_time(func):
         def inner():
             start_time = time.time()#开始时间
             func()#函数调用
             end_time = time.time()#结束时间
             print('服务器响应时间: ',end_time-start_time)
             print('执行者：',name)
         return inner#返回函数对象
     return show_time
 
 @some_body_run('tom') #语法糖 等价 foo=some_body_run('tom')
 def foo():
     print('执行测试用例')
     time.sleep(1)
 
 foo()
 
 #输出结果
 执行测试用例
 服务器响应时间:  1.0
 执行者： tom
```

2、项目 log 技术
-----------

**日志查看：**

**方式：**

*   1、直接打开 xxx.log 就可以
    
*   2、像在 docker、k8s 集群  
    

*   docker logs -f  容器 id
    
*   kubectl logs -f  pod 名字
    

*   3、有些项目 log  是以 web 页面体现
    

作用：

*   分析问题
    
*   开发找 bug
    
*   性能测试分析性能
    
*   报错信息
    
*   定制化输出一些需要查看数据
    

```
 logging.basicConfig(format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s',
                             filename=f'../logs/{datetime.datetime.now().strftime("%Y-%m-%d-%H.%M.%S")}.txt',
                             level=logging.INFO,
                             filemode='a')
         return logging
```

> logging.basicConfig 函数各参数：
> 
> filename：指定日志文件名；
> 
> filemode：和 file 函数意义相同，指定日志文件的打开模式，'w'或者'a'；
> 
> format：指定输出的格式和内容，format 可以输出很多有用的信息，

<table><thead><tr cid="n121" mdtype="table_row"><th>参数</th><th>作用</th></tr></thead><tbody><tr cid="n124" mdtype="table_row"><td>%(levelno)s</td><td>打印日志级别的数值</td></tr><tr cid="n127" mdtype="table_row"><td>%(levelname)s</td><td>打印日志级别的名称</td></tr><tr cid="n130" mdtype="table_row"><td>%(pathname)s</td><td>打印当前执行程序的路径，其实就是 sys.argv[0]</td></tr><tr cid="n133" mdtype="table_row"><td>%(filename)s</td><td>打印当前执行程序名</td></tr><tr cid="n136" mdtype="table_row"><td>%(funcName)s</td><td>打印日志的当前函数</td></tr><tr cid="n139" mdtype="table_row"><td>%(lineno)d</td><td>打印日志的当前行号</td></tr><tr cid="n142" mdtype="table_row"><td>%(asctime)s</td><td>打印日志的时间</td></tr><tr cid="n145" mdtype="table_row"><td>%(thread)d</td><td>打印线程 ID</td></tr><tr cid="n148" mdtype="table_row"><td>%(threadName)s</td><td>打印线程名称</td></tr><tr cid="n151" mdtype="table_row"><td>%(process)d</td><td>打印进程 ID</td></tr></tbody></table>

```
 def Log():
 
         logging.basicConfig(format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s',
                             filename=f'../logs/{datetime.datetime.now().strftime("%Y-%m-%d-%H.%M.%S")}.txt',
                             level=logging.INFO,
                             filemode='a')
         return logging
```

*   **跳过 / 条件跳过**
    

*   如果您希望有条件地跳过某些内容，则可以使用 skipif 代替。 if 条件为真，跳过
    
*   跳过测试函数的最简单方法是使用跳过装饰器标记它，可以传递一个可选的原因
    
*   pytest.main(['-rs','test01.py']) 用 - rs 执行，跳过原因才会显示 SKIPPED [1] test01.py:415: 跳过 Test 类，会跳过类中所有方法
    
*   在我们自动化测试过程中，经常会遇到功能阻塞、功能未实现、环境等一系列外部因素问题导致的一些用例执行不了，这时我们就可以用到跳过 skip 用例，如果我们注释掉或删除掉，后面还要进行恢复操作
    
*   **跳过 --skip------- 相当于注释的效果**
    
*   **有条件的跳过 --skipif--**- 在执行过程中会对项目的一些前置条件进行判断 **
    

如果需要同时执行两个 py 文件，可以在 cmd 中在文件 py 文件所在目录下执行命令：

```
 pytest -s test_1.py test_2.py
```

3、Allure 报告优化
-------------

**1、浏览器打开 allure 报告**

建议使用火狐浏览器 -   谷歌是 loading 和 404     不要用 chrome,ie 浏览器打开

**2、定制化标签**

> @allure.epic("外卖系统") @allure.feature("商铺模块") @allure.tag("核心关注")

**allure 用例描述**

<table><thead><tr cid="n191" mdtype="table_row"><th>使用方法</th><th>参数值</th><th>参数说明</th></tr></thead><tbody><tr cid="n195" mdtype="table_row"><td>@allure.epic()</td><td>epic 描述</td><td>敏捷里面的概念，定义史诗，往下是 feature</td></tr><tr cid="n199" mdtype="table_row"><td>@allure.feature()</td><td>模块名称</td><td>功能点的描述，往下是 story</td></tr><tr cid="n203" mdtype="table_row"><td>@allure.story()</td><td>用户故事</td><td>用户故事，往下是 title</td></tr><tr cid="n207" mdtype="table_row"><td>@allure.title(用例的标题)</td><td>用例的标题</td><td>重命名 html 报告名称</td></tr><tr cid="n211" mdtype="table_row"><td>@allure.testcase()</td><td>测试用例的链接地址</td><td>对应功能测试用例系统里面的 case</td></tr><tr cid="n215" mdtype="table_row"><td>@allure.issue()</td><td>缺陷</td><td>对应缺陷管理系统里面的链接</td></tr><tr cid="n219" mdtype="table_row"><td>@allure.description()</td><td>用例描述</td><td>测试用例的描述</td></tr><tr cid="n223" mdtype="table_row"><td>@allure.step()</td><td>操作步骤</td><td>测试用例的步骤</td></tr><tr cid="n227" mdtype="table_row"><td>@allure.severity()</td><td>用例等级</td><td>blocker，critical，normal，minor，trivial</td></tr><tr cid="n231" mdtype="table_row"><td>@allure.link()</td><td>链接</td><td>定义一个链接，在测试报告展现</td></tr><tr cid="n235" mdtype="table_row"><td>@allure.attachment()</td><td>附件</td><td>报告添加附件</td></tr></tbody></table>

```
 import pytest
 import allure
 @allure.feature('这里是一级标签')
 class TestAllure():
     @allure.title("用例标题0")
     @allure.story("这里是第一个二级标签")
     @allure.title("用例标题1")
     @allure.story("这里是第二个二级标签")
     def test_1(self):
         allure.attach.file(r'E:\Myproject\pytest-allure\test\test_1.jpg', '我是附件截图的名字', attachment_type=allure.attachment_type.JPG)
 
     @allure.title("用例标题2")
     @allure.story("这里是第三个二级标签")
     
 @allure.severity("critical")
 @allure.description("这里只是做一个web ui自动化的截图效果")
```

**3、设置用例级别**

```
 pytest  -sq --alluredir=../report/tmp --allure-severities=normal,critical
```

```
 import pytest
 import allure
 
 '''
 @allure.severity装饰器按严重性级别来标记case　　　
 执行指定测试用例 --allure-severities blocker
 BLOCKER = 'blocker'　　阻塞缺陷
 CRITICAL = 'critical'　严重缺陷
 NORMAL = 'normal'　　  一般缺陷
 MINOR = 'minor'　　    次要缺陷
 TRIVIAL = 'trivial'　　轻微缺陷　
 '''
 
 
 @allure.severity("normal")
 def test_case_1():
     '''修改个人信息-sex参数为空'''
     print("test case 11111111")
     
 
 @allure.severity("critical")
 def test_case_2():
     '''修改个人信息-sex参数传F和M两种类型，成功(枚举类型)'''
     print("test case 222222222")
 
 
 @allure.severity("critical")
 def test_case_3():
     '''修改个人信息-修改不是本人的用户信息，无权限操作'''
     print("test case 333333333")
 
 @allure.severity("blocker")
 def test_case_4():
     '''修改个人信息-修改自己的个人信息，修改成功'''
     print("test case 4444444")
 
 
 def test_case_5():
     '''没标记severity的用例默认为normal'''
     print("test case 5555555555")
```

**4、设置 allure 显示环境**

在 Allure 报告中添加环境信息，通过创建 environment.properties 或者 environment.xml 文件，并把文件存放到 allure-results(这个目录是生成最后的 html 报告之前，生成依赖文件的目录) 目录下

environment.properties

```
 Browser=Firefox
 Browser.Version=77
 Stand=songqin_teach
 ApiUrl=127.0.0.1/login
 python.Version=3.6
```