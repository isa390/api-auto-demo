> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a2d3941407b99)

> 所有方法：186 个方法或属性，其中不一定都支持，特别要注意其支持的平台（版本）、驱动（版本）、appium 版本等信息。你如果全部都是最新的一般都没问题（除非未实现）

高级操作
----

*   所有方法：186 个方法或属性，其中不一定都支持，特别要注意其支持的平台（版本）、驱动（版本）、appium 版本等信息。你如果全部都是最新的一般都没问题（除非未实现）
    
*   appium 官网并没有对所有的操作都有说明，而说明的有的也是错的
    

### 会话 session

#### 创建会话 Remote

*   示例
    
    ```
    desired_caps = {
      'platformName': 'Android',
      'platformVersion': '7.0',
      'deviceName': 'Android Emulator',
      'automationName': 'UiAutomator2',
      'app': PATH('/path/to/app')
    }
    self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
    
    ```
    

#### 退出会话  quit

*   示例
    
    ```
    driver.quit()
    
    ```
    

#### 获取会话能力 Get Session Capabilities

*   示例代码：Remote 连接后打印下
    
    ```
    ...#Remote连接
    pprint(driver.session)
    
    ```
    
*   示例输出
    
    ```
    {'appActivity': 'com.ddnapalon.calculator.gp.StartActivity',
     'appPackage': 'com.ddnapalon.calculator.gp',
     'autoGrantPermissions': True,
     'databaseEnabled': False,
     'desired': {'appActivity': 'com.ddnapalon.calculator.gp.StartActivity',
                 'appPackage': 'com.ddnapalon.calculator.gp',
                 'autoGrantPermissions': True,
                 'platformName': 'android',
                 'platformVersion': '11'},   #这是你的能力值，其他都不是，但有的属性值迷惑性很强
     'deviceApiLevel': 30,
     'deviceManufacturer': 'Google',
     'deviceModel': 'sdk_gphone_x86',
     'deviceName': 'emulator-5554',
     'deviceScreenDensity': 440,
     'deviceScreenSize': '1080x2220',
     'deviceUDID': 'emulator-5554',
     'javascriptEnabled': True,
     'lastScrollData': None,
     'locationContextEnabled': False,
     'networkConnectionEnabled': True,
     'pixelRatio': 2.75,
     'platform': 'LINUX',
     'platformName': 'android',
     'platformVersion': '11',
     'statBarHeight': 66,
     'takesScreenshot': True,
     'viewportRect': {'height': 2022, 'left': 0, 'top': 66, 'width': 1080},
     'warnings': {},
     'webStorageEnabled': False}
    
    ```
    

#### 返回 back

*   示例，相当于返回键
    
    ```
    driver.back()
    
    ```
    

#### 页面截图 get_screenshot_as_base64

*   示例
    
    ```
    import base64
    screenshotBase64 = driver.get_screenshot_as_base64()
    png = base64.b64decode(screenshotBase64)
    with open('2.png','wb') as f:
        f.write(bytes(png))
    
    ```
    
*   推荐用 selenium 中的 driver.save_screenshot('1.png') 这种方法
    

#### 页面源代码 page_source

*   示例
    
    ```
    driver.page_source
    
    ```
    

#### implicitly_wait 隐式等待

*   在手机测试中由于波动性更大，一般都建议加上
    

* * *

### 设备 device

#### 活动 activity

<table><thead><tr cid="n2554" mdtype="table_row"><th>方法</th><th>示例</th><th>说明</th></tr></thead><tbody><tr cid="n2558" mdtype="table_row"><td>start_activity(包名, Activity 名);</td><td>driver.start_activity('com.kejia.mine','.app.Mine')</td><td>启动指定的包，需要指定 activity</td></tr><tr cid="n2562" mdtype="table_row"><td>current_activity</td><td>driver.current_activity</td><td>当前的活动</td></tr><tr cid="n2566" mdtype="table_row"><td>current_package</td><td>driver.current_package</td><td>当前的包名</td></tr></tbody></table>

*   属性 2、3 是一种获取包名和 activity 名的手段，但要注意 activity 是会变化的（比如微信在启动的时候会有多个页面，页面的切换，有的时候你获取的 activity 可能无法用来启动）
    

#### 软件包操作 app

<table><thead><tr cid="n2576" mdtype="table_row"><th>方法</th><th>示例</th><th>说明</th></tr></thead><tbody><tr cid="n2580" mdtype="table_row"><td>install_app(apk 位置)</td><td>driver.install_app('d:\com.kejia.mine.apk')</td><td>安装本地软件包</td></tr><tr cid="n2584" mdtype="table_row"><td>isAppInstalled(包名)</td><td>driver.isAppInstalled("com.kejia.mine")</td><td>是否安装了软件，返回 T/F</td></tr><tr cid="n2588" mdtype="table_row"><td>launch_app()</td><td>driver.launch_app()</td><td>运行 des_cap 传递过来的值</td></tr><tr cid="n2592" mdtype="table_row"><td>close_app()</td><td>driver.close_app()</td><td>关闭 des_cap 带起来的 app</td></tr><tr cid="n2596" mdtype="table_row"><td>background_app(秒数)</td><td>driver.background_app(10)</td><td>app 在后台运行 10 秒后回来</td></tr><tr cid="n2600" mdtype="table_row"><td>reset()</td><td>driver.reset()</td><td>重启启动的 app</td></tr><tr cid="n2604" mdtype="table_row"><td>remove_app(包名)</td><td>driver.remove_app('com.kejia.mine')</td><td>卸载某个 APP</td></tr><tr cid="n2608" mdtype="table_row"><td>activate_app(包名)</td><td>driver.activate_app('com.apple.Preferences')</td><td>激活某个 APP</td></tr><tr cid="n2612" mdtype="table_row"><td>terminate_app(包名)</td><td>driver.terminate_app('com.apple.Preferences')</td><td>终止某个 APP</td></tr><tr cid="n2616" mdtype="table_row"><td>query_app_state(包名)</td><td>print(driver.query_app_state('com.kejia.mine'))</td><td>查询 app 状态</td></tr><tr cid="n2620" mdtype="table_row"><td>app_strings()</td><td>print(driver.app_strings())</td><td>查询 app 的字符串，需要跟随 des_cap</td></tr></tbody></table>

*   安装 app 的时候 apk 需要在本地，而且支持比较丰富的参数
    
    ```
    driver.install_app(app_path='d:\\com.kejia.mine.apk',
                       replace = True,  #是否覆盖
                       timeout = 60000, #超时时间，默认60秒
                       useSdcard = True, #是否使用sdcard
                       grantPermissions=True) #是否自动授权
    
    ```
    
*   background_app 结束后 app 回到前台
    
*   query_app_state
    
    ```
    0 is not installed.        没有安装
    1 is not running.      不在运行
    2 is running in background or suspended.   后台运行或挂起    #未测试出是什么状态
    3 is running in background.   在后台    #手工测试
    4 is running in foreground.   在前台
    
    ```
    
*   以下三个方法需要在 des_cap 中启动 app
    

*   launch_app()
    
*   close_app()
    
*   app_strings()
    

*   reset，有地方声称是卸载重装，有地方是说重置。
    

#### 锁屏相关 interaction

<table><thead><tr cid="n2647" mdtype="table_row"><th>方法</th><th>示例</th><th>说明</th></tr></thead><tbody><tr cid="n2651" mdtype="table_row"><td>lock(秒)</td><td>driver.lock();</td><td>锁屏 N 秒后自动解锁</td></tr><tr cid="n2655" mdtype="table_row"><td>unlock()</td><td>driver.unlock();</td><td>解锁</td></tr><tr cid="n2659" mdtype="table_row"><td>is_locked()</td><td>driver.is_locked()</td><td>是否锁屏</td></tr><tr cid="n2663" mdtype="table_row"><td><br></td><td><br></td><td><br></td></tr><tr cid="n2667" mdtype="table_row"><td><br></td><td><br></td><td><br></td></tr></tbody></table>

*   可以配合 driver.press_keycode(26) 进行测试
    

#### 文件读写 files

<table><thead><tr cid="n2677" mdtype="table_row"><th>方法</th><th>示例</th><th>说明</th></tr></thead><tbody><tr cid="n2681" mdtype="table_row"><td>push_file</td><td><br></td><td>将内容写入文件</td></tr><tr cid="n2685" mdtype="table_row"><td>pull_file</td><td><br></td><td>从文件中读取内容</td></tr></tbody></table>

*   参考代码：需要理解 base64 编解码
    
    ```
    from appium import webdriver
    import base64
    des_cap = {}
    des_cap['platformName']='android'
    des_cap['platformVersion']='11'
    driver = webdriver.Remote(command_executor='http://127.0.0.1:4723/wd/hub',
                              desired_capabilities=des_cap)
    dest_file = '/data/local/tmp/push.txt'
    # data = b'appium'
    # #如果要携带中文信息,需要带上编码信息
    content = bytes('松勤自动化测试','utf-8')
    driver.push_file(dest_file, base64.b64encode(content).decode('utf-8')) #将数据写入到目标文件中
    file_content = driver.pull_file(dest_file) #str类型，但经过了编解码
    print(str(base64.b64decode(file_content),encoding='utf-8'))
    
    ```
    

#### 按键

> 键码：[https://developer.android.google.cn/reference/android/view/KeyEvent.html](https://developer.android.google.cn/reference/android/view/KeyEvent.html)

<table><thead><tr cid="n2698" mdtype="table_row"><th>方法</th><th>示例</th><th>说明</th></tr></thead><tbody><tr cid="n2702" mdtype="table_row"><td>press_keycode(键码)</td><td>driver.press_keycode(10)</td><td>按 XX 键</td></tr><tr cid="n2706" mdtype="table_row"><td>long_press_keycode(键码);</td><td>driver.long_press_keycode(10);</td><td>长按 XX 键</td></tr><tr cid="n2710" mdtype="table_row"><td>is_keyboard_shown()</td><td>driver.is_keyboard_shown()</td><td>是否显示输入法键盘</td></tr><tr cid="n2714" mdtype="table_row"><td>hide_keyboard()</td><td>driver.hide_keyboard()</td><td>隐藏键盘</td></tr></tbody></table>

*   常用键码
    
    <table><thead><tr cid="n2722" mdtype="table_row"><th>键</th><th>码</th></tr></thead><tbody><tr cid="n2725" mdtype="table_row"><td>拨号</td><td>5</td></tr><tr cid="n2728" mdtype="table_row"><td>HOME</td><td>3</td></tr><tr cid="n2731" mdtype="table_row"><td>电源</td><td>26</td></tr><tr cid="n2734" mdtype="table_row"><td>菜单</td><td>82</td></tr><tr cid="n2737" mdtype="table_row"><td>返回</td><td>4</td></tr><tr cid="n2740" mdtype="table_row"><td>挂机</td><td>6</td></tr><tr cid="n2743" mdtype="table_row"><td>音量 +</td><td>24</td></tr><tr cid="n2746" mdtype="table_row"><td>音量 -</td><td>25</td></tr><tr cid="n2749" mdtype="table_row"><td><br></td><td><br></td></tr></tbody></table>
*   此处的键盘是指输入法键盘，并非拨号键盘
    
*   long_press_keycode 默认没有时间控制
    
    ![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)
    

#### 其他（特定场景使用）

<table><thead><tr cid="n2762" mdtype="table_row"><th>条目</th><th>方法</th><th>说明</th></tr></thead><tbody><tr cid="n2766" mdtype="table_row"><td>clipboard</td><td>get_clipboard_text()</td><td>获取剪切板内容</td></tr><tr cid="n2770" mdtype="table_row"><td><br></td><td>get_clipboard()</td><td>对 Android 而言同上，只有一个参数 ClipboardContentType.PLAINTEXT，而且是默认值；返回值是 bytes 类型</td></tr><tr cid="n2774" mdtype="table_row"><td>network</td><td>toggle_location_services()</td><td>开关位置服务</td></tr><tr cid="n2778" mdtype="table_row"><td><br></td><td>toggle_wifi()</td><td>开关 wifi</td></tr><tr cid="n2782" mdtype="table_row"><td><br></td><td>send_sms('phone number','message')</td><td>发短信，仅限模拟器使用</td></tr><tr cid="n2786" mdtype="table_row"><td><br></td><td>make_gsm_call('phone number',GsmCallActions.CALL)</td><td>打电话，仅限模拟器使用</td></tr><tr cid="n2790" mdtype="table_row"><td>performance data</td><td>get_performance_data()</td><td>获取性能数据</td></tr><tr cid="n2794" mdtype="table_row"><td>system</td><td>open_notifications()</td><td>打开通知栏</td></tr><tr cid="n2798" mdtype="table_row"><td><br></td><td>get_system_bars()</td><td>参见下方示例数据</td></tr><tr cid="n2802" mdtype="table_row"><td><br></td><td>get_display_density()</td><td>dpi 数据</td></tr><tr cid="n2806" mdtype="table_row"><td><br></td><td>device_time</td><td>其实是个方法，被装饰了</td></tr><tr cid="n2810" mdtype="table_row"><td><br></td><td>get_device_time()</td><td>同上</td></tr><tr cid="n2814" mdtype="table_row"><td><br></td><td>get_device_time('YYYY-MM-DD HH:mm:ss')</td><td>时间格式化</td></tr></tbody></table>

*   appium 还支持 toggle_data、toggle_AIREPLANEMODE，但目前 Python 还不支持
    
*   性能数据: 目前只测出来一个 cpuinfo，其他的内存、网络、电量不知道具体的值是多少。
    
    ```
    print(driver.get_performance_data('com.smarthome.ohosure','cpuinfo',5))
    #[['user', 'kernel'], ['0', '0']]  可以理解为用户态和内核态的CPU占比
    
    #但不是所有的package都可以获取数据
    Original error: Unable to parse cpu usage data for 'com.kejia.mine' 
    
    ```
    
*   系统状态条
    
    ```
    get_system_bars()
    
    #{'statusBar': {'visible': True, 'x': 0, 'y': 0, 'width': 1080, 'height': 66}, 'navigationBar': {'visible': True, 'x': 0, 'y': 0, 'width': 1080, 'height': 132}}
    
    ```
    

### 交互 interactions

*   语法
    

```
from appium.webdriver.common.touch_action import TouchAction
touchaction = TouchAction(driver) #driver要定义好
touchaction.动作1
touchaction.动作2
touchaction.perform()  #最终执行

```

> > 以下所有跟 TouchAction 里面的操作相关

<table><thead><tr cid="n2895" mdtype="table_row"><th>隶属</th><th>方法</th><th>示例</th><th>说明</th></tr></thead><tbody><tr cid="n2898" mdtype="table_row"><td>driver</td><td>tap</td><td>driver.tap([(100, 20), (100, 60), (100, 100)], 500)</td><td>点击一个元素或某个坐标或一个元素上偏移一定位置</td></tr><tr cid="n5138" mdtype="table_row"><td><br></td><td>scroll</td><td>driver.scroll(el1, el2)</td><td>从一个元素滑动到另外一个元素</td></tr><tr cid="n2901" mdtype="table_row"><td><br></td><td>drag_and_drop</td><td>driver.drag_and_drop(el1,el2)</td><td>把一个元素按住拖动到另外一个元素</td></tr><tr cid="n2904" mdtype="table_row"><td><br></td><td>swipe</td><td>driver.swipe(100, 100, 100, 400,1000)</td><td>从某个元素位置开始滑动到另外一个坐标，用一定时间</td></tr><tr cid="n2907" mdtype="table_row"><td><br></td><td>flick</td><td>driver.flick(100, 100, 100, 400)</td><td>从某个元素位置开始滑动到另外一个坐标，没有时间的概念</td></tr><tr cid="n2910" mdtype="table_row"><td>TouchAction</td><td>tap</td><td>tap(ele)tap(x=?,y=?)tap(ele,count=2)</td><td>点击某个元素或某个坐标，1 次或多次</td></tr><tr cid="n2913" mdtype="table_row"><td><br></td><td>press</td><td>press(ele)press(x=?,y=?)</td><td>按某个元素或某个坐标，可给予一定压力 (iOS 支持)</td></tr><tr cid="n2916" mdtype="table_row"><td><br></td><td>long_press</td><td>long_press(ele,duration=1000)long_press(x=?,y=?,duration=1000)</td><td>长按某个元素或某个坐标一定时间</td></tr><tr cid="n2919" mdtype="table_row"><td><br></td><td>wait</td><td>wait(1000)</td><td>等待一定毫秒</td></tr><tr cid="n2922" mdtype="table_row"><td><br></td><td>move_to</td><td>move_to(x=?,y=?)move_to(ele1)</td><td>移动到某个元素或某个坐标</td></tr><tr cid="n2925" mdtype="table_row"><td><br></td><td>release</td><td>release()</td><td>释放</td></tr><tr cid="n2928" mdtype="table_row"><td><br></td><td>perform</td><td>perform()</td><td>执行，所有 TouchAction 的结束</td></tr></tbody></table>

**driver.tap 方法详解，不是 TouchAction 的 tap，用法不同**

>   

*   源代码
    
    ```
    def tap(self: T, positions: List[Tuple[int, int]], duration: Optional[int] = None) -> T:
        #第一个参数positions是一个列表，里面每个元素都是一个元组，(x坐标,y坐标)，最大不能超过5个手指。
        #第二个参数是一个时长，单位是毫秒
            if len(positions) == 1:#如果是一个元组（单根手指）
                action = TouchAction(self)
                x = positions[0][0]#获取第一个元素（元组）的第一个元素X坐标
                y = positions[0][1]#获取第一个元素（元组）的第一个元素Y坐标
                if duration:#如果有时长
                    action.long_press(x=x, y=y, duration=duration).release()#就用长按+释放
                else:
                    action.tap(x=x, y=y) #没有时长就用TouchAction(self).tap命令，两个命令不一样
                action.perform() #最后要执行霞
            else: #如果不是一个手指
                ma = MultiAction(self) #用多操作实例化
                for position in positions:#遍历手指
                    x = position[0] #以下逻辑同上
                    y = position[1]
                    action = TouchAction(self)
                    if duration:
                        action.long_press(x=x, y=y, duration=duration).release()
                    else:
                        action.press(x=x, y=y).release()
                    ma.add(action) #把操作加进来
    
                ma.perform() #最终执行
            return self#返回实例对象
            
    
    ```
    
*   **踩过的坑**：TouchAction(driver).tap(x=x 坐标, y=y 坐标)   #括号里面一定要写 x = 和 y=，因为第一个参数是 element
    
*   可以通过 AppiumInspector 录制来生成样板代码