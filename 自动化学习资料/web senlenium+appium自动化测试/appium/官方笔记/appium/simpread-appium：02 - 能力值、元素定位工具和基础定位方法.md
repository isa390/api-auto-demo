> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a2d3279da7b8a)

> 第二天：能力值介绍、元素定位工具

> 第二天：能力值介绍、元素定位工具

desired_capabilities 能力值
------------------------

*   它告诉 appium 服务器要做什么事情
    

*   用什么测试引擎
    
*   操作系统是什么
    
*   操作系统版本是什么
    
*   要测试哪个应用
    
*   应用的哪个界面
    
*   是否是浏览器等等
    

> [https://github.com/appium/appium/blob/master/docs/cn/writing-running-appium/caps.md](https://github.com/appium/appium/blob/master/docs/cn/writing-running-appium/caps.md)  中文

*   以 Android 为例，常见的参数如下
    

<table><thead><tr cid="n1383" mdtype="table_row"><th>键</th><th>描述</th><th>值</th><th>说明</th></tr></thead><tbody><tr cid="n1388" mdtype="table_row"><td>platformName</td><td>使用的手机操作系统</td><td>iOS, Android, 或者 FirefoxOS</td><td>忽略大小写但不能不写</td></tr><tr cid="n1393" mdtype="table_row"><td>platformVersion</td><td>手机操作系统的版本</td><td>如 11,11.0</td><td>需要匹配，可以通过 getprop 查询。但可以不写。</td></tr><tr cid="n1398" mdtype="table_row"><td>app</td><td>本地绝对路径或远程 http URL 所指向的一个安装包</td><td>如 r'D:\com.kejia.mine.apk'</td><td>这样会自动安装到设备上，并且带起。</td></tr><tr cid="n1403" mdtype="table_row"><td>appActivity</td><td>Activity 的名字是指从你的包中所要启动的 Android acticity</td><td>如'.app.Mine'</td><td>与 appPackage 组合使用，而且不是任意的 activity 都可以的</td></tr><tr cid="n1408" mdtype="table_row"><td>appPackage</td><td>运行的 Android 应用的包名</td><td>如'com.kejia.mine'</td><td><br></td></tr><tr cid="n1413" mdtype="table_row"><td>autoGrantPermissions</td><td>让 Appium 自动确定您的应用需要哪些权限，并在安装时将其授予应用。默认设置为 <code>false</code></td><td>Ture 或 False</td><td>如果设置为 True，不会弹出一些提示，不能与 noReset 一起用</td></tr><tr cid="n1418" mdtype="table_row"><td>browserName</td><td>做自动化时使用的浏览器名字。如果是一个应用则只需填写个空的字符串</td><td>'Safari' 对应 iOS，'Chrome', 'Chromium', 或'Browser' 则对应 Android</td><td><br></td></tr><tr cid="n1423" mdtype="table_row"><td>chromedriverExecutable</td><td>webdriver 可执行文件的绝对路径（如果 Chromium 内嵌一个自己提供的 webdriver，则应使用他去替换掉 Appium 自带的 chromedriver）</td><td><br></td><td><br></td></tr><tr cid="n1428" mdtype="table_row"><td>udid</td><td>连接真机的唯一设备号</td><td>可以通过它来控制连接哪个设备，而不是 deviceUDID、deviceName 这些</td><td><br></td></tr><tr cid="n1433" mdtype="table_row"><td>fullReset</td><td>(iOS) 删除所有的模拟器文件夹。(Android) 要清除 app 里的数据，请<strong>将应用卸载</strong>才能达到重置应用的效果。在 Android, 在 session 完成之后也会将应用卸载掉。默认值为 false</td><td>True 或 False</td><td>配合 app 参数使用，卸载重装</td></tr></tbody></table>

*   获取操作系统版本
    
    ```
    generic_x86_arm:/ $ getprop |grep  ro.build.version.release
    [ro.build.version.release]: [11]
    
    ```
    
*   首次连接会安装 3 个软件包
    
    ```
    generic_x86_arm:/ $ pm list packages -3
    package:io.appium.settings
    package:io.appium.uiautomator2.server
    package:io.appium.uiautomator2.server.test
    
    ```
    

**Appium 服务器初始化参数**

<table><thead><tr cid="n1449" mdtype="table_row"><th>键</th><th>描述</th><th>值</th></tr></thead><tbody><tr cid="n1453" mdtype="table_row"><td>automationName</td><td>自动化测试的引擎</td><td><code>Appium</code> （默认）或者 <code>Selendroid</code></td></tr><tr cid="n1457" mdtype="table_row"><td>platformName</td><td>使用的手机操作系统</td><td><code>iOS</code>, <code>Android</code>, 或者 <code>FirefoxOS</code></td></tr><tr cid="n1461" mdtype="table_row"><td>platformVersion</td><td>手机操作系统的版本</td><td>例如 <code>7.1</code>, <code>4.4</code></td></tr><tr cid="n1465" mdtype="table_row"><td>deviceName</td><td>使用的手机或模拟器类型</td><td><code>iPhone Simulator</code>, <code>iPad Simulator</code>, <code>iPhone Retina 4-inch</code>, <code>Android Emulator</code>, <code>Galaxy S4</code>, 等等.... 在 iOS 上，使用 Instruments 的 <code>instruments -s devices</code> 命令可返回一个有效的设备的列表。在 Andorid 上虽然这个参数目前已被忽略，但仍然需要添加上该参数</td></tr><tr cid="n1469" mdtype="table_row"><td>app</td><td>本地绝对路径或远程 http URL 所指向的一个安装包（<code>.ipa</code>,<code>.apk</code>, 或 <code>.zip</code> 文件）。Appium 将其安装到合适的设备上。请注意，如果您指定了 <code>appPackage</code> 和 <code>appActivity</code> 参数（见下文），Android 则不需要此参数了。该参数也与 <code>browserName</code> 不兼容。</td><td><code>/abs/path/to/my.apk</code> 或 <code>http://myapp.com/app.ipa</code></td></tr><tr cid="n1473" mdtype="table_row"><td>browserName</td><td>做自动化时使用的浏览器名字。如果是一个应用则只需填写个空的字符串</td><td>'Safari' 对应 iOS，'Chrome', 'Chromium', 或'Browser' 则对应 Android</td></tr><tr cid="n1477" mdtype="table_row"><td>newCommandTimeout</td><td>用于客户端在退出或者结束 session 之前，Appium 等待客户端发送一条新命令所花费的时间（秒为单位）</td><td>例如 <code>60</code></td></tr><tr cid="n1481" mdtype="table_row"><td>language</td><td>(Sim/Emu-only) 为模拟器设置语言</td><td>例如 <code>fr</code></td></tr><tr cid="n1485" mdtype="table_row"><td>locale</td><td>(Sim/Emu-only) 为模拟器设置所在区域</td><td>例如 <code>fr_CA</code></td></tr><tr cid="n1489" mdtype="table_row"><td>udid</td><td>连接真机的唯一设备号</td><td>例如 <code>1ae203187fc012g</code></td></tr><tr cid="n1493" mdtype="table_row"><td>orientation</td><td>(Sim/Emu-only) 模拟器当前的方向</td><td><code>竖屏</code> 或 <code>横屏</code></td></tr><tr cid="n1497" mdtype="table_row"><td>autoWebview</td><td>直接转换到 Webview 上下文（context）。默认值为 <code>false</code></td><td><code>true</code>, <code>false</code></td></tr><tr cid="n1501" mdtype="table_row"><td>noReset</td><td>在当前 session 下不会重置应用的状态。默认值为 <code>false</code></td><td><code>true</code>, <code>false</code></td></tr><tr cid="n1505" mdtype="table_row"><td>fullReset</td><td>(iOS) 删除所有的模拟器文件夹。(Android) 要清除 app 里的数据，请将应用卸载才能达到重置应用的效果。在 Android, 在 session 完成之后也会将应用卸载掉。默认值为 <code>false</code></td><td><code>true</code>, <code>false</code></td></tr></tbody></table>

*   不知为何，有个能力参数没有说：（个人怀疑是官方没有及时更新）
    
    ```
    'autoLaunch':False   #意义是app不会自动带起来，然后可以用launch_app()来运行它
    
    ```
    

**Android 特有**

<table><thead><tr cid="n1516" mdtype="table_row"><th>键</th><th>描述</th><th>值</th></tr></thead><tbody><tr cid="n1520" mdtype="table_row"><td>appActivity</td><td>Activity 的名字是指从你的包中所要启动的 Android acticity。他通常需要再前面添加<code>.</code> （例如 使用 <code>.MainActivity</code> 代替 <code>MainActivity</code>）</td><td><code>MainActivity</code>, <code>.Settings</code></td></tr><tr cid="n1524" mdtype="table_row"><td>appPackage</td><td>运行的 Android 应用的包名</td><td><code>com.example.android.myApp</code>, <code>com.android.settings</code></td></tr><tr cid="n1528" mdtype="table_row"><td>appWaitActivity</td><td>用于等待启动的 Android Activity 名称</td><td><code>SplashActivity</code></td></tr><tr cid="n1532" mdtype="table_row"><td>appWaitPackage</td><td>用于等待启动的 Android 应用的包</td><td><code>com.example.android.myApp</code>, <code>com.android.settings</code></td></tr><tr cid="n1536" mdtype="table_row"><td>appWaitDuration</td><td>用于等待 appWaitActivity 启动的超时时间（以毫秒为单位）（默认值为 <code>20000</code>)</td><td><code>30000</code></td></tr><tr cid="n1540" mdtype="table_row"><td>deviceReadyTimeout</td><td>用于等待模拟器或真机准备就绪的超时时间</td><td><code>5</code></td></tr><tr cid="n1544" mdtype="table_row"><td>androidCoverage</td><td>用于执行测试的 instrumentation 类。 传送 <code>-w</code> 参数到如下命令 <code>adb shell am instrument -e coverage true -w</code></td><td><code>com.my.Pkg/com.my.Pkg.instrumentation.MyInstrumentation</code></td></tr><tr cid="n1548" mdtype="table_row"><td>enablePerformanceLogging</td><td>（仅适用于 Chrome 与 webview）开启 Chromedriver 的性能日志。（默认值为 <code>false</code>）</td><td><code>true</code>, <code>false</code></td></tr><tr cid="n1552" mdtype="table_row"><td>androidDeviceReadyTimeout</td><td>用于等待设备在启动应用后准备就绪的超时时间。以秒为单位。</td><td>例如 <code>30</code></td></tr><tr cid="n1556" mdtype="table_row"><td>androidInstallTimeout</td><td>用于等待在设备中安装 apk 所花费的时间（以毫秒为单位）。默认值为 <code>90000</code></td><td>例如 <code>90000</code></td></tr><tr cid="n1560" mdtype="table_row"><td>adbPort</td><td>用来连接 ADB 服务器的端口（默认值为 <code>5037</code>）</td><td><code>5037</code></td></tr><tr cid="n1564" mdtype="table_row"><td>androidDeviceSocket</td><td>开发工具的 socket 名称。只有在被测应用是一个使用 Chromium 内核的浏览器时才需要。socket 会被浏览器打开，然后 Chromedriver 把它作为开发者工具来进行连接。</td><td>例如 <code>chrome_devtools_remote</code></td></tr><tr cid="n1568" mdtype="table_row"><td>avd</td><td>被启动 avd 的名字</td><td>例如 <code>api19</code></td></tr><tr cid="n1572" mdtype="table_row"><td>avdLaunchTimeout</td><td>用于等待 avd 启动并连接 ADB 的超时时间（以毫秒为单位），默认值为 <code>120000</code>。</td><td><code>300000</code></td></tr><tr cid="n1576" mdtype="table_row"><td>avdReadyTimeout</td><td>用于等待 avd 完成启动动画的超时时间（以毫秒为单位），默认值为 <code>120000</code>。</td><td><code>300000</code></td></tr><tr cid="n1580" mdtype="table_row"><td>avdArgs</td><td>启动 avd 时使用的额外参数</td><td>例如 <code>-netfast</code></td></tr><tr cid="n1584" mdtype="table_row"><td>useKeystore</td><td>使用自定义的 keystore 给 apk 签名，默认值为 <code>false</code></td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1588" mdtype="table_row"><td>keystorePath</td><td>自定义 keystore 的路径, 默认路径为 ~/.android/debug.keystore</td><td>例如 <code>/path/to.keystore</code></td></tr><tr cid="n1592" mdtype="table_row"><td>keystorePassword</td><td>自定义 keystore 的密码</td><td>例如 <code>foo</code></td></tr><tr cid="n1596" mdtype="table_row"><td>keyAlias</td><td>key 的别名</td><td>例如 <code>androiddebugkey</code></td></tr><tr cid="n1600" mdtype="table_row"><td>keyPassword</td><td>key 的密码</td><td>例如 <code>foo</code></td></tr><tr cid="n1604" mdtype="table_row"><td>chromedriverExecutable</td><td>webdriver 可执行文件的绝对路径（如果 Chromium 内嵌一个自己提供的 webdriver，则应使用他去替换掉 Appium 自带的 chromedriver）</td><td><code>/abs/path/to/webdriver</code></td></tr><tr cid="n1608" mdtype="table_row"><td>autoWebviewTimeout</td><td>用于等待 Webview 上下文（context）激活的时间（以毫秒为单位）。默认值为 <code>2000</code></td><td>例如 <code>4</code></td></tr><tr cid="n1612" mdtype="table_row"><td>intentAction</td><td>用于启动 activity 的 intent action（默认值为 <code>android.intent.action.MAIN</code>)</td><td>例如 <code>android.intent.action.MAIN</code>, <code>android.intent.action.VIEW</code></td></tr><tr cid="n1616" mdtype="table_row"><td>intentCategory</td><td>用于启动 activity 的 intent category。（默认值为 <code>android.intent.category.LAUNCHER</code>)</td><td>例如 <code>android.intent.category.LAUNCHER</code>, <code>android.intent.category.APP_CONTACTS</code></td></tr><tr cid="n1620" mdtype="table_row"><td>intentFlags</td><td>用于启动 activity 的标识（flags）（默认值为 <code>0x10200000</code>）</td><td>例如 <code>0x10200000</code></td></tr><tr cid="n1624" mdtype="table_row"><td>optionalIntentArguments</td><td>用于启动 activity 的额外 intent 参数。请查看 <a spellcheck="false" href="http://developer.android.com/reference/android/content/Intent.html">Intent 参数</a></td><td>例如 <code>--esn<extra_key></extra_key></code>, <code>--ez<extra_key><extra_boolean_value></extra_boolean_value></extra_key></code>, 等等。</td></tr><tr cid="n1628" mdtype="table_row"><td>dontStopAppOnReset</td><td>在使用 adb 启动应用之前，不要终止被测应用的进程。如果被测应用是被其他钩子 (anchor) 应用所创建的，设置该参数为 false 后，就允许钩子 (anchor) 应用的进程在使用 adb 启动被测应用期间仍然存在。换而言之，设置 <code>dontStopAppOnReset</code> 为 <code>true</code> 后，我们在 <code>adb shell am start</code> 的调用中不需要包含 <code>-S</code>标识（flag）。忽略该 capability 或 设置为 <code>false</code> 的话，就需要包含 <code>-S</code> 标识（flag）。默认值为 <code>false</code></td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1632" mdtype="table_row"><td>unicodeKeyboard</td><td>使用 Unicode 输入法。 默认值为 <code>false</code></td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1636" mdtype="table_row"><td>resetKeyboard</td><td>在设定了 <code>unicodeKeyboard</code> 关键字的 Unicode 测试结束后，重置输入法到原有状态。如果单独使用，将会被忽略。默认值为 <code>false</code></td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1640" mdtype="table_row"><td>noSign</td><td>跳过检查和对应用进行 debug 签名的步骤。仅适用于 UiAutomator，不适用于 selendroid。 默认值为 <code>false</code></td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1644" mdtype="table_row"><td>ignoreUnimportantViews</td><td>调用 uiautomator 的函数 <code>setCompressedLayoutHierarchy()</code>。由于 Accessibility 命令在忽略部分元素的情况下执行速度会加快，这个关键字能加快测试执行的速度。被忽略的元素将不能够被找到，因此这个关键字同时也被实现成可以随时改变的 <em>设置 (settings)</em>。 默认值为 <code>false</code></td><td><code>true</code> 或 <code>false</code></td></tr><tr cid="n1648" mdtype="table_row"><td>disableAndroidWatchers</td><td>禁用 android 监视器（watchers）。监视器用于见识应用程序的无响应状态（anr）和崩溃（crash），禁用会降低 Android 设备或模拟器的 CPU 使用率。该 capability 仅在使用 UiAutomator 时有效，不适用于 selendroid，默认设置为 <code>false</code>。</td><td><code>true</code> 或 <code>false</code></td></tr><tr cid="n1652" mdtype="table_row"><td>chromeOptions</td><td>允许对 ChromeDriver 传 chromeOptions 的参数。了解更多信息请查阅 <a spellcheck="false" href="https://sites.google.com/a/chromium.org/chromedriver/capabilities">chromeOptions</a></td><td><code>chromeOptions: {args: ['--disable-popup-blocking']}</code></td></tr><tr cid="n1656" mdtype="table_row"><td>recreateChromeDriverSessions</td><td>当移除非 ChromeDriver webview 时，终止掉 ChromeDriver 的 session。默认设置为 <code>false</code></td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1660" mdtype="table_row"><td>nativeWebScreenshot</td><td>在 web 的上下文（context），使用原生（native）的方法去截图，而不是用过代理的 ChromeDriver。默认值为 <code>false</code></td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1664" mdtype="table_row"><td>androidScreenshotPath</td><td>在设备中截图被保存的目录名。默认值为 <code>/data/local/tmp</code></td><td>例如 <code>/sdcard/screenshots/</code></td></tr><tr cid="n1668" mdtype="table_row"><td>autoGrantPermissions</td><td>让 Appium 自动确定您的应用需要哪些权限，并在安装时将其授予应用。默认设置为 <code>false</code></td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1672" mdtype="table_row"><td>skipServerInstallation</td><td>跳过 UI2 的安装，如果第一次运行程序，不要添加该配置</td><td>True 或 False</td></tr></tbody></table>

**iOS 独有**

<table><thead><tr cid="n1678" mdtype="table_row"><th>键</th><th>描述</th><th>值</th></tr></thead><tbody><tr cid="n1682" mdtype="table_row"><td>calendarFormat</td><td>（仅支持模拟器） 为 iOS 的模拟器设置日历格式</td><td>例如 <code>gregorian</code></td></tr><tr cid="n1686" mdtype="table_row"><td>bundleId</td><td>被测应用的 bundle ID 。用于在真实设备中启动测试，也用于使用其他需要 bundle ID 的关键字启动测试。在使用 bundle ID 在真实设备上执行测试时，你可以不提供 <code>app</code> 关键字，但你必须提供 <code>udid</code> 。</td><td>例如 <code>io.appium.TestApp</code></td></tr><tr cid="n1690" mdtype="table_row"><td>udid</td><td>连接的真实设备的唯一设备编号 (Unique device identifier)</td><td>例如 <code>1ae203187fc012g</code></td></tr><tr cid="n1694" mdtype="table_row"><td>launchTimeout</td><td>以毫秒为单位，在 Appium 运行失败之前设置一个等待 instruments 的时间</td><td>例如 <code>20000</code></td></tr><tr cid="n1698" mdtype="table_row"><td>locationServicesEnabled</td><td>（仅支持模拟器）强制打开或关闭定位服务。默认值是保持当前模拟器的设定.</td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1702" mdtype="table_row"><td>locationServicesAuthorized</td><td>（仅支持模拟器）通过修改 plist 文件设定是否允许应用使用定位服务，从而避免定位服务的警告出现。默认值是保持当前模拟器的设定。请注意在使用这个关键字时，你同时需要使用 <code>bundleId</code> 关键字来发送你的应用的 bundle ID。</td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1706" mdtype="table_row"><td>autoAcceptAlerts</td><td>当警告弹出的时候，都会自动去点接受。包括隐私访问权限的警告（例如 定位，联系人，照片）。默认值为 false。不支持基于 <code>XCUITest</code> 的测试。</td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1710" mdtype="table_row"><td>autoDismissAlerts</td><td>当警告弹出的时候，都会自动去点取消。包括隐私访问权限的警告（例如 定位，联系人，照片）。默认值为 false。不支持基于 <code>XCUITest</code> 的测试。</td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1714" mdtype="table_row"><td>nativeInstrumentsLib</td><td>使用原生 intruments 库（即关闭 instruments-without-delay）。</td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1718" mdtype="table_row"><td>nativeWebTap</td><td>（仅支持模拟器）在 Safari 中允许 “真实的 "，非基于 javascript 的 web 点击 (tap) 。 默认值：<code>false</code>。注意：取决于 viewport 大小 / 比例， 点击操作不一定能精确地点中对应的元素。</td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1722" mdtype="table_row"><td>safariInitialUrl</td><td>（仅支持模拟器） (&gt;= 8.1) 初始化 safari 的时使用的地址。默认是一个本地的欢迎页面</td><td>例如 <code>https://www.github.com</code></td></tr><tr cid="n1726" mdtype="table_row"><td>safariAllowPopups</td><td>（仅支持模拟器）允许 javascript 在 Safari 中创建新窗口。默认保持模拟器当前设置。</td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1730" mdtype="table_row"><td>safariIgnoreFraudWarning</td><td>（仅支持模拟器）阻止 Safari 显示此网站可能存在风险的警告。默认保持浏览器当前设置。</td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1734" mdtype="table_row"><td>safariOpenLinksInBackground</td><td>（仅支持模拟器）Safari 是否允许链接在新窗口打开。默认保持浏览器当前设置。</td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1738" mdtype="table_row"><td>keepKeyChains</td><td>（仅支持模拟器）当 Appium 会话开始 / 结束时是否保留存放密码存放记录 (keychains) 库 (Library)/ 钥匙串 (Keychains))</td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1742" mdtype="table_row"><td>localizableStringsDir</td><td>从哪里查找本地化字符串。默认值为 <code>en.lproj</code></td><td><code>en.lproj</code></td></tr><tr cid="n1746" mdtype="table_row"><td>processArguments</td><td>通过 instruments 传递到 AUT 的参数</td><td>例如 <code>-myflag</code></td></tr><tr cid="n1750" mdtype="table_row"><td>interKeyDelay</td><td>以毫秒为单位，按下每一个按键之间的延迟时间</td><td>例如 <code>100</code></td></tr><tr cid="n1754" mdtype="table_row"><td>showIOSLog</td><td>是否在 Appium 的日志中显示设备的日志。默认值为 <code>false</code></td><td><code>true</code>或<code>false</code></td></tr><tr cid="n1758" mdtype="table_row"><td>sendKeyStrategy</td><td>输入文字到文字框的策略。模拟器默认值：<code>oneByOne</code>(一个接着一个)。真实设备默认值：<code>grouped</code> (分组输入)</td><td><code>oneByOne</code>, <code>grouped</code>或<code>setValue</code></td></tr><tr cid="n1762" mdtype="table_row"><td>screenshotWaitTimeout</td><td>以秒为单位，生成屏幕截图的最长等待时间。默认值为：10</td><td>例如 <code>5</code></td></tr><tr cid="n1766" mdtype="table_row"><td>waitForAppScript</td><td>用于判断 " 应用是否被启动” 的 iOS 自动化脚本代码。默认情况下系统等待直到页面内容非空。结果必须是布尔类型。</td><td>例如 <code>true;</code>, <code>target.elements().length &gt; 0;</code>, <code>$.delay(5000); true;</code></td></tr><tr cid="n1770" mdtype="table_row"><td>webviewConnectRetries</td><td>用于获取 webview 失败时，发送连接信息到远程调试器的次数。默认次数为: <code>8</code></td><td>例如 <code>12</code></td></tr><tr cid="n1774" mdtype="table_row"><td>appName</td><td>被测应用的名字。 用于支持 iOS 9 以上系统的应用的自动化。</td><td>例如 <code>UICatalog</code></td></tr><tr cid="n1778" mdtype="table_row"><td>customSSLCert</td><td>(Sim/Emu-only) 给模拟器添加一个 SSL 证书。</td><td>例如 <code>-----BEGIN CERTIFICATE-----MIIFWjCCBEKg...</code> <code>-----END CERTIFICATE-----</code></td></tr></tbody></table>

**使用 XCUITest(iOS 独有)**

<table><thead><tr cid="n1785" mdtype="table_row"><th>键</th><th>描述</th><th>值</th></tr></thead><tbody><tr cid="n1789" mdtype="table_row"><td>processArguments</td><td>将会传送到 WebDriverAgent 的进程参数与环境</td><td><code>{ args: ["a", "b", "c"] , env: { "a": "b", "c": "d" } }</code> 或 <code>'{"args": ["a", "b", "c"], "env": { "a": "b", "c": "d" }}'</code></td></tr><tr cid="n1793" mdtype="table_row"><td>wdaLocalPort</td><td>如果这个值被指定了，Mac 主机就会使用这个端口，通过 USB 发送数据到 iOS 设备中。默认的端口与 iOS 设备中 WDA 的端口号是一致的。</td><td>例如 <code>8100</code></td></tr><tr cid="n1797" mdtype="table_row"><td>showXcodeLog</td><td>是否显示运行测试时 Xcode 的输出日志，如果值设置为 <code>true</code> ，则会在启动的时候产生<strong>大量</strong>的额外日志。默认设置为 <code>false</code>。</td><td>例如 <code>true</code></td></tr><tr cid="n1801" mdtype="table_row"><td>iosInstallPause</td><td>安装应用程序与启动 WebDriverAgent 之间停止的间隔时间（以毫秒为单位），特别适用于体积较大的包。默认是设置为 <code>0</code>。</td><td>例如 <code>8000</code></td></tr><tr cid="n1805" mdtype="table_row"><td>xcodeConfigFile</td><td>一个可选的 Xcode 可配置文件的完整路径，用于指定在真机上运行 WebDriverAgent 的个人身份或者团队身份的代码签名。</td><td>例如 <code>/path/to/myconfig.xcconfig</code></td></tr><tr cid="n1809" mdtype="table_row"><td>keychainPath</td><td>从系统的 keychain 中导出私有开发秘钥的完整路径。在真机测试时与 <code>keychainPassword</code> 配合使用。</td><td>例如 <code>/path/to/MyPrivateKey.p12</code></td></tr><tr cid="n1813" mdtype="table_row"><td>keychainPassword</td><td>在 <code>keychainPath</code> 中指定 keychain 的解锁密码。</td><td>例如 <code>super awesome password</code></td></tr><tr cid="n1817" mdtype="table_row"><td>scaleFactor</td><td>模拟器缩放因子。这对于默认分辨率是大于实际分辨的模拟器来说非常有用。因此，你不用上下滑动模拟器的屏幕就能看到所有模拟器显示的内容了。</td><td>可接受的值为: <code>'1.0', '0.75', '0.5', '0.33' 和 '0.25'</code>。 这些值都应该是一个字符串</td></tr><tr cid="n1821" mdtype="table_row"><td>preventWDAAttachments</td><td>设置 WebDriverAgent 项目中的 DerivedData 文件夹的权限为<code>仅可读</code>。为了防止 XCTest 框架产生大量无用的截屏与日志，该设置是非常必要的，因为这是不可能通过 Apple 提供的接口去关闭的。</td><td>设置 capabilitity 为 <code>true</code> 将会设置 Posix 的文件夹的权限为 <code>555</code>，设置为 <code>false</code> 则会将权限重置回 <code>755</code></td></tr><tr cid="n1825" mdtype="table_row"><td>webDriverAgentUrl</td><td>若提供了 URL，Appium 将在这 URL 上连接现有的 WebDriverAgent 实例，而不是重新启动一个。</td><td>例如 <code>http://localhost:8100</code></td></tr><tr cid="n1829" mdtype="table_row"><td>useNewWDA</td><td>若设置为 <code>true</code>，则直接卸载设备上现存的所有 WebDriverAgent 客户端。在某些情况，该做法可以提高稳定性。默认设置为 <code>false</code>。</td><td>例如 <code>true</code></td></tr><tr cid="n1833" mdtype="table_row"><td>wdaLaunchTimeout</td><td>等待 WebDriverAgent 可 ping 同的时间（以毫秒为单位）。默认设置为 60000ms。</td><td>例如 <code>30000</code></td></tr><tr cid="n1837" mdtype="table_row"><td>calendarAccessAuthorized</td><td>若设置为 <code>true</code>，则允许在 iOS 模拟器上访问日历。若设置为 <code>false</code>，则不被允许。否则，日历的 authorizationStatus 会保持不变。</td><td>-</td></tr></tbody></table>

**注意事项**

1.  autoGrantPermissions，可以设置自动授权权限
    
2.  unicodeKeyboard：输入中文时要加，要不然输入不了中文
    
3.  resetKeyboard：输入中文时要加，要不然输入不了中文
    

第一个自动化脚本
--------

*   前提条件
    

*   安装好 appium-python-client，并导入包
    
*   配置好能力值
    
*   开启模拟器，adb devices 能看到设备
    
*   启动 appium  sever
    
*   python 环境（略）
    

*   示例代码
    
    ```
    from appium import webdriver #导入webdriver
    des_cap = { #是个字典
        'platformName':'Android',  #大小写无关
        'platformVersion':'11'  #平台版本必须匹配
    }
    driver = webdriver.Remote(command_executor='http://127.0.0.1:4723/wd/hub',
                              #注意此处的command_executor=可以不写，后面的url就是appiumserver的地址，注意是http的不是https
                              desired_capabilities=des_cap)
    
    ```
    

定位工具
----

### appium 定位工具 (推荐)

1.  启动检查器会话
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635652815142059178.png)
    

2.  编辑启动参数
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635652835870013375.png)

3.  启动会话
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635652860006073765.png)

4.  检查器界面
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635652895591093503.png)

*   一定要善用搜索工具，app 的 resource-id 非常有可能是重复的
    
*   此处的源类似于 DOM 树
    
*   而操作就是我们后面要讲到高级操作
    

### uiautomatorviewer

*   该工具位于 andriod sdk 目录下的：tools\bin
    
*   初次启动更改下文件 uiautomatorviewer.bat 中的最后一行代码：
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635652947647049067.png)

*   打开后是如下界面
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635652974970017320.png)![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635652996291000953.png)
    
*   界面区域介绍
    

*   注意：可以搜索某个元素，但元素右侧的坐标位置需要手工点击界面方可准确显示（不然是你滑动后显示的位置）
    
*   打开：打开已保存的布局，一个是图片文件（png 格式），一个是. uix 文件（XML 布局结构）
    
*   设备截图：Device Screenshoot uiautomator dump
    
*   设备截图 (带压缩)：Device Screenshoot with Compressed Hierarchy uiautomator dump –compressed
    
*   保存：保存为一个 uix 文件。
    
*   第二按钮都是把全部布局呈现出来，而第三按钮只呈现有用的控件布局。比如某一 Frame 存在，但只有装饰功能，那么点击第三按钮时，可能不被呈现。
    
*   菜单栏：
    
*   截图区：显示当前设备屏幕显示的布局图片
    
*   布局区：以 XML 树的形式，显示控件布局
    
*   控件属性区：当鼠标点击某一控件时，将显示控件属性，同时还有元素的坐标显示。
    

* * *

*   定位工具的区别
    

*   两个工具不能同时启动
    
*   appium 工具：能给你智能分析出 id 的值，给你 xpath 的表达式
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635653018338088554.png)
    

**三个工具的对比**

<table><thead><tr cid="n1984" mdtype="table_row"><th>功能</th><th>Appium Inspector</th><th>UiautomatorViewer</th><th>Weditor</th><th>备注</th></tr></thead><tbody><tr cid="n1990" mdtype="table_row"><td>元素定位</td><td>√</td><td>√</td><td>√</td><td>AppiumInspect 会提供 XPATH 表达式，weditor 提供优良的 xpath 表达式</td></tr><tr cid="n1996" mdtype="table_row"><td>操作设备 | 元素</td><td>√√√</td><td>X</td><td>✓</td><td>AppiumInspect 可以实现几乎所有的操作</td></tr><tr cid="n2002" mdtype="table_row"><td>录制</td><td>√√√</td><td>×</td><td>✓</td><td>AppiumInspect 录制非常强大，Weditor 录制的功能弱一些</td></tr><tr cid="n2008" mdtype="table_row"><td>界面坐标</td><td>元素级的</td><td>√</td><td>✓</td><td>AppiumInspect 是元素级的，UIA 和 Weditor 是像素点级别</td></tr><tr cid="n2014" mdtype="table_row"><td>实时获取</td><td>X</td><td>X</td><td>✓</td><td>前两者都要操作一下获取一下</td></tr><tr cid="n2020" mdtype="table_row"><td>搜索元素</td><td>√√√</td><td>X</td><td>✓</td><td>UIA 可以简单定位但几乎没啥用；AppiumInspect 支持语法搜索，可以找到并告诉你是几个，哪几个；Weditor 搜索跟 web 类似，不支持语法，string 搜索</td></tr><tr cid="n2026" mdtype="table_row"><td>界面保存</td><td>X</td><td>√</td><td>X</td><td><br></td></tr><tr cid="n2032" mdtype="table_row"><td>保存为代码</td><td>✓</td><td>X</td><td>✓</td><td><br></td></tr></tbody></table>

元素定位方法
------

### 概览

<table><thead><tr cid="n1992" mdtype="table_row"><th>元素定位方法</th><th>说明</th></tr></thead><tbody><tr cid="n1995" mdtype="table_row"><td>find_element</td><td>★祖宗方法</td></tr><tr cid="n1998" mdtype="table_row"><td>find_element_by_accessibility_id</td><td>★content-desc 属性的值</td></tr><tr cid="n2001" mdtype="table_row"><td>find_element_by_android_data_matcher</td><td>仅限 Espresso</td></tr><tr cid="n2004" mdtype="table_row"><td>find_element_by_android_uiautomator</td><td>★uia 定位</td></tr><tr cid="n2007" mdtype="table_row"><td>find_element_by_android_view_matcher</td><td>不知道咋用</td></tr><tr cid="n2010" mdtype="table_row"><td>find_element_by_android_viewtag</td><td>仅限 Espresso</td></tr><tr cid="n2013" mdtype="table_row"><td>find_element_by_class_name</td><td>★class 属性的值</td></tr><tr cid="n2016" mdtype="table_row"><td>find_element_by_css_selector</td><td>web 可用</td></tr><tr cid="n2019" mdtype="table_row"><td>find_element_by_custom</td><td>不知道咋用</td></tr><tr cid="n2022" mdtype="table_row"><td>find_element_by_id</td><td>★resource-id 属性值</td></tr><tr cid="n2025" mdtype="table_row"><td>find_element_by_image</td><td>不知道咋用</td></tr><tr cid="n2028" mdtype="table_row"><td>find_element_by_ios_class_chain</td><td>ios 不讲</td></tr><tr cid="n2031" mdtype="table_row"><td>find_element_by_ios_predicate</td><td>ios 不讲</td></tr><tr cid="n2034" mdtype="table_row"><td>find_element_by_ios_uiautomation</td><td>ios 不讲</td></tr><tr cid="n2037" mdtype="table_row"><td>find_element_by_link_text</td><td>app 不可用，web 可用</td></tr><tr cid="n2040" mdtype="table_row"><td>find_element_by_name</td><td>app 废弃，web 可用</td></tr><tr cid="n2043" mdtype="table_row"><td>find_element_by_partial_link_text</td><td>app 不可用，web 可用</td></tr><tr cid="n2046" mdtype="table_row"><td>find_element_by_tag_name</td><td>app 不可用，web 可用</td></tr><tr cid="n2049" mdtype="table_row"><td>find_element_by_windows_uiautomation</td><td>不知道咋用</td></tr><tr cid="n2052" mdtype="table_row"><td>find_element_by_xpath</td><td>★可用，效率低</td></tr><tr cid="n2055" mdtype="table_row"><td>find_elements</td><td><br></td></tr><tr cid="n2058" mdtype="table_row"><td>find_elements_by_accessibility_id</td><td><br></td></tr><tr cid="n2061" mdtype="table_row"><td>find_elements_by_android_data_matcher</td><td><br></td></tr><tr cid="n2064" mdtype="table_row"><td>find_elements_by_android_uiautomator</td><td><br></td></tr><tr cid="n2067" mdtype="table_row"><td>find_elements_by_android_viewtag</td><td><br></td></tr><tr cid="n2070" mdtype="table_row"><td>find_elements_by_class_name</td><td><br></td></tr><tr cid="n2073" mdtype="table_row"><td>find_elements_by_css_selector</td><td><br></td></tr><tr cid="n2076" mdtype="table_row"><td>find_elements_by_custom</td><td><br></td></tr><tr cid="n2079" mdtype="table_row"><td>find_elements_by_id</td><td><br></td></tr><tr cid="n2082" mdtype="table_row"><td>find_elements_by_image</td><td><br></td></tr><tr cid="n2085" mdtype="table_row"><td>find_elements_by_ios_class_chain</td><td><br></td></tr><tr cid="n2088" mdtype="table_row"><td>find_elements_by_ios_predicate</td><td><br></td></tr><tr cid="n2091" mdtype="table_row"><td>find_elements_by_ios_uiautomation</td><td><br></td></tr><tr cid="n2094" mdtype="table_row"><td>find_elements_by_link_text</td><td><br></td></tr><tr cid="n2097" mdtype="table_row"><td>find_elements_by_name</td><td><br></td></tr><tr cid="n2100" mdtype="table_row"><td>find_elements_by_partial_link_text</td><td><br></td></tr><tr cid="n2103" mdtype="table_row"><td>find_elements_by_tag_name</td><td><br></td></tr><tr cid="n2106" mdtype="table_row"><td>find_elements_by_windows_uiautomation</td><td><br></td></tr><tr cid="n2109" mdtype="table_row"><td>find_elements_by_xpath</td><td><br></td></tr><tr cid="n2112" mdtype="table_row"><td>find_image_occurrence</td><td><br></td></tr></tbody></table>

### 元素定位基础方法

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635653058912022297.png)

#### id 定位   find_element_by_id  

*   resource-id 的值
    
*   一样会重复（下图能找到 7 个）
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635653074060078266.png)

#### text 文本定位  find_element_by_name  text 文本定位

*   text 的值
    
*   由于 text 稳定性不是很好，所以 appium1.5 开始废弃了该方法
    
*   执行报错：Locator Strategy 'name' is not supported for this session
    
*   对于 text 的值没有 link_text 的说法，因为 text 是个属性
    

#### class 属性值定位  find_element_by_class_name

*   注意重复性
    
*   可以使用
    

#### xpath 语法定位  find_element_by_xpath

*   官方不建议使用 XPath 定位器，这可能会导致脆弱的测试。请您的开发团队提供独特的辅助定位器!
    
*   但 xpath 的确是可以用的，但要注意几点
    

*   定位工具给的绝对路径太长，节点名是 class 属性的值，不是 tag_name
    
*   text 是个属性，不是 >< 中间的内容，所以要用属性来定义
    
    ```
    //*[@text='同意']   #正确的表达式，此处的text是个属性
    //*[text()='同意']  #不要以为是这个，selenium的text是标签的文本
    
    ```
    

*   XPATH 的几个语法复习下
    
    ```
    //*[@text='同意']                #属性值
    //*[@resource-id='com.ddnapalon.calculator.gp:id/agreement_text']
    
    //*[contains(@text,'不同意')]    #属性包含
    //*[starts-with(@text,'同意')]  #属性开头
    //*[ends-with(@text,'提示')]    #属性结尾
    
    ```
    

#### 内容描述定位 find_element_by_accessibility_id

*   content-desc 的值
    

### 元素相关操作及属性

<table><thead><tr cid="n2169" mdtype="table_row"><th>操作 / 属性</th><th>说明</th></tr></thead><tbody><tr cid="n2172" mdtype="table_row"><td>click()</td><td>点击</td></tr><tr cid="n2175" mdtype="table_row"><td>clear()</td><td>清空</td></tr><tr cid="n2178" mdtype="table_row"><td>send_keys()</td><td>输入内容</td></tr><tr cid="n2181" mdtype="table_row"><td>text</td><td>文本</td></tr><tr cid="n2184" mdtype="table_row"><td>tag_name</td><td>标签名</td></tr><tr cid="n2187" mdtype="table_row"><td>get_attribute()</td><td>获取属性值</td></tr><tr cid="n2190" mdtype="table_row"><td>location</td><td>位置</td></tr><tr cid="n2193" mdtype="table_row"><td>size</td><td>大小</td></tr><tr cid="n2196" mdtype="table_row"><td>rect</td><td>矩形</td></tr><tr cid="n2199" mdtype="table_row"><td>is_Selected()</td><td>是否可选</td></tr><tr cid="n2202" mdtype="table_row"><td>is_enabled()</td><td>是否使能</td></tr><tr cid="n2205" mdtype="table_row"><td>is_displayed()</td><td>是否显示</td></tr><tr cid="n2208" mdtype="table_row"><td>submit()</td><td>提交</td></tr></tbody></table>