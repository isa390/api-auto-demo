> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b587eddaee2017f19d94aa53dbe)

> 下面的代码可以输出对象的方法，并生成 md 表格

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645406407389012944.png)
------------------------------------------------------------------------------------------

第一部分：selenium 核心技术概览
--------------------

*   下面的代码可以输出对象的方法，并生成 md 表格
    
    ```
    from selenium import webdriver
    print('webdriver的对象类型是',type(webdriver))
    driver = webdriver.Chrome()
    print('driver的对象类型',type(driver))
    driver.get('https://www.baidu.com')
    ele_search = driver.find_element('id','kw')
    print('html元素的对象类型是',type(ele_search))
    print('|对象属性(方法或类)|说明|')
    print('| ---- | ---- |')
    for _ in dir(ele_search):
        if _[0]!='_':
            print(f'|{_}||')
    
    ```
    

**webdriver 模块所有类**

<table><thead><tr cid="n788" mdtype="table_row"><th>类</th><th>说明</th></tr></thead><tbody><tr cid="n791" mdtype="table_row"><td>ActionChains</td><td>动作链</td></tr><tr cid="n794" mdtype="table_row"><td>Chrome</td><td>Chrome 类</td></tr><tr cid="n797" mdtype="table_row"><td>ChromeOptions</td><td>Chrome 选项类</td></tr><tr cid="n800" mdtype="table_row"><td>ChromiumEdge</td><td><br></td></tr><tr cid="n803" mdtype="table_row"><td>DesiredCapabilities</td><td><br></td></tr><tr cid="n806" mdtype="table_row"><td>Edge</td><td><br></td></tr><tr cid="n809" mdtype="table_row"><td>EdgeOptions</td><td><br></td></tr><tr cid="n812" mdtype="table_row"><td>Firefox</td><td>火狐类</td></tr><tr cid="n815" mdtype="table_row"><td>FirefoxOptions</td><td>火狐选项类</td></tr><tr cid="n818" mdtype="table_row"><td>FirefoxProfile</td><td><br></td></tr><tr cid="n821" mdtype="table_row"><td>Ie</td><td><br></td></tr><tr cid="n824" mdtype="table_row"><td>IeOptions</td><td><br></td></tr><tr cid="n827" mdtype="table_row"><td>Keys</td><td>按键</td></tr><tr cid="n830" mdtype="table_row"><td>Opera</td><td><br></td></tr><tr cid="n833" mdtype="table_row"><td>Proxy</td><td><br></td></tr><tr cid="n836" mdtype="table_row"><td>Remote</td><td>远程</td></tr><tr cid="n839" mdtype="table_row"><td>Safari</td><td><br></td></tr><tr cid="n842" mdtype="table_row"><td>TouchActions</td><td><br></td></tr><tr cid="n845" mdtype="table_row"><td>WPEWebKit</td><td><br></td></tr><tr cid="n848" mdtype="table_row"><td>WPEWebKitOptions</td><td><br></td></tr><tr cid="n851" mdtype="table_row"><td>WebKitGTK</td><td><br></td></tr><tr cid="n854" mdtype="table_row"><td>WebKitGTKOptions</td><td><br></td></tr><tr cid="n857" mdtype="table_row"><td>chrome</td><td><br></td></tr><tr cid="n860" mdtype="table_row"><td>chromium</td><td><br></td></tr><tr cid="n863" mdtype="table_row"><td>common</td><td><br></td></tr><tr cid="n866" mdtype="table_row"><td>edge</td><td><br></td></tr><tr cid="n869" mdtype="table_row"><td>firefox</td><td><br></td></tr><tr cid="n872" mdtype="table_row"><td>ie</td><td><br></td></tr><tr cid="n875" mdtype="table_row"><td>opera</td><td><br></td></tr><tr cid="n878" mdtype="table_row"><td>remote</td><td><br></td></tr><tr cid="n881" mdtype="table_row"><td>safari</td><td><br></td></tr><tr cid="n884" mdtype="table_row"><td>support</td><td><br></td></tr><tr cid="n887" mdtype="table_row"><td>webkitgtk</td><td><br></td></tr><tr cid="n890" mdtype="table_row"><td>wpewebkit</td><td><br></td></tr></tbody></table>

**WebDriver 对象所有属性、方法**

<table><thead><tr cid="n896" mdtype="table_row"><th>对象属性、方法</th><th>说明</th></tr></thead><tbody><tr cid="n899" mdtype="table_row"><td>add_cookie</td><td>添加 cookie</td></tr><tr cid="n902" mdtype="table_row"><td>application_cache</td><td><br></td></tr><tr cid="n905" mdtype="table_row"><td>back</td><td>返回</td></tr><tr cid="n908" mdtype="table_row"><td>bidi_connection</td><td><br></td></tr><tr cid="n911" mdtype="table_row"><td>capabilities</td><td><br></td></tr><tr cid="n914" mdtype="table_row"><td>caps</td><td><br></td></tr><tr cid="n917" mdtype="table_row"><td>close</td><td>关闭不退出进程</td></tr><tr cid="n920" mdtype="table_row"><td>command_executor</td><td><br></td></tr><tr cid="n923" mdtype="table_row"><td>create_options</td><td><br></td></tr><tr cid="n926" mdtype="table_row"><td>create_web_element</td><td><br></td></tr><tr cid="n929" mdtype="table_row"><td>current_url</td><td>当前 URL</td></tr><tr cid="n932" mdtype="table_row"><td>current_window_handle</td><td>当前窗口句柄</td></tr><tr cid="n935" mdtype="table_row"><td>delete_all_cookies</td><td>删除所有 cookie</td></tr><tr cid="n938" mdtype="table_row"><td>delete_cookie</td><td>删除指定 cookie</td></tr><tr cid="n941" mdtype="table_row"><td>delete_network_conditions</td><td><br></td></tr><tr cid="n944" mdtype="table_row"><td>desired_capabilities</td><td>能力值</td></tr><tr cid="n947" mdtype="table_row"><td>error_handler</td><td><br></td></tr><tr cid="n950" mdtype="table_row"><td>execute</td><td><br></td></tr><tr cid="n953" mdtype="table_row"><td>execute_async_script</td><td><br></td></tr><tr cid="n956" mdtype="table_row"><td>execute_cdp_cmd</td><td>执行 cdp 命令</td></tr><tr cid="n959" mdtype="table_row"><td>execute_script</td><td>执行 js 代码</td></tr><tr cid="n962" mdtype="table_row"><td>file_detector</td><td><br></td></tr><tr cid="n965" mdtype="table_row"><td>file_detector_context</td><td><br></td></tr><tr cid="n968" mdtype="table_row"><td>find_element</td><td>通用元素定位方法，找一个元素</td></tr><tr cid="n971" mdtype="table_row"><td>find_element_by_class_name</td><td>通过 class 属性值定位, 4.0 废弃</td></tr><tr cid="n974" mdtype="table_row"><td>find_element_by_css_selector</td><td>通过 css 选择器定位, 4.0 废弃</td></tr><tr cid="n977" mdtype="table_row"><td>find_element_by_id</td><td>通过 id 属性值定位, 4.0 废弃</td></tr><tr cid="n980" mdtype="table_row"><td>find_element_by_link_text</td><td>通过超链接文本定位, 4.0 废弃</td></tr><tr cid="n983" mdtype="table_row"><td>find_element_by_name</td><td>通过 name 属性值定位, 4.0 废弃</td></tr><tr cid="n986" mdtype="table_row"><td>find_element_by_partial_link_text</td><td>通过部分超链接文本定位, 4.0 废弃</td></tr><tr cid="n989" mdtype="table_row"><td>find_element_by_tag_name</td><td>通过标签名定位, 4.0 废弃</td></tr><tr cid="n992" mdtype="table_row"><td>find_element_by_xpath</td><td>通过 xpath 定位, 4.0 废弃</td></tr><tr cid="n995" mdtype="table_row"><td>find_elements</td><td>通用元素定位方法，找多个元素，返回列表</td></tr><tr cid="n998" mdtype="table_row"><td>find_elements_by_class_name</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1001" mdtype="table_row"><td>find_elements_by_css_selector</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1004" mdtype="table_row"><td>find_elements_by_id</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1007" mdtype="table_row"><td>find_elements_by_link_text</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1010" mdtype="table_row"><td>find_elements_by_name</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1013" mdtype="table_row"><td>find_elements_by_partial_link_text</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1016" mdtype="table_row"><td>find_elements_by_tag_name</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1019" mdtype="table_row"><td>find_elements_by_xpath</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1022" mdtype="table_row"><td>forward</td><td>前进</td></tr><tr cid="n1025" mdtype="table_row"><td>fullscreen_window</td><td>全屏</td></tr><tr cid="n1028" mdtype="table_row"><td>get</td><td>打开网址</td></tr><tr cid="n1031" mdtype="table_row"><td>get_cookie</td><td>获取 cookie</td></tr><tr cid="n1034" mdtype="table_row"><td>get_cookies</td><td>获取所有 cookie</td></tr><tr cid="n1037" mdtype="table_row"><td>get_issue_message</td><td><br></td></tr><tr cid="n1040" mdtype="table_row"><td>get_log</td><td><br></td></tr><tr cid="n1043" mdtype="table_row"><td>get_network_conditions</td><td><br></td></tr><tr cid="n1046" mdtype="table_row"><td>get_pinned_scripts</td><td><br></td></tr><tr cid="n1049" mdtype="table_row"><td>get_screenshot_as_base64</td><td><br></td></tr><tr cid="n1052" mdtype="table_row"><td>get_screenshot_as_file</td><td><br></td></tr><tr cid="n1055" mdtype="table_row"><td>get_screenshot_as_png</td><td><br></td></tr><tr cid="n1058" mdtype="table_row"><td>get_sinks</td><td><br></td></tr><tr cid="n1061" mdtype="table_row"><td>get_window_position</td><td>获取窗口位置</td></tr><tr cid="n1064" mdtype="table_row"><td>get_window_rect</td><td>获取窗口矩形</td></tr><tr cid="n1067" mdtype="table_row"><td>get_window_size</td><td>获取窗口大小</td></tr><tr cid="n1070" mdtype="table_row"><td>implicitly_wait</td><td>隐式等待</td></tr><tr cid="n1073" mdtype="table_row"><td>launch_app</td><td><br></td></tr><tr cid="n1076" mdtype="table_row"><td>log_types</td><td><br></td></tr><tr cid="n1079" mdtype="table_row"><td>maximize_window</td><td>最大化窗口</td></tr><tr cid="n1082" mdtype="table_row"><td>minimize_window</td><td>最小化窗口</td></tr><tr cid="n1085" mdtype="table_row"><td>mobile</td><td><br></td></tr><tr cid="n1088" mdtype="table_row"><td>name</td><td>浏览器名字</td></tr><tr cid="n1091" mdtype="table_row"><td>orientation</td><td><br></td></tr><tr cid="n1094" mdtype="table_row"><td>page_source</td><td>页面源码</td></tr><tr cid="n1097" mdtype="table_row"><td>pin_script</td><td><br></td></tr><tr cid="n1100" mdtype="table_row"><td>pinned_scripts</td><td><br></td></tr><tr cid="n1103" mdtype="table_row"><td>port</td><td><br></td></tr><tr cid="n1106" mdtype="table_row"><td>print_page</td><td><br></td></tr><tr cid="n1109" mdtype="table_row"><td>quit</td><td>退出浏览器进程</td></tr><tr cid="n1112" mdtype="table_row"><td>refresh</td><td>刷新</td></tr><tr cid="n1115" mdtype="table_row"><td>save_screenshot</td><td>保存截图</td></tr><tr cid="n1118" mdtype="table_row"><td>service</td><td><br></td></tr><tr cid="n1121" mdtype="table_row"><td>session_id</td><td><br></td></tr><tr cid="n1124" mdtype="table_row"><td>set_network_conditions</td><td><br></td></tr><tr cid="n1127" mdtype="table_row"><td>set_page_load_timeout</td><td>设置页面加载超时时间</td></tr><tr cid="n1130" mdtype="table_row"><td>set_permissions</td><td><br></td></tr><tr cid="n1133" mdtype="table_row"><td>set_script_timeout</td><td><br></td></tr><tr cid="n1136" mdtype="table_row"><td>set_sink_to_use</td><td><br></td></tr><tr cid="n1139" mdtype="table_row"><td>set_window_position</td><td>设置窗口位置</td></tr><tr cid="n1142" mdtype="table_row"><td>set_window_rect</td><td>设置窗口矩形</td></tr><tr cid="n1145" mdtype="table_row"><td>set_window_size</td><td>设置窗口大小</td></tr><tr cid="n1148" mdtype="table_row"><td>start_client</td><td><br></td></tr><tr cid="n1151" mdtype="table_row"><td>start_session</td><td><br></td></tr><tr cid="n1154" mdtype="table_row"><td>start_tab_mirroring</td><td><br></td></tr><tr cid="n1157" mdtype="table_row"><td>stop_casting</td><td><br></td></tr><tr cid="n1160" mdtype="table_row"><td>stop_client</td><td><br></td></tr><tr cid="n1163" mdtype="table_row"><td>switch_to</td><td>切换到</td></tr><tr cid="n1166" mdtype="table_row"><td>timeouts</td><td><br></td></tr><tr cid="n1169" mdtype="table_row"><td>title</td><td>浏览器标题</td></tr><tr cid="n1172" mdtype="table_row"><td>unpin</td><td><br></td></tr><tr cid="n1175" mdtype="table_row"><td>vendor_prefix</td><td><br></td></tr><tr cid="n1178" mdtype="table_row"><td>window_handles</td><td>窗口句柄</td></tr></tbody></table>

第二部分：6 大基础定位
------------

**WebDriver 对象所有定位方法**

<table><thead><tr cid="n1186" mdtype="table_row"><th>对象属性、方法</th><th>说明</th></tr></thead><tbody><tr cid="n1189" mdtype="table_row"><td>find_element</td><td>通用元素定位方法，找一个元素</td></tr><tr cid="n1192" mdtype="table_row"><td>find_element_by_class_name</td><td>通过 class 属性值定位, 4.0 废弃 (打上了删除线, 仍然可用)</td></tr><tr cid="n1195" mdtype="table_row"><td>find_element_by_css_selector</td><td>通过 css 选择器定位, 4.0 废弃</td></tr><tr cid="n1198" mdtype="table_row"><td>find_element_by_id</td><td>通过 id 属性值定位, 4.0 废弃</td></tr><tr cid="n1201" mdtype="table_row"><td>find_element_by_link_text</td><td>通过超链接文本定位, 4.0 废弃</td></tr><tr cid="n1204" mdtype="table_row"><td>find_element_by_name</td><td>通过 name 属性值定位, 4.0 废弃</td></tr><tr cid="n1207" mdtype="table_row"><td>find_element_by_partial_link_text</td><td>通过部分超链接文本定位, 4.0 废弃</td></tr><tr cid="n1210" mdtype="table_row"><td>find_element_by_tag_name</td><td>通过标签名定位, 4.0 废弃</td></tr><tr cid="n1213" mdtype="table_row"><td>find_element_by_xpath</td><td>通过 xpath 定位, 4.0 废弃</td></tr><tr cid="n1216" mdtype="table_row"><td>find_elements</td><td>通用元素定位方法，找多个元素，返回列表</td></tr><tr cid="n1219" mdtype="table_row"><td>find_elements_by_class_name</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1222" mdtype="table_row"><td>find_elements_by_css_selector</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1225" mdtype="table_row"><td>find_elements_by_id</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1228" mdtype="table_row"><td>find_elements_by_link_text</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1231" mdtype="table_row"><td>find_elements_by_name</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1234" mdtype="table_row"><td>find_elements_by_partial_link_text</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1237" mdtype="table_row"><td>find_elements_by_tag_name</td><td>同上，找多个元素，返回列表</td></tr><tr cid="n1240" mdtype="table_row"><td>find_elements_by_xpath</td><td>同上，找多个元素，返回列表</td></tr></tbody></table>

**注意事项**

<table><thead><tr cid="n1246" mdtype="table_row"><th>定位方法</th><th>含义</th><th>注意事项</th></tr></thead><tbody><tr cid="n1250" mdtype="table_row"><td>tag</td><td>标签名定位</td><td>因太多重复, 基本没用</td></tr><tr cid="n1254" mdtype="table_row"><td><br></td><td><br></td><td>head 中的元素较为特殊</td></tr><tr cid="n1258" mdtype="table_row"><td>name</td><td>name 属性值定位</td><td>易重复</td></tr><tr cid="n1262" mdtype="table_row"><td><br></td><td><br></td><td><strong>定位到多个，操作的是第一个，所有的 find_element 方法都是如此</strong></td></tr><tr cid="n1266" mdtype="table_row"><td>class</td><td>class 属性值定位</td><td>易重复</td></tr><tr cid="n1270" mdtype="table_row"><td><br></td><td><br></td><td>不允许使用复合类名（即多值）</td></tr><tr cid="n1274" mdtype="table_row"><td><br></td><td><br></td><td><strong>多值问题说明 </strong><a href="https://www.w3school.com.cn/tags/att_standard_class.asp">https://www.w3school.com.cn/tags/att_standard_class.asp</a></td></tr><tr cid="n1278" mdtype="table_row"><td>id</td><td>id 属性值定位</td><td>推荐用</td></tr><tr cid="n1282" mdtype="table_row"><td><br></td><td><br></td><td>注意变化的 ID</td></tr><tr cid="n1286" mdtype="table_row"><td><br></td><td><br></td><td>注意数字开头的 ID</td></tr><tr cid="n1290" mdtype="table_row"><td>link_text</td><td>a 标签的文本定位</td><td>会重复</td></tr><tr cid="n1294" mdtype="table_row"><td><br></td><td><br></td><td>只能是 a 标签</td></tr><tr cid="n1298" mdtype="table_row"><td><br></td><td><br></td><td>理解什么是文本</td></tr><tr cid="n1302" mdtype="table_row"><td>partial_link_text</td><td>a 标签的部分文本定位</td><td>易重复</td></tr><tr cid="n1306" mdtype="table_row"><td><br></td><td><br></td><td>包含的意思</td></tr></tbody></table>

*   关于 class 属性
    

*   class 属性规定元素的类名（classname）。
    
*   class 属性大多数时候用于指向样式表中的类（class）。不过，也可以利用它通过 JavaScript 来改变带有指定 class 的 HTML 元素。、
    
*   class 属性不能在以下 HTML 元素中使用：base, head, html, meta, param, script, style 以及 title
    
*   可以给 HTML 元素赋予多个 class，例如：。这么做可以把若干个 CSS 类合并到一个 HTML 元素。
    
    ```
    class='aa bb'意思是有2个类，一个类的值是aa，一个类的值是bb（见下图）
    
    ```
    
*   类名不能以数字开头！只有 Internet Explorer 支持这种做法。
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220226/1645838640677065177.png)

第三部分：xpath 定位
-------------

*   官方： [https://www.w3school.com.cn/xpath/index.asp](https://www.w3school.com.cn/xpath/index.asp)
    
*   xpath 是什么？
    

*   XPath 是一门在 XML 文档中查找信息的语言。XPath 可用来在 XML 文档中对元素和属性进行遍历。
    
*   XPath 使用路径表达式在 XML 文档中进行导航
    
*   XPath 包含一个标准函数库
    
*   XPath 是 XSLT 中的主要元素
    
*   XPath 是一个 W3C 标准
    

*   普通语法
    
    <table><thead><tr cid="n1348" mdtype="table_row"><th>名称</th><th>表达式</th><th>示例</th><th>描述</th></tr></thead><tbody><tr cid="n1353" mdtype="table_row"><td>绝对路径</td><td>/</td><td>/html/body/input</td><td>从根节点 html，逐级往下（不跳跃）</td></tr><tr cid="n1358" mdtype="table_row"><td>相对路径</td><td>//</td><td>//input</td><td>所有的 input 标签</td></tr><tr cid="n1363" mdtype="table_row"><td><br></td><td><br></td><td>//*</td><td>所有标签</td></tr><tr cid="n1368" mdtype="table_row"><td><br></td><td><br></td><td>//input[1]</td><td>每个父节点下的第一个 input 标签</td></tr><tr cid="n1373" mdtype="table_row"><td><br></td><td><br></td><td>//input[last()]</td><td>每个父节点下的最后个 input 标签</td></tr><tr cid="n1378" mdtype="table_row"><td><br></td><td><br></td><td>//input[last()-1]</td><td>每个父节点下的倒数第二个 input 标签</td></tr><tr cid="n1383" mdtype="table_row"><td><br></td><td><br></td><td>//input[position()&lt;3]</td><td>每个父节点下的前 2 个 input 标签</td></tr><tr cid="n1388" mdtype="table_row"><td>符号</td><td>.</td><td><br></td><td>当前节点，一般不用</td></tr><tr cid="n1393" mdtype="table_row"><td><br></td><td>..</td><td>//*[@id='username']/..</td><td>父节点，常用于子代元素特征明显父代元素无明显特征的情况</td></tr><tr cid="n1398" mdtype="table_row"><td>属性法</td><td>[@属性 ='属性值']</td><td>//*[@id='username']</td><td>id 唯一，所以前面的标签名可以不写，但不可省略 //*</td></tr><tr cid="n1403" mdtype="table_row"><td><br></td><td><br></td><td>//input[@name='password']</td><td><br></td></tr><tr cid="n1408" mdtype="table_row"><td><br></td><td><br></td><td>//button[@class='pn vm']</td><td>注意跟 by_class_name 的区别，此处要传递所有值，不能是部分</td></tr><tr cid="n1413" mdtype="table_row"><td><br></td><td>其他运算符</td><td>//*[@width&gt;700]</td><td>对于数字的属性值，还可以用运算符来比较定位</td></tr><tr cid="n1418" mdtype="table_row"><td><br></td><td>//*[@属性]</td><td>//*[@style]</td><td>具有某个属性的标签</td></tr><tr cid="n1423" mdtype="table_row"><td>函数</td><td>starts-with(@属性名,'属性开头的值')</td><td>//*[starts-with(@id,'username')]</td><td>匹配属性开头的值，处理属性值变化的元素</td></tr><tr cid="n1428" mdtype="table_row"><td><br></td><td>contains(@属性名,'属性包含的值')</td><td>//*[contains(@id,'username')]</td><td>匹配属性包含的值</td></tr><tr cid="n1433" mdtype="table_row"><td><br></td><td>text()='文本的值'</td><td>//a[text()='百度搜索']</td><td>匹配文本的值，可以替代 link_text 方法</td></tr><tr cid="n1438" mdtype="table_row"><td><br></td><td>contains(text(),'文本包含的值')</td><td>//a[contains(text(),'搜索')]</td><td>匹配文本包含的值，可以替代 partial_link_text 方法</td></tr></tbody></table>
*   xpath 轴语法
    
    <table><thead><tr cid="n1446" mdtype="table_row"><th>轴名称</th><th>结果</th></tr></thead><tbody><tr cid="n1449" mdtype="table_row"><td>ancestor</td><td>选取当前节点的所有先辈（父、祖父等）。</td></tr><tr cid="n1452" mdtype="table_row"><td>ancestor-or-self</td><td>选取当前节点的所有先辈（父、祖父等）以及当前节点本身。</td></tr><tr cid="n1455" mdtype="table_row"><td>attribute</td><td>选取当前节点的所有属性。</td></tr><tr cid="n1458" mdtype="table_row"><td>child</td><td>选取当前节点的所有子元素。</td></tr><tr cid="n1461" mdtype="table_row"><td>descendant</td><td>选取当前节点的所有后代元素（子、孙等）。</td></tr><tr cid="n1464" mdtype="table_row"><td>descendant-or-self</td><td>选取当前节点的所有后代元素（子、孙等）以及当前节点本身。</td></tr><tr cid="n1467" mdtype="table_row"><td>following</td><td>选取文档中当前节点的结束标签之后的所有节点。</td></tr><tr cid="n1470" mdtype="table_row"><td>namespace</td><td>选取当前节点的所有命名空间节点。</td></tr><tr cid="n1473" mdtype="table_row"><td>parent</td><td>选取当前节点的父节点。</td></tr><tr cid="n1476" mdtype="table_row"><td>preceding</td><td>选取文档中当前节点的开始标签之前的所有节点。</td></tr><tr cid="n1479" mdtype="table_row"><td>preceding-sibling</td><td>选取当前节点之前的所有同级节点。</td></tr><tr cid="n1482" mdtype="table_row"><td>self</td><td>选取当前节点。</td></tr></tbody></table>

**注意事项**

*   关于运算符：[https://www.w3school.com.cn/xpath/xpath_operators.asp](https://www.w3school.com.cn/xpath/xpath_operators.asp)，在 selenium 中一般用 and 来缩小范围（反之 or 是扩大范围），定位到具体的元素，但 xpath 本身支持其他运算符
    
    ```
    //*[@id='username' and @type='text']
    
    ```
    
*   xpath 语法中的值用单引号或双引号引起来的时候，注意在 python 代码中要注意区分。
    
*   xpath 的属性不局限于 id、name、class，可以是任意属性
    
*   xpath 的 class 属性需要填写全部值，而不是跟 class 定位只能填写部分（涉及到底层的封装）
    
*   xpath 官方支持的函数特别多，但不是浏览器都支持的
    
    ```
    //*[contains(@id,'username')]   #可以
    //*[contains(@id,'username')]   #可以
    //*[ends-with(@id,'username')]  #不支持
    
    ```
    
*   xpath 可以替换前面 6 种定位元素的语法，甚至还有扩充（如非 a 标签的文本），可以说没有 xpath 语法定位不到的元素
    
*   xpath 相对是慢的（加载整个 DOM）
    
*   link_text 和 partial_link_text 底层是调用 xpath 的