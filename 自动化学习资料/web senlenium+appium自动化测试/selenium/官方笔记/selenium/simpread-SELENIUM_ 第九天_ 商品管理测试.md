> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a2c36eb2377da)

> 截图

截图和日志
-----

*   截图
    

*   用 selenium 提供的 save_screenshot 即可
    
*   注意存放位置
    
*   文件命名可以用参数传递进来
    

*   日志
    

*   不要迷信日志，特别是你才写了一点点代码的时候
    
*   日志相对成熟，可以直接调用已有的
    
*   代码的调试可以借助 print，debug，trace 等手段
    
*   python 标准库的 logging 相对复杂（虽然也有简单的调用方式）
    
*   loguru 是一种新兴的日志方式
    

*   示例
    
    ```
        def get_element(self,locator,desc=None):
            """
            定位元素
            异常保护,如果定位不到就可以截图、日志
            :param locator:   定位器 如 (By.ID,'username') ['id','username']
            :return: 返回元素
    
            """
            try:
                return  self.driver.find_element(*locator)
            except:
                current_time = strftime('%Y%m%d%H%M%S')
                self.driver.save_screenshot(f'{screenshots_path}{desc}定位不到{current_time}.png')
                #方式一 ： 甚至可以用print，当你的代码比较小的时候
                #logging.error(f'{desc}无法定位')
                #方式二 : 写到文件中
                # log_time = strftime('%Y%m%d')  #再控制下追加写入
                # logging.basicConfig(filename=f'{logs_path}{current_time}.log')
                # logging.error(f'{desc}无法定位')
                #方式三：封装logging
                log.error(f'{desc}无法定位')
    
    ```
    

*   扩展
    

*   截图也可以单独封装为一个方法，供各个基类方法调用捕获
    

### logging

*   示例代码
    
    ```
    print()
    """
    日志相关内容：
        1- 日志的输出渠道：文件xxx.log    控制台输出
        2- 日志级别： DEBUG-INFO-WARNING-ERROR-CRITICAL
        3- 日志的内容：2021-10-20 13:50:52,766 - INFO - handle_log.py[49]:我是日志信息
                      年- 月 - 日 时:分:秒,毫秒       -级别   -哪个文件[哪行]:具体报错信息
        https://www.liujiangblog.com/course/python/71
    """
    from time import strftime
    import logging
    from utils.handle_path import logs_path
    #这部分不存在单例
    def logger(fileLog=True,name=__name__):
        """
        :param fileLog: bool值，如果为True则记录到文件中否则记录到控制台
        :param name: 默认是模块名
        :return: 返回一个日志对象
        """
        #0 定义一个日志文件的路径，在工程的logs目录下，AutoPolly开头-年月日时分.log
        logDir = f'{logs_path}/AutoPolly-{strftime("%Y%m%d%H%M")}.log'
        #1 创建一个日志收集器对象
        logObject = logging.getLogger(name)
        #2- 设置日志的级别
        logObject.setLevel(logging.INFO)
        #3- 日志内容格式
        fmt = "%(asctime)s - %(levelname)s - %(filename)s[%(lineno)d]:%(message)s"
        formater = logging.Formatter(fmt)
    
        if fileLog:#输出到文件
            #设置日志渠道--文件输出
            handle = logging.FileHandler(logDir,encoding='utf-8')
            #日志内容与渠道绑定
            handle.setFormatter(formater)
            #把日志对象与渠道绑定
            logObject.addHandler(handle)
        else: #输出到控制台
            #设置日志渠道--控制台输出
            handle2 = logging.StreamHandler()
            #日志内容与渠道绑定
            handle2.setFormatter(formater)
            #把日志对象与渠道绑定
            logObject.addHandler(handle2)
    
        return logObject
    
    log = logger()#文件输出日志
    
    if __name__ == '__main__':
        log = logger(fileLog=False)#控制台输出日志
        log.info('我是日志信息')
    
    ```
    

### loguru

*   示例代码
    
    ```
    # Author:  wuxianfeng
    # Company: songqin
    # File:    handle_loguru.py
    # Date:    2021/10/24
    # Time:    9:30
    print()
    '''
    知识点:
    https://loguru.readthedocs.io/en/stable/overview.html#ready-to-use-out-of-the-box-without-boilerplate
    '''
    
    # Author:  wuxianfeng
    # Company: songqin
    # File:    handle_loguru.py
    # Date:    2021/9/26
    # Time:    21:26
    from configparser  import  ConfigParser
    from loguru import logger
    from utils.handle_path import logs_path, config_path
    from time import strftime
    class MyLog():
        __instance = None #单例实现
        __init_flag = True #控制init调用，如果调用过就不再调用
        def __new__(cls, *args, **kwargs):
            if not cls.__instance:
                cls.__instance = super().__new__(cls)
            return cls.__instance
        def __init__(self):
            if self.__init_flag: #看是否调用过
                __curdate = strftime('%Y%m%d-%H%M')
                cfg = ConfigParser()
                cfg.read(config_path+'loguru.ini',encoding='utf-8')
                logger.remove(handler_id=None)  #关闭console输出
                logger.add(logs_path+'AutoPolly_'+__curdate+'.log', #日志存放位置
                           retention=cfg.get('log','retention'), #清理
                           rotation=cfg.get('log','rotation'), #循环 达到指定大小后建立新的日志
                           format=cfg.get('log','format'), #日志输出格式
                           level=cfg.get('log','level')) #日志级别
                self.__init_flag = False #如果调用过就置为False
        def info(self,msg):
            logger.info(msg)
        def warning(self,msg):
            logger.warning(msg)
        def critical(self,msg):
            logger.critical(msg)
        def error(self,msg):
            logger.error(msg)
        def debug(self,msg):
            logger.debug(self,msg)
    log = MyLog()
    if __name__ == '__main__':
    
            log1 = MyLog()
            log1.critical('test critical')
            from time import sleep
            sleep(1)
            log2 = MyLog()
            log2.error('test error')
            print(id(log1))
            print(id(log2))
    
    ```
    

钩子函数
----

*   针对日志出现'\uXXXX'可以使用钩子函数 pytest_collection_modifyitems，对用例的名字进行处理，这个钩子函数还可以对用例进行处理（比如调整顺序）
    

*   示例
    
    ```
    def pytest_collection_modifyitems(items):
        """
        测试用例收集完成时，将收集到的item的name和nodeid的中文显示在控制台上
        :return:
        """
        for item in items:
            item.name = item.name.encode("utf-8").decode("unicode_escape")
            print(item.nodeid)
            item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")
    
    ```
    

### 成功和失败用例的混合 allure 输出报告

*   注意事项
    

*   数据都封装在 yaml 中
    
*   混合后要注意成功后要退出，失败后无需退出，需要增加一个判断标记，推荐使用 url 来判断
    
*   allure 的基础
    
*   pytest.assume 插件
    

*   示例代码
    
    ```
        @allure.severity('critical')  #登录功能是其他模块的依赖，可以用插件pytest-dependency
        @pytest.mark.parametrize('casetitle,username,password,locator,expected',
                                 get_yaml_data(f'{test_datas_path}login_all_data.yaml'))
        @allure.title('{casetitle}') #此处不要写错
        def  test_login_all(self,casetitle,username,password,locator,expected):
            with allure.step('1. 打开登录页面'):
                test_loginpage = LoginPage()
                test_loginpage.open_loginpage()
            with allure.step('2. 登录宝利商城'):
                test_loginpage.login_polly(username,password)
            # assert test_loginpage.wait_get_element_text(locator) == expected
            # if test_loginpage.driver.current_url == MAINPAGE_URL:
            #     # TODO 你可以去尝试用XX元素存在与否来控制下面的语句执行，你可能会遇到问题。可以用的！
            #     test_loginpage.click_element(['xpath','//img'])  #如果不控制直接点击该元素，就无法定位到
            #     test_loginpage.wait_click_element(['xpath',"//span[text()='退出']"])
    
            # 如果断言失败了（界面跳转），也要去做下面的if控制流
            # 可以用pytest的插件 pytest-assume
            with allure.step('3. 断言'):
                pytest.assume(test_loginpage.wait_get_element_text(locator) == expected)
            with allure.step('4. 断言后的处理'):
                if test_loginpage.driver.current_url == MAINPAGE_URL:
                    # TODO 你可以去尝试用XX元素存在与否来控制下面的语句执行，你可能会遇到问题。可以用的！
    
                    test_loginpage.click_element(['xpath','//img'])  #如果不控制直接点击该元素，就无法定位到
                    test_loginpage.wait_click_element(['xpath',"//span[text()='退出']"])
    
    
    
    ```
    

allure 基础
---------

<table><thead><tr cid="n128" mdtype="table_row"><th>使用方法</th><th>参数值</th><th>参数说明</th></tr></thead><tbody><tr cid="n132" mdtype="table_row"><td>@allure.epic()</td><td>epic 描述</td><td>敏捷中的概念，定义为史诗，往下是 feature</td></tr><tr cid="n136" mdtype="table_row"><td>@allure.feature()</td><td>模块名称</td><td>功能点的描述，往下是 story</td></tr><tr cid="n140" mdtype="table_row"><td>@allure.story()</td><td>用户故事</td><td>往下是 title</td></tr><tr cid="n144" mdtype="table_row"><td>@allure.title('用例的标题')</td><td>用例的标题</td><td>html 报告的名称</td></tr><tr cid="n148" mdtype="table_row"><td>@allure.testcase()</td><td>测试用例的链接地址</td><td>对应功能测试用例系统中的具体用例</td></tr><tr cid="n152" mdtype="table_row"><td>@allure.issue()</td><td>缺陷</td><td>对应缺陷管理系统中的链接</td></tr><tr cid="n156" mdtype="table_row"><td>@allure.description()</td><td>测试用例描述</td><td><br></td></tr><tr cid="n160" mdtype="table_row"><td>@allure.step()</td><td>测试步骤</td><td><br></td></tr><tr cid="n164" mdtype="table_row"><td>@allure.severity()</td><td>用例等级</td><td>blocker,critical,normal,minor,trivial</td></tr><tr cid="n168" mdtype="table_row"><td>@allure.link()</td><td>链接</td><td>定义一个链接，在测试报告中展现</td></tr><tr cid="n172" mdtype="table_row"><td>@allure.attachment()</td><td>附件</td><td>报告添加附件</td></tr></tbody></table>

pytest.assume 断言后继续跑
--------------------

> [https://blog.51cto.com/u_14900374/2605161](https://blog.51cto.com/u_14900374/2605161)

*   安装
    
    ```
    pip install pytest-assume
    
    ```
    
*   实际测试的过程中，有可能遇到一种情况，就是你某个断言执行失败也想要做下去（比如登录的测试，测试失败后，还是要返回主页继续下一轮的测试）。而默认情况下，如果断言失败，assert 后面的语句是不会执行的了。
    
*   可以应用在多重断言的场景！（可以同时做多个断言）
    
*   没有 assume 的示例
    
    ```
    import pytest
    def test_001():
        assert 1==2   #如果改为1==1，下面是不会执行的
        print('\n对了会做，错了不会做')
    
    if __name__ == '__main__':
        pytest.main(['-s','test_order_001.py'])
    
    ```
    
*   有 assume 的示例
    
    ```
    import pytest
    def test_001():
        pytest.assume(1==2)
        print('\n对了会做，错了也会做')
    
    if __name__ == '__main__':
        pytest.main(['-sv','test_order_001.py'])
    
    ```
    

*   第二个用例：我们先写用例再看要用到哪些页面（方法 -> 元素）
    

*   用例步骤
    
    ```
    1. 打开登录页
    2. 登录宝利商城
    3. 进入首页     : 新建一个首页的页面对象
    4. 点击添加商品页面  : 新建一个添加商品的页面对象
    5. 操作步骤         : 添加商品页面的方法
    6. 断言: 切换到商品列表界面，判断第一个商品是否是刚才新增的商品
            新建一个商品列表界面
             商品列表界面：新增一个获取第一个商品名称的方法
    
    操作步骤
    #1. 点击商品分类
    #2. 选择1级分类
    #3. 选择2级分类
    #4. 输入商品名称
    #5. 输入副标题
    #6. 点击商品品牌
    #7. 选择1级分类
    #8. 下一步，填写商品促销
    #9. 下一步，填写商品属性
    #10. 下一步，选择商品关联
    #11. 完成，提交商品
    #12. 确定
    
    ```
    

MainPage 首页
-----------

*   注意事项
    

*   **PO 设计原则：Methods return other PageObjects**
    
*   登录成功后出现首页
    
*   首页方法可以跳转到各个子菜单，返回页面对象的实例
    
*   页面元素定位器的获取优化
    

*   参考代码
    
    ```
    class MainPage(BasePage):
        """
        首页
        1. 跳转到各个子菜单（N多的方法）
        """
        def goto_addproductpage(self):
            self.click_element(self.menu_productmanage)
            self.wait_click_element(self.submenu_pm_addproduct)
            return AddProductPage()
        def goto_productlistpage(self):
            self.click_element(self.menu_productmanage)
            self.wait_click_element(self.submenu_pm_productlist)
            return ProductListPage()
    
    ```
    
*   loginPage 的改造
    
    ```
        def login_polly(self,username,password):
            #要用到页面元素
            #元素的封装可以用py文件，也可以用其他类型的配置文件
            self.input_text(self.username_input,username)
            self.input_text(self.password_input,password)
            self.click_element(self.login_button)
            return MainPage()   #返回首页
    
    ```
    

### 页面定位器的优化

*   实现目标：
    

*   每个页面类拥有自己的页面元素（定向分发）
    
    ```
    元素通过yaml封装成如下格式:
    
    页面类:
     元素名: 定位器
    
    ```
    
*   页面元素名是类的属性，值就是对应的定位器
    
    ```
    setattr(obj,attr,value)  #为一个对象设置属性
    
    ```
    

*   示例：basePage 初始化方法
    
    ```
        def __init__(self):
            self.driver = CommDriver().get_driver()
            #元素的获取在基类中实现，下面的方法一下获取所有的元素
            #self.locators = get_yaml_data(f'{common_path}allelements.yaml')
            #最佳实践: 每个页面获取自己的元素
            #每个页面: 类名区分页面
            #应该在元素封装中定义不同的页面对应不同的元素-定位器
            self.locators = get_yaml_data(f'{common_path}elements.yaml')[self.__class__.__name__]
            #优化  让具体的页面有这个属性（元素的名字）
            for  element_name,locator in self.locators.items():
                setattr(self,element_name,locator) #setattr设置 self实例 属性element_name的值是locator
     
    
    ```
    

AddProductPage 添加商品页面
---------------------

*   示例代码：**注意 MD 格式的缩进看的跟实际会有差异**
    
    ```
    class AddProductPage(BasePage):
        # 因为要切换回主页，所以要增加一个方法
        def back_mainpage(self):
            self.click_element(self.home_button)
    
        def addproduct(self,kidx1,kidx2,pname,subtitle,bidx):
            # 1. 点击商品分类
            self.click_element(self.product_kind_select)
            # 2. 选择1级分类 #
            # TODO 如果没有一分类？
            # 这个一级分类的元素定义是动态的
            self.product_kind_select_index1[-1]=self.product_kind_select_index1[-1].format(kidx1)
            self.click_element(self.product_kind_select_index1)
            # 3. 选择2级分类
            # TODO 如果没有二级分类？
            self.product_kind_select_index2[-1]=self.product_kind_select_index2[-1].format(kidx2)
            self.click_element(self.product_kind_select_index2)
            # 4. 输入商品名称
            self.input_text(self.product_name,pname)
            # 5. 输入副标题
            self.input_text(self.product_subtitle,subtitle)
            # 6. 点击商品品牌
            self.click_element(self.product_brand_select)
            # 7. 选择1级分类
            self.product_brand_select_idx[-1]=self.product_brand_select_idx[-1].format(bidx)
            self.click_element(self.product_brand_select_idx)
            # 8. 下一步，填写商品促销
            self.click_element(self.next_commodity_promotion_btn)
            # 9. 下一步，填写商品属性
            self.click_element(self.next_product_attribute_btn)
            # 10. 下一步，选择商品关联
            self.click_element(self.netxt_product_related_btn)
            # 11. 完成，提交商品
            self.click_element(self.complete_btn)
            # 12. 确定
            self.click_element(self.submit_btn)
    
    
    ```
    

ProductListPage 商品列表页面
----------------------

*   示例代码
    
    ```
    class ProductListPage(BasePage):
        def get_firstproduct_name(self):
            return self.get_element_text(self.first_product)
    
    ```
    

测试方法 test_addproduct
--------------------

*   注意页面的切换！！
    
*   示例代码
    
    ```
    def test_addproduct():
        """
        1. 打开登录页
        2. 登录宝利商城
        3. 进入首页     : 新建一个首页的页面对象
        4. 点击添加商品页面  : 新建一个添加商品的页面对象
        5. 操作步骤         : 添加商品页面的方法
        6. 断言: 切换到商品列表界面，判断第一个商品是否是刚才新增的商品
                 新建一个商品列表界面
                    新增一个获取第一个商品名称的方法
        :return:
        """
        # test_loginpage = LoginPage()
        # test_loginpage.open_loginpage()
        # test_loginpage.login_polly('松勤老师','123456')
        # test_mainpage = MainPage()
        # test_mainpage.goto_addproductpage()
        #登录成功后切换到首页
        test_mainpage = LoginPage().open_loginpage().login_polly('松勤老师','123456')
        test_addproductpage = test_mainpage.goto_addproductpage()
        # productname要做到不可重复，随机
        test_pname = '自动化'+get_rand_str(5)
        title_name = '副标题'+get_rand_str(5)
        test_addproductpage.addproduct('1','1',test_pname,title_name,'1')
        # 添加完一个商品后还在添加商品页面
        # 回到首页mainpage，再切换到productlist界面，获取第一个商品名字
        test_addproductpage.back_mainpage()
        # 去到商品列表界面，要在test_mainpage上做，已经在首页
        test_productlistpage = test_mainpage.goto_productlistpage()
        # 获取商品列表页面的第一个商品名字
        result_productname = test_productlistpage.get_firstproduct_name()
        #断言 输入的商品名字跟当前页面中的商品名字一致
        assert test_pname == result_productname
    
    if __name__ == '__main__':
        pytest.main(['-sv',__file__])
    
    ```