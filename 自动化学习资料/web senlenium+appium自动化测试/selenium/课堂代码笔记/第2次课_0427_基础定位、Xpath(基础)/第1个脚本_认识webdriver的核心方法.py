# Author:  wuxianfeng
# Company: songqin
# File:    第1个脚本_认识webdriver的核心方法.py
# Date:    22-4-27
# Time:    下午 8:08
print()
'''
知识点:  dir
>>> dir(list)   ==>list
['__add__', '__class__', '__class_getitem__', '__contains__', '__delattr__', '__delitem__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__gt__', '__hash__', '__iadd__', '__imul__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__reversed__', '__rmul__', '__setattr__', '__setitem__', '__sizeof__', '__str__', '__subclasshook__', 'append', 'clear', 'copy', 'count', 'extend', 'index', 'insert', 'pop', 'remove', 'reverse', 'sort']
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.get('https://www.baidu.com')
print('driver的对象类型是:',type(driver))
print('|对象属性(方法)|说明|')
print('|---|---|')
for _ in dir(driver):  #dir 查看对象的所有方法  #_是变量名
    if _[0]!='_':         #首字符不是_ ,不是魔术方法
        print('|',_,'||')