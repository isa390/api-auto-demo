# Author:  wuxianfeng
# Company: songqin
# File:    第9个脚本_属性法.py
# Date:    22-4-27
# Time:    下午 9:38
print()
'''
知识点:
1. 语法
    //*
    //标签名[@属性名='属性值']
    示例   //input[@name='username']  #建议加标签名
    示例   //*[@id='username']   #id属性常配合//*
    class属性，如果是多值，在xpath的属性法中都要写上！！
    示例   //*[@class='px vm']
2. 属性法的引号要和xpath定位的引号注意区别！
3. 前面6大基础定位用的 是id/class/name
   xpath可以替换前的所有方法，而且可以利用其他任何属性来定位
   xpath是万能的
4. xpath是慢的（css是相对快）
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium0425\第2次课_0427_基础定位、Xpath(基础)\day2.html')
driver.find_element('xpath', "//*[@class='px vm']").send_keys('admin')

