# Author:  wuxianfeng
# Company: songqin
# File:    第2个脚本_tag定位.py
# Date:    22-4-27
# Time:    下午 8:22
from selenium.webdriver.common.by import By

print()
'''
知识点:
1. 书写自动补齐的方法的时候写关键字即可
2. find_element_by_* 这类方法都在4.0中弃用,代表今后可能会失效，当前仍然可用
    弃用的实现： warnings.warn('message',DeprecationWarning)
        warnings.warn(
            "find_element_by_tag_name is deprecated. Please use find_element(by=By.TAG_NAME, value=name) instead",
            DeprecationWarning,
            stacklevel=2,
        )
3.     def find_element_by_tag_name(self, name) -> WebElement:
        typing 模块作用，python弱类型，加了类型检查的功能
                       -> WebElement 返回值类型
                       不做校验，仅作提示！（pycharm右上角）
4. pycharm的自动导入，  alt+enter（万能键），mac不一样的。  
5. tag定位基本没用，重复太多
6. ★对于find_element方法，如果定位到多个，操作的是第一个，所有定位方法都是如此！
7. 对 HEAD中的TITLE标签，无法输出其文本
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get(r'./day2.html')
# 1. tag定位
# driver.find_element_by_tag_name('p')  #定位到了元素
#  webelement对象 操作
# print(driver.find_element_by_tag_name('p').text)  #===>这是一个段落
print(driver.find_element(by=By.TAG_NAME, value='p').text)
print(driver.find_element(By.TAG_NAME, 'p').text)
print(driver.find_element('tag name', 'a').text)
# print(driver.find_element('tag name', 'title').text)
