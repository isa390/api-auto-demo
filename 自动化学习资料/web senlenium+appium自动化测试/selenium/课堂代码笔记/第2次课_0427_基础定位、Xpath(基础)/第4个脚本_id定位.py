# Author:  wuxianfeng
# Company: songqin
# File:    第4个脚本_id定位.py
# Date:    22-4-27
# Time:    下午 8:43
print()
'''
知识点:  注意点
1. id的属性的值是唯一的，推荐用！
2. id的属性的值是可能会变
3. id的属性的值数字开头的，下一堂课揭晓
4. send_keys是追加输入，不会清空原来的内容的！
5. 如果要清空，在元素上操作clear
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium0425\第2次课_0427_基础定位、Xpath(基础)\day2.html')
# 定位到id属性的值是username的元素，并输入username
driver.find_element('id','username').send_keys('username')
ele_password = driver.find_element('id','password')
ele_password.send_keys('pass')
ele_password.send_keys('word')
sleep(3)
ele_password.clear()
#老师，能帮忙演示下find_elements_by_id吗？没搞明白
#--->list [] 下标取元素，元素的值必须是一样的元素，列表中每个元素的类型都是webelement





