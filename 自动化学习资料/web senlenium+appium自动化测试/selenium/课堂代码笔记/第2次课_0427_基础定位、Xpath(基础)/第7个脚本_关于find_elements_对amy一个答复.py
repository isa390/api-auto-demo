# Author:  wuxianfeng
# Company: songqin
# File:    第7个脚本_关于find_elements_对amy一个答复.py
# Date:    22-4-27
# Time:    下午 9:06
from selenium.webdriver.common.by import By

print()
'''
知识点:
1 .find_elements 返回是个list
2. 返回是个list中都是webelement对象
3. 
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium0425\第2次课_0427_基础定位、Xpath(基础)\day2.html')
ele_names = driver.find_elements(By.NAME,'username')
ele_names[0].send_keys('username')
ele_names[1].send_keys('password')