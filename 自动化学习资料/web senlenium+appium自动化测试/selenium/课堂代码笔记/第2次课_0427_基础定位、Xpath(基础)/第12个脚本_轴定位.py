# Author:  wuxianfeng
# Company: songqin
# File:    第12个脚本_轴定位.py
# Date:    22-4-27
# Time:    下午 10:08
print()
'''
知识点:

轴名称	结果
ancestor	选取当前节点的所有先辈（父、祖父等）。
ancestor-or-self	选取当前节点的所有先辈（父、祖父等）以及当前节点本身。
attribute	选取当前节点的所有属性。   //*[@属性名]
child	选取当前节点的所有子元素。
descendant	选取当前节点的所有后代元素（子、孙等）。
descendant-or-self	选取当前节点的所有后代元素（子、孙等）以及当前节点本身。
following	选取文档中当前节点的结束标签之后的所有节点。
namespace	选取当前节点的所有命名空间节点。
parent	选取当前节点的父节点。  #相当于..
preceding	选取文档中当前节点的开始标签之前的所有节点。
preceding-sibling	选取当前节点之前的所有同级节点。
self	选取当前节点。


----
//*[@id='username']/ancestor::body
//*[@id='username']/ancestor::div

ancestor-or-self  自学
----
//div[2]/child::a   #无法定位    a不在//div[2]下面
//div[2]//child::a   #可以定位到  
----
//div[2]/descendant::a   
descendant-or-self   :包含自己
----
//body/div[1]/following::input    #</div>关闭之后的
----
preceding   选取文档中当前节点的开始标签之前的所有节点。
//*[@id='username']/preceding::a
//*[@id='username']/preceding::div  #div不能包含这个id=username的元素


 









'''
