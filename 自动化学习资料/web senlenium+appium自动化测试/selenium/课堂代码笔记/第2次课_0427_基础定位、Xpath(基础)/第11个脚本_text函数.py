# Author:  wuxianfeng
# Company: songqin
# File:    第11个脚本_text函数.py
# Date:    22-4-27
# Time:    下午 9:58
print()
'''
知识点:
1. xpath可以替代所有的6大基础写法
   tag name  ==> //a
   id/class/name ==> 属性法
   link_text       ==> //a[text()="链接的所有文本"]
   partial_link_text  ==> //a[contains(text(),'含有的文本')]
2. //input[@id='username' and @class='px vm']  逻辑运算符的示例
3. //*[@id]    所有具有id属性的元素！（爬虫）
4. 元素定位是最重要的基本功，一定要手写，松勤的项目课不要有任何的copy xpath.
5. 
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium0425\第2次课_0427_基础定位、Xpath(基础)\day2.html')
# driver.find_element('xpath','//a[text()="腾讯QQ"]').click()

driver.find_element('xpath',"//a[contains(text(),'QQ')]").click()


