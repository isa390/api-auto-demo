# Author:  wuxianfeng
# Company: songqin
# File:    第10个脚本_函数法.py
# Date:    22-4-27
# Time:    下午 9:43
print()
'''
知识点:
1. 语法
    starts-with(@属性名,属性开头的值)
    contains(@属性名,属性包含的值)
    text()='文本的值'
    组合 contains(text(),'文本包含的值')
2. 浏览器的定位技巧 ★★★★★★
    2.1 inspector  左上角的箭头
    2.2 点击元素  elements中能定位到HTML源码（右键检查直接定位过来，在自动化打开的浏览器中会失效）
    ----
    2.3 初学者一定要先在浏览器中保证能定位到1of1，再把你的表达式放到代码中
3. 点击元素 webelement.click()  
4 . 报错的提示是 css  selector？？？
5.  ★★★★★★ 元素定位的过程不能省！不要100%相信！！是分析网页的过程！！！
6. 如果一个元素是点击后产生的，可能需要加载，可能需要等待！
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.get('http://106.14.1.150:8090/forum.php')
driver.find_element('xpath',"//button[@class='pn vm']").click()
# driver.find_element('id','username_Lczmy').send_keys('admin')
#selenium.common.exceptions.NoSuchElementException: Message: no such element: Unable to locate element: {"method":"css selector","selector":"[id="username_Lczmy"]"}
  # (Session info: chrome=100.0.4896.127)
sleep(2)
driver.find_element('xpath',"//*[starts-with(@id,'username_L')]").send_keys('ad')
driver.find_element('xpath',"//*[contains(@id,'username_L')]").send_keys('min')
