from selenium import webdriver
print('webdriver的对象类型是',type(webdriver))
driver = webdriver.Chrome()
print('driver的对象类型',type(driver))
driver.get('https://www.baidu.com')
ele_search = driver.find_element('id','kw')
print('html元素的对象类型是',type(ele_search))
print('|对象属性(方法或类)|说明|')
print('| ---- | ---- |')
for _ in dir(ele_search):
    if _[0]!='_':
        print(f'|{_}||')