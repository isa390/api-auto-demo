# Author:  wuxianfeng
# Company: songqin
# File:    第6个脚本_每日百度热点_find_elements.py
# Date:    22-4-29
# Time:    下午 9:43
print()
'''
知识点:
1. find_elements的返回类型list 
2. 每个元素都是webelement
3. 应用场景：
    爬虫
    获取多个同类型元素的文本
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get('https://www.baidu.com')
css = '.title-content-title'
ele_hots = driver.find_elements('css selector',css)
print('find_elements对象的返回类型',type(ele_hots))
for ele_hot  in ele_hots:
    print(ele_hot.text)