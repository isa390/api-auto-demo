# Author:  wuxianfeng
# Company: songqin
# File:    第1个脚本_css基础选择器.py
# Date:    22-4-29
# Time:    下午 8:19
print()
'''
知识点:
1  浏览器的技巧： 复制属性值的是时候不要手写，双击属性值即可全选，复制黏贴即可。
    l和1，o0O等
'''

from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get('http://106.14.1.150:8090/forum.php')
# driver.find_element('css selector','#ls_username').send_keys('admin')
driver.find_element('css selector','.pn.vm').click()
sleep(2)
driver.find_element('css selector',"input[id^='username_L']").send_keys('admin')