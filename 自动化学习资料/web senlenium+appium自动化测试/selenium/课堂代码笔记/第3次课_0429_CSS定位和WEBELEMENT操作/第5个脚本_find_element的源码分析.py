# Author:  wuxianfeng
# Company: songqin
# File:    第5个脚本_find_element的源码分析.py
# Date:    22-4-29
# Time:    下午 9:25
print()
'''
知识点:
1. id、class、name、tag底层都是css
2. link_text / partial_link_text 底层都是xpath（无法从源码分析得到）
    def find_element(self, by=By.ID, value=None) -> WebElement:
    # find_element方法 by默认值就是id
        if isinstance(by, RelativeBy): #相对定位的判断
            elements = self.find_elements(by=by, value=value)
            if not elements:
                raise NoSuchElementException(f"Cannot locate relative element with: {by.root}")
            return elements[0]
        # 重点
        if by == By.ID:   # 如果是ID定位，底层是css定位
            by = By.CSS_SELECTOR  # 就变成CSS定位
            value = '[id="%s"]' % value  #怎么变的呢?
            # ('ID','ls_username')   #id的写法
            # [id="ls_username"]     #css的写法
        elif by == By.TAG_NAME:  #如果是tag定位
            by = By.CSS_SELECTOR  # TAG:P ==> CSS:P 字符串value没有做替换
        elif by == By.CLASS_NAME:  #如果是class定位，底层也是css
            by = By.CSS_SELECTOR
            value = ".%s" % value
            # pn
            # vm
            # pn.vm
            # CSS写法: .pn.vm  #value前面加.
        elif by == By.NAME:   #NAME定位的替换跟ID一样的，也是用属性法替换
            by = By.CSS_SELECTOR
            value = '[name="%s"]' % value

        return self.execute(Command.FIND_ELEMENT, {
            'using': by,
            'value': value})['value']
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get('D:\pythonProject\AutoTest\AutoSelenium0425\第3次课_0429_CSS定位和WEBELEMENT操作\day3css.html')
# 1. 利用默认值写代码
driver.find_element(value='ls_password').send_keys('p')
driver.find_element(by='id',value='ls_password').send_keys('a')
driver.find_element('css selector','#ls_password').send_keys('s')
# 2. 其他写法
# 2.1 最重要的写法！！！
ele_password_locator = 'css selector','#ls_password' #tuple
driver.find_element(*ele_password_locator).send_keys('s')  #解包 *
# #selenium.common.exceptions.InvalidArgumentException: Message: invalid argument: 'using' must be a string
#   (Session info: chrome=100.0.4896.127)
# 2.2
driver.find_element(*['css selector','#ls_password']).send_keys('w')

#