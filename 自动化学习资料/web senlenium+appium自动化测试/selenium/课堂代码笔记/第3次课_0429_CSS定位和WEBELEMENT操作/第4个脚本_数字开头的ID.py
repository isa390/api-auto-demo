# Author:  wuxianfeng
# Company: songqin
# File:    第4个脚本_数字开头的ID.py
# Date:    22-4-29
# Time:    下午 8:57
print()
'''
知识点:  数字开头的ID

'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get('D:\pythonProject\AutoTest\AutoSelenium0425\第3次课_0429_CSS定位和WEBELEMENT操作\day3css.html')
# driver.find_element('css selector','#9XCDM').send_keys('hello')
# selenium.common.exceptions.InvalidSelectorException: Message: invalid selector: An invalid or illegal selector was specified
#   (Session info: chrome=100.0.4896.127)
# 无效的表达式（非法的表达式）
# #\39 XCDM  右键copy得到的表达式
# 还要加个转义！
driver.find_element('css selector','#\\39 XCDM').send_keys('hello')
