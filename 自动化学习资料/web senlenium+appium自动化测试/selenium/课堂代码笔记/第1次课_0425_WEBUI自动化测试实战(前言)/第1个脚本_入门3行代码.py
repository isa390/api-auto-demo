# Author:  wuxianfeng
# Company: songqin
# File:    第1个脚本_入门3行代码.py
# Date:    22-4-25
# Time:    下午 9:32
print()
'''
知识点:

'''
from selenium import webdriver
#1.  导入
#    1.1 必须要安装好环境才可以导入
#    1.2 ctrl+鼠标左键点击selenium
#    1.3 selenium物理文件的位置
#    1.4 from  selenium（包） import webdriver（包）

driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
#2. 定义一个变量driver 接收 webdriver.Chrome()
#   2.1 webdriver.Chrome()===>Chrome类,实例化
#   2.2 点击webdriver得到
#           from .chrome.webdriver import WebDriver as Chrome  # noqa
#　　　　　　　　　webdriver目录下>chrome目录>webdriver.py WebDriver类别名Chrome
#   2.3 Chrome首字母要大写， ()实例化不要漏了

driver.get('http://121.41.14.39:8088/index.html#/')
# 3.  driver.get('www.baidu.com')报错
# selenium.common.exceptions.InvalidArgumentException: Message: invalid argument
#     3.1 http协议的基础知识：URL的格式  schema不要漏了(协议：http/https/FILE/FTP/magnet)....
#     3.2 http的请求方式 get！ 如果是接口要知道的更多！
#     3.3 输入url的时候直接复制地址栏的地址即可自动携带协议








