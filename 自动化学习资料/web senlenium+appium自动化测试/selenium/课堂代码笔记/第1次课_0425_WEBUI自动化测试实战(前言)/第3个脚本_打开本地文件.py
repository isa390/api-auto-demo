# Author:  wuxianfeng
# Company: songqin
# File:    第3个脚本_打开本地文件.py
# Date:    22-4-25
# Time:    下午 9:50
print()
'''
知识点:
1. pycharm的实时模板: file->settings -> 实时模板- > user->定义模板
2. pycharm的插件 ： file->settings->plugin -> marketplace ->搜chinese 第二个
'''
from selenium import webdriver

driver = webdriver.Chrome()
test_flag = 4
if test_flag == 4:
    from pathlib import Path  # 标准库

    test_html_v2 = str(Path('test.html').resolve())
    # Path('test.html') 得到windowpathlib的对象，resolve()得到一个绝对路径，str转换成字符串
    driver.get(test_html_v2)

if test_flag == 3:  # os库打开
    import os

    test_html_v1 = os.path.abspath('./test.html')
    driver.get(test_html_v1)
if test_flag == 1:  # 练习就用1即可
    # driver.get('./test.html')  #直接写文件名不行的  或者./ 都不行的
    driver.get(r'D:\pythonProject\AutoTest\AutoSelenium0425\第1次课_0425_WEBUI自动化测试实战(前言)\test.html')
    # mac这样好像不行，加file:///
if test_flag == 2:  # mac  win 通用的
    driver.get(r'file:///D:\pythonProject\AutoTest\AutoSelenium0425\第1次课_0425_WEBUI自动化测试实战(前言)\test.html')
