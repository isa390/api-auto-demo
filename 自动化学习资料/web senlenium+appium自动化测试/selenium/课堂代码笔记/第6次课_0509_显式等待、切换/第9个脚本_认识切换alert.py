# Author:  wuxianfeng
# Company: songqin
# File:    第9个脚本_认识切换alert.py
# Date:    22-5-9
# Time:    下午 9:46
from selenium.webdriver.common.alert import Alert

print()
'''
知识点:  alert切换
1. 识别alert
    alert弹出框是无法定位到元素  ： 只能用alert来处理
    UI->弹窗->elementui风格，这个是可以定位 : 正常的元素操作
    打开爱奇艺的时候有个允许和禁止，这个无法定位到元素，但也不是alert（要用option来控制）
2. alert弹框要用switch_to.alert的这种方法操作
3. 掌握切换alert后的三种方法
    dismiss   取消 ===> confirm/prompt
    accept    确定 ===>alert/confirm/prompt
    send_keys  输入内容  ===>prompt
4. alert_is_present
   --  传参： 无参数
   --  返回： 切换
   --  作用： 切换？
'''
from selenium import webdriver
from time import sleep

driver = webdriver.Chrome()
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium0425\第6次课_0509_显式等待、切换\alert.html')
test_flag = 5
if test_flag == 1:
    driver.find_element('id', 'alert').click()
    sleep(2)
    driver.switch_to.alert.accept()
if test_flag == 2:
    driver.find_element('id', 'confirm').click()
    sleep(2)
    driver.switch_to.alert.dismiss()
if test_flag == 3:
    driver.find_element('id', 'prompt').click()
    driver.switch_to.alert.send_keys('hello china')
    sleep(5)
    driver.switch_to.alert.accept()
if  test_flag == 4: # Alert的操作
    driver.find_element('id', 'alert').click()
    sleep(2)
    Alert(driver).accept()

# 看到点击，有可能会有延迟
# 要实现可靠的切换
if  test_flag == 5: #  可靠的切换
    driver.find_element('id', 'alert').click()
    sleep(2)   #不可靠
    from selenium.webdriver.support.wait import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    method = EC.alert_is_present()
    WebDriverWait(driver,5,0.5).until(method).accept()
    #until(METHOD)的返回是 driver.switch_to.alert
    # 1.  if  WebDriverWait(driver,5,0.5).until(EC.alert_is_present())
    # 2.  WebDriverWait(driver,5,0.5).until(EC.alert_is_present().accept())
    # 3.      WebDriverWait(driver,5,0.5).until(EC.alert_is_present())
              #driver.switch_to.alert.accept()
    # 4.  WebDriverWait(driver,5,0.5).until(EC.alert_is_present()).accept()