# Author:  wuxianfeng
# Company: songqin
# File:    第1个脚本_认识隐式等待的缺陷.py
# Date:    22-5-9
# Time:    下午 8:16
print()
'''
知识点:

'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.implicitly_wait(5)
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
# 元素已经找到了，但是属性没有
print(driver.find_element('id', 'ls_username').get_attribute('placeholder')) #str
# 通过这个实例，你会发现implicitly_wait 对这种没有加载出来的属性是无能为力的！
# CASE: 这个属性就是有的，但不会一下子加载出来的，你会怎么办



