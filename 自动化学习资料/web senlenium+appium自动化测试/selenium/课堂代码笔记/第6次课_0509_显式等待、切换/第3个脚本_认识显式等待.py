# Author:  wuxianfeng
# Company: songqin
# File:    第3个脚本_认识显式等待.py
# Date:    22-5-9
# Time:    下午 8:31
print()
'''
知识点:  理解显式等待的最大超时时间，轮询====>self._poll
1. 包导入
2. WebDriverWait   等待
    def __init__(self, driver, timeout, poll_frequency=POLL_FREQUENCY, ignored_exceptions=None):
    driver:打开的浏览器实例
    timeout:最大超时时间
    poll_frequency:轮询的频率  默认是0.5s
    ignored_exceptions: 忽略的异常 ，tuple类型，默认只有一个NoSuchElementException，相对不那么重要一般不改
3. until/until_not  处理轮询的核心代码

4. expected_conditions   预期的条件(4.0之前是类，4.0之后是函数)
    所有的方法都要有个概念
    重点的方法老师会提
5. 学习这个的三部曲
    5.1 会用
    5.2 知道怎么作用的
    5.3 自己定义方法
'''
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from time import sleep,ctime
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
ele_username_locator = ('id','ls_usernam')  # find_element===>locator
print('当前时间是:',ctime())
WebDriverWait(driver,25,0.5).until(EC.presence_of_element_located(ele_username_locator)).send_keys('admin')
# 演示1. 正确的locator
# 演示2. 错误的locator，演示最大超时时间

