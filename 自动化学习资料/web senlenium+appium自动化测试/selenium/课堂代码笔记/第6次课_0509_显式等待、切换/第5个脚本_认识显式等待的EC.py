# Author:  wuxianfeng
# Company: songqin
# File:    第5个脚本_认识显式等待的EC.py
# Date:    22-5-9
# Time:    下午 8:57
print()
'''
知识点: EC: expected_conditions 预期条件
关键点:看源码
1. 传入什么参数
2. 返回值是什么
3. 这个方法的意义是什么

----  ☆☆☆☆☆visibility_of_element_located（在presence_of_element_located的基础上加了是否显示）
1. 传入什么参数 : locator=>定位器=>('id','ls_username')
2. 返回值是什么 : 元素/False
3. 这个方法的意义是什么 ： 当元素的is_displayed()为真的时候就返回该元素；否则不返回
---   ☆☆☆presence_of_element_located
1. 传入什么参数 : locator=>定位器=>('id','ls_username')
2. 返回值是什么 : 元素
3. 这个方法的意义是什么 ： 如果能定位到就返回元素，否则就超时
'''
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from time import sleep,ctime
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
# WebDriverWait(driver,5,0.5).until(EC.presence_of_element_located())
WebDriverWait(driver,5,0.5).\
    until(EC.visibility_of_element_located(('id','ls_username'))).send_keys('admin')