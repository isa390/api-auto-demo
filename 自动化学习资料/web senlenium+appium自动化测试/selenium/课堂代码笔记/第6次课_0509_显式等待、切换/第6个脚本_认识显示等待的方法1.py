# Author:  wuxianfeng
# Company: songqin
# File:    第6个脚本_认识显示等待的方法1.py
# Date:    22-5-9
# Time:    下午 9:22
print()
'''
知识点:text_to_be_present_in_element_value
1. 传入什么参数 :   locator 定位器, text_ 文本
2. 返回值是什么 :    True/False
3. 这个方法的意义是什么 ： 判断一个元素(locator)的value属性值中是否含有text_
        element_text = driver.find_element(*locator).get_attribute("value")
        return text_ in element_text
'''
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from time import sleep,ctime
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
mywait = WebDriverWait(driver,25,0.5)
ele_username = 'id','ls_username'
expected_value_text = 'ab'
#value="abcd"
if mywait.until(EC.text_to_be_present_in_element_value(ele_username,expected_value_text)):
    print(f'{ele_username}元素的value属性中出现了{expected_value_text}')