# Author:  wuxianfeng
# Company: songqin
# File:    第5个脚本_认识js操作.py
# Date:    22-5-8
# Time:    上午 10:17
print()
'''
知识点: 
js可以实现网页的任何操作
1. 基础操作
    alert('hello')
    window.open()
    window.open('https://www.baidu.com')
    document.title
    '敏聚UI定位实战平台'
    document.URL
    'http://121.41.14.39:8088/index.html#/'
2. 滚动条
3. 定位
    3.1 输入值
    3.2 点击
4. 修改/删除属性
5. 二次扩展（封装）
-----
1. execute_script(js代码)
2. execute_script(js代码,参数1,参数2...)
----
遇到过selenium的定位失效了，无法去操作，但是用js却可以（这跟网页的实现有关）
'''

from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
test_flag = 6
if test_flag ==6:  #封装，二次扩展
    driver.get('http://106.14.1.150:8090/forum.php')
    ele_username = driver.find_element('css selector','#ls_username')
    value = 'admin'
    js = 'arguments[0].value=arguments[1]'
    #arguments[0]=>ele_username  #arguments[1]=>value
    driver.execute_script(js,ele_username,value)
if  test_flag == 5: #修改属性
    driver.get('http://106.14.1.150:8090/forum.php')
    js_passwordinput = "document.querySelector('#ls_password').value='123456'"
    driver.execute_script(js_passwordinput)
    sleep(1)
    js_showpassword = "document.querySelector('#ls_password').type='text'"
    driver.execute_script(js_showpassword)

if test_flag == 4: # 定位 及简单操作
    # document.getElementById   √ 如果没有id就不太号定位
    # document.getElementsByName  不常用
    # document.getElementsByClassName  注意有s
    # document.getElementsByTagName   不常用
    # document.getElementsByTagNameNS 不常用
    # xpath 没有的
    # 推荐用css
    # css 有的  document.querySelector  ==>等价于find_element('css selector')
    #          document.querySelectorAll ==>等价于find_elements('css selector')
    driver.get('http://106.14.1.150:8090/forum.php')
    # 如果是两句话用分号隔开，js的基础
    js_usernameinput = "var username = document.getElementById('ls_username');username.value='admin'"
    js_passwordinput = "document.querySelector('#ls_password').value='123456'"
    js_click_loginbutton = "document.querySelector('button.pn.vm').click()"
    driver.execute_script(js_usernameinput)
    sleep(2)
    driver.execute_script(js_passwordinput)
    sleep(2)
    driver.execute_script(js_click_loginbutton)

if test_flag ==3 : #滚动条 # 分两种，window的滚动条/网页内部的滚动
    #窗口的滚动
    # window.scrollTo(10000, 0)  #给一个大值，缺点是不够精确
    # window.scrollTo(0, 10000)
    # window.scrollTo(0, document.body.scrollHeight) #精确的滚动
    # window.scrollTo(document.body.scrollWidth, 0)
    js1 = "window.scrollTo(10000, 0)"
    js2 = "window.scrollTo(0, 0)"
    js3 = "window.scrollTo(0, 10000)"
    driver.get('http://106.14.1.150:8090/forum.php')
    driver.set_window_size(600,600)
    sleep(2)
    driver.execute_script(js1)  #水平滚动到最右侧
    sleep(2)
    driver.execute_script(js2)  #归位
    sleep(2)
    driver.execute_script(js3)  #滚动到底部

if test_flag == 2:  #需要具有一定的js的基础
    driver.get('http://121.41.14.39:8088/index.html#/')
    js2 = "return document.title"  #需要return一下
    print(driver.execute_script(js2))
if test_flag ==1:
    driver.get('http://121.41.14.39:8088/index.html#/')

    js1 = "alert('hello')"
    driver.execute_script(js1)

