# Author:  wuxianfeng
# Company: songqin
# File:    第3个脚本_cookie的登录.py
# Date:    22-5-8
# Time:    上午 9:41
from pprint import pprint

print()
'''
知识点:

'''
# 第一步先获取cookies：
# from selenium import webdriver
# from time import sleep
# driver = webdriver.Chrome()
# driver.get('https://music.163.com/')  #此时是没有登录
# driver.delete_all_cookies()  #先清理下环境
# input('请扫码')
# cookies = driver.get_cookies()  #获取所有的cookie
# pprint(cookies)

cookies_1 = [{'domain': '.music.163.com',
  'expiry': 1967334374,
  'httpOnly': False,
  'name': 'WNMCID',
  'path': '/',
  'sameSite': 'Strict',
  'secure': False,
  'value': 'zptzcn.1651974374426.01.0'},
 {'domain': '.music.163.com',
  'expiry': 1967334374,
  'httpOnly': False,
  'name': 'WEVNSM',
  'path': '/',
  'sameSite': 'Strict',
  'secure': False,
  'value': '1.0.0'},
 {'domain': '.music.163.com',
  'expiry': 1667526373,
  'httpOnly': True,
  'name': 'MUSIC_U',
  'path': '/',
  'secure': False,
  'value': '51c140b73c5a5660595aad4866fad2eecf34cb06c52baf0087c4e0cfd85621a0519e07624a9f0053960cd13117177631240adf1e0ff0f47e584f7aa18f76386ce4e4162e42eb6abea0d2166338885bd7'},
 {'domain': '.music.163.com',
  'expiry': 1653270383,
  'httpOnly': False,
  'name': '__csrf',
  'path': '/',
  'secure': False,
  'value': '0b896171c27a354a3f70727f2f70de76'},
 {'domain': '.music.163.com',
  'expiry': 1967334367,
  'httpOnly': False,
  'name': 'NMTID',
  'path': '/',
  'secure': False,
  'value': '00OAPZrFtq7xrDBvkQXv9XiTufFCNIAAAGAoVhqqg'},
 {'domain': '.163.com',
  'expiry': 4805574367,
  'httpOnly': False,
  'name': '_ntes_nnid',
  'path': '/',
  'secure': False,
  'value': '0427ef650bcbf0a4efa2e5073ff91518,1651974367401'},
 {'domain': '.music.163.com',
  'expiry': 1809656167,
  'httpOnly': False,
  'name': '_iuqxldmzr_',
  'path': '/',
  'secure': False,
  'value': '33'},
 {'domain': '.music.163.com',
  'expiry': 1809656167,
  'httpOnly': False,
  'name': 'JSESSIONID-WYYY',
  'path': '/',
  'secure': False,
  'value': 'odmI2p34I0FIOyqYlESpO%2B7IAaH1ntYcdqGfoI4ym%5CJSpY4qo2mjm8ru6nGTAwHcVUwJX5p4W9w0m3bfKvxfg9GZPuvuXIqn0SukZw67XovVJqJXbffyPgiqxDCeSXDAshkf9fsyH382w5a3tIJryPTWiJuH740nZ79DhQrkIPVhFKCf%3A1651976167315'},
 {'domain': '.music.163.com',
  'expiry': 2282694374,
  'httpOnly': False,
  'name': 'ntes_kaola_ad',
  'path': '/',
  'secure': False,
  'value': '1'},
 {'domain': '.163.com',
  'expiry': 4805574367,
  'httpOnly': False,
  'name': '_ntes_nuid',
  'path': '/',
  'secure': False,
  'value': '0427ef650bcbf0a4efa2e5073ff91518'},
 {'domain': 'music.163.com',
  'expiry': 2147483647,
  'httpOnly': False,
  'name': 'WM_TID',
  'path': '/',
  'secure': False,
  'value': 'UbdAx6vgw2hEQEFBAUbAUM1SY5Q2wdfN'}]

from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.maximize_window()
driver.get('https://music.163.com/')  #此时是没有登录,为这个网站添加cookie #先有cookie再打开网站
# 添加cookie
for _ in cookies_1:
    driver.add_cookie(_)
sleep(1)
driver.get('https://music.163.com/')