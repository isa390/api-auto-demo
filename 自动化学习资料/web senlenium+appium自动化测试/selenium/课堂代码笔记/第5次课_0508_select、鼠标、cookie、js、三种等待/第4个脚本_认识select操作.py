# Author:  wuxianfeng
# Company: songqin
# File:    第4个脚本_认识select操作.py
# Date:    22-5-8
# Time:    上午 9:51
from selenium.webdriver.support.select import Select

print()
'''
知识点:
1. select标签下的option可以直接click的
2. 但是推荐用Select类
3. 方法
    select_by_index    索引，第一个元素是0
    select_by_value    value是属性
    select_by_visible_text  >可见的文本<
'''

from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium0425\第5次课_0508_select、鼠标、cookie、js、三种等待\select.html')
# driver.find_element('css selector','#sex>option:nth-child(2)').click()
ele_select = driver.find_element('css selector','#sex')  #select元素
sleep(2)
Select(ele_select).select_by_value('2')
sleep(2)
Select(ele_select).select_by_index(0)
sleep(2)
Select(ele_select).select_by_visible_text('Tailand')
#  xpath = ".//option[normalize-space(.) = %s]" % self._escapeString(text)
#  normalize-space  删除开头和结尾的空格
#  在用select_by_visible_text方法的时候()里面塞的文本不需要包括前后的空格
#  > 男 <  ==>这种情况只需塞"男"不要塞" 男 "
#  >男<