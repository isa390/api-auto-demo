# Author:  wuxianfeng
# Company: songqin
# File:    第5个脚本_认识webdriver的基础操作.py
# Date:    22-5-6
# Time:    下午 8:51
print()
'''
知识点:
1. tasklist|findstr chromedriver  # close/quit 先看下没有进程
   

'''
from selenium import webdriver
from time import sleep

driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
test_flag = 4

if test_flag == 4:  # back/forward/refresh
    driver.get('http://106.14.1.150:8090/forum.php')
    ele_username = driver.find_element('id','ls_username')
    ele_username.send_keys('ad')
    sleep(2)
    driver.find_element('link text','立即注册').click()
    sleep(2)  #click（跳转页面）经常要加等待
    print('马上回退')
    driver.back()   #回到首页了
    sleep(2)
    driver.refresh()
    #ele_username.send_keys('min')  #试图输入完整admin
    #selenium.common.exceptions.StaleElementReferenceException:
    # Message: stale element reference: element is not attached to the page document
    # 哪怕你每次都是find，也有可能遇到该错误，那就加个等待试试~（某同学的公司的web自动刷新）
    driver.find_element('id', 'ls_username').send_keys('min') #重新找一遍就可以了
    sleep(2)
    print('马上前进')
    driver.forward()

if test_flag == 3:  # close/quit
    driver.get('http://121.41.14.39:8088/index.html#/')
    sleep(5)
    # driver.close()  #关闭，关闭的是一个tab，不会退出chromedriver
    driver.quit()  # 退出，整个浏览器实例的退出，会结束chromedriver进程
if test_flag == 2:  # 浏览器的基础属性
    driver.get('http://121.41.14.39:8088/index.html#/')
    print('浏览器的当前的URL地址:', driver.current_url)  # http://121.41.14.39:8088/index.html#/
    print('浏览器的名字:', driver.name)  # chrome
    print('浏览器的标题:', driver.title)  # 敏聚UI定位实战平台
    print('浏览器的页面源码:', driver.page_source)
    # current_url和title  测试中常用于断言，或者条件判断继续下一步操作
    # page_source一般自动化测试很少用，但是在爬虫中有用

if test_flag == 1:  # 窗口相关的属性和方法
    driver.get('http://121.41.14.39:8088/index.html#/')
    sleep(2)
    driver.maximize_window()  # 最大
    sleep(2)
    driver.fullscreen_window()  # 全屏
    sleep(2)
    driver.minimize_window()  # 最小化
    sleep(2)
    driver.set_window_size(800, 600)  # 800 width， 600的heigh
    print(driver.get_window_position())  # {'x': 8, 'y': 8}
    print(driver.get_window_rect())  # {'height': 602, 'width': 802, 'x': 8, 'y': 8}
    print(driver.get_window_size())  # {'width': 802, 'height': 602}
