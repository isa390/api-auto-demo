# Author:  wuxianfeng
# Company: songqin
# File:    第2个脚本_认识webelement操作_v2.py
# Date:    22-5-6
# Time:    下午 8:23
print()
'''
知识点:

'''
from selenium import webdriver
from time import sleep

driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get('https://www.runoob.com/')
test_flag = 3
if test_flag == 3:  #value_of_css_property css的属性值
    ele_website = driver.find_element('xpath', "//h4[contains(text(),'网站建设指南')]")
    print(ele_website.value_of_css_property('color'))   #rgba(100, 133, 76, 1)
    print(ele_website.value_of_css_property('font-size')) #
    #
if test_flag == 2:  # 跟人的操作一样：先滚动到你那个元素，再点击
    ele_website = driver.find_element('xpath', "//h4[contains(text(),'网站建设指南')]")
    ele_website.location_once_scrolled_into_view
    sleep(2)
    ele_website.click()
if test_flag == 1:  # 网页打开后，元素在当前页面看不到，要往下滚动方可  #直接点击亦可！
    driver.find_element('xpath', "//h4[contains(text(),'网站建设指南')]").click()
