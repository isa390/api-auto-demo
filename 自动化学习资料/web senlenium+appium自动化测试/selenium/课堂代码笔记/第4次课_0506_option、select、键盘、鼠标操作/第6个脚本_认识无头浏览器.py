# Author:  wuxianfeng
# Company: songqin
# File:    第6个脚本_认识无头浏览器.py
# Date:    22-5-6
# Time:    下午 9:37
print()
'''
知识点: 
1. 无头的来历  : https://phantomjs.org/
2. 3x的selenium版本中driver.Phantomjs()
3. 4X的selennium版本中废弃了driver.Phantomjs()
4. 统一用headless的option来实现
5. Phantomjs的做着不维护了！
6. 无头
    - 有时候测试的时候不关心过程，只关注结果，不要看界面
    - 界面打开了之后还是比较耗资源
7. add_argument('headless') 对于chrome而言可以是-，可以是--，可以不带-，但是firefox是不一样的！
'''
from selenium import webdriver
from time import sleep

myoption = webdriver.ChromeOptions()
myoption.add_argument('headless')
driver = webdriver.Chrome('/usr/local/bin/chromeDriver',options=myoption)
driver.get('http://121.41.14.39:8088/index.html')
driver.find_element('id','username').send_keys('admin')
driver.save_screenshot('chrome_headless.png')
print(driver.get_window_rect())  #{'height': 600, 'width': 800, 'x': 0, 'y': 0}
print(driver.name)  #chrome
